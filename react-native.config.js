module.exports = {
    project: {
        ios: {},
        android: {},
    },
    dependencies: {
        'react-native-maps': {
            platforms: {
                android: null,
                ios: null,
            },
        },
    },
    assets: ['./assets/fonts'],
};
