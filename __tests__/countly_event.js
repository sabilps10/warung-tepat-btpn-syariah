function sendEvent(callback, event) {
    if (event === true) {
        callback(event);
    }
}

describe('sendEvent', () => {
    test('Event countly have been called', () => {
        const customEvent = jest.fn();
        sendEvent(customEvent, true);
        expect(customEvent).toHaveBeenCalled();
    });

    test('Event countly not have been called', () => {
        const customEvent = jest.fn();
        sendEvent(customEvent, false);
        expect(customEvent).not.toHaveBeenCalled();
    });
});
