import { empty } from '../src/assets/Dummy';

test('Expect a load category to return empty object', () => {
    const loadCategory = jest.fn((id) => Promise.resolve(empty));

    return loadCategory(1).then((data) => {
        expect(data).toEqual({});
    });
});
