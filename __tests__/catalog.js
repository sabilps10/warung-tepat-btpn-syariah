import { cat_2, cat_3 } from '../src/assets/Dummy';

test('Expect a promise to resolve', () => {
    const loadCatalog = jest.fn((id, page) => Promise.resolve(cat_2));

    return loadCatalog(2, 1).then((data) => {
        expect(data).toBe(cat_2);
    });
});

test('Expect a promise to rejected', () => {
    const loadCatalog = jest.fn((id, page) => Promise.reject('Something went wrong'));

    return loadCatalog(2, 1).catch((e) => expect(e).toMatch('Something went wrong'));
});

test('Expect total catalog less than 10', () => {
    const loadCatalog = jest.fn((id, page) => Promise.resolve(cat_2));

    return loadCatalog(2, 1).then((res) => {
        expect(res.total).toBeLessThan(10);
    });
});

test('Expect total catalog is 10 then fetch load more', () => {
    const loadCatalog = jest.fn((id, page) => Promise.resolve(cat_3));
    const loadMore = jest.fn((id, page) => Promise.resolve(cat_2));

    return loadCatalog(2, 1).then((res) => {
        expect(res.total).toEqual(10);

        return loadMore(3, 2).then((data) => {
            expect(data).toBe(cat_2);
        });
    });
});
