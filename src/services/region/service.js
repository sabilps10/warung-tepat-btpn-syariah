import RegionRest from '../rest/rest.region';

export const service = class RegionService {
    constructor() {
        this.rest = new RegionRest();
    }

    getRegency(page, limit, search) {
        return this.rest.getRegency(page, limit, search);
    }

    getDistrict(id, page, limit, search) {
        return this.rest.getDistrict(id, page, limit, search);
    }

    getVillage(id, page, limit, search) {
        return this.rest.getVillage(id, page, limit, search);
    }
};

export const RegionService = new service();
