import { TYPE } from './action';

const INITIAL_STATE = {
    // page loading state
    page: false,
    // component loading state
    components: {},
};

/**
 * Reducer Activity
 *
 * @param state
 * @param action
 * @returns {{loading: boolean}}
 */
export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.PAGE_PROCESSING:
            return {
                ...state,
                page: true,
            };
        case TYPE.PAGE_DONE:
            return {
                ...state,
                page: false,
            };
        case TYPE.COMPONENT_PROCESSING:
            state.components[action.name] = true;
            return {
                ...state,
            };
        case TYPE.COMPONENT_DONE:
            state.components[action.name] = false;
            return {
                ...state,
            };
        default:
            return state;
    }
}
