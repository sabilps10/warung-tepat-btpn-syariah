import Rest from './rest';

export default class AkadRest extends Rest {
    postAkad(body) {
        return this.POST('cart/akad-pembiayaan', body);
    }
}
