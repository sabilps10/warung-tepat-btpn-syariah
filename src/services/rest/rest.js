import { ToastAndroid } from 'react-native';
import VersionCheck from 'react-native-version-check';

import { createLogger } from '@common/logger';
import { EventEmitter } from '@common/events';
import { isObject } from '@common/utils';
import { AuthService } from '../../modules/auth/rx/service';
import { store } from '../../app';

const events = new EventEmitter();
const Logger = createLogger('REST');

export default class Rest {
    constructor() {
        this.headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'user-agent': 'com.btpns.lmd/' + VersionCheck.getCurrentBuildNumber(),
        };
    }

    _fullRoute(url) {
        return `${global.API_URL}${url}`;
    }

    _jwtHeader() {
        let jwtHeader = {};
        let jwt = AuthService.getAccessToken();

        if (jwt !== '') {
            jwtHeader = {
                Authorization: 'Bearer ' + jwt,
            };
        }

        return jwtHeader;
    }

    async _fetch(route, method, body, isQuery = false) {
        if (!route) {
            throw new Error('Route is undefined');
        }

        let fullRoute = this._fullRoute(route);
        let jwtHeader = this._jwtHeader();

        Object.assign(this.headers, jwtHeader);

        if (isQuery && body) {
            let qs = require('qs');
            const query = qs.stringify(body);
            fullRoute = `${fullRoute}?${query}`;
            body = undefined;
        }

        events.emit('started', fullRoute);

        let opts = {
            method,
            headers: this.headers,
        };

        if (body) {
            Object.assign(opts, { body: JSON.stringify(body) });
        }

        let response = {};
        try {
            let req = await fetch(fullRoute, opts)
                .then((res) => {
                    if (res.status === 401) {
                        AuthService.logout().then(() => {
                            store.dispatch({ type: 'logout' });
                        });

                        throw res;
                    }
                    return res;
                })
                .catch((error) => {
                    if (!isObject(error)) {
                        ToastAndroid.show(
                            'Tidak Dapat Terhubung Dengan Server',
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50,
                        );
                    }
                    throw error;
                });

            let resp = await req.json();

            if (resp.status === 'success') {
                if (resp.data === null && method === 'GET') {
                    throw resp;
                }

                response = resp;
            } else if (resp.status === 'OK') {
                if (resp.data === null && method === 'GET') {
                    throw resp;
                }

                response = resp;
            } else {
                if (typeof resp.errors !== 'undefined') {
                    events.emit('failure', 'invalid', resp.errors);
                }

                throw resp;
            }

            return response;
        } catch (e) {
            throw e;
        }
    }

    GET(route, query) {
        return this._fetch(route, 'GET', query, true);
    }

    POST(route, body) {
        return this._fetch(route, 'POST', body);
    }

    PUT(route, body) {
        return this._fetch(route, 'PUT', body);
    }

    DELETE(route, query) {
        return this._fetch(route, 'DELETE', query, true);
    }
}

if (process.env.NODE_ENV === 'development') {
    events.on('started', (path) => {
        Logger.info('@requesting', path);
    });

    events.on('success', (payload, response) => {
        Logger.info('@success', response.url, response.status, payload);
    });
    events.on('failure', (message, errors) => {
        Logger.info('@failure', message, errors);
    });
}
