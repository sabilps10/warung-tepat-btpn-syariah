import Rest from './rest';

export default class GroupBuyingRest extends Rest {
    get() {
        return this.GET('group-buying');
    }

    approve(id) {
        return this.PUT('group-buying/' + id + '/approve', { data: null });
    }

    delete(id) {
        return this.DELETE('group-buying/' + id);
    }
}
