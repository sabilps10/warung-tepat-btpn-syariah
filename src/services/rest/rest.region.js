import Rest from './rest';

export default class RegionRest extends Rest {
    getRegency(page, limit, search) {
        let qs = {
            page: page,
            limit: limit,
        };

        if (search != '') {
            qs.search = search;
        }

        return this.GET('region/regency', qs);
    }

    getDistrict(id, page, limit, search) {
        let qs = {
            page: page,
            limit: limit,
        };

        if (search != '') {
            qs.search = search;
        }

        return this.GET('region/district/' + id, qs);
    }

    getVillage(id, page, limit, search) {
        let qs = {
            page: page,
            limit: limit,
        };

        if (search != '') {
            qs.search = search;
        }

        return this.GET('region/village/' + id, qs);
    }
}
