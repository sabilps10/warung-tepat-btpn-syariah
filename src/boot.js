import React, { PureComponent } from 'react';
import { NavigationActions } from 'react-navigation';

import { AuthService } from './modules/auth/rx/service';
import { Config } from './common/config';

import { NotificationForeground } from './common/utils';
import { NotificationService } from './modules/notification/rx/service';

global.API_URL = Config.apiURL.warungtepat[global.ENVI];

class BootLoader extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            forceUpdate: false,
        };
    }

    navigate = (name) => {
        this.props.navigation.dispatch(NavigationActions.navigate({ routeName: name }));
    };

    hasToken = () => {
        AuthService.initialize().finally((res) => {
            if (AuthService.isAuthenticated()) {
                this.navigate('Main');
            } else {
                this.navigate('Login');
            }
        });
    };

    initializeFirebaseDeviceId = async () => {
        NotificationService.initialize().finally(() => {
            const deviceId = NotificationService.getDeviceToken();
            const isAlreadyRegister = NotificationService.isRegisteredDevice();

            if (!isAlreadyRegister) {
                // send device id via api
                NotificationService.registerDevicePost({
                    user_id: 0,
                    device_id: deviceId,
                    is_login: false,
                }).then(() => {
                    NotificationService.registerDevice();
                });
            }
        });
    };

    async componentWillMount() {
        await NotificationForeground.init();
        this.hasToken();
        console.log(global.API_URL);
    }

    render() {
        return null;
    }
}

export default BootLoader;
