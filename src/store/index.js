import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { AsyncStorage } from 'react-native';
import { createLogger } from 'redux-logger';
import persistState, { mergePersistedState } from 'redux-localstorage';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import persistAdapter from 'redux-localstorage/lib/adapters/AsyncStorage';
import thunk from 'redux-thunk';

import $state from './state';

/**
 * Setup Redux Store Application
 *
 * @returns {any}
 */
export function setupStore() {
    let reducer = _setupReducer();
    let persistEnhancer = _setupPresister();
    let enhancer = _setupEnchancers(persistEnhancer);
    let store = createStore(reducer, enhancer);

    Object.values($state)
        .reduce((prev, state) => {
            return prev.then(() => (state.initializer ? state.initializer(store) : Promise.resolve(null)));
        }, Promise.resolve(null))
        .catch((error) => {
            throw error;
        });

    return store;
}

/**
 * Merging reducers
 *
 * @param target
 * @param source
 * @returns {{[p: string]: *}}
 */
function _merge(target, source) {
    const result = { ...target };
    for (const [key, value] of Object.entries(source)) {
        if (value && Object.prototype.toString.call(value) === '[object Object]' && result[key]) {
            result[key] = _merge(result[key], value);
        } else {
            result[key] = value;
        }
    }
    return result;
}

/**
 * Setting up all reducer from the state file
 *
 * @returns {*}
 * @private
 */
function _setupReducer() {
    let reducer = combineReducers(
        Object.entries($state)
            .reduce((result, [name, substate]) => {
                return substate.reducer
                    ? {
                        ...result,
                        [name]: substate.reducer,
                    }
                    : result;
            }, {}),
    );

    return compose(mergePersistedState(_merge))(reducer);
}

/**
 * Setting up all presister redux from the state file
 *
 * @private
 */
function _setupPresister() {
    let persistSelector = (state) =>
        Object.entries($state)
            .reduce((result, [name, substate]) => {
                return substate.persister
                    ? {
                        ...result,
                        [name]: substate.persister(state[name]),
                    }
                    : result;
            }, {});

    let persistStorage = compose((storage) => {
        storage._put = storage.put;
        storage.put = function (key, state, callback) {
            storage._put(key, persistSelector(state), callback);
        };
        return storage;
    })(persistAdapter(AsyncStorage));

    return persistState(persistStorage, 'redux');
}

/**
 * Setting up enchancer redux
 *
 * @param persistEnhancer
 * @returns
 * @private
 */
function _setupEnchancers(persistEnhancer) {
    let enhancerMiddleware = [thunk];

    if (process.env.NODE_ENV === 'development') {
        enhancerMiddleware.push(createLogger());
    }

    enhancerMiddleware.push(createReactNavigationReduxMiddleware((state) => state.Router));

    return compose(applyMiddleware(...enhancerMiddleware), persistEnhancer);
}

/**
 * Dev mode
 * Check semua state files
 */
if (process.env.NODE_ENV === 'development') {
    Object.entries($state)
        .forEach(([module, state]) => {
            if (!state) {
                console.warn(`$state.${module}: invalid descriptor`);
                return;
            }
            if (!state.reducer || typeof state.reducer !== 'function') {
                console.warn(`$state.${module}: missing or invalid 'reducer' fn`);
            }
            if (state.persister && typeof state.persister === 'function') {
                console.info(`$state.${module}: found 'persister' fn`);
            }
            if (state.initializer && typeof state.initializer === 'function') {
                console.info(`$state.${module}: found 'initializer' fn`);
            }
        });
}
