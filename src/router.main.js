import { createSwitchNavigator } from 'react-navigation';

import BootLoader from './boot';
import LoginRoutes from './modules/router.login';
import EformRoutes from './modules/router.eform';
import UpdateScreen from './modules/version/update.js';
import MainRoutes from './modules/router.stack';

const StackNavigator = createSwitchNavigator(
    {
        Boot: BootLoader,
        Eform: EformRoutes,
        Login: LoginRoutes,
        Main: MainRoutes,
        ForceUpdate: UpdateScreen,
    },
    {
        lazy: true,
        headerMode: 'none',
    },
);

export { StackNavigator };
