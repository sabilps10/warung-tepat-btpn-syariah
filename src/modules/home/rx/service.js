import CatalogRest from '../../../services/rest/rest.catalog';
import CartRest from '../../../services/rest/rest.cart';

export const service = class HomeService {
    constructor() {
        this.rest = new CatalogRest();
        this.restCart = new CartRest();
    }

    getSlider() {
        return this.rest.getSlider();
    }

    getCategory() {
        return this.rest.getCategory(0);
    }

    getRecoms() {
        return this.rest.getCatalog(0, 1, 0, 25);
    }

    getPackages() {
        return this.rest.getCatalog(96, 0, 0, 25);
    }

    getCounter() {
        return this.restCart.getCounter();
    }
};

export const HomeService = new service();
