import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    sliders: {},
    recomens: {},
    packages: {},
    counter: {},
    event: false,
};

/**
 * Home Reducer
 *
 * @param state
 * @param action
 */
export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_SLIDER:
            return {
                ...state,
                sliders: action.payload,
            };
        case TYPE.FETCH_RECOM:
            return {
                ...state,
                recomens: action.payload,
            };
        case TYPE.FETCH_PACKAGE:
            return {
                ...state,
                packages: action.payload,
            };
        case TYPE.FETCH_COUNTER:
            return {
                ...state,
                counter: action.payload,
            };
        case TYPE.RESET_HOME:
            return {
                ...INITIAL_STATE,
            };
        case TYPE.SEND_EVENT:
            return {
                ...state,
                event: action.payload,
            };
        default:
            return state;
    }
}

/**
 * Home Screen Presister State
 *
 * @param sliders
 * @param categories
 * @param recomens
 * @returns {{recomens: *, categories: *, sliders: *}}
 */
export function persister({ sliders, recomens, packages, counter }) {
    return {
        sliders,
        recomens,
        packages,
        counter,
    };
}
