import * as Activity from '../../../services/activity/action';
import { HomeService } from './service';

export const TYPE = {
    FETCH_CATEGORY: 'home::fetch.category',
    FETCH_SLIDER: 'home::fetch.slider',
    FETCH_RECOM: 'home::fetch.recom',
    FETCH_PACKAGE: 'home::fetch.packages',
    FETCH_COUNTER: 'home::fetch.counters',
    RESET_HOME: 'home::reset',
    SEND_EVENT: 'home::send.event',
};

export function $load() {
    return (dispatch) => {
        Activity.processing(dispatch, 'Home', 'Load');

        let slider = HomeService.getSlider()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_SLIDER,
                    payload: res.data,
                });
            })
            .catch((error) => {
                dispatch({
                    type: 'TYPE.FETCH_SLIDER',
                    payload: {},
                });
            });

        // category is on catalog
        let category = HomeService.getCategory()
            .then((res) => {
                dispatch({
                    type: 'catalog::fetch.category',
                    payload: res.data,
                });
            })
            .catch((error) => {
                dispatch({
                    type: 'catalog::fetch.category',
                    payload: {},
                });
            });

        let recom = HomeService.getRecoms()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_RECOM,
                    payload: res.data,
                });
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.FETCH_RECOM,
                    payload: {},
                });
            });

        let packages = HomeService.getPackages()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_PACKAGE,
                    payload: res.data,
                });
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.FETCH_PACKAGE,
                    payload: {},
                });
            });

        let counters = HomeService.getCounter()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_COUNTER,
                    payload: {
                        total: res.data,
                    },
                });
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.FETCH_COUNTER,
                    payload: {},
                });
            });

        Promise.all([category, slider, recom, packages, counters]).finally(() => {
            Activity.done(dispatch, 'Home', 'Load');
        });
    };
}

// action send event
export function $sendEvent() {
    return (dispatch) => {
        dispatch({ type: TYPE.SEND_EVENT, payload: true });
    };
}
