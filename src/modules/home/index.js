import React, { Component } from 'react';
import { BackHandler, RefreshControl, ScrollView, StyleSheet, View, Linking } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import Countly from 'countly-sdk-react-native-bridge';
import Placeholder from 'rn-placeholder';
import JailMonkey from 'jail-monkey';

import Headers from '../../components/header';
import { COLOR } from '../../common/styles';
import { Toast } from '../../common/utils';
import { Config } from '../../common/config';

import { $load, $sendEvent } from './rx/action';
import { $checkVersion } from '../version/rx/action';
import { $toggleModal } from '../version/rx/action';
import { $counter } from '../cart/rx/action';
import { $verifyUser } from '../link_saldo/rx/action';
import { $getCountMessage } from '../notification/rx/action';
import SectionSlider from './section/slider';
import SectionRecomended from './section/recom';
import SectionDebit from './section/saldo';
import SectionConnectDebit from './section/FirstSaldo';
import SectionCategory from './section/category';
import SectionPackages from './section/package';
import LmdModal from '../version/LmdModal';

class MainScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            exit: false,
        };

        this._onRefresh = this._onRefresh.bind(this);
    }

    _onRefresh(data) {
        this.props.$load();
        this.props.$counter();
        this.props.$verifyUser(data);
        this.props.$getCountMessage();
    }

    _renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} onRefresh={this._onRefresh} colors={[COLOR.primary]} />;
    }

    componentWillMount() {
        const { versionName } = Config;
        const { session, event } = this.props;
        const { user } = session;
        const data = { mobile_no: user.phone };
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            this.props.$verifyUser(data);
        });

        const eventLogin = { eventName: 'Login', eventCount: 1 };
        eventLogin.segments = {
            Username: user.username,
            Email: user.email,
            Name: user.name,
            Phone: user.phone,
        };

        const eventVersion = { eventName: 'Version', eventCount: 1 };
        eventVersion.segments = {
            [versionName]: user.username,
        };

        const eventRoot = { eventName: 'Root Detection', eventCount: 1 };
        eventRoot.segments = {
            Phone: user.phone,
        };

        if (!event) {
            Countly.sendEvent(eventLogin);
            Countly.sendEvent(eventVersion);

            if (JailMonkey.isJailBroken()) {
                Countly.sendEvent(eventRoot);
            }

            this.props.$sendEvent();
        }

        this._onRefresh(data);
        this.props.$checkVersion({ version: versionName });

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.focusListener.remove();
    }

    renderModal() {
        return (
            <LmdModal
                onCancel={this.toggleModal}
                onSubmit={this.update}
                title="Update Aplikasi"
                text={'Versi Aplikasi Warung Tepat terbaru tersedia'}
                visible={this.props.showModal}
            />
        );
    }

    renderConnect() {
        if (this.props.isConnected) {
            return <SectionDebit navigation={this.props.navigation} />;
        }

        return <SectionConnectDebit navigation={this.props.navigation} />;
    }

    toggleModal = () => {
        this.props.$toggleModal(false);
    };

    update = () => {
        Linking.openURL(this.props.url);
    };

    handleBackPress = () => {
        if (!this.props.navigation.isFocused('Main')) {
            this.props.navigation.dispatch(NavigationActions.back());
            this.setState({ exit: false });
            return true;
        }

        if (this.state.exit) {
            return false;
        }

        Toast('Tekan sekali lagi untuk keluar');

        this.setState({ exit: true });

        return true;
    };

    render() {
        const width = { width: '100%' };

        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Headers navigation={this.props.navigation} />
                <ScrollView
                    refreshControl={this._renderRefresh()}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={width}
                >
                    <SectionSlider navigation={this.props.navigation} />
                    <View style={styles.placeholder}>
                        <Placeholder.ImageContent
                            size={40}
                            animate="fade"
                            lineNumber={3}
                            lineSpacing={5}
                            onReady={!this.props.loading}
                        >
                            {this.renderConnect()}
                        </Placeholder.ImageContent>
                    </View>
                    <SectionCategory navigation={this.props.navigation} />
                    <SectionPackages navigation={this.props.navigation} />
                    <SectionRecomended navigation={this.props.navigation} />
                </ScrollView>
                {this.renderModal()}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        showModal: state.Version.showModal,
        url: state.Version.url,
        session: state.Auth.session,
        event: state.Home.event,
        connectLoading: state.LinkSaldo.connectLoading,
        dataConnect: state.LinkSaldo.data,
        isConnected: state.LinkSaldo.isConnected,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $load,
            $sendEvent,
            $checkVersion,
            $toggleModal,
            $counter,
            $verifyUser,
            $getCountMessage,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLOR.light,
    },
    placeholder: {
        backgroundColor: COLOR.white,
        paddingTop: 16,
        paddingRight: 24,
        paddingBottom: 16,
        paddingLeft: 24,
    },
});
