import React, { PureComponent } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import Section from './section';
import { ProductThumb } from '../../../components/product';

class SectionPackages extends PureComponent {
    _renderProduct(i, product) {
        return (
            <ProductThumb key={i} size='small' product={product} hideButton
                          navigation={this.props.navigation}/>
        );
    }

    navigate = () => {
        this.props.navigation.navigate('Catalog', {
            category_id: 96,
        });
    };

    render() {
        return this.props.packages.length > 0 ?
            <Section title='Rekomendasi Paket' moreText='LIHAT SEMUA' moreTextPress={this.navigate}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}
                            howsVerticalScrollIndicator={false}>
                    {this.props.packages.map((p, i) => this._renderProduct(i, p))}
                </ScrollView>
            </Section>
            : null;
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        packages: state.Home.packages,
    };
};

export default connect(mapStateToProps)(SectionPackages);
