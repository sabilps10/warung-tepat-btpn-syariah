import React, { PureComponent } from 'react';
import { Dimensions, Image, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';
import { View } from 'react-native-animatable';

import { COLOR } from '../../../common/styles';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 150;

class SectionSlider extends PureComponent {
    _renderSlider(i, slider) {
        return (
            <View key={i}>
                <Image source={{ uri: slider.image }} style={styles.image}/>
            </View>
        );
    }

    _renderDot() {
        return (
            <View style={styles.indicator}/>
        );
    }

    _renderDotActive() {
        return (
            <View animation="bounceIn" useNativeDriver style={styles.indicatorActive}/>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Swiper height={BannerHeight} showsButtons={false} autoplay={true} loop={true}
                        paginationStyle={styles.indicatorContainer}
                        dot={this._renderDot()}
                        activeDot={this._renderDotActive()}>
                    {
                        this.props.sliders.length > 0 ?
                            this.props.sliders.map((slider, i) => this._renderSlider(i, slider))
                            : <View/>
                    }
                </Swiper>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        sliders: state.Home.sliders,
        loading: state.Activity.components.slider,
    };
};

export default connect(mapStateToProps)(SectionSlider);

const styles = StyleSheet.create({
    container: {
        marginBottom: 5,
        borderBottomWidth: 0.6,
        borderBottomColor: '#dbdbdb',
    },
    image: {
        width: BannerWidth,
        height: BannerHeight,
    },
    indicatorContainer: {
        bottom: 10,
        left: 10,
        right: null,
    },
    indicator: {
        backgroundColor: '#fff',
        borderColor: '#dbdbdb',
        borderWidth: 1,
        width: 5,
        height: 5,
        borderRadius: 7,
        marginLeft: 7,
        marginRight: 7,
    },
    indicatorActive: {
        backgroundColor: COLOR.primary,
        borderColor: '#fff',
        borderWidth: 0.3,
        width: 20,
        height: 5,
        borderRadius: 7,
        marginLeft: 7,
        marginRight: 7,
    },
});
