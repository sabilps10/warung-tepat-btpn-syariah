import React, { PureComponent } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import Section from './section';
import { ProductThumb } from '../../../components/product';

class SectionRecomended extends PureComponent {
    _renderProduct(i, product) {
        return <ProductThumb key={i} size="small" product={product} hideButton navigation={this.props.navigation} />;
    }

    render() {
        return this.props.recomens.length > 0 ? (
            <Section title="Rekomendasi Produk">
                <ScrollView horizontal showsHorizontalScrollIndicator={false} howsVerticalScrollIndicator={false}>
                    {this.props.recomens.map((p, i) => this._renderProduct(i, p))}
                </ScrollView>
            </Section>
        ) : null;
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        recomens: state.Home.recomens,
    };
};

export default connect(mapStateToProps)(SectionRecomended);
