import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import { COLOR, FONT_SIZE, PADDING } from '../../../common/styles';
import iconC from '@assets/icons/icon_people.png';
import ButtonPrimary from '../../link_saldo/com/buttonPrimary';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 36;

class SectionConnectDebit extends Component {
    constructor(props) {
        super(props);
    }

    toVerifyPin = () => {
        this.props.navigation.navigate('VerifyPinSaldo');
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <Image source={iconC} style={styles.iconImage} resizeMode="contain" />
                    <View style={styles.column}>
                        <Text style={styles.titleText}>Rekening Anda Belum Terhubung</Text>
                        <ButtonPrimary style={styles.button} text="Hubungkan" onPress={this.toVerifyPin} />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        session: state.Auth.session,
    };
};

export default connect(mapStateToProps)(SectionConnectDebit);

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: COLOR.white,
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLOR.darkGreen,
        width: DEVICE_WIDTH - MARGIN,
        marginTop: -2,
        marginBottom: -2,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        paddingBottom: PADDING.PADDING_VERTICAL_CONTAINER,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        shadowColor: '#3c000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 100,
        shadowRadius: 2,
        elevation: 1,
    },
    column: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    button: {
        backgroundColor: COLOR.primary,
        width: 191,
        height: 32,
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 2,
        paddingTop: 2,
    },
    iconImage: {
        height: 80,
        width: 120,
    },
    titleText: {
        fontFamily: 'NunitoSans-Bold',
        textAlign: 'center',
        color: COLOR.white,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        marginBottom: 8,
    },
});
