import React, { Component } from 'react';

import { View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { COLOR, FONT_SIZE, ROBOTO } from '../../../common/styles';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import multiple from '@assets/img/multiple_login.png';

class MulipleLogin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            visible: false,
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.visible !== nextState.visible) {
            return true;
        }

        return false;
    }

    onVisibleTrue = () => {
        this.setState({ visible: true });
    };

    onVisibleFalse = () => {
        this.setState({ visible: false });
    };

    toMain = () => {
        this.setState({ visible: false });
    };

    _renderPopUp = () => {
        return (
            <Dialog
                visible={this.state.visible}
                rounded={true}
                onTouchOutside={() => {
                    this.onVisibleTrue();
                }}
                onHardwareBackPress={() => {
                    this.onVisibleFalse();
                    return true;
                }}
            >
                <DialogContent style={styles.popupcontainer}>
                    <Image source={multiple} style={styles.iconImage} resizeMode="contain" />
                    <Text style={styles.popuptitle}>Akun Anda Sudah Login Di Perangkat Lain</Text>
                    <Text style={styles.popuptext}>
                        Untuk masuk di perangkat ini, harap login kembali di perangkat ini.
                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.onVisibleFalse();
                        }}
                        style={styles.closebutton}
                    >
                        <Text style={styles.closetext}>Ok, Saya Mengerti</Text>
                    </TouchableOpacity>
                </DialogContent>
            </Dialog>
        );
    };

    render() {
        return <View>{this._renderPopUp()}</View>;
    }
}

export default MulipleLogin;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    flexRowTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    flexRow: {
        flexDirection: 'row',
        marginTop: 7,
        justifyContent: 'space-between',
    },
    justifyContentSpaceBetween: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textSmall: {
        fontFamily: ROBOTO.bold,
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.off,
    },
    mL24: {
        width: '48%',
    },
    priceInfo: {
        fontFamily: ROBOTO.bold,
        color: COLOR.textSecondary,
        fontSize: FONT_SIZE.EXTRA_LARGE,
        fontWeight: 'bold',
    },
    cardTwo: {
        width: '94%',
        height: 219,
        backgroundColor: COLOR.white,
        borderRadius: 10,
        shadowColor: COLOR.shadow,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 3,
    },
    iconContainer: {
        width: 24,
        height: 24,
    },
    textRed: {
        color: COLOR.lightRed,
        marginTop: 3,
        fontWeight: 'bold',
    },
    loan_number: {
        marginTop: 3,
    },
    border: {
        left: '-5%',
        width: '110%',
        borderWidth: 0.7,
        borderColor: COLOR.shadow,
        marginTop: 20,
    },
    buttoncontainer: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    akadbutton: {
        borderColor: COLOR.darkGreen,
        backgroundColor: COLOR.white,
        width: '48%',
        height: 36,
        borderTopWidth: 1.5,
        borderLeftWidth: 1.5,
        borderRightWidth: 1.5,
        borderBottomWidth: 1.5,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    akadtext: {
        fontSize: 14,
        fontWeight: '700',
        color: COLOR.darkGreen,
    },
    pelunasanbutton: {
        width: '48%',
        height: 36,
        backgroundColor: COLOR.darkGreen,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    pelunasantext: {
        fontSize: 14,
        fontWeight: '700',
        color: COLOR.white,
    },
    popupcontainer: {
        width: 350,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    popuptitle: {
        marginTop: 20,
        fontSize: 20,
        color: COLOR.darkBlack,
        fontWeight: '700',
        letterSpacing: 0.25,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    popuptext: {
        marginTop: 10,
        fontSize: 15,
        textAlign: 'center',
        color: COLOR.textGray,
        lineHeight: 22,
        width: 300,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    confirmbutton: {
        marginTop: 15,
        width: 300,
        height: 39,
        backgroundColor: COLOR.primary,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    confirmtext: {
        fontSize: 16,
        color: COLOR.white,
        letterSpacing: 0.32,
        fontWeight: '700',
    },
    closebutton: {
        marginTop: 10,
        borderColor: COLOR.darkGreen,
        width: 300,
        height: 39,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closetext: {
        fontSize: 14,
        fontWeight: '700',
        color: COLOR.darkGreen,
        letterSpacing: 0.32,
    },
    goodRep: {
        height: 30,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: COLOR.tertiaryLight,
    },
    badRep: {
        height: 30,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: COLOR.lightRed,
    },
    iconImage: {
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: 280,
        width: 280,
        marginHorizontal: '20%',
        marginVertical: '-7%',
    },
});
