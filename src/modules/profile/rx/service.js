import ProfileRest from '../../../services/rest/rest.profile';
import AuthRest from '../../../services/rest/rest.auth';

export const service = class ProfileAllService {
    constructor() {
        this.rest = new ProfileRest();
        this.authRest = new AuthRest();
    }

    getMe() {
        return this.authRest.getMe();
    }

    updateLocation(data) {
        let body = {
            shipping_address: data.shipping_address,
            latitude: data.latitude,
            longitude: data.longitude,
            regency_id: data.regency_id,
            district_id: data.district_id,
            village_id: data.village_id,
        };

        return this.rest.saveLocation(body);
    }

    updatePassword(data) {
        let body = {
            old_password: data.old_password,
            password: data.password,
            confirm_password: data.confirm_password,
        };

        return this.rest.putPassword(body);
    }

    getLocation() {
        return this.rest.getLocation();
    }

    putProfile(data) {
        return this.rest.putProfile(data);
    }

    putAvatar(name, source) {
        return this.rest.putPicture(name, source);
    }

    getPaylaterBalance() {
        return this.rest.getPaylaterBalance();
    }

    getPaylaterHistory() {
        return this.rest.getPaylaterHistory();
    }

    getAkadDetail(id) {
        return this.rest.getAkadDetail(id);
    }

    getverifySaldoPin() {
        return this.rest.getverifySaldoPin();
    }

    verifyPinSaldo(data) {
        return this.rest.postVerifyPinSaldo(data);
    }
};

export const ProfileService = new service();
