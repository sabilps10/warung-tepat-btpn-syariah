import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    location: {},
    paylaterBalance: {},
    paylaterHistory: {},
    isConnected: false,
    isConnected_History: false,
    akadData: [],
    isLoading: false,
    error: false,
    balance: '',
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_LOC:
            return {
                ...state,
                location: action.payload,
            };
        case TYPE.SAVE_ERROR:
            return {
                ...state,
                errors: action.payload,
            };
        case 'form::reset':
            return {
                ...state,
                errors: {},
            };
        case 'logout':
            return INITIAL_STATE;
        case TYPE.VERIFY_SUCCESS:
            return {
                ...state,
                paylaterBalance: action.payload,
                isConnected: true,
            };
        case TYPE.VERIFY_ERROR:
            return {
                ...INITIAL_STATE,
                isConnected: false,
            };
        case TYPE.VERIFY_SUCCESS_HISTORY:
            return {
                ...state,
                paylaterHistory: action.payload,
                isConnected_History: true,
            };
        case TYPE.VERIFY_ERROR_HISTORY:
            return {
                ...INITIAL_STATE,
                isConnected_History: false,
            };
        case TYPE.FETCH_PAYLATER_BALANCE:
            return {
                ...state,
                paylaterBalance: action.payload,
            };
        case TYPE.FETCH_PAYLATER_HISTORY:
            return {
                ...state,
                paylaterHistory: action.payload,
            };
        case TYPE.FETCH_AKAD_CONTENT:
            return {
                ...state,
                akadData: action.payload,
            };
        case TYPE.VERIFY_SALDO_SUCCESS:
            return {
                ...state,
                isConnectSaldo: true,
                isLoading: action.payload.isLoading,
                balance: action.balance,
            };
        case TYPE.VERIFY_SALDO_TIMEOUT:
            return {
                ...state,
                isConnectSaldo: false,
                isLoading: action.payload.isLoading,
            };
        case TYPE.VERIFY_SALDO_FAILED:
            return {
                ...INITIAL_STATE,
                verified: action.payload,
                isConnectSaldo: action.payload.isConnectSaldo,
                isLoading: action.payload.isLoading,
                error: action.payload.error,
            };
        case TYPE.RESET_VERIFY_SALDO:
            return {
                ...state,
                isConnectSaldo: false,
            };
        case TYPE.VERIFY_PAYLATER_PIN_TIMEOUT:
            return {
                ...state,
                isConnected: true,
            };
        case TYPE.VERIFY_PAYLATER_PIN_FAILED:
            return {
                ...INITIAL_STATE,
                verified: action.payload,
                errorPinPaylater: action.payload.errorPinPaylater,
                isLoadingPinPaylater: action.payload.isLoadingPinPaylater,
                isConnected: action.payload.isConnected,
            };
        default:
            return state;
    }
}

export function persister({
    location,
    session,
    paylaterBalance,
    paylaterHistory,
    isConnected,
    isConnected_History,
    akadData,
    isLoading,
    error,
}) {
    return {
        location,
        session,
        paylaterBalance,
        paylaterHistory,
        isConnected,
        isConnected_History,
        akadData,
        isLoading,
        error,
    };
}
