import React, { Component } from 'react';
import { Text, StyleSheet, View, Image, ScrollView, RefreshControl } from 'react-native';
import { Card } from 'react-native-elements';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { COLOR, FONT_SIZE, ROBOTO, NUNITO } from '../../common/styles';
import { $loadPaylaterBalance, $loadPaylaterHistory } from './rx/action';
import { CurrencyFormatter } from '../../common/utils';

import iconSystem from '@assets/icons/icon_system_action_account_mutation.png';
import iconWarning from '@assets/icons/icon_system_alert_exclamation.png';
import iconCash from '@assets/icons/icon_system_action_cash_dark.png';
import HistoryList from './com/historyList';
import LoadingDialog from '../cart/com/loading_dialog';
import PaylaterHeader from './com/PaylaterHeader';

class paylater extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataPaylaterBalance: [],
            dataPaylaterHistory: [],
            isEmptyBalance: false,
            hasDataBalance: false,
            showLoadingDialog: true,
        };
        this.loadBalance = this.loadBalance.bind(this);
        this.loadPaylaterHistory = this.loadPaylaterHistory.bind(this);
    }

    componentDidMount() {
        this.loadBalance();
    }

    componentDidUpdate(prevProps) {
        if (this.props.dataPaylaterBalance !== prevProps.dataPaylaterBalance) {
            this.loadBalance();
        }
    }

    onRender = () => {
        this.doLoad();
    };

    doLoad() {
        this.loadPaylaterBalance();
        this.loadPaylaterHistory();
    }

    renderLoadingDialog() {
        const { showLoadingDialog } = this.state;
        return <LoadingDialog visible={showLoadingDialog} />;
    }

    loadBalance() {
        this.setState({
            dataPaylaterBalance: this.props.dataPaylaterBalance,
            hasDataBalance: true,
            isEmptyBalance: false,
        });
        this.loadPaylaterHistory();
    }

    loadPaylaterBalance() {
        this.props.$loadPaylaterBalance().then((res) => {
            if (res) {
                this.setState({
                    dataPaylaterBalance: res,
                    hasDataBalance: true,
                    isEmptyBalance: false,
                });
            } else {
                this.setState({
                    isEmptyBalance: true,
                    hasDataBalance: false,
                });
            }
        });
    }

    loadPaylaterHistory() {
        this.props.$loadPaylaterHistory().then((res) => {
            this.setState({
                dataPaylaterHistory: res,
                showLoadingDialog: false,
            });
        });
    }

    renderBalance() {
        let { dataPaylaterBalance, dataPaylaterHistory } = this.state;

        if (dataPaylaterHistory.code === '112') {
            return (
                <View style={styles.alignCenter}>
                    <View style={[styles.bgGreen, styles.headOne]}>
                        <Image source={iconSystem} style={styles.iconImage} resizeMode="contain" />
                        <Text style={styles.title}> Informasi Pembiayaan Murabahah </Text>
                    </View>
                    <Card containerStyle={styles.card}>
                        <View style={styles.flexRow}>
                            <Image source={iconCash} style={styles.iconImage} resizeMode="contain" />
                            <Text style={styles.subtitle}>Limit yang dapat digunakan</Text>
                        </View>
                        <Text style={styles.textMainSaldoDeliquent}>
                            {CurrencyFormatter(Number(dataPaylaterBalance.balance))}
                        </Text>
                        <View>
                            <View style={[styles.progressGrey, styles.progressBar]} />
                            <View
                                // eslint-disable-next-line react-native/no-inline-styles
                                style={{
                                    position: 'absolute',
                                    top: 15,
                                    left: 0,
                                    width: dataPaylaterBalance.usage_percent + '%',
                                    backgroundColor: COLOR.progressBarBlue,
                                    height: 14,
                                    borderRadius: 10,
                                    overflow: 'hidden',
                                }}
                            />
                        </View>
                        <View style={styles.infoBottom}>
                            <Text style={styles.titleInfo}>Total Pembiayaan</Text>
                            <Text style={styles.titleInfoPrice}>
                                {CurrencyFormatter(Number(dataPaylaterBalance.total_balance))}
                            </Text>
                        </View>
                        <View style={styles.infoFinancialBottom}>
                            <Text style={styles.titleInfoFinancial}>Batas akhir pembiayaan</Text>
                            <Text style={styles.titleInfoDate}>{dataPaylaterBalance.limit_date}</Text>
                        </View>
                    </Card>
                    <View style={styles.bgWhiteDeliquent}>
                        <View style={styles.warningContainer}>
                            <Image source={iconWarning} style={styles.iconImageWarning} resizeMode="contain" />
                            <Text style={styles.textWarning}>{dataPaylaterHistory.message}</Text>
                        </View>
                        <Text style={styles.titleTwoDeliquent}>Jatuh Tempo Tagihan</Text>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.alignCenter}>
                    <View style={[styles.bgGreen, styles.headOne]}>
                        <Image source={iconSystem} style={styles.iconImage} resizeMode="contain" />
                        <Text style={styles.title}> Informasi Pembiayaan Murabahah </Text>
                    </View>
                    <Card containerStyle={styles.card}>
                        <View style={styles.flexRow}>
                            <Image source={iconCash} style={styles.iconImage} resizeMode="contain" />
                            <Text style={styles.subtitle}>Limit yang dapat digunakan</Text>
                        </View>
                        <Text style={styles.textMainSaldo}>
                            {CurrencyFormatter(Number(dataPaylaterBalance.balance))}
                        </Text>
                        <View>
                            <View style={[styles.progressGrey, styles.progressBar]} />
                            <View
                                // eslint-disable-next-line react-native/no-inline-styles
                                style={{
                                    position: 'absolute',
                                    top: 15,
                                    left: 0,
                                    width: dataPaylaterBalance.usage_percent + '%',
                                    backgroundColor: COLOR.progressBarBlue,
                                    height: 14,
                                    borderRadius: 10,
                                    overflow: 'hidden',
                                }}
                            />
                        </View>
                        <View style={styles.infoBottom}>
                            <Text style={styles.titleInfo}>Total Pembiayaan</Text>
                            <Text style={styles.titleInfoPrice}>
                                {CurrencyFormatter(Number(dataPaylaterBalance.total_balance))}
                            </Text>
                        </View>
                        <View style={styles.infoFinancialBottom}>
                            <Text style={styles.titleInfoFinancial}>Batas akhir pembiayaan</Text>
                            <Text style={styles.titleInfoDate}>{dataPaylaterBalance.limit_date}</Text>
                        </View>
                    </Card>
                    <View style={styles.bgWhite}>
                        <Text style={styles.titleTwo}>Jatuh Tempo Tagihan</Text>
                    </View>
                </View>
            );
        }
    }

    renderCardLists() {
        const { dataPaylaterHistory } = this.state;
        const details = dataPaylaterHistory.details;
        const items = [];
        if (details.length >= 1) {
            // eslint-disable-next-line no-unused-vars
            for (const [index, item] of details.entries()) {
                items.push(<HistoryList key={index} data={item} navigation={this.props.navigation} />);
            }
            return items;
        } else {
            return (
                <View style={styles.headTwoEmpty}>
                    <Text style={styles.emptyHistoryTitle}>Belum Ada Tagihan</Text>
                </View>
            );
        }
    }

    renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} onRefresh={this.onRender} colors={[COLOR.primary]} />;
    }

    render() {
        let { isEmptyBalance, hasDataBalance } = this.state;
        let { dataPaylaterHistory } = this.state;
        const details = dataPaylaterHistory.details;

        if (isEmptyBalance === hasDataBalance) {
            return null;
        }

        return (
            <ScrollView style={styles.container} refreshControl={this.renderRefresh()}>
                <PaylaterHeader navigation={this.props.navigation} />
                {this.renderBalance()}
                {this.renderLoadingDialog()}
                <View style={styles.width100}>
                    {details !== undefined ? (
                        <View style={styles.headTwo}>{this.renderCardLists()}</View>
                    ) : (
                        <View style={styles.headTwoEmpty}>
                            <Text style={styles.emptyHistoryTitle}>Belum Ada Tagihan</Text>
                        </View>
                    )}
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        dataPaylaterBalance: state.Profile.paylaterBalance,
        isConnected: state.Profile.isConnected,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $loadPaylaterBalance,
            $loadPaylaterHistory,
        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(paylater);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.shadow,
    },
    bgGreen: {
        backgroundColor: COLOR.darkGreen,
        width: '100%',
    },
    bgWhite: {
        width: '100%',
        marginTop: -116,
        height: 150,
        backgroundColor: COLOR.white,
    },
    bgWhiteDeliquent: {
        width: '100%',
        marginTop: -116,
        height: 250,
        backgroundColor: COLOR.white,
    },
    headOne: {
        minHeight: 150,
        padding: 24,
        flexDirection: 'row',
    },
    title: {
        fontFamily: ROBOTO.bold,
        color: COLOR.white,
        fontSize: FONT_SIZE.LARGE,
        fontWeight: 'bold',
    },
    flexRow: {
        flexDirection: 'row',
    },
    card: {
        width: '91%',
        marginTop: -81,
        marginBottom: 20,
        backgroundColor: COLOR.white,
        borderRadius: 10,
        shadowColor: COLOR.shadow,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
    },
    subtitle: {
        fontFamily: ROBOTO.bold,
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.textGray,
        marginLeft: 10,
        marginTop: 3,
    },
    progressGrey: {
        width: '100%',
        backgroundColor: COLOR.backgroundCart,
        height: 10,
        borderRadius: 10,
        overflow: 'hidden',
    },
    progressBar: {
        position: 'absolute',
        top: 15,
        left: 0,
    },
    textMainSaldo: {
        fontFamily: ROBOTO.bold,
        fontSize: FONT_SIZE.XLARGE,
        fontWeight: 'bold',
        color: COLOR.textSecondary,
    },
    textMainSaldoDeliquent: {
        fontFamily: ROBOTO.bold,
        fontSize: FONT_SIZE.XLARGE,
        fontWeight: 'bold',
        color: COLOR.shadow,
    },
    warningContainer: {
        width: '91%',
        height: '33%',
        borderRadius: 10,
        backgroundColor: COLOR.warningRed,
        marginTop: 115,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        marginHorizontal: '4.3%',
    },
    textWarning: {
        color: COLOR.lightRed,
        marginLeft: 5,
        marginRight: 5,
        lineHeight: 20,
    },
    infoBottom: {
        marginTop: 50,
        flexDirection: 'row',
    },
    infoFinancialBottom: {
        flexDirection: 'row',
    },
    titleInfo: {
        fontFamily: NUNITO.bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.textSecondary,
    },
    titleInfoPrice: {
        fontFamily: NUNITO.bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.darkGreen,
        marginLeft: 10,
    },
    titleInfoFinancial: {
        fontFamily: NUNITO.bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.off,
    },
    titleInfoDate: {
        fontFamily: NUNITO.extraBold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.lightRed,
        marginLeft: 10,
    },
    titleTwo: {
        fontFamily: ROBOTO.bold,
        color: COLOR.textSecondary,
        fontSize: FONT_SIZE.LARGE,
        fontWeight: 'bold',
        marginHorizontal: 24,
        marginTop: 110,
    },
    titleTwoDeliquent: {
        fontFamily: ROBOTO.bold,
        color: COLOR.textSecondary,
        fontSize: FONT_SIZE.LARGE,
        fontWeight: 'bold',
        marginHorizontal: 24,
        marginTop: 15,
    },
    headTwo: {
        backgroundColor: COLOR.shadow,
        marginLeft: '2%',
        marginRight: '2%',
        marginBottom: 20,
    },
    iconImage: {
        width: 24,
        height: 24,
    },
    iconImageWarning: {
        width: 24,
        height: 24,
        top: '-2%',
    },
    headTwoEmpty: {
        backgroundColor: COLOR.shadow,
        height: 400,
        paddingVertical: 32,
        paddingHorizontal: 100,
        alignContent: 'center',
        alignItems: 'center',
    },
    emptyHistoryTitle: {
        fontFamily: ROBOTO.regular,
        fontSize: FONT_SIZE.LARGE,
        color: COLOR.textSecondary,
    },
    alignCenter: {
        alignItems: 'center',
    },
    width100: {
        width: '100%',
    },
});
