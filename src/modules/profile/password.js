import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, View } from 'react-native';

import { COLOR, FONT_SIZE } from '../../common/styles';
import FloatingFooter from '../../components/floating_footer';
import PkaButton from '../../components/button';
import { TextField } from 'react-native-material-textfield';
import { bindActionCreators } from 'redux';
import { $updatePassword } from './rx/action';
import { $reset } from '../../services/form/action';

class PasswordScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            old_password: '',
            password: '',
            confirm_password: '',
        };
    }

    onSubmit = () => {
        this.resetForm();

        let form = {
            old_password: this.state.old_password,
            password: this.state.password,
            confirm_password: this.state.confirm_password,
        };

        this.props.$updatePassword(form).then((res) => {
            if (res) {
                this.props.navigation.navigate('ProfileScreen');
            }
        });
    };

    resetForm() {
        this.props.$reset();
    }

    render() {
        let { old_password, password, confirm_password } = this.state;
        return (
            <View style={styles.container}>
                <ScrollView style={styles.containerScrollView} keyboardShouldPersistTaps="handled">
                    <View style={styles.section}>
                        <View style={styles.field}>
                            <TextField
                                label="Kata Sandi Sekarang"
                                value={old_password}
                                error={this.props.errors.old_password}
                                returnKeyType="next"
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                secureTextEntry={true}
                                onChangeText={(oldPassword) => this.setState({ old_password: oldPassword })}
                            />
                        </View>
                        <View style={styles.field}>
                            <TextField
                                label="Kata Sandi Baru"
                                value={password}
                                error={this.props.errors.password}
                                returnKeyType="next"
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                secureTextEntry={true}
                                onChangeText={(passwords) => this.setState({ password: passwords })}
                            />
                        </View>
                        <View style={{ ...styles.field, ...styles.containerConfirmPassword }}>
                            <TextField
                                label="Ketik Ulang Kata Sandi"
                                value={confirm_password}
                                error={this.props.errors.confirm_password}
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                secureTextEntry={true}
                                onChangeText={(confirmPassword) => this.setState({ confirm_password: confirmPassword })}
                            />
                        </View>
                    </View>
                </ScrollView>
                <FloatingFooter>
                    <PkaButton
                        color={COLOR.success}
                        title="Simpan Kata Sandi"
                        onPress={this.onSubmit}
                        loading={this.props.isLoading}
                    />
                </FloatingFooter>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: state.Activity.components.password,
        errors: state.Profile.errors,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $updatePassword,
            $reset,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(PasswordScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.light,
        margin: 0,
        padding: 0,
        paddingBottom: 70,
    },
    section: {
        backgroundColor: COLOR.white,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: COLOR.border,
        paddingBottom: 5,
        marginBottom: 15,
    },
    field: {
        paddingHorizontal: 20,
        borderBottomWidth: 0.7,
        borderColor: COLOR.border,
        paddingTop: 5,
    },
    title: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        fontSize: FONT_SIZE.EXTRA_SMALL,
    },
    containerScrollView: {
        paddingTop: 15,
    },
    containerConfirmPassword: {
        borderBottomWidth: 0,
    },
});
