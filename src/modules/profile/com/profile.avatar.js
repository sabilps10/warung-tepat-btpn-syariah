import React, { PureComponent } from 'react';
import ImagePicker from 'react-native-image-picker';
import { COLOR, PADDING } from '../../../common/styles';
import { StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import PropTypes from 'prop-types';

export default class AvatarUser extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            avatar: 'logo',
            name: '',
            username: '',
        };
    }

    componentDidMount() {
        this.props.user;
    }

    componentWillUnmount() {
        this.setState({
            avatar: 'logo',
            name: '',
            username: '',
        });
    }

    choosePhoto = () => {
        const options = {
            title: 'Pilih Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.data) {
                this.props.action('avatar-' + this.state.username, 'data:image/jpeg;base64,' + response.data);
            }

            if (response.uri) {
                this.setState({ avatar: response.uri });
            }
        });
    };

    render() {
        return (
            <Avatar
                size="xlarge"
                rounded
                source={{ uri: this.state.avatar }}
                resizeMode="cover"
                containerStyle={styles.container}
                avatarStyle={styles.avatarSize}
                title={this.state.name[0]}
                editButton={{
                    name: 'camera',
                    size: 18,
                    type: 'feather',
                    color: COLOR.darkGray,
                    containerStyle: {
                        backgroundColor: COLOR.light,
                        padding: PADDING.PADDING_CARD,
                        width: 28,
                        height: 28,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 50,
                        marginTop: -9,
                    },
                }}
                showEditButton
                onEditPress={this.choosePhoto}
                onPress={this.choosePhoto}
            />
        );
    }
}

AvatarUser.propTypes = {
    user: PropTypes.object.isRequired,
    action: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    container: {
        height: 110,
        width: 110,
        borderColor: COLOR.white,
        borderWidth: 5,
    },
    avatarSize: {
        height: 100,
        width: 100,
    },
});
