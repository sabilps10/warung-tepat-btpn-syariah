import React, { Component } from 'react';
import Moment from 'react-moment';

import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import { COLOR, FONT_SIZE, ROBOTO } from '../../../common/styles';
import { CurrencyFormatter } from '../../../common/utils';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

class HistoryList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            visible: false,
        };
    }

    onVisibleTrue = () => {
        this.setState({ visible: true });
    };

    onVisibleFalse = () => {
        this.setState({ visible: false });
    };

    viewAkad = () => {
        const data = this.props.data;
        const noakad = data.loan_number;

        this.props.navigation.navigate('ViewAkad', { noakad: noakad }, true);
    };

    _renderPopUp = () => {
        return (
            <Dialog
                visible={this.state.visible}
                rounded={true}
                onTouchOutside={() => {
                    this.onVisibleTrue();
                }}
                onHardwareBackPress={() => {
                    this.onVisibleFalse();
                    return true;
                }}
            >
                <DialogContent style={styles.popupcontainer}>
                    <Text style={styles.popuptitle}>Percepat Pelunasan?</Text>
                    <Text style={styles.popuptext}>
                        Anda akan mempercepat pelunasan tagihan. Pastikan dana sudah tersedia di rekening Mitra Anda.
                        Tekan konfirmasi jika ingin melanjutkan pelunasan.
                    </Text>
                    <TouchableOpacity style={styles.confirmbutton}>
                        <Text style={styles.confirmtext}>Konfirmasi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.onVisibleFalse();
                        }}
                        style={styles.closebutton}
                    >
                        <Text style={styles.closetext}>Kembali</Text>
                    </TouchableOpacity>
                </DialogContent>
            </Dialog>
        );
    };

    _isDeliquent = () => {
        const data = this.props.data;

        return (
            <View style={data.is_delinquent ? styles.badRep : styles.goodRep}>
                <Text style={{ color: COLOR.white }}>{data.is_delinquent ? 'Tidak Lancar' : 'Lancar'}</Text>
            </View>
        );
    };

    renderItem() {
        const data = this.props.data;

        return (
            <Card containerStyle={styles.cardTwo}>
                {this._renderPopUp()}
                <View style={styles.justifyContentSpaceBetween}>
                    <Moment style={styles.textSmall} element={Text} format="DD MMMM YYYY">
                        {data.transaction_date}
                    </Moment>
                    {this._isDeliquent()}
                </View>
                <View style={styles.flexRowTop}>
                    <View style={styles.noakad}>
                        <Text style={styles.textSmall}>Tagihan</Text>
                        <Text style={styles.priceInfo}>{CurrencyFormatter(Number(data.next_installment_amount))}</Text>
                    </View>
                    <View style={styles.mL24}>
                        <Text style={styles.textSmall}>Tenor</Text>
                        <Text style={styles.loan_number}>
                            {data.tenor_installment}/{data.tenor}
                        </Text>
                    </View>
                </View>
                <View style={styles.flexRow}>
                    <View style={styles.noakad}>
                        <Text style={styles.textSmall}>Transaksi</Text>
                        <Text style={styles.total_installment}>
                            {CurrencyFormatter(Number(data.total_installment))}
                        </Text>
                    </View>
                    <View style={styles.mL24}>
                        <Text style={styles.textSmall}>Jatuh Tempo</Text>
                        <Moment style={styles.textRed} element={Text} format="DD MMMM YYYY">
                            {data.next_installment_date}
                        </Moment>
                    </View>
                </View>
                <View style={styles.border} />
                <View style={styles.buttoncontainer}>
                    <TouchableOpacity onPress={() => this.viewAkad()} style={styles.akadbutton}>
                        <Text style={styles.akadtext}>Lihat Akad</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => { this.onVisibleTrue() }} style={styles.pelunasanbutton}>
                        <Text style={styles.pelunasantext}>Pelunasan</Text>
                    </TouchableOpacity> */}
                </View>
            </Card>
        );
    }

    render() {
        return <View>{this.renderItem()}</View>;
    }
}

export default HistoryList;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    flexRowTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    flexRow: {
        flexDirection: 'row',
        marginTop: 7,
        justifyContent: 'space-between',
    },
    justifyContentSpaceBetween: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textSmall: {
        fontFamily: ROBOTO.bold,
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.off,
    },
    mL24: {
        width: '48%',
    },
    priceInfo: {
        fontFamily: ROBOTO.bold,
        color: COLOR.textSecondary,
        fontSize: FONT_SIZE.EXTRA_LARGE,
        fontWeight: 'bold',
    },
    cardTwo: {
        width: '94%',
        // height: '50%',
        backgroundColor: COLOR.white,
        borderRadius: 10,
        shadowColor: COLOR.shadow,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 3,
    },
    iconContainer: {
        width: 24,
        height: 24,
    },
    textRed: {
        color: COLOR.lightRed,
        marginTop: 3,
        fontWeight: 'bold',
    },
    loan_number: {
        marginTop: 3,
    },
    border: {
        left: '-5%',
        width: '110%',
        borderWidth: 0.7,
        borderColor: COLOR.shadow,
        marginTop: 20,
    },
    buttoncontainer: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    akadbutton: {
        borderColor: COLOR.darkGreen,
        backgroundColor: COLOR.white,
        width: '48%',
        height: 36,
        borderTopWidth: 1.5,
        borderLeftWidth: 1.5,
        borderRightWidth: 1.5,
        borderBottomWidth: 1.5,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    akadtext: {
        fontSize: 14,
        fontWeight: '700',
        color: COLOR.darkGreen,
    },
    pelunasanbutton: {
        width: '48%',
        height: 36,
        backgroundColor: COLOR.darkGreen,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    pelunasantext: {
        fontSize: 14,
        fontWeight: '700',
        color: COLOR.white,
    },
    popupcontainer: {
        width: 350,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    popuptitle: {
        marginTop: 20,
        fontSize: 20,
        color: COLOR.darkBlack,
        fontWeight: '700',
        letterSpacing: 0.25,
    },
    popuptext: {
        marginTop: 10,
        fontSize: 15,
        textAlign: 'center',
        color: COLOR.textGray,
        lineHeight: 22,
        width: 300,
    },
    confirmbutton: {
        marginTop: 15,
        width: 300,
        height: 39,
        backgroundColor: COLOR.primary,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    confirmtext: {
        fontSize: 16,
        color: COLOR.white,
        letterSpacing: 0.32,
        fontWeight: '700',
    },
    closebutton: {
        marginTop: 10,
        borderColor: COLOR.darkGreen,
        width: 300,
        height: 39,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closetext: {
        fontSize: 14,
        fontWeight: '700',
        color: COLOR.darkGreen,
        letterSpacing: 0.32,
    },
    goodRep: {
        height: 30,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: COLOR.tertiaryLight,
    },
    badRep: {
        height: 30,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: COLOR.lightRed,
    },
});
