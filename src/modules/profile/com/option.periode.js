import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList, StyleSheet, Text, TouchableNativeFeedback, View } from 'react-native';
import { Dialog, DialogButton, DialogFooter, DialogTitle, SlideAnimation } from 'react-native-popup-dialog';
import moment from 'moment';

import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';
import { ListItem } from 'react-native-elements';

export default class PeriodePicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: {},
            periodes: null,
        };
    }

    _onLoad() {
        let periodes = [];
        let i;
        for (i = 0; i > -4; i--) {
            let p = {
                value: this.getPeriode(i).format('YYYY-MM'),
                title: this.getPeriode(i).format('MMMM YYYY'),
            };
            periodes.push(p);
        }

        this.setState({ periodes: periodes });
    }

    getPeriode(m) {
        return moment().add(m, 'month');
    }

    componentDidMount() {
        this._onLoad();
    }

    _onSelected = () => {
        this.props.onSubmit(this.state.selected);
    };

    _renderFooter() {
        return (
            <DialogFooter>
                <DialogButton
                    text="BATAL"
                    onPress={this._onClose}
                    style={styles.button}
                    key="button-1"
                    textStyle={{
                        ...styles.textButton,
                        color: COLOR.danger,
                    }}
                />
                <DialogButton
                    text="PILIH"
                    onPress={this._onSelected}
                    style={styles.button}
                    key="button-2"
                    textStyle={{
                        ...styles.textButton,
                        color: !this.state.selected.value ? COLOR.darkGray : COLOR.success,
                    }}
                    disabled={!this.state.selected.value}
                />
            </DialogFooter>
        );
    }

    _onClose = () => {
        this.setState({ selected: {} });
        this.props.onCancel();
    };

    _onPressItem = (item) => {
        this.setState({ selected: item });
    };

    _flatItem = ({ index, item }) => (
        <TouchableNativeFeedback onPress={() => this._onPressItem(item)}>
            <ListItem
                containerStyle={styles.itemContainer}
                title={<Text>{item.title}</Text>}
                rightIcon={
                    this.state.selected.value === item.value
                        ? {
                              name: 'check',
                              color: COLOR.success,
                              size: 15,
                          }
                        : null
                }
            />
        </TouchableNativeFeedback>
    );

    _flatKey = (item, index) => index.toString();

    _flatReff = (ref) => (this.listRef = ref);

    render() {
        const { visible, onCancel } = this.props;

        return (
            <Dialog
                visible={visible}
                onTouchOutside={onCancel}
                footer={this._renderFooter()}
                dialogAnimation={new SlideAnimation()}
                onHardwareBackPress={onCancel}
                width={0.8}
                dialogTitle={
                    <DialogTitle
                        title="PILIH PERIODE"
                        hasTitleBar={true}
                        style={styles.titleContainer}
                        textStyle={styles.textTitle}
                    />
                }
            >
                <View style={styles.list}>
                    <FlatList
                        data={this.state.periodes}
                        keyExtractor={this._flatKey}
                        renderItem={this._flatItem}
                        extraData={this.state}
                        initialNumToRender={15}
                        showsVerticalScrollIndicator={true}
                    />
                </View>
            </Dialog>
        );
    }
}

PeriodePicker.propTypes = {
    visible: PropTypes.bool,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    titleContainer: {
        paddingVertical: 10,
    },
    itemContainer: {
        padding: 13,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: COLOR.light,
    },
    textTitle: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
    },
    dialogContainer: {
        padding: 0,
        paddingHorizontal: 0,
        margin: 0,
    },
    textContent: {
        textAlign: 'center',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.semi_bold,
    },
    button: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.success,
        fontFamily: SSP.semi_bold,
    },
    list: {
        height: 170,
    },
});
