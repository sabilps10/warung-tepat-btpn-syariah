/* eslint-disable prettier/prettier */
export const dataMain = [
    {
        id: 1,
        jumlah_pemakaian: '250.000',
        total_pembiayaan: '1.000.000',
        percent_pemakaian: 30,
        date_end: '30 September 2021',
    },
    {
        id: 2,
        jumlah_pemakaian: '150.000',
        total_pembiayaan: '1.000.000',
        percent_pemakaian: 40,
        date_end: '15 September 2021',
    },
    {
        id: 3,
        jumlah_pemakaian: '450.000',
        total_pembiayaan: '1.000.000',
        percent_pemakaian: 70,
        date_end: '20 December 2021',
    },
    {
        id: 4,
        jumlah_pemakaian: '900.000',
        total_pembiayaan: '1.000.000',
        percent_pemakaian: 90,
        date_end: '10 November 2021',
    },
];

export const dataCard = [
    {
        id: 1,
        date_start: '14 Juli 2021',
        jumlah_pemakaian: '250.000',
        no_akad: 'ORD/2021/IV/004',
        date_end: '4 Juni 2021',
    },
    {
        id: 2,
        date_start: '14 Juli 2021',
        jumlah_pemakaian: '250.000',
        no_akad: 'ORD/2021/IV/003',
        date_end: '7 Juni 2021',
    },
    {
        id: 3,
        date_start: '14 Juli 2021',
        jumlah_pemakaian: '250.000',
        no_akad: 'ORD/2021/IV/002',
        date_end: '12 Juni 2021',
    },
    {
        id: 4,
        date_start: '14 Juli 2021',
        jumlah_pemakaian: '250.000',
        no_akad: 'ORD/2021/IV/001',
        date_end: '25 Juni 2021',
    },
];
