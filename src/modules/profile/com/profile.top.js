import React, { Component } from 'react';
import { LayoutAnimation, Platform, StyleSheet, Text, UIManager, View } from 'react-native';
import PropTypes from 'prop-types';
import { Card } from 'react-native-elements';

import { COLOR, FONT_SIZE, PADDING, SSP, ROBOTO } from '../../../common/styles';
import AvatarUser from './profile.avatar';

export default class ProfileTop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: true,
        };

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    _navigate(route) {
        this.props.navigation.navigate(route);
    }

    toggle = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ collapsed: !this.state.collapsed });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerAvatar}>
                    <AvatarUser user={this.props.session.user} action={this.props.avatarAction} />
                </View>
                <Card containerStyle={styles.containerCard}>
                    {this.props.session.poskora ? (
                        <View>
                            <View style={styles.containerName}>
                                <Text style={styles.name}>{this.props.session.user.name.toUpperCase()}</Text>
                            </View>
                            <View style={styles.containerCodeCard}>
                                <Text style={styles.id}>{this.props.session.poskora.code}</Text>
                            </View>
                        </View>
                    ) : null}
                </Card>
            </View>
        );
    }
}

ProfileTop.propTypes = {
    navigation: PropTypes.object.isRequired,
    session: PropTypes.object.isRequired,
    avatarAction: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
    },
    containerAvatar: {
        alignContent: 'center',
        alignItems: 'center',
        zIndex: 2,
    },
    avatarSize: {
        height: 100,
        width: 100,
    },
    containerCard: {
        padding: 0,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        borderColor: COLOR.border,
        borderWidth: 0.2,
        borderBottomWidth: 0.5,
        elevation: 0,
        marginTop: -50,
        marginLeft: 24,
        marginRight: 24,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    containerName: {
        paddingTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    name: {
        fontFamily: ROBOTO.bold,
        fontSize: FONT_SIZE.LARGE,
        color: COLOR.black,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        textAlign: 'center',
    },
    id: {
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.off,
        textAlign: 'center',
        paddingBottom: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    btnGroup: {
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 0,
        marginTop: PADDING.PADDING_VERTICAL_CONTAINER,
        borderColor: COLOR.border,
        borderRadius: 0,
        borderBottomWidth: 0,
        borderWidth: 0.5,
    },
    textButton: {
        color: COLOR.black,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
    },
    columnContainer: {
        paddingVertical: PADDING.PADDING_VERTICAL_CONTAINER,
        borderRightWidth: 0.5,
        borderTopWidth: 0.5,
        borderColor: COLOR.border,
    },
    bodyContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerProfile: {
        padding: 0,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingVertical: PADDING.PADDING_VERTICAL_CONTAINER,
        borderColor: COLOR.border,
        borderWidth: 0.2,
        borderBottomWidth: 0.5,
        elevation: 0,
    },
    containerProfileName: {
        paddingBottom: PADDING.PADDING_CARD,
    },
    label: {
        color: COLOR.darkGray,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
        padding: PADDING.PADDING_CARD,
        paddingLeft: 0,
    },
    input: {
        width: '100%',
        borderColor: COLOR.border,
        borderWidth: 0.5,
        padding: PADDING.PADDING_CARD,
    },
    textInput: {
        color: COLOR.black,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    containerCodeCard: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});
