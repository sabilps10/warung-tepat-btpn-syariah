import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    AppState,
    Keyboard,
    PermissionsAndroid,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    View,
    ActivityIndicator,
} from 'react-native';
import { COLOR, FONT_SIZE } from '../../common/styles';
import MapView, { Marker } from 'react-native-maps';
import { Icon, Text } from 'react-native-elements';
import FusedLocation from 'react-native-fused-location';
import { bindActionCreators } from 'redux';
import { TextField } from 'react-native-material-textfield';
import AndroidOpenSettings from 'react-native-android-open-settings';

import { $loadLocation, $updateLocation } from './rx/action';
import { $resetStore } from '../auth/rx/action';
import { $load } from '../home/rx/action';
import { $reset } from '../../services/form/action';
import { Toast } from '../../common/utils';
import { DistrictPicker, RegencyPicker, VillagePicker } from '../../components/region';
import FloatingFooter from '../../components/floating_footer';
import PkaButton from '../../components/button';

class LocationScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            appState: AppState.currentState,
            screen_mode: 'view',
            region: {
                latitudeDelta: 0.00922,
                longitudeDelta: 0.00421,
            },
            regionDefault: {
                latitude: -6.1753924,
                longitude: 106.8249641,
                latitudeDelta: 0.11922,
                longitudeDelta: 0.11421,
            },
            shipping_address: '',
            province_id: 0,
            district_id: 0,
            village_id: 0,
            pick_regency: false,
            pick_district: false,
            pick_village: false,
            regency: {},
            district: {},
            village: {},
        };
    }
    _reload() {
        this.props.$loadLocation().then((res) => {
            if (res) {
                this.setState({
                    shipping_address: res.shipping_address,
                    province_id: 0,
                    district_id: 0,
                    village_id: 0,
                    pick_regency: false,
                    pick_district: false,
                    pick_village: false,
                    regency: res.village.district.regency,
                    district: res.village.district,
                    village: res.village,
                    region: {
                        latitude: res.latitude,
                        longitude: res.longitude,
                        latitudeDelta: 0.00922,
                        longitudeDelta: 0.00421,
                    },
                    screen_mode: !res.latitude ? 'update' : 'view',
                });

                if (res.latitude === '') {
                    this.readLocation();
                }
            }
        });
    }

    componentDidMount() {
        this._reload();
        AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.readLocation();
        }

        this.setState({ appState: nextAppState });
    };

    async readLocation() {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
            title: 'App needs to access your location',
            message: 'App needs access to your location so we can let our app be even more awesome.',
        });
        if (granted) {
            const on = await FusedLocation.areProvidersAvailable();
            if (on) {
                const location = await FusedLocation.getFusedLocation();

                let region = {
                    latitude: location.latitude,
                    longitude: location.longitude,
                    latitudeDelta: 0.00122,
                    longitudeDelta: 0.00121,
                };

                this.setState({ region });
            } else {
                AndroidOpenSettings.locationSourceSettings();
            }
        }
    }

    onRegionChange = (region) => {
        if (this.state.screen_mode === 'update') {
            this.setState({
                region,
            });
        }
    };

    onRegionChangeManual = (regionDefault) => {
        if (this.state.screen_mode === 'update') {
            this.setState({
                regionDefault,
            });
        }
    };

    showPickRegency = () => {
        if (this.state.screen_mode === 'update') {
            Keyboard.dismiss();
            this.setState({ pick_regency: true });
        }
    };

    hidePickRegency = () => {
        this.setState({ pick_regency: false });
    };

    selectedRegency = (picked) => {
        this.setState({
            pick_regency: false,
            regency: picked,
            district: this.state.regency.id === picked.id ? this.state.district : { name: '' },
            village: this.state.regency.id === picked.id ? this.state.village : { name: '' },
        });
    };

    showPickDistrict = () => {
        if (this.state.regency.id && this.state.screen_mode === 'update') {
            this.setState({ pick_district: true });
        } else {
            Toast('Pilih Kota/Kabupaten terlebih dahulu');
        }
    };

    hidePickDistrict = () => {
        this.setState({ pick_district: false });
    };

    selectedDistrict = (picked) => {
        this.setState({
            pick_district: false,
            district: picked,
            village: this.state.district.id !== picked.id ? { name: '' } : this.state.village,
        });
    };

    showPickVillage = () => {
        if (this.state.district.id && this.state.screen_mode === 'update') {
            this.setState({ pick_village: true });
        } else {
            Toast('Pilih Kecamatan terlebih dahulu');
        }
    };

    hidePickVillage = () => {
        this.setState({ pick_village: false });
    };

    selectedVillage = (picked) => {
        this.setState({
            pick_village: false,
            village: picked,
        });
    };

    onChangeText = (text) => {
        this.setState({ shipping_address: text.replace(/[\!\<\%\>]/g, '') });
    };

    onSubmit = () => {
        let form = {
            shipping_address: this.state.shipping_address,
            latitude: !this.state.region.latitude ? this.state.regionDefault.latitude : this.state.region.latitude,
            longitude: !this.state.region.longitude ? this.state.regionDefault.longitude : this.state.region.longitude,
            regency_id: this.state.regency.id,
            district_id: this.state.district.id,
            village_id: this.state.village.id,
        };

        this.props.$updateLocation(form).then((res) => {
            if (res) {
                this.props.$resetStore();
                this.props.$load();
                Toast('Alamat pengiriman telah tersimpan');
                this._reload();
            }
        });
    };

    goToSetting = () => {
        AndroidOpenSettings.locationSourceSettings();
    };

    onUpdateMode = () => {
        this.setState({ screen_mode: 'update' });
        this.readLocation();
    };

    resetForm() {
        this.props.$reset();
    }

    render() {
        const mapStyle = {
            flex: 1,
            height: 300,
            position: 'relative',
        };
        const sectionStyle = {
            marginBottom: 0,
            paddingHorizontal: 20,
            paddingVertical: 5,
        };
        const fieldStyle = {
            borderBottomWidth: 0,
        };
        let { shipping_address } = this.state;

        if (!shipping_address) {
            return <ActivityIndicator style={styles.indicator} size={50} color={COLOR.secondary} />;
        }

        return (
            <View style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View>
                        {this.state.region.latitude ? (
                            <MapView
                                style={mapStyle}
                                region={this.state.region}
                                onRegionChangeComplete={this.onRegionChange}
                            >
                                {this.state.screen_mode === 'view' ? (
                                    <Marker
                                        coordinate={{
                                            latitude: this.state.region.latitude,
                                            longitude: this.state.region.longitude,
                                        }}
                                    >
                                        <Icon size={40} name="person-pin" type="MaterialIcons" color={COLOR.primary} />
                                    </Marker>
                                ) : null}
                            </MapView>
                        ) : (
                            <View>
                                <MapView
                                    style={mapStyle}
                                    region={this.state.regionDefault}
                                    onRegionChangeComplete={this.onRegionChangeManual}
                                />
                                <View style={{ ...styles.section, sectionStyle }}>
                                    <TouchableOpacity onPress={this.goToSetting}>
                                        <Text>
                                            Tidak dapat membaca lokasi Anda, Pastikan Mode Lokasi Android menjadi
                                            Akurasi Tinggi, Klik disini untuk membuka konfigurasi android
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        )}
                        {this.state.screen_mode !== 'view' ? (
                            <View style={styles.markerFixed}>
                                <Icon size={40} name="person-pin" type="MaterialIcons" color={COLOR.primary} />
                            </View>
                        ) : null}
                    </View>

                    <Text style={styles.title}>ALAMAT PENGIRIMAN</Text>
                    <View style={styles.section}>
                        <View style={styles.field}>
                            <TextField
                                label="Alamat Lengkap"
                                value={shipping_address}
                                error={this.props.errors.shipping_address}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType="done"
                                multiline={true}
                                lineWidth={0}
                                activeLineWidth={0}
                                inputContainerPadding={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                onFocus={() => this.resetForm()}
                                editable={this.state.screen_mode === 'update'}
                                onChangeText={this.onChangeText}
                            />
                        </View>
                        <View style={styles.field}>
                            <TouchableOpacity
                                onPress={this.showPickRegency}
                                disabled={this.state.screen_mode !== 'update'}
                            >
                                <View style={styles.textContainer}>
                                    <TextField
                                        label="Kota / Kabupaten"
                                        error={this.props.errors.regency_id}
                                        lineWidth={0}
                                        activeLineWidth={0}
                                        tintColor={COLOR.primary}
                                        labelHeight={16}
                                        fontSize={14}
                                        editable={false}
                                        onChangeText={this.onChangeText}
                                        onFocus={() => this.resetForm()}
                                    />
                                    <Text style={styles.textTitle}>{this.state.regency.name}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.field}>
                            <TouchableOpacity
                                onPress={this.showPickDistrict}
                                disabled={this.state.screen_mode !== 'update'}
                            >
                                <View style={styles.textContainer}>
                                    <TextField
                                        label="Kecamatan"
                                        error={this.props.errors.district_id}
                                        lineWidth={0}
                                        activeLineWidth={0}
                                        inputContainerPadding={0}
                                        tintColor={COLOR.primary}
                                        labelHeight={16}
                                        fontSize={14}
                                        editable={this.props.errors.regency_id > 0}
                                    />
                                    <Text style={styles.textTitle}>{this.state.district.name}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ ...styles.field, fieldStyle }}>
                            <TouchableOpacity
                                onPress={this.showPickVillage}
                                disabled={this.state.screen_mode !== 'update'}
                            >
                                <View style={styles.textContainer}>
                                    <TextField
                                        label="Kelurahan"
                                        error={this.props.errors.village_id}
                                        lineWidth={0}
                                        activeLineWidth={0}
                                        inputContainerPadding={0}
                                        tintColor={COLOR.primary}
                                        labelHeight={16}
                                        fontSize={14}
                                        editable={this.props.errors.district_id > 0}
                                    />
                                    <Text style={styles.textTitle}>{this.state.village.name}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <FloatingFooter>
                    {this.state.screen_mode === 'view' ? (
                        <PkaButton
                            color={COLOR.secondary}
                            title="Perbaharui Alamat Pengiriman"
                            onPress={this.onUpdateMode}
                            loading={this.props.isLoading}
                        />
                    ) : (
                        <PkaButton
                            color={COLOR.success}
                            title="Simpan Alamat Pengiriman"
                            onPress={this.onSubmit}
                            loading={this.props.isLoading}
                        />
                    )}
                </FloatingFooter>

                {this.state.screen_mode === 'update' ? (
                    <RegencyPicker
                        visible={this.state.pick_regency}
                        onSubmit={this.selectedRegency}
                        onCancel={this.hidePickRegency}
                    />
                ) : null}

                {this.state.regency.id ? (
                    <DistrictPicker
                        visible={this.state.pick_district}
                        regencyId={this.state.regency.id}
                        onSubmit={this.selectedDistrict}
                        onCancel={this.hidePickDistrict}
                    />
                ) : null}

                {this.state.district.id ? (
                    <VillagePicker
                        visible={this.state.pick_village}
                        districtId={this.state.district.id}
                        onSubmit={this.selectedVillage}
                        onCancel={this.hidePickVillage}
                    />
                ) : null}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        errors: state.Profile.errors,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $loadLocation,
            $reset,
            $updateLocation,
            $resetStore,
            $load,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.light,
        margin: 0,
        padding: 0,
        paddingBottom: 70,
    },
    markerFixed: {
        left: '50%',
        marginLeft: -20,
        marginTop: -41,
        position: 'absolute',
        top: '50%',
    },
    section: {
        backgroundColor: COLOR.white,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: COLOR.border,
        paddingBottom: 5,
        marginBottom: 15,
    },
    field: {
        paddingHorizontal: 20,
        borderBottomWidth: 0.7,
        borderColor: COLOR.border,
        paddingTop: 5,
    },
    title: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        fontSize: 11,
        paddingTop: 15,
        borderTopWidth: 1,
        borderColor: COLOR.border,
    },
    textContainer: {
        marginTop: '-6%',
        borderBottomWidth: 0.7,
        borderColor: COLOR.border,
        paddingTop: 5,
    },
    textTitle: {
        marginTop: '-3%',
        fontSize: FONT_SIZE.MEDIUM,
        marginBottom: '5%',
    },
});
