import React, { PureComponent } from 'react';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';
import { Config } from '../../common/config';
class TermScreen extends PureComponent {
    render() {
        let id = this.props.session.poskora.id;
        return <WebView source={{ uri: Config.orderUrl + 'term/?source=share&id=' + { id } }} />;
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        session: state.Auth.session,
    };
};

export default connect(mapStateToProps)(TermScreen);
