import React, { Component } from 'react';
import { ScrollView, StyleSheet, View, Image, Text, Dimensions } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, ListItem, Button } from 'react-native-elements';

import { COLOR, FONT_SIZE, ROBOTO, NUNITO, MARKOT } from '../../common/styles';
import PkaDialog from '../../components/dialog';

import { $logout, $resetStore, $notificaiondevice } from '../auth/rx/action';
import { $updateAvatar, $verifyPaylater } from './rx/action';
import ProfileTop from './com/profile.top';

import iconProfile from '@assets/icons/icon_profile.png';
import iconLocation from '@assets/icons/icon_location.png';
import iconFinancial from '@assets/icons/icon_f.png';
import iconHelp from '@assets/icons/icon_help.png';
import iconTerm from '@assets/icons/icon_term.png';
import iconPassword from '@assets/icons/icon_lock.png';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 40;

const ProfileMenu = [
    {
        icon: iconProfile,
        title: 'Profile',
        link: 'ProfileUpdate',
    },
    {
        icon: iconPassword,
        title: 'Ubah Kata Sandi',
        link: 'ProfileResetPassword',
    },
    {
        icon: iconLocation,
        title: 'Lokasi',
        link: 'ProfileLocation',
    },
    {
        icon: iconHelp,
        title: 'Bantuan',
        link: 'ProfileHelp',
    },
    {
        icon: iconTerm,
        title: 'Syarat & Ketentuan',
        link: 'ProfileTerm',
    },
];

const ProfileMenuPaylater = [
    {
        icon: iconProfile,
        title: 'Profile',
        link: 'ProfileUpdate',
    },
    {
        icon: iconPassword,
        title: 'Ubah Kata Sandi',
        link: 'ProfileResetPassword',
    },
    {
        icon: iconLocation,
        title: 'Lokasi',
        link: 'ProfileLocation',
    },
    {
        icon: iconFinancial,
        title: 'Pembiayaan',
        link: 'PaylaterLoading',
    },
    {
        icon: iconHelp,
        title: 'Bantuan',
        link: 'ProfileHelp',
    },
    {
        icon: iconTerm,
        title: 'Syarat & Ketentuan',
        link: 'ProfileTerm',
    },
];

class ProfileScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDialog: false,
        };
    }

    componentDidMount() {
        this.props.$verifyPaylater();
    }

    _navigate(route) {
        this.props.navigation.navigate(route);
    }

    _renderIcon(menu) {
        return <Image source={menu.icon} style={styles.iconImage} resizeMode="contain" />;
    }

    logout() {
        this.setState({
            showDialog: true,
        });
    }
    logout = () => {
        this.setState({
            showDialog: true,
        });
    };
    onNotificationDevice = () => {
        const token = this.props.token;
        let body = {
            user_id: 0,
            device_id: token,
            is_login: false,
        };
        this.props.$notificaiondevice(body);
    };
    onLogout = () => {
        this.props.$logout();
        this.props.$resetStore();
        this.onNotificationDevice();
    };

    onLogoutCancelled = () => {
        this.setState({
            showDialog: false,
        });
    };

    _renderVerify() {
        if (this.props.isConnected) {
            return (
                <Card containerStyle={styles.card}>
                    {ProfileMenuPaylater.map((menu, i) => (
                        <ListItem
                            containerStyle={styles.divider}
                            activeOpacity={0.97}
                            key={i}
                            onPress={() => this._navigate(menu.link)}
                            leftIcon={this._renderIcon(menu)}
                            title={menu.title}
                            titleStyle={styles.title}
                            chevron
                        />
                    ))}
                </Card>
            );
        }
        return (
            <Card containerStyle={styles.card}>
                {ProfileMenu.map((menu, i) => (
                    <ListItem
                        containerStyle={styles.divider}
                        activeOpacity={0.97}
                        key={i}
                        onPress={() => this._navigate(menu.link)}
                        leftIcon={this._renderIcon(menu)}
                        title={menu.title}
                        titleStyle={styles.title}
                        chevron
                    />
                ))}
            </Card>
        );
    }

    render() {
        let isConnected = this.props.isConnected;
        return (
            <View style={styles.container}>
                <View style={styles.back}>
                    <View style={styles.firstColumn}>
                        <Text style={styles.titleProfile}>Akun Saya</Text>
                    </View>
                    <View style={isConnected === true ? styles.secondColumnPaylater : styles.secondColumn} />
                </View>

                <ScrollView>
                    <ProfileTop
                        navigation={this.props.navigation}
                        avatarAction={this.props.$updateAvatar}
                        session={this.props.session}
                    />
                    {this._renderVerify()}
                    <Button
                        title="Keluar Aplikasi"
                        buttonStyle={styles.quitButton}
                        titleStyle={styles.quitButtonText}
                        onPress={() => this.logout()}
                    />
                </ScrollView>

                <PkaDialog
                    onCancel={this.onLogoutCancelled}
                    onSubmit={this.onLogout}
                    title="Keluar Aplikasi"
                    text={'Anda yakin akan keluar dari Aplikasi?'}
                    visible={this.state.showDialog}
                />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        session: state.Auth.session,
        isConnected: state.Profile.isConnected,
        token: state.Notification.token,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $logout,
            $resetStore,
            $updateAvatar,
            $verifyPaylater,
            $notificaiondevice,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.light,
    },
    back: {
        flexDirection: 'column',
        marginTop: -5,
        width: '100%',
        height: 550,
        position: 'absolute',
        elevation: 0,
        zIndex: -999,
    },
    firstColumn: {
        height: 150,
        backgroundColor: COLOR.primary,
    },
    secondColumn: {
        height: 360,
        backgroundColor: COLOR.shadow,
    },
    secondColumnPaylater: {
        height: 410,
        backgroundColor: COLOR.shadow,
    },
    card: {
        marginTop: 0.3,
        marginLeft: 24,
        marginRight: 24,
        elevation: 0,
        backgroundColor: COLOR.white,
        borderWidth: 0.2,
        borderColor: COLOR.border,
        borderBottomWidth: 0.3,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    divider: {
        borderBottomColor: COLOR.light,
        borderBottomWidth: 1,
        paddingVertical: 13,
    },
    title: {
        textAlign: 'left',
        fontFamily: ROBOTO.bold,
        fontSize: FONT_SIZE.LARGE,
        color: COLOR.textGray,
    },
    quitButton: {
        width: DEVICE_WIDTH - MARGIN,
        height: MARGIN + 2,
        alignSelf: 'center',
        marginTop: 35,
        marginBottom: 32,
        backgroundColor: 'transparent',
        borderRadius: 10,
        borderWidth: 2,
        borderColor: COLOR.lightRed,
    },
    quitButtonText: {
        fontFamily: NUNITO.extraBold,
        color: COLOR.lightRed,
        fontSize: FONT_SIZE.MEDIUM,
    },
    iconImage: {
        width: 24,
        height: 24,
    },
    titleProfile: {
        marginTop: 40,
        marginBottom: 40,
        marginLeft: 23,
        marginRight: 123,
        fontFamily: MARKOT.bold,
        fontSize: FONT_SIZE.EXTRA_LARGE,
        color: COLOR.white,
    },
});
