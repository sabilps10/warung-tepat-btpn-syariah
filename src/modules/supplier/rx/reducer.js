import { TYPE } from './action';

const INITIAL_STATE = {
    loadmore: false,
    suppliers: {},
    supplier: {},
    search_results: {
        data: [],
        total: 0,
        page: 1,
        query: '',
    },
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_CATALOG:
            state.suppliers[action.supplier_id] = {
                data: action.payload,
                total: action.total,
                page: 1,
            };
            return {
                ...state,
                supplier_id: action.supplier_id,
            };
        case TYPE.START_LOADMORE:
            return {
                ...state,
                loadmore: true,
            };
        case TYPE.FINISH_LOADMORE:
            state.suppliers[action.supplier_id] = {
                data:
                    action.payload !== null
                        ? state.suppliers[action.supplier_id].data.concat(action.payload)
                        : state.suppliers[action.supplier_id].data,
                total: action.total,
                page: action.page !== -1 ? action.page : state.page,
            };
            return {
                ...state,
                loadmore: false,
            };
        case TYPE.SHOW_CATALOG:
            return {
                ...state,
                catalog: action.payload,
            };
        case TYPE.RESET_CATALOG:
            return {
                ...INITIAL_STATE,
                suppliers: {},
            };
        default:
            return state;
    }
}

export function persister({ suppliers,supplier_id }) {
    return {
        suppliers,
        supplier_id
    };
}
