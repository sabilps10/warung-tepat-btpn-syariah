import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import Countly from 'countly-sdk-react-native-bridge';

import { $search } from './rx/action';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import { NavigationActions } from 'react-navigation';
import { HeaderBackButton } from 'react-navigation-stack';
import { Header, ListItem } from 'react-native-elements';
import { CurrencyFormatter } from '../../common/utils';
import SearchBar from '../../components/search_bar';

class SearchScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            query: '',
            results: [],
            isEmpty: false,
        };
    }

    load(query) {
        const { session } = this.props;
        const { user } = session;
        const { supplier_id } = this.props;
        const suppliers_id = supplier_id.toString();
        if (query !== '') {
            const eventSearch = { eventName: 'Search', eventCount: 1 };
            eventSearch.segments = {
                Username: user.username,
                Name: user.name,
                Keyword: query,
                Supplier_ID: suppliers_id,
            };
            Countly.sendEvent(eventSearch);
            this.props.$search(query, suppliers_id).then((res) => {
                if (res) {
                    this.setState({
                        results: res,
                        isEmpty: false,
                    });
                } else {
                    this.setState({
                        isEmpty: true,
                    });
                }
            });
        } else {
            this.setState({
                results: [],
                isEmpty: false,
            });
        }
    }

    onSearchKeyword(query) {
        if (query !== this.state.query) {
            this.setState({ query });
            this.load(query);
        } else {
            this.setState({
                results: [],
                query: '',
                isEmpty: false,
            });
        }
    }

    clearState = () => {
        this.setState({
            query: '',
            results: [],
            isEmpty: false,
        });
    };

    _renderHeader() {
        const containerStyle = { marginLeft: -15 };
        const inputStyle = {
            lineHeight: 20,
            fontSize: 12,
            padding: 0,
        };
        let { query } = this.state;

        return (
            <Header
                containerStyle={styles.headerContainer}
                centerContainerStyle={containerStyle}
                placement="left"
                leftComponent={
                    <HeaderBackButton
                        tintColor={COLOR.secondary}
                        onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                    />
                }
                centerComponent={
                    <SearchBar
                        lightTheme
                        placeholder="Cari ditoko ini ..."
                        onChange={(value) => this.setState({ query: value })}
                        onSubmitEditing={() => this.load(query)}
                        onClear={this.clearState}
                        value={query}
                        inputStyle={inputStyle}
                        autoFocus
                    />
                }
            />
        );
    }

    _renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} colors={[COLOR.primary]} />;
    }

    _renderResult() {
        let { results } = this.state;
        if (results.length === 0) {
            return null;
        }

        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.titleSection}>HASIL PENCARIAN</Text>
                <View style={styles.historyProductContainer}>
                    {results.map((catalog, i) => {
                        return (
                            <ListItem
                                key={i}
                                leftAvatar={{ source: { uri: catalog.image_default } }}
                                title={catalog.name}
                                subtitle={
                                    <Text style={styles.historyProductSubTitle}>
                                        {catalog.size ? (catalog.size !== '' ? `Isi: ${catalog.size}` : null) : null}

                                        {catalog.item
                                            ? catalog.item.unit !== ''
                                                ? ' / ' + catalog.item.unit
                                                : null
                                            : null}

                                        {catalog.max_monthly !== 0 ? (
                                            <Text>&nbsp;(Max: {catalog.max_monthly})</Text>
                                        ) : null}
                                    </Text>
                                }
                                rightTitle={`${CurrencyFormatter(catalog.selling_price)}`}
                                rightSubtitle={
                                    catalog.discount !== 0 ? ` ${CurrencyFormatter(catalog.regular_price)}` : null
                                }
                                containerStyle={styles.productList}
                                titleStyle={styles.historyProductTitle}
                                rightTitleStyle={styles.historyProductRightTitle}
                                rightSubtitleStyle={styles.historyProductRightSubTitle}
                                onPress={() => this.navigateCatalog(catalog)}
                                activeOpacity={0.97}
                            />
                        );
                    })}
                </View>
            </View>
        );
    }

    _renderEmpty() {
        let { isEmpty, query } = this.state;

        if (!isEmpty) {
            return null;
        }

        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.titleSection}>HASIL PENCARIAN</Text>
                <View style={styles.historyProductContainer}>
                    <Text style={styles.searchEmpty}>
                        Tidak dapat menemukan produk dengan kata kunci{' '}
                        <Text style={styles.searchEmptyKeyword}>"{query}"</Text>
                    </Text>
                </View>
            </View>
        );
    }

    navigateCatalog(catalog) {
        const event = { eventName: 'Products', eventCount: 1 };

        event.segments = {
            Name: catalog.name,
        };

        Countly.sendEvent(event);
        setTimeout(() => {
            this.props.navigation.push('CatalogDetail', {
                catalog_id: catalog.id,
                product: catalog,
            });
        }, 0);
    }

    render() {
        return (
            <View style={styles.container}>
                {this._renderHeader()}
                <ScrollView refreshControl={this._renderRefresh()}>
                    {this._renderEmpty()}
                    {this._renderResult()}
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        recomens: state.Home.recomens,
        history_catalogs: state.Auth.history.catalogs,
        history_keywords: state.Auth.history.search_keywords,
        session: state.Auth.session,
        supplier_id: state.Supplier.supplier_id,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $search }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f4f4',
    },
    headerContainer: {
        backgroundColor: '#fff',
        height: 55,
        paddingTop: 10,
        paddingBottom: 10,
        marginLeft: 0,
        paddingLeft: 0,
        paddingRight: 10,
        zIndex: 100,
        borderBottomWidth: 0,
        elevation: 1,
    },
    sectionContainer: {
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
    titleSection: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
    },
    historyKeywordContainer: {
        flex: 2,
        alignItems: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    keywordBadge: {
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: COLOR.border,
        padding: 5,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        position: 'relative',
        marginRight: PADDING.PADDING_CARD,
        width: 'auto',
        borderRadius: 20,
        marginTop: 7,
    },
    keywordBadgeActive: {
        borderColor: COLOR.success,
    },
    keywordBadgeText: {
        color: '#555',
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
    },
    historyProductContainer: {
        paddingTop: 5,
    },
    historyProductTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    historyProductRightTitle: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.bold,
    },
    historyProductSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
    historyProductRightSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
        textDecorationLine: 'line-through',
    },
    productList: {
        paddingVertical: 5,
        borderBottomWidth: 1,
        borderColor: '#f4f4f4',
    },
    searchEmpty: {},
    searchEmptyKeyword: {
        color: COLOR.danger,
    },
});
