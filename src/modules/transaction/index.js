import React, { PureComponent } from 'react';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { View, StyleSheet } from 'react-native';

import { COLOR, FONT_SIZE, SSP } from '../../common/styles';
import Headers from '../../components/header';
import TabView from './tab';

export default class Transaction extends PureComponent {
    render() {
        let Tabs = createAppContainer(
            createMaterialTopTabNavigator(
                {
                    Bill: {
                        screen: () => <TabView status="pending" title="Tagihan" navigation={this.props.navigation} />,
                        navigationOptions: {
                            title: 'Tagihan',
                        },
                    },
                    Delivery: {
                        screen: () => (
                            <TabView status="proceed" title="Pengiriman" navigation={this.props.navigation} />
                        ),
                        navigationOptions: {
                            title: 'Pengiriman',
                        },
                    },
                    History: {
                        screen: () => (
                            <TabView status="finished" title="Riwayat Transaksi" navigation={this.props.navigation} />
                        ),
                        navigationOptions: {
                            title: 'Riwayat Transaksi',
                        },
                    },
                },
                {
                    tabBarPosition: 'top',
                    initialRouteName: 'Bill',
                    animationEnabled: true,
                    lazy: true,
                    optimizationsEnabled: true,
                    backBehavior: 'none',
                    tabBarOptions: {
                        activeTintColor: COLOR.primaryDarkest,
                        inactiveTintColor: COLOR.primaryDarkest,
                        allowFontScaling: false,
                        style: {
                            backgroundColor: COLOR.primary,
                            elevation: 0.5,
                            height: 40,
                        },
                        tabStyle: {
                            paddingVertical: 12,
                        },
                        indicatorStyle: {
                            backgroundColor: COLOR.primaryDarkest,
                            height: 3,
                        },
                        labelStyle: {
                            fontFamily: SSP.semi_bold,
                            fontSize: FONT_SIZE.SMALL,
                            padding: 0,
                            margin: 0,
                        },
                    },
                },
            ),
        );
        return (
            <View style={styles.container}>
                <Headers title="TRANSAKSI PEMBELIAN" navigation={this.props.navigation} />
                <Tabs />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.light,
    },
});
