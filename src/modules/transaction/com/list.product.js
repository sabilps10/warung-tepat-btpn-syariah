import React, { Component } from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import { ListItem } from 'react-native-elements';
import { CurrencyFormatter } from '../../../common/utils'
import { COLOR, FONT_SIZE, SSP, PADDING } from '../../../common/styles'
import PropTypes from 'prop-types';

class ListProduct extends Component {
    render() {
        return (
            <ListItem
                leftAvatar={
                    <Image source={{ uri : this.props.data.catalog.image_default}} style={{ height: 100, width: 100, resizeMode: 'contain' }} />
                }
                title={
                    <Text style={{...styles.semiBold, fontSize: FONT_SIZE.LARGE }} >
                        {this.props.data.catalog.name}
                    </Text>
                }
                subtitle={
                    <View style={{ flexDirection: 'column', flexWrap: 'wrap' }} >
                        {
                            this.props.data.catalog.item.weight ?
                                <Text style={{...styles.regular, fontSize: FONT_SIZE.MEDIUM}} >
                                    {this.props.data.catalog.item.weight}
                                </Text>
                            : null
                        }
                        <Text style={{...styles.semiBold, fontSize: FONT_SIZE.LARGE, color: COLOR.red }} >
                            {`${CurrencyFormatter(this.props.data.catalog.selling_price)}`}
                        </Text>
                    </View>
                }

                rightTitle={
                    <View style={styles.input}>
                        <Text style={styles.textInput}>
                            {this.props.data.quantity}
                        </Text>
                    </View>
                }
                containerStyle={{ padding: 0, margin: 0, paddingVertical: PADDING.PADDING_CARD, paddingHorizontal: PADDING.PADDING_CARD }}
                bottomDivider={true}
            />
        )
    }
}

ListProduct.propTypes = {
    data: PropTypes.object.isRequired
}

export default ListProduct

const styles = StyleSheet.create({
    input: {
        width: 70,
        backgroundColor: COLOR.border,
        height: 30,
        alignContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        color: COLOR.black,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 'auto',
        marginBottom: 'auto',
    },
    regular: {
        fontFamily: SSP.regular,
        color: COLOR.darkGray
    },
    semiBold: {
        fontFamily: SSP.semi_bold,
        color: COLOR.black

    }
})
