import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActivityIndicator, RefreshControl, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Icon } from 'react-native-elements';
import { FlatList, NavigationEvents } from 'react-navigation';
import { View } from 'react-native-animatable';

import ListCom from './com/list.com';
import { $load, $loadmore } from './rx/action';
import { COLOR, FONT_SIZE, SSP } from '../../common/styles';
import EmptyState from '../../components/empty_state';

class TabView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showTop: false,
            data: [],
            page: 1,
            total: 0,
            is_empty: false,
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            nextState.data === this.state.data &&
            nextState.showTop === this.state.showTop &&
            nextState.is_empty === this.state.is_empty
        ) {
            return false;
        }

        return true;
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.navigation.getParam('reload') === true) {
            this._onLoad(false);
        }
    }

    _onLoad(init = true) {
        if (init && this.state.data.length > 0) {
            return;
        }

        let { status, transactions } = this.props;

        this.props.$load(status).then((res) => {
            if (typeof transactions[status].data !== 'undefined') {
                this.setState(transactions[status]);
            } else {
                this.setState({ is_empty: true });
            }
        });
    }

    onLoadMore = () => {
        let { status, transactions, loadmore } = this.props;
        let { total, data, page } = this.state;

        if (!loadmore && total > data.length) {
            this.props.$loadmore(status, page + 1).then((res) => {
                if (typeof transactions[status] !== 'undefined') {
                    this.setState(transactions[status]);
                }
            });
        }
    };

    onScroll = (event) => {
        if (event.nativeEvent.contentOffset.y > 500) {
            this.setState({ showTop: true });
        } else {
            this.setState({ showTop: false });
        }
    };

    onGoTop = () => {
        this.listRef.scrollToOffset({
            offset: 0,
            animated: true,
        });
    };

    _renderRefresh() {
        return (
            <RefreshControl
                refreshing={this.props.loading}
                onRefresh={() => this._onLoad(false)}
                colors={[COLOR.primary]}
            />
        );
    }

    _renderFooter() {
        if (this.state.total <= this.state.data.length) {
            return null;
        }

        return <ActivityIndicator color={COLOR.secondary} style={styles.activityContainer} animating size="large" />;
    }

    _renderToTop() {
        if (!this.state.showTop) {
            return null;
        }

        return (
            <TouchableOpacity onPress={this.onGoTop} style={styles.fab}>
                <Icon name="chevron-up" type="feather" size={21} />
            </TouchableOpacity>
        );
    }

    _navigationFocus = (payload) => this._onLoad();

    _flatKey = (item, index) => 'id_' + index;

    _flatReff = (ref) => (this.listRef = ref);

    _flatItem = ({ index, item }) => <ListCom data={item} navigation={this.props.navigation} />;

    getSubtitle() {
        let { title } = this.props;

        if (title === 'Tagihan') {
            return 'Data transaksi yang belum dibayar akan tampil disini';
        } else if (title === 'Pengiriman') {
            return 'Transaksi yang sudah dibayar dan dalam proses pengiriman';
        } else {
            return 'Transaksi yang telah selesai akan tampil disini';
        }
    }

    render() {
        let { data, is_empty } = this.state;

        if (is_empty) {
            return <EmptyState title={'Belum Ada ' + this.props.title} subtitle={this.getSubtitle()} />;
        }

        return (
            <View>
                <NavigationEvents onWillFocus={this._navigationFocus} />
                <View animation="fadeIn" useNativeDriver>
                    <FlatList
                        scrollEventThrottle={16}
                        onScroll={this.onScroll}
                        refreshControl={this._renderRefresh()}
                        ListFooterComponent={this._renderFooter()}
                        data={data}
                        keyExtractor={this._flatKey}
                        renderItem={this._flatItem}
                        onEndReached={this.onLoadMore}
                        onEndReachedThreshold={0.1}
                        initialNumToRender={15}
                        showsVerticalScrollIndicator={true}
                        ref={this._flatReff}
                    />
                    {this._renderToTop()}
                </View>
            </View>
        );
    }
}

TabView.propTypes = {
    navigation: PropTypes.object.isRequired,
    status: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        loadmore: state.Transaction.loadmore,
        transactions: state.Transaction.transactions,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $load, $loadmore }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(TabView);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bodyContainer: {
        flex: 1,
        paddingBottom: 70,
        backgroundColor: COLOR.light,
    },
    fab: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 100,
        backgroundColor: '#fff',
        borderRadius: 30,
        elevation: 8,
        zIndex: 9999,
    },
    fabIcon: {
        fontSize: 30,
        color: 'black',
    },
    totalText: {
        textAlign: 'center',
        fontFamily: SSP.semi_bold,
        color: COLOR.darkGray,
        fontSize: FONT_SIZE.SMALL,
        margin: 10,
    },
    activityContainer: {
        paddingTop: 20,
        paddingBottom: 20,
    },
});
