import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class Header extends Component {
    render() {
        const { source, title, title2, subtitle, isLeft } = this.props;

        return (
            <View style={styles.container}>
                <Image
                    style={isLeft ? [styles.image, styles.imageLeft] : styles.image}
                    source={source}
                    resizeMode="contain"
                />
                <Text style={styles.title}>{title}</Text>
                {title2 ? <Text style={styles.title}>{title2}</Text> : null}
                <Text style={styles.subtitle}>{subtitle}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 24,
        marginTop: 16,
    },
    title: {
        fontFamily: 'MarkOT',
        fontSize: 24,
        lineHeight: 36,
    },
    subtitle: {
        fontFamily: 'MarkOT-Bold',
        fontSize: 32,
        lineHeight: 36,
    },
    image: {
        width: 122,
        height: 122,
        marginBottom: 24,
    },
    imageLeft: {
        marginLeft: 12,
    },
});
