import { Toast } from 'native-base';

export const LmdToast = {
    showToast: (text, type, duration = 2500, position = 'bottom', buttonText = 'Okay', buttonStyle, style) => {
        Toast.show({
            text,
            type,
            duration,
            position,
            buttonText,
            buttonStyle,
            style,
        });
    },
    hideToast: () => {
        Toast.hide();
    },
};
