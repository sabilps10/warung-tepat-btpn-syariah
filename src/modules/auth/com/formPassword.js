import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, Text } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import Input from './input';
import ButtonSubmit from './button';
import { $login, $otp } from '../rx/action';

class FormPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
        };
    }

    onSubmit = () => {
        const { password } = this.state;
        let body = {
            username: this.props.username,
            password,
            android_id: this.props.token,
        };

        this.props.$login(body);
    };

    onForgot = () => {
        const data = {
            username: this.props.username,
            is_check: true,
            is_forgot_password: true,
            existing: true,
        };

        this.props.$otp(data);
    };

    render() {
        return (
            <View animation="bounceIn" delay={20} style={styles.container} useNativeDriver>
                <Text style={styles.text}>Silahkan masukkan password Anda</Text>
                <Input
                    secureTextEntry={true}
                    placeholder="Password"
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    showEye={true}
                    onChangeText={(password) => this.setState({ password })}
                    errorText={this.props.isLoading ? '' : this.props.errors.password}
                />
                <ButtonSubmit title="Submit" loading={this.props.isLoading} onPress={this.onSubmit} />
                <View style={styles.urlContainer}>
                    <Text style={styles.url} onPress={this.onForgot}>
                        Lupa Password
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 24,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        marginLeft: 24,
        marginBottom: 8,
    },
    urlContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: 'NunitoSans-ExtraBold',
        fontSize: 14,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        username: state.Auth.username,
        errors: state.Auth.errors,
        isLoading: state.Activity.loading,
        token: state.Notification.token,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $login, $otp }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(FormPassword);
