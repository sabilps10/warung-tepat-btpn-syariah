import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, Text } from 'react-native-animatable';

import Input from './input';
import ButtonSubmit from './button';
import { $saveUser, $otp } from '../rx/action';

class FormLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            error: null,
        };
    }

    onChangeText = (username) => {
        this.setState({ username, error: null });
    };

    onSubmit = () => {
        const data = {
            username: this.state.username,
            is_check: true,
            is_forgot_password: true,
            existing: true,
        };

        if (!this.state.username) {
            this.setState({ error: 'Nomor handphone tidak boleh kosong' });
        } else {
            this.props.$saveUser({
                username: this.state.username,
            });
            this.props.$otp(data);
        }
    };

    render() {
        return (
            <View animation="bounceIn" delay={30} style={styles.container} useNativeDriver>
                <Text style={styles.text}>Masukkan nomor handphone anda</Text>
                <Input
                    placeholder="081234567890"
                    autoCapitalize={'none'}
                    returnKeyType={'next'}
                    autoCorrect={false}
                    onChangeText={this.onChangeText}
                    errorText={this.state.error}
                    keyboardType="numeric"
                />
                <ButtonSubmit title="Lanjut" loading={this.props.isLoading} onPress={this.onSubmit} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        marginLeft: 24,
        marginBottom: 8,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: state.Activity.loading,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $saveUser, $otp }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);
