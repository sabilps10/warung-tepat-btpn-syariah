import { NavigationActions } from 'react-navigation';

import * as Activity from '@src/services/activity/action';
import { LmdToast } from '../com/lmdToast';
import { AuthService } from './service';

export const TYPE = {
    LOGIN_SUCCESS: 'auth::login.success',
    LOGIN_EFORM: 'auth::login.eform',
    LOGIN_FAILED: 'auth::login.failed',
    LOGOUT: 'auth::logout',
    SAVE_USER: 'auth::save.user',
    BEGIN_CHECK: 'auth::check',
    CHECK_SUCCESS: 'auth::check.success',
    CHECK_FAILED: 'auth::check.failed',
    OTP_SUCCESS: 'auth::otp.success',
    OTP_FAILED: 'auth::otp.failed',
    VERIFY_SUCCESS: 'auth::verify.success',
    VERIFY_FAILED: 'auth::verify.failed',
    PASSWORD_SUCCESS: 'auth::password.success',
    PASSWORD_FAILED: 'auth::password.failed',
    RESET: 'reset',
    NOTIFICATION_DEVICE: 'auth:notification.device',
};

const notUser = 'data tidak valid, periksa username Anda kembali';
const invalidOTP = 'Invalid OTP Code';
const blockedText = 'User anda terblokir. Silahkan hubungi contact center Warung Tepat BTPN Syariah.';
const blockedVerify =
    'Batas percobaan OTP Sudah berakhir, Akun anda terblokir. Silahkan hubungi contact center BTPN Syariah untuk kembali mengaktifkan akun anda.';

// action login
export function $login(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Auth', 'Login');
        AuthService.login(data)
            .then((res) => {
                const { type } = res.data.poskora;

                if (type === 'sr') {
                    dispatch({
                        type: TYPE.LOGIN_EFORM,
                        payload: res.data,
                    });
                } else {
                    AuthService.saveSession(res.data);
                    dispatch({
                        type: TYPE.LOGIN_SUCCESS,
                        payload: res.data,
                    });
                }
            })
            .catch((error) => {
                const { username } = error.errors;
                if (username) {
                    dispatch({
                        type: TYPE.LOGIN_FAILED,
                        payload: {
                            password: 'Password yang anda masukkan salah',
                        },
                    });
                } else {
                    dispatch({
                        type: TYPE.LOGIN_FAILED,
                        payload: error.errors,
                    });
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'Auth', 'Login');
            });
    };
}

// action logout
export function $logout() {
    return (dispatch) => {
        Activity.processing(dispatch, 'Auth', 'Logout');
        AuthService.logout().finally(async (res) => {
            dispatch({ type: TYPE.LOGOUT });
            Activity.done(dispatch, 'Auth', 'Logout');
        });
    };
}

// action notification device
export function $notificaiondevice(body) {
    return (dispatch) => {
        AuthService.registerDevicePost(body).finally(() => {
            dispatch({ type: TYPE.NOTIFICATION_DEVICE });
        });
    };
}

// action check
export function $check(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Auth', 'Check');
        dispatch({
            type: TYPE.BEGIN_CHECK,
            payload: data,
        });
        AuthService.check(data)
            .then((res) => {
                const { step } = res.data;

                if (step === 1) {
                    dispatch({
                        type: TYPE.CHECK_SUCCESS,
                        payload: true,
                    });
                    dispatch(NavigationActions.navigate({ routeName: 'VerifyOtp' }));
                } else if (step === 2) {
                    dispatch({
                        type: TYPE.CHECK_SUCCESS,
                        payload: false,
                    });
                    dispatch(NavigationActions.navigate({ routeName: 'Password' }));
                }
            })
            .catch((error) => {
                const { errors } = error;

                dispatch({
                    type: TYPE.CHECK_FAILED,
                    payload: errors,
                });

                if (errors.otp_code) {
                    dispatch(NavigationActions.navigate({ routeName: 'LimitUser' }));
                } else if (errors.username === blockedText) {
                    dispatch(NavigationActions.navigate({ routeName: 'BlockedUser' }));
                } else if (errors.username === notUser) {
                    dispatch(NavigationActions.navigate({ routeName: 'NotUser' }));
                } else {
                    LmdToast.showToast('Uncaught error, please try again', 'danger');
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'Auth', 'Check');
            });
    };
}

// action send otp
export function $otp(data) {
    return (dispatch) => {
        const { existing } = data;
        Activity.processing(dispatch, 'Auth', 'Otp');
        AuthService.otp(data)
            .then((res) => {
                if (existing) {
                    dispatch(NavigationActions.navigate({ routeName: 'VerifyOtp' }));
                    LmdToast.showToast('Kode OTP berhasil dikirim', 'success');
                    dispatch({ type: TYPE.OTP_SUCCESS });
                } else {
                    LmdToast.showToast('Kode OTP berhasil dikirim', 'success');
                    dispatch({ type: TYPE.OTP_SUCCESS });
                }
            })
            .catch((error) => {
                const { errors } = error;
                dispatch({
                    type: TYPE.OTP_FAILED,
                    payload: errors,
                });

                if (errors.otp_code) {
                    dispatch(NavigationActions.navigate({ routeName: 'LimitUser' }));
                } else if (errors.username === blockedText) {
                    dispatch(NavigationActions.navigate({ routeName: 'BlockedUser' }));
                } else if (errors.username === notUser) {
                    dispatch(NavigationActions.navigate({ routeName: 'NotUser' }));
                } else {
                    LmdToast.showToast('Uncaught error, please try again', 'danger');
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'Auth', 'Otp');
            });
    };
}

// action verify OTP
export function $verifyOTP(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Auth', 'verifyOTP');
        AuthService.verifyOTP(data)
            .then((res) => {
                const { sign_password } = res.data;

                dispatch({
                    type: TYPE.VERIFY_SUCCESS,
                    payload: sign_password,
                });
                dispatch(NavigationActions.navigate({ routeName: 'NewPassword' }));
            })
            .catch((error) => {
                const { errors } = error;
                dispatch({
                    type: TYPE.VERIFY_FAILED,
                    payload: errors,
                });

                if (errors.otp_code === invalidOTP) {
                    LmdToast.showToast('Kode OTP tidak sesuai!', 'danger');
                } else if (errors.next_retry) {
                    dispatch(NavigationActions.navigate({ routeName: 'LimitOtp' }));
                } else if (errors.username === blockedText || errors.otp_code === blockedVerify) {
                    dispatch(NavigationActions.navigate({ routeName: 'BlockedUser' }));
                } else {
                    LmdToast.showToast('Uncaught error, please try again', 'danger');
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'Auth', 'verifyOTP');
            });
    };
}

// action new password
export function $password(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Auth', 'Password');
        AuthService.password(data)
            .then((res) => {
                dispatch({ type: TYPE.PASSWORD_SUCCESS });
                dispatch(NavigationActions.navigate({ routeName: 'Success' }));
            })
            .catch((error) => {
                const { errors } = error;
                dispatch({
                    type: TYPE.PASSWORD_FAILED,
                    payload: errors,
                });
                LmdToast.showToast('Uncaught error, please try again', 'danger');
            })
            .finally((res) => {
                Activity.done(dispatch, 'Auth', 'Password');
            });
    };
}

// action save userData
export function $saveUser(data) {
    return (dispatch) => {
        dispatch({
            type: TYPE.SAVE_USER,
            payload: data,
        });
    };
}

// action reset error
export function $resetError() {
    return (dispatch) => {
        dispatch({ type: TYPE.RESET });
    };
}

// action reset store
export function $resetStore() {
    return (dispatch) => {
        Activity.processing(dispatch, 'Auth', 'Reset');
        dispatch({ type: 'home::reset' });
        dispatch({ type: 'catalog::reset' });
        dispatch({ type: 'cart::reset' });
        Activity.done(dispatch, 'Auth', 'Reset');
    };
}
