import React, { Component } from 'react';
import { StyleSheet, BackHandler, Keyboard } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import source from '@assets/img/warung_tepat.png';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Toast } from '../../common/utils';
import Header from './com/header';
import FormLogin from './com/formLogin';
import Logo from './com/logo';
import { $notificaiondevice } from './rx/action';

class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            exit: false,
            keyboard: false,
        };
    }

    onNotificationDevice = () => {
        const token = this.props.token;
        let body = {
            user_id: 0,
            device_id: token,
            is_login: false,
        };
        this.props.$notificaiondevice(body);
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
        Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
        this.onNotificationDevice();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        Keyboard.removeListener('keyboardDidShow', this.keyboardDidShow);
        Keyboard.removeListener('keyboardDidHide', this.keyboardDidHide);
    }

    keyboardDidShow = () => {
        this.setState({ keyboard: true });
    };

    keyboardDidHide = () => {
        this.setState({ keyboard: false });
    };

    handleBackPress = () => {
        if (!this.props.navigation.isFocused('Login')) {
            this.props.navigation.dispatch(NavigationActions.back());
            this.setState({ exit: false });
            return true;
        }

        if (this.state.exit) {
            return false;
        }

        Toast('Tekan sekali lagi untuk keluar');

        this.setState({ exit: true });

        return true;
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title="Selamat Datang" subtitle="Di Warung Tepat" isLeft />
                <FormLogin navigation={this.props.navigation} />
                {this.state.keyboard ? null : <Logo text={'Versi 2.9.3'} />}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        token: state.Notification.token,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $notificaiondevice,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
