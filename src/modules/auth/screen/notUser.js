import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import source from '@assets/img/false.png';

import Header from '../com/header';
import ButtonBack from '../com/button';
import { contactCenter } from '../../../assets/Utils';

export default class NotUserScreen extends Component {
    constructor(props) {
        super(props);
    }

    goBack = () => {
        this.props.navigation.goBack();
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title="Nomor Handphone" title2="Anda Belum Terdaftar" />
                <View style={styles.textContainer}>
                    <Text style={styles.text}>
                        Nomor Anda belum terdaftar sebagai nasabah. Silahkan menghubungi pihak Bank BTPN Syariah dinomor{' '}
                        {contactCenter} atau kunjungi cabang terdekat.
                    </Text>
                </View>
                <ButtonBack title="Kembali" onPress={this.goBack} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    textContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 0,
        paddingTop: 0,
        marginLeft: 24,
        marginRight: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
        color: COLOR.textGray,
    },
});
