import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { View } from 'react-native-animatable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { COLOR } from '@common/styles';
import check from '@assets/img/check.png';
import lock from '@assets/img/lock.png';

import Header from '../com/header';
import ButtonVerif from '../com/button';
import OtpInputs from '../../../components/OtpInputs';
import { $otp, $verifyOTP, $resetError } from '../rx/action';

class verifyOtpScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            otp: '',
        };
    }

    resetError = () => {
        this.props.$resetError();
    };

    getOtp = (otp) => {
        this.setState({ otp });
    };

    requestOTP = () => {
        const data = { username: this.props.username, is_check: true };
        const newData = { is_forgot_password: true };

        if (this.props.newUser) {
            this.props.$otp(data);
        } else {
            this.props.$otp({ ...data, ...newData });
        }
    };

    verif = () => {
        const data = {
            otp_code: this.state.otp,
            phone_number: this.props.username,
            otp_type: 2,
        };

        if (this.props.newUser) {
            data.otp_type = 1;
        }

        this.props.$verifyOTP(data);
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                {this.props.newUser ? (
                    <Header source={check} title="Nomor Handphone" title2="Anda Terdaftar" />
                ) : (
                    <Header source={lock} title="Anda Lupa Password" />
                )}
                <View style={styles.textContainer}>
                    <Text style={styles.text}>Masukkan 6 nomor kode yang dikirimkan ke nomor Anda</Text>
                    <OtpInputs getOtp={this.getOtp} isError={this.props.errors.otp_code} resetError={this.resetError} />
                    <View style={styles.urlContainer}>
                        <Text style={styles.url} onPress={this.requestOTP}>
                            Kirim Ulang
                        </Text>
                    </View>
                    <ButtonVerif
                        title="Verifikasi"
                        loading={this.props.isLoading}
                        onPress={this.verif}
                        disabled={this.state.otp.length < 6 ? true : false}
                        flexGrow
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    textContainer: {
        flex: 3,
        justifyContent: 'flex-start',
        marginLeft: 24,
        marginRight: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
        color: COLOR.textGray,
        marginBottom: 16,
    },
    urlContainer: {
        flex: 1,
        alignItems: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: 'NunitoSans-ExtraBold',
        fontSize: 14,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        username: state.Auth.username,
        newUser: state.Auth.newUser,
        errors: state.Auth.errors,
        isLoading: state.Activity.loading,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $otp, $verifyOTP, $resetError }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(verifyOtpScreen);
