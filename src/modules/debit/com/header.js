import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class Header extends Component {
    render() {
        const { source, title } = this.props;

        return (
            <View style={styles.container}>
                <Image style={styles.image} source={source} />
                <Text style={styles.title}>{title}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 80,
        marginBottom: 24,
    },
    title: {
        fontFamily: 'Roboto-Bold',
        fontSize: 24,
        lineHeight: 36,
    },
    image: {
        width: 270,
        height: 254,
        marginBottom: 32,
    },
});
