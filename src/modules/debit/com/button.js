import React, { Component } from 'react';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { Dimensions, StyleSheet, View } from 'react-native';
import { COLOR } from '../../../common/styles';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 40;

class ButtonDebit extends Component {
    render() {
        return (
            <View style={this.props.flexGrow ? [styles.container, styles.grow] : styles.container}>
                <Button
                    buttonStyle={styles.button}
                    onPress={this.props.onPress}
                    title={this.props.title}
                    titleStyle={styles.text}
                    loading={this.props.isLoading}
                    disabled={this.props.disabled}
                />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: state.Activity.page,
    };
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLOR.primary,
        height: MARGIN,
        borderRadius: 8,
        marginBottom: 32,
        width: DEVICE_WIDTH - MARGIN,
        padding: 8,
    },
    text: {
        color: COLOR.white,
        fontFamily: 'NunitoSans-ExtraBold',
        fontSize: 16,
    },
    grow: {
        flex: 2,
    },
});

export default connect(mapStateToProps)(ButtonDebit);
