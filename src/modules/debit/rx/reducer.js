import { TYPE } from './action';

const INITIAL_STATE = {
    error: false,
    verified: false,
    isLoading: false,
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.BEGIN_VERIFY:
            return {
                ...state,
                isLoading: action.payload.isLoading,
            };
        case TYPE.VERIFY_SUCCESS:
            return {
                ...state,
                verified: action.payload,
                error: action.payload.error,
                isLoading: action.payload.isLoading,
            };
        case TYPE.VERIFY_FAILED:
            return {
                ...INITIAL_STATE,
                verified: action.payload,
                isLoading: action.payload.isLoading,
            };
        case TYPE.VERIFY_ERROR:
            return {
                ...INITIAL_STATE,
                verified: action.payload,
                error: action.payload.error,
                isLoading: action.payload.isLoading,
            };
        case TYPE.RESET:
            return {
                ...state,
                error: false,
            };
        default:
            return state;
    }
}

export function persister({ error, isLoading }) {
    return {
        error,
        isLoading,
    };
}
