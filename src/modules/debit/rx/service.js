import DebitRest from '@src/services/rest/rest.debit';

const rest = new DebitRest();

export const DebitServiceImplementation = class DebitService {
    verifyPin(data) {
        return rest.postVerifyPin(data);
    }
};

export const DebitService = new DebitServiceImplementation();
