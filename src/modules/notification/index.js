import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Text, TouchableOpacity, RefreshControl } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { $getAllMessage, $getCountMessage, $readMessage, $readAll } from './rx/action';
import NotificationHeader from './com/notification_header';
import NotificationItem from './com/notification_item';
import { COLOR, FLEX, ROBOTO, FONT_SIZE, SSP } from '../../common/styles';
import Countly from 'countly-sdk-react-native-bridge';

class NotificationIndex extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
        };
        this.doLoad = this.doLoad.bind(this);
        this.onChanges = this.onChanges.bind(this);
    }

    componentDidMount() {
        this.onRender();
    }

    onRender = () => {
        this.doLoad();
    };

    onChanges(messageId, createdDate, title) {
        const { session } = this.props;
        const { user } = session;
        const eventNotificationDashboard = { eventName: 'Message click by user' };
        eventNotificationDashboard.segments = {
            Phone: user.username,
            Message_Date: createdDate,
            Message_Title: title,
        };
        this.props.$readMessage(messageId).then((res) => {
            if (res) {
                this.doLoad();
            }
        });
        Countly.sendEvent(eventNotificationDashboard);
    }

    doLoad() {
        this.props.$getAllMessage().then((res) => {
            this.setState({
                data: res,
            });
        });
        this.props.$getCountMessage();
    }

    readAllMessage = () => {
        this.props.$readAll().then((res) => {
            if (res) {
                this.doLoad();
            }
        });
    };

    renderItem() {
        let { data } = this.state;

        if (data === false) {
            return <Text style={styles.emptyNotificationTitle}>Belum Ada Notifikasi</Text>;
        } else {
            const items = [];
            // eslint-disable-next-line no-unused-vars
            for (const [index, item] of this.state.data.entries()) {
                items.push(<NotificationItem key={index} data={item} onChange={this.onChanges} />);
            }
            return items;
        }
    }

    renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} onRefresh={this.onRender} colors={[COLOR.primary]} />;
    }

    render() {
        let { counterMessage } = this.props;

        return (
            <View style={styles.view}>
                <NotificationHeader navigation={this.props.navigation} />
                <View style={styles.readContainer}>
                    <View style={styles.notificationItemContainer}>
                        <Text style={styles.countText}>{counterMessage}</Text>
                        <Text style={styles.messageText}>Pesan Baru</Text>
                    </View>
                    <TouchableOpacity onPress={this.readAllMessage}>
                        <Text style={styles.readText}>Tanda Semua Dibaca</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView refreshControl={this.renderRefresh()}>{this.renderItem()}</ScrollView>
            </View>
        );
    }
}

NotificationIndex.propTypes = {
    onChange: PropTypes.func,
    onRender: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        counterMessage: state.Notification.countMessage,
        messages: state.Notification.messages,
        session: state.Auth.session,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $getAllMessage,
            $getCountMessage,
            $readMessage,
            $readAll,
        },
        dispatch,
    );
};

const styles = StyleSheet.create({
    view: {
        flex: FLEX.full,
    },
    container: {
        width: '100%',
        padding: 16,
        paddingTop: 100,
    },
    readContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: COLOR.white,
        padding: 16,
    },
    countText: {
        color: COLOR.danger,
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        marginRight: 5,
    },
    messageText: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    notificationItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    readText: {
        color: COLOR.darkGreen,
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    emptyNotificationTitle: {
        textAlign: 'center',
        fontFamily: ROBOTO.regular,
        fontSize: FONT_SIZE.LARGE,
        color: COLOR.textSecondary,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationIndex);
