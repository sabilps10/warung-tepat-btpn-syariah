import { NotificationService } from './service';
import * as Activity from '../../../services/activity/action';
import { NavigationActions } from 'react-navigation';
import { serverError } from '../../../assets/Utils';

export const TYPE = {
    SAVE_TOKEN: 'notification::token',
    REGIS_TOKEN: 'notification::register',
    GET_MESSAGE: 'notification::getMessage',
    COUNT_MESSAGE: 'notification::countMessage',
    READ_POST: 'notification::read',
    READ_ALL_POST: 'notification::readAll',
};

export function $saveToken(token) {
    return (dispatch) => {
        NotificationService.saveToken(token);

        dispatch({
            type: TYPE.SAVE_TOKEN,
            payload: token,
        });
    };
}

export function $getAllMessage() {
    return (dispatch) => {
        return NotificationService.getMessages()
            .then((res) => {
                dispatch({
                    type: TYPE.GET_MESSAGE,
                    payload: res.data.notif_datas,
                    total: res.data.total_unread,
                });
                return res.data.notif_datas;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                return res;
            });
    };
}

export function $getCountMessage() {
    return (dispatch) => {
        NotificationService.getCountMessage().then((res) => {
            dispatch({
                type: TYPE.COUNT_MESSAGE,
                payload: res.data.unread_message,
            });
        });
    };
}

export function $readMessage(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Notification', 'Load');

        return NotificationService.readPost(data)
            .then((res) => {
                dispatch({
                    type: TYPE.READ_POST,
                });
                return res.data;
            })
            .catch((error) => {
                dispatch(
                    NavigationActions.navigate({
                        routeName: 'OrderFailed',
                        params: {
                            data: serverError,
                        },
                    }),
                );
                return false;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Notification', 'Load');

                return res;
            });
    };
}

export function $readAll() {
    return (dispatch) => {
        Activity.processing(dispatch, 'Notification', 'Load');

        return NotificationService.readAllPost()
            .then((res) => {
                dispatch({
                    type: TYPE.READ_ALL_POST,
                });
                return res;
            })
            .catch((error) => {
                dispatch(
                    NavigationActions.navigate({
                        routeName: 'OrderFailed',
                        params: {
                            data: serverError,
                        },
                    }),
                );
                return false;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Notification', 'Load');

                return res;
            });
    };
}
