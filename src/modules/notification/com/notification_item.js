import React, { Component } from 'react';
import { connect } from 'react-redux';
import Moment from 'react-moment';
import { View, StyleSheet, Text, TouchableOpacity, RefreshControl } from 'react-native';
import { ListItem } from 'react-native-elements';
import { COLOR, FONT_SIZE, SSP, ROBOTO } from '../../../common/styles';

class NotificationItem extends Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.read === this.props.read) {
            return true;
        }

        return false;
    }

    readNotifications = (message_id, created_date, title) => {
        this.props.onChange(message_id, created_date, title);
    };

    renderRefresh() {
        return (
            <RefreshControl refreshing={this.props.loading} onRefresh={this.props.onRender} colors={[COLOR.primary]} />
        );
    }

    renderItem() {
        const data = this.props.data;

        return (
            <View style={styles.listContainer}>
                <ListItem
                    title={
                        <View>
                            <TouchableOpacity
                                onPress={() => this.readNotifications(data.message_id, data.created_date, data.title)}
                                activeOpacity={0.7}
                            >
                                <View style={styles.shopContainer}>
                                    <Moment style={styles.cartItemDate} element={Text} format="DD MMMM YYYY">
                                        {data.created_date}
                                    </Moment>
                                </View>
                                <View style={styles.notificationItemContainer}>
                                    <Text style={styles.codeItemTitle}>{data.title}</Text>
                                </View>
                                <Text style={styles.descriptionItemTitle}>{data.body}</Text>
                                <View style={styles.lastNotificationContainer} />
                            </TouchableOpacity>
                        </View>
                    }
                    titleStyle={styles.cartItemTitle}
                    rightTitleStyle={styles.dateItemTitle}
                    containerStyle={data.is_read === false ? styles.readContainer : styles.unReadContainer}
                />
            </View>
        );
    }

    render() {
        return <View>{this.renderItem()}</View>;
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        read: state.Notification.read,
        messages: state.Notification.messages,
    };
};

export default connect(mapStateToProps, null)(NotificationItem);

const styles = StyleSheet.create({
    shopContainer: {
        flexDirection: 'row',
    },
    notificationItemContainer: {
        flexDirection: 'row',
    },
    cartItemTitle: {
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        color: COLOR.primary,
        fontFamily: ROBOTO.regular,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        marginRight: 16,
    },
    cartItemDate: {
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        color: COLOR.off,
        fontFamily: ROBOTO.regular,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        marginRight: 16,
        paddingTop: 16,
        marginLeft: 16,
    },
    codeItemTitle: {
        fontFamily: ROBOTO.regular,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        paddingTop: 8,
        marginLeft: 16,
    },
    statusItemTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    descriptionItemTitle: {
        alignContent: 'flex-end',
        alignItems: 'flex-end',
        color: COLOR.off,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        paddingTop: 8,
        paddingBottom: 16,
        marginLeft: 16,
    },
    dateItemTitle: {
        color: COLOR.darkGray,
        fontFamily: ROBOTO.regular,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    cartItemRightTitle: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    cartItemSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    readContainer: {
        backgroundColor: COLOR.primaryTransparent,
        padding: 1,
    },
    lastNotificationContainer: {
        borderBottomColor: COLOR.border,
        borderBottomWidth: 0.7,
    },
    unReadContainer: {
        backgroundColor: COLOR.white,
        padding: 1,
    },
});
