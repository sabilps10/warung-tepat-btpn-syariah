import GroupBuyingRest from '../../../services/rest/rest.group.buying';

export const service = class GroupBuyingService {
    constructor() {
        this.rest = new GroupBuyingRest();
    }

    get() {
        return this.rest.get();
    }

    approve(id) {
        return this.rest.approve(id);
    }

    delete(id) {
        return this.rest.delete(id);
    }
};

export const GroupBuyingService = new service();
