import CatalogRest from '../../../services/rest/rest.catalog';

export const service = class CatalogService {
    constructor() {
        this.rest = new CatalogRest();
    }

    getCategory() {
        return this.rest.getCategory(0);
    }

    getCatalog(cid = 0, page = 1) {
        return this.rest.getCatalog(cid, 0, page, 10);
    }

    showCatalog(id) {
        return this.rest.showCatalog(id);
    }

    getRelated(id) {
        return this.rest.getRelated(id);
    }

    search(query, page = 1) {
        return this.rest.search(query, page);
    }
};
export const CatalogService = new service();
