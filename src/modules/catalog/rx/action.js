import { CatalogService } from './service';
import * as Activity from '@src/services/activity/action';

export const TYPE = {
    FETCH_CATEGORY: 'catalog::fetch.category',
    FETCH_CATALOG: 'catalog::fetch.catalog',
    START_LOADMORE: 'catalog::loadmore.start',
    FINISH_LOADMORE: 'catalog::loadmore.finish',
    CHANGE_CATEGORY: 'catalog::category.change',
    SHOW_CATALOG: 'catalog::show.catalog',
    CATALOG_SEARCH: 'catalog::search',
    RESET_CATALOG: 'catalog::reset',
};

export function $load(cid) {
    return (dispatch) => {
        return CatalogService.getCatalog(cid)
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_CATALOG,
                    category_id: cid,
                    payload: res.data,
                    total: res.total,
                });

                return true;
            })
            .catch((error) => {
                return false;
            });
    };
}

export function $search(query, page) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Search', 'Catalog');
        return CatalogService.search(query, page)
            .then((res) => {
                dispatch({
                    type: TYPE.CATALOG_SEARCH,
                    payload: res.data,
                    query: query,
                    page: page,
                });

                return res.data;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Search', 'Catalog');

                return res;
            });
    };
}

export function $loadmore(cid, page) {
    return (dispatch) => {
        dispatch({ type: TYPE.START_LOADMORE });

        return CatalogService.getCatalog(cid, page)
            .then((res) => {
                dispatch({
                    type: TYPE.FINISH_LOADMORE,
                    payload: res.data,
                    category_id: cid,
                    page: page,
                    total: res.total,
                });
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.FINISH_LOADMORE,
                    payload: null,
                    page: -1,
                });
            });
    };
}

export function $loadCategory() {
    return (dispatch) => {
        return CatalogService.getCategory().then((res) => {
            dispatch({
                type: TYPE.FETCH_CATEGORY,
                payload: res.data,
            });

            return res.data;
        });
    };
}

export function $loadCatalog(id) {
    return (dispatch) => {
        return CatalogService.showCatalog(id).then((res) => {
            dispatch({
                type: TYPE.SHOW_CATALOG,
                payload: res.data,
            });

            return res.data;
        });
    };
}

export function $loadRelated(id) {
    return (dispatch) => {
        return CatalogService.getRelated(id).then((res) => {
            return res.data;
        });
    };
}
