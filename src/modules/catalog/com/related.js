import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { $loadRelated } from '../rx/action';
import { ProductThumb } from '../../../components/product';

class RelatedProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
        };

        this._onLoad = this._onLoad.bind(this);
    }

    _onLoad() {
        if (this.state.data.length == 0) {
            this.props.$loadRelated(this.props.id)
                .then(res => {
                    if (res) {
                        this.setState({ data: res });
                    }
                });

        }
    }

    componentDidMount() {
        this._onLoad();
    }

    _renderProduct(i, product) {
        return (
            <ProductThumb key={i} size='small' product={product} hideButton
                          navigation={this.props.navigation}/>
        );
    }

    render() {
        return this.state.data.length > 0 ?
            <ScrollView style={{ marginTop: 10 }} horizontal showsHorizontalScrollIndicator={false}
                        howsVerticalScrollIndicator={false}>
                {this.state.data.map((p, i) => this._renderProduct(i, p))}
            </ScrollView>
            : null;
    }
}


const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({
        $loadRelated,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(RelatedProduct);
