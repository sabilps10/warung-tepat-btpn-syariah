import React, { Component } from 'react';
import {
    ActivityIndicator,
    Animated,
    Dimensions,
    Image,
    Modal,
    StyleSheet,
    Text,
    TouchableNativeFeedback,
    View,
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
// import RNFetchBlob from 'rn-fetch-blob';
import { bindActionCreators } from 'redux';
import Swiper from 'react-native-swiper';
import { Button, ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import Moment from 'react-moment';
import { Col, Row } from 'react-native-easy-grid';

import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import { CurrencyFormatter, Toast, NumberRounder } from '../../common/utils';
import Headers from '../../components/headerDetail';

import { $loadCatalog } from './rx/action';
import RelatedProduct from './com/related';
import AddToCart from '../cart/com/add_cart';
import { Config } from '../../common/config';

const { width } = Dimensions.get('window');
const IMAGE_HEIGHT = width;

import { $process, $counter, $show } from '../cart/rx/action';

class CatalogDetailScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            product: null,
            scrollY: 0,
            showModalSlider: -1,
            showCart: false,
            showMessage: false,
        };

        this._onLoad = this._onLoad.bind(this);
    }

    componentWillMount() {
        this._onLoad();
    }

    _onLoad() {
        this.setState({ product: this.props.navigation.getParam('product') });

        this.props.$loadCatalog(this.props.navigation.getParam('catalog_id')).then((data) => {
            this.setState({ product: data });
        });
    }

    toSupplierPage = () => {
        const supplierName = this.state.product.supplier_name;
        const supplierCode = this.state.product.supplier_id;
        const type = true;
        this.props.navigation.push(
            'Supplier',
            {
                supplierCode,
                supplierName,
                type,
            },
            true,
        );
    };

    _renderDiscount() {
        let { product } = this.state;

        if (!product || product.discount === 0) {
            return null;
        }

        return (
            <View style={styles.fab}>
                <Text style={styles.discountLabel}>Discount</Text>
                <Text style={styles.discount}>{NumberRounder(this.state.product.discount)}%</Text>
            </View>
        );
    }

    _renderInfo() {
        if (this.state.product.max_monthly === 0 && !this.state.product.preorder && this.state.product.packages) {
            return null;
        }

        return (
            <View style={styles.section}>
                <ListItem
                    containerStyle={styles.container}
                    titleStyle={styles.subtitle}
                    rightTitleStyle={styles.subtitle}
                    title="Informasi Barang"
                    allowFontScaling={false}
                    rightTitle={
                        <Text style={styles.add} onPress={() => this.toSupplierPage()}>
                            {this.state.product.supplier_name}
                        </Text>
                    }
                />
                {this.state.product.max_monthly > 0 ? (
                    <ListItem
                        containerStyle={styles.divider}
                        titleStyle={styles.listText}
                        rightTitleStyle={styles.listText}
                        title="Max. Pembelian Per Bulan"
                        rightTitle={this.state.product.max_monthly.toString()}
                    />
                ) : null}

                {this.state.product.preorder ? (
                    <ListItem
                        containerStyle={styles.divider}
                        titleStyle={styles.listText}
                        rightTitleStyle={styles.listText}
                        title="Jadwal Pengiriman Barang"
                        rightTitle={
                            <Moment element={Text} style={styles.listText} format="DD/MM/YYYY" utc>
                                {this.state.product.preorder.shipment_at}
                            </Moment>
                        }
                    />
                ) : null}

                {this.state.product ? (
                    <View>
                        <ListItem
                            containerStyle={styles.divider}
                            titleStyle={styles.listText}
                            rightTitleStyle={styles.listText}
                            title="Merek"
                            allowFontScaling={false}
                            rightTitle={this.state.product.brand}
                        />
                        <ListItem
                            containerStyle={styles.divider}
                            title="Ukuran"
                            rightTitle={this.state.product.size}
                            titleStyle={styles.listText}
                            rightTitleStyle={styles.listText}
                        />
                        <ListItem
                            containerStyle={styles.divider}
                            title="Kemasan"
                            titleStyle={styles.listText}
                            rightTitleStyle={styles.listText}
                            rightTitle={this.state.product.unit}
                        />
                    </View>
                ) : null}
            </View>
        );
    }

    _renderPackage() {
        let { product } = this.state;

        if (!product.packages) {
            return null;
        }

        return (
            <View style={styles.section}>
                <Text style={styles.subtitle}>Rincian Paket</Text>

                {product.packages.map((pack, i) => (
                    <ListItem
                        key={i}
                        containerStyle={styles.divider}
                        titleStyle={styles.listText}
                        rightTitleStyle={styles.listText}
                        title={pack.name}
                        subtitle={
                            pack.size + ' (' + pack.unit + ')' + ' @ ' + `${CurrencyFormatter(pack.selling_price)}`
                        }
                        subtitleStyle={styles.listSubText}
                        rightTitle={pack.quantity.toString()}
                    />
                ))}
            </View>
        );
    }

    _renderImages() {
        let product = this.state.product;

        return (
            <Swiper
                height={IMAGE_HEIGHT}
                showsButtons={false}
                dot={<View style={styles.indicator} />}
                activeDot={<View style={styles.indicatorActive} />}
            >
                {typeof product.images !== 'undefined' ? (
                    product.images.map((img, i) => (
                        <View style={styles.sliderWrapper} key={i}>
                            <TouchableNativeFeedback
                                background={TouchableNativeFeedback.Ripple('#f4f4f4')}
                                onPress={() => this.setState({ showModalSlider: i })}
                            >
                                <Image style={styles.image} resizeMode="contain" source={{ uri: img.url }} />
                            </TouchableNativeFeedback>
                        </View>
                    ))
                ) : (
                    <View style={styles.sliderWrapper} />
                )}
            </Swiper>
        );
    }

    _renderTitle() {
        let { product } = this.state;

        return (
            <View style={styles.section}>
                <Text style={styles.title}>{product.name}</Text>
                <Text style={styles.price}>
                    {`${CurrencyFormatter(product.selling_price)}`}
                    {product.discount !== 0 ? (
                        <Text style={styles.priceDefault}>{` ${CurrencyFormatter(product.regular_price)}`}</Text>
                    ) : null}
                </Text>
            </View>
        );
    }

    _renderRelated() {
        let { product } = this.state;
        let { navigation } = this.props;

        if (!product.id) {
            return null;
        }

        return (
            <View style={styles.section}>
                <Text style={styles.subtitle}>Produk Serupa</Text>
                <RelatedProduct navigation={navigation} id={product.id} />
            </View>
        );
    }

    _renderImageViewer() {
        let { product, showModalSlider } = this.state;
        if (!product) {
            return null;
        }

        return (
            <Modal
                visible={showModalSlider !== -1}
                transparent={true}
                onRequestClose={() => this.setState({ showModalSlider: -1 })}
            >
                <ImageViewer
                    imageUrls={product.images}
                    index={showModalSlider}
                    menuContext={{
                        saveToLocal: 'Simpan Gambar',
                        cancel: 'Batal',
                    }}
                    // onSave={this._handleDownload}
                    renderFooter={this._renderFooter}
                    footerContainerStyle={styles.width100}
                    loadingRender={() => (
                        <View style={styles.indicator}>
                            <ActivityIndicator />
                        </View>
                    )}
                />
            </Modal>
        );
    }

    _renderFooter(x) {
        let ix = x === -1 ? 0 : x;

        return (
            <View style={styles.footerStyle}>
                <Button
                    title="Download Gambar"
                    buttonStyle={{
                        backgroundColor: COLOR.success,
                    }}
                    onPress={() => this.onSave(this.imageUrls[ix].url)}
                />
            </View>
        );
    }

    _renderCart() {
        let { product, showCart, showMessage } = this.state;
        if (!product) {
            return null;
        }

        return (
            <AddToCart
                showMessage={showMessage}
                visible={showCart}
                catalog={product}
                navigation={this.props.navigation}
                onCancel={this.toggleCart}
            />
        );
    }

    _handleScroll = (event) => {
        this.setState({ scrollY: event.nativeEvent.contentOffset.y });
    };

    // _handleDownload = (url) => {
    //     RNFetchBlob.config({
    //         fileCache: true,
    //         addAndroidDownloads: {
    //             useDownloadManager: true,
    //             notification: true,
    //             title: this.state.product.name,
    //         },
    //     }).fetch('GET', url);
    //
    //     Toast('Download Completed');
    // };

    toggleCart = () => {
        let catalogID = this.state.product.id;
        let kk = this.props.session.kk;
        let body = {
            catalog_id: catalogID,
            quantity: 1,
        };

        this.props.$process(kk.id, body).then((res) => {
            if (!res) {
                this.setState({
                    showCart: !this.state.showCart,
                    showMessage: false,
                });
            } else {
                this.setState({
                    showCart: !this.state.showCart,
                    showMessage: true,
                });
            }
            this.props.$counter();
        });
    };

    getLinkStore() {
        return this.props.session.poskora.id
            ? Config.orderUrl + '?source=share&id=' + this.props.session.poskora.id
            : null;
    }

    _renderCheckStock() {
        let { is_stockable, stock_available } = this.state.product;

        if (is_stockable === 1) {
            return (
                <Button
                    title={stock_available <= 0 ? 'Stock Habis' : 'Beli Barang'}
                    buttonStyle={styles.button}
                    disabled={stock_available <= 0}
                    titleStyle={styles.buttonText}
                    containerStyle={styles.width100}
                    onPress={this.toggleCart.bind(this)}
                />
            );
        } else {
            return (
                <Button
                    title="Beli Barang"
                    buttonStyle={styles.button}
                    titleStyle={styles.buttonText}
                    containerStyle={styles.width100}
                    onPress={this.toggleCart.bind(this)}
                />
            );
        }
    }

    render() {
        let { navigation, counter } = this.props;
        let isSolid = this.state.scrollY > IMAGE_HEIGHT / 2;
        return (
            <View style={styles.wrapper}>
                <Headers solid={isSolid} navigation={navigation} counter={counter} />
                <Animated.ScrollView scrollEventThrottle={1} onScroll={this._handleScroll}>
                    {this._renderImages()}
                    {this._renderDiscount()}
                    {this._renderTitle()}
                    {this._renderInfo()}
                    {this._renderPackage()}
                    {this._renderRelated()}
                </Animated.ScrollView>

                <Row style={styles.footerContainer}>
                    <Col style={styles.width100}>{this._renderCheckStock()}</Col>
                </Row>

                {this._renderImageViewer()}
                {this._renderCart()}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        session: state.Auth.session,
        counter: state.Home.counter,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $loadCatalog, $process, $counter, $show }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(CatalogDetailScreen);

const styles = StyleSheet.create({
    wrapper: {
        flex: 2,
        backgroundColor: '#f4f4f4',
        paddingBottom: 70,
    },
    add: {
        textAlign: 'right',
        color: COLOR.darkGreen,
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.SMALL,
        paddingLeft: 68,
    },
    footerContainer: {
        position: 'absolute',
        bottom: 0,
        height: 'auto',
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
    indicator: {
        backgroundColor: COLOR.white,
        borderColor: '#ccc',
        borderWidth: 1,
        width: 8,
        height: 8,
        borderRadius: 7,
        marginLeft: 7,
        marginRight: 7,
    },
    indicatorActive: {
        backgroundColor: COLOR.primary,
        width: 8,
        height: 8,
        borderRadius: 7,
        marginLeft: 7,
        marginRight: 7,
    },
    sliderWrapper: {
        backgroundColor: '#f3f3f3',
        position: 'absolute',
        top: 0,
        width: width,
        height: '100%',
    },
    image: {
        width: width,
        height: '100%',
    },
    section: {
        borderColor: COLOR.border,
        elevation: 0,
        borderWidth: 0.2,
        borderBottomWidth: 0.5,
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginBottom: 10,
        backgroundColor: COLOR.white,
    },
    title: {
        fontWeight: '500',
        fontSize: FONT_SIZE.LARGE,
        fontFamily: SSP.semi_bold,
    },
    subtitle: {
        fontWeight: '500',
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    price: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        fontSize: FONT_SIZE.LARGE,
        fontFamily: SSP.semi_bold,
    },
    priceDefault: {
        textDecorationLine: 'line-through',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.light,
        color: COLOR.darkGray,
    },
    fab: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        top: IMAGE_HEIGHT - 30,
        backgroundColor: COLOR.danger,
        borderRadius: 30,
        elevation: 8,
        zIndex: 9999,
    },
    discountLabel: {
        color: COLOR.white,
        fontSize: 9,
    },
    discount: {
        color: COLOR.white,
        fontSize: FONT_SIZE.SMALL,
        fontWeight: '500',
    },
    divider: {
        padding: 0,
        margin: 0,
        borderBottomColor: COLOR.light,
        borderBottomWidth: 1,
        paddingVertical: 10,
    },
    container: {
        padding: 0,
        margin: 0,
        paddingVertical: 10,
    },
    listText: {
        fontSize: FONT_SIZE.SMALL,
    },
    listSubText: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.light,
        fontWeight: '500',
    },
    button: {
        backgroundColor: COLOR.tertiaryLight,
        width: '100%',
    },
    buttonText: {
        fontFamily: SSP.bold,
        letterSpacing: 1.2,
        fontSize: FONT_SIZE.MEDIUM,
    },
    width100: {
        width: '100%',
    },
    footerStyle: {
        height: 100,
        padding: 30,
    },
});
