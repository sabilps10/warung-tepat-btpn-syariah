import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import Icon from '../components/custom_icon';
import { COLOR } from '../common/styles';

import MainScreen from './home';
import CatalogTabs from './catalog';
import TransactionScreen from './transaction';
import ProfileScreen from './profile';

const MainRoute = createMaterialTopTabNavigator(
    {
        Home: {
            screen: MainScreen,
            navigationOptions: {
                tabBarLabel: 'Beranda',
                tabBarIcon: ({ tintColor }) => <Icon name="action_home" size={24} color={tintColor} />,
            },
        },
        Catalog: {
            screen: CatalogTabs,
            navigationOptions: {
                params: { category_id: 0 },
                tabBarLabel: 'Katalog',
                tabBarIcon: ({ tintColor }) => <Icon name="navigation_drag" size={24} color={tintColor} />,
            },
        },
        Transaction: {
            screen: TransactionScreen,
            navigationOptions: {
                tabBarLabel: 'Transaksi',
                tabBarIcon: ({ tintColor }) => <Icon name="action_history" size={24} color={tintColor} />,
            },
        },
        ProfileScreen: {
            screen: ProfileScreen,
            navigationOptions: {
                tabBarLabel: 'Akun',
                tabBarIcon: ({ tintColor }) => {
                    return <Icon name="action_user" size={24} color={tintColor} />;
                },
            },
        },
    },
    {
        initialRouteName: 'Home',
        animationEnabled: false,
        swipeEnabled: false,
        lazy: true,
        tabBarPosition: 'bottom',
        optimizationsEnabled: false,
        tabBarOptions: {
            activeTintColor: COLOR.primary,
            inactiveTintColor: COLOR.darkBlack,
            allowFontScaling: false,
            indicatorStyle: {
                backgroundColor: 'transparent',
            },
            style: {
                backgroundColor: COLOR.white,
                height: 55,
                borderTopColor: '#dbdbdb',
                borderTopWidth: 0.5,
            },
            labelStyle: {
                fontSize: 10,
                margin: 0,
                textTransform: 'capitalize',
            },
            showIcon: true,
            showLabel: true,
        },
    },
);

const MainTabs = createAppContainer(MainRoute);
export default MainTabs;
