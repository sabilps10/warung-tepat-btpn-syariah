import { TYPE } from './action';

const INITIAL_STATE = {
    akadData: [],
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_AKAD_CONTENT:
            return {
                ...state,
                akadData: action.payload,
            };

        default:
            return state;
    }
}
