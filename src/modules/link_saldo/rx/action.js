import { NavigationActions } from 'react-navigation';

import * as Activity from '@src/services/activity/action';
import { LmdToast } from '../../auth/com/lmdToast';
import { ConnectSaldoService } from './service';
import { serverError, connectSuccess, invalidAccountConnect, blockedAccount } from '../../../assets/Utils';

export const TYPE = {
    VERIFY_SUCCESS: 'connect::verify.success',
    VERIFY_ERROR: 'connect::verify.error',
    BEGIN_CONNECT: 'connect::begin',
    CONNECT_SUCCESS: 'connect::success',
    CONNECT_FAILED: 'connect::failed',
    CONNECT_ERROR: 'connect::error',
    RESET: 'reset',
};

// action verify user
export function $verifyUser(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Connect', 'Verify');
        ConnectSaldoService.verifyUser(data)
            .then((res) => {
                dispatch({
                    type: TYPE.VERIFY_SUCCESS,
                    payload: res.data,
                });
            })
            .catch((error) => {
                dispatch({ type: TYPE.VERIFY_ERROR });
            })
            .finally((res) => {
                Activity.done(dispatch, 'Connect', 'Verify');
            });
    };
}

// action connect user
export function $connectUser(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Connect', 'User');
        dispatch({
            type: TYPE.BEGIN_CONNECT,
            payload: {
                isLoading: true,
            },
        });
        ConnectSaldoService.connectUser(data)
            .then((res) => {
                const { response_code } = res.data;

                if (response_code === '00') {
                    dispatch({
                        type: TYPE.CONNECT_SUCCESS,
                        payload: {
                            verified: true,
                            error: false,
                            isLoading: false,
                        },
                    });
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'ConnectSuccess',
                            params: {
                                data: connectSuccess,
                            },
                        }),
                    );
                }

                return res;
            })
            .catch((error) => {
                const { response_code } = error.data;

                if (response_code === '06') {
                    LmdToast.showToast('PIN yang anda masukkan salah', 'danger');

                    dispatch({
                        type: TYPE.CONNECT_ERROR,
                        payload: {
                            verified: false,
                            error: true,
                            isLoading: false,
                        },
                    });
                } else if (response_code === '01') {
                    dispatch({
                        type: TYPE.CONNECT_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'ConnectFailed',
                            params: {
                                data: invalidAccountConnect,
                            },
                        }),
                    );
                } else if (response_code === '12') {
                    dispatch({
                        type: TYPE.CONNECT_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'ConnectFailed',
                            params: {
                                data: blockedAccount,
                            },
                        }),
                    );
                } else {
                    dispatch({
                        type: TYPE.CONNECT_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'ConnectFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'Connect', 'User');

                return res;
            });
    };
}

// action reset error
export function $resetError() {
    return (dispatch) => {
        dispatch({ type: TYPE.RESET });
    };
}
