import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import source from '@assets/img/order_success_hdpi.png';

import Header from '../com/header';
import ButtonDone from '../com/button';

export default class ConnectSuccessPage extends Component {
    constructor(props) {
        super(props);
    }

    toNavigate = () => {
        this.props.navigation.navigate(this.props.navigation.state.params.data.url);
    };

    render() {
        const { data } = this.props.navigation.state.params;

        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title={data.title} />
                <View style={styles.textContainer}>
                    <Text style={styles.text}>{data.text}</Text>
                </View>
                <ButtonDone title={data.buttonText} onPress={this.toNavigate} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    textContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 0,
        paddingTop: 0,
        marginLeft: 24,
        marginRight: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
    },
});
