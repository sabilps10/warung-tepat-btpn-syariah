import React, { Component } from 'react';
import { Linking, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import source from '@assets/img/warung_tepat.png';
import Header from '../auth/com/header';
import Logo from '../auth/com/logo';
import ButtonUpdate from '../auth/com/button';

class UpdateScreen extends Component {
    constructor(props) {
        super(props);
    }

    update = () => {
        Linking.openURL(this.props.url);
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title="Selamat Datang" subtitle="Di Warung Tepat" isLeft />
                <View animation="bounceIn" delay={30} style={styles.textContainer} useNativeDriver>
                    <Text style={styles.text}>
                        Aplikasi Warung Tepat Anda masih versi lama. Silahkan update terlebih dahulu untuk melanjutkan.
                    </Text>
                    <ButtonUpdate title="Update" onPress={this.update} />
                </View>
                <Logo />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        url: state.Version.url,
    };
};

export default connect(mapStateToProps, null)(UpdateScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    textContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 30,
        marginLeft: 24,
        marginBottom: 32,
    },
});
