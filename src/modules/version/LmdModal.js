import React, { PureComponent } from 'react';
import { StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import {
    Dialog,
    DialogButton,
    DialogContent,
    DialogFooter,
    DialogTitle,
    ScaleAnimation,
} from 'react-native-popup-dialog';

import { COLOR, FONT_SIZE, SSP } from '../../common/styles';

export default class LmdModal extends PureComponent {
    _renderFooter() {
        return (
            <DialogFooter>
                <DialogButton
                    text="Update"
                    onPress={this.props.onSubmit}
                    style={styles.button}
                    key="button-1"
                    textStyle={styles.textButton}
                />
            </DialogFooter>
        );
    }

    render() {
        return (
            <Dialog
                visible={this.props.visible}
                onTouchOutside={this.props.onCancel}
                footer={this._renderFooter()}
                dialogAnimation={new ScaleAnimation()}
                width={0.8}
                dialogTitle={
                    <DialogTitle
                        title={this.props.title}
                        hasTitleBar={true}
                        style={styles.titleContainer}
                        textStyle={styles.textTitle}
                    />
                }
            >
                <DialogContent style={styles.dialogContainer}>
                    <Text style={styles.textContent}>{this.props.text}</Text>
                </DialogContent>
            </Dialog>
        );
    }
}

LmdModal.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    visible: PropTypes.bool,
};

const styles = StyleSheet.create({
    titleContainer: {
        paddingVertical: 10,
    },
    textTitle: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
    },
    dialogContainer: { paddingVertical: 15 },
    textContent: {
        textAlign: 'center',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.semi_bold,
    },
    button: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.success,
        fontFamily: SSP.semi_bold,
    },
});
