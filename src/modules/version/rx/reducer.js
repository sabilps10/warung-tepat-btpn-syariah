import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    url: {},
    showModal: false,
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.CHECK_SUCCESS:
            return {
                ...state,
                url: action.payload,
                errors: {},
            };
        case TYPE.CHECK_FAILED:
            return {
                ...INITIAL_STATE,
                errors: action.payload,
            };
        case TYPE.SHOW_MODAL:
            return {
                ...state,
                showModal: action.payload,
                errors: {},
            };
        default:
            return state;
    }
}

export function persister({ url, showModal }) {
    return {
        url,
        showModal,
    };
}
