import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { ScrollView, StyleSheet, Text, View, BackHandler, ActivityIndicator } from 'react-native';
import { Button, ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';

import { CurrencyFormatter } from '../../common/utils';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import { LmdToast } from '../auth/com/lmdToast';
import PaymentMethod from './com/payment_method';
import PaymentMethodHeader from './com/payment_method_header';
import { $loadLocation } from '../profile/rx/action';
import { $verifyOrder, $checkout, $loadPaymentMethod } from './rx/action';

class CartCheckout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            paymentMethods: false,
            selectedPaymentMethod: false,
            noAccount: false,
            paylater: false,
        };

        this.onLoad = this.onLoad.bind(this);
        this.onCheckout = this.onCheckout.bind(this);
        this.onSelectedPaymentMethod = this.onSelectedPaymentMethod.bind(this);
    }

    onSelectedPaymentMethod(item) {
        const { carts, session } = this.props;
        const account_name = carts[0].kk.account_name;
        const account_no = carts[0].kk.account_no;
        const { type } = session.poskora;
        const userAccountName = session.kk.account_name;
        const userAccountNo = session.kk.account_no;
        let noAccount = account_name === '' && account_no === '';

        if (type === 'regular') {
            noAccount = userAccountName && userAccountNo ? false : true;
        }

        if (item.id === 2 && noAccount) {
            LmdToast.showToast('Rekening belum didaftarkan', 'danger');
            this.setState({ noAccount: true, paylater: false });
        } else if (item.id === 3) {
            this.setState({ paylater: true, noAccount: false });
        } else {
            LmdToast.hideToast();
            this.setState({ noAccount: false, paylater: false });
        }

        this.setState({ selectedPaymentMethod: item });
    }

    onCheckout() {
        let bank_id = this.state.selectedPaymentMethod.id;
        let body = { bank_id };

        if (bank_id === 1) {
            this.props.$verifyOrder(body).then((res) => {
                if (res) {
                    this.props.$checkout(body).then((newRes) => {
                        if (newRes) {
                            let navigationAction = StackActions.reset({
                                index: 2,
                                actions: [
                                    NavigationActions.navigate({ routeName: 'Main' }),
                                    NavigationActions.navigate({ routeName: 'Cart' }),
                                    NavigationActions.navigate({
                                        routeName: 'TransactionDetail',
                                        params: {
                                            id: newRes.id,
                                            transaction: newRes,
                                            checkout: true,
                                        },
                                    }),
                                ],
                            });

                            this.props.navigation.dispatch(navigationAction);
                        }
                    });
                }
            });
        } else if (bank_id === 3) {
            this.props.$verifyOrder(body).then((res) => {
                if (res) {
                    this.props.navigation.navigate('AkadScreen');
                }
            });
        } else {
            this.props.$verifyOrder(body).then((res) => {
                if (res) {
                    this.props.navigation.navigate('VerifyPin');
                }
            });
        }
    }

    onLoad() {
        let { summary, navigation } = this.props;

        if (!summary.number_order) {
            navigation.navigate('Cart');
        }

        this.props.$loadPaymentMethod().then((res) => {
            this.setState({
                paymentMethods: res.data,
            });
        });

        this.props.$loadLocation();
    }

    componentWillMount() {
        this.onLoad();

        if (!this.props.summary.number_order) {
            this.props.navigation.navigate('Cart');
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.push('Cart');
        return true;
    };

    _renderSummary() {
        let { summary } = this.props;

        return (
            <View style={styles.summaryContainer}>
                <Text style={styles.titleSection}>RINGKASAN TRANSAKSI</Text>
                <View style={styles.cartItemContainer}>
                    <ListItem
                        title="JUMLAH ORDER"
                        rightTitle={summary.number_order + ' ORD MEMBER'}
                        containerStyle={styles.listContainer}
                        titleStyle={styles.listTitle}
                        rightTitleStyle={styles.listRightTitle}
                    />
                    <ListItem
                        title="TOTAL BARANG"
                        rightTitle={summary.total_item}
                        containerStyle={styles.listContainer}
                        titleStyle={styles.listTitle}
                        rightTitleStyle={styles.listRightTitle}
                    />
                    <ListItem
                        title="TOTAL TRANSAKSI"
                        rightTitle={CurrencyFormatter(summary.total_transaction)}
                        containerStyle={styles.listContainer}
                        titleStyle={styles.listTitle}
                        rightTitleStyle={styles.listRightTitle}
                    />
                    {this.state.paylater ? (
                        <View>
                            <ListItem
                                title="BIAYA MARGIN MURABAHAH"
                                rightTitle={CurrencyFormatter(summary.margin_amount)}
                                containerStyle={styles.listContainer}
                                titleStyle={styles.listTitle}
                                rightTitleStyle={styles.listRightTitle}
                            />
                            <ListItem
                                title="TOTAL PEMBIAYAAN MURABAHAH"
                                rightTitle={CurrencyFormatter(summary.total_pembiayaan)}
                                containerStyle={styles.listContainer}
                                titleStyle={styles.listTitle}
                                rightTitleStyle={styles.listRightTitle}
                            />
                        </View>
                    ) : null}
                </View>
            </View>
        );
    }

    _renderPayment() {
        let { paymentMethods, selectedPaymentMethod } = this.state;

        if (!paymentMethods) {
            return <ActivityIndicator style={styles.indicator} size={50} color={COLOR.secondary} />;
        }

        return (
            <View style={styles.summaryContainer}>
                <Text style={styles.titleSection}>PILIH METODE PEMBAYARAN</Text>
                <View style={styles.cartItemContainer}>
                    {paymentMethods.map((pm, i) => {
                        return (
                            <PaymentMethod
                                key={pm.id}
                                data={pm}
                                isActive={selectedPaymentMethod.id === pm.id}
                                onSelected={this.onSelectedPaymentMethod}
                                navigation={this.props.navigation}
                            />
                        );
                    })}
                </View>
            </View>
        );
    }

    _renderFooter() {
        if (this.state.noAccount) {
            return null;
        }

        return (
            <View style={styles.footerContainer}>
                <Button
                    title={this.state.selectedPaymentMethod.id === 2 ? 'Bayar Sekarang' : 'Pesan Sekarang'}
                    buttonStyle={styles.button}
                    titleStyle={styles.buttonText}
                    containerStyle={styles.bodyContainer}
                    onPress={this.onCheckout}
                    disabled={!this.state.selectedPaymentMethod}
                />
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <PaymentMethodHeader navigation={this.props.navigation} />
                <ScrollView style={styles.bodyContainer}>
                    {this._renderPayment()}
                    {this._renderSummary()}
                </ScrollView>

                {this._renderFooter()}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        summary: state.Cart.summary,
        carts: state.Cart.carts,
        errors: state.Cart.errors,
        session: state.Auth.session,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $verifyOrder,
            $checkout,
            $loadLocation,
            $loadPaymentMethod,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(CartCheckout);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 70,
        backgroundColor: COLOR.white,
    },
    bodyContainer: {},
    titleSection: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        paddingTop: 15,
        paddingBottom: 5,
        paddingLeft: 15,
        borderBottomWidth: 0.5,
        borderColor: COLOR.border,
        letterSpacing: 2,
    },
    listContainer: {
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderColor: COLOR.border,
        backgroundColor: 'transparent',
    },
    listTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        letterSpacing: 1,
    },
    listRightTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    listSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
    error: {
        fontFamily: SSP.semi_bold,
        color: COLOR.danger,
        fontSize: FONT_SIZE.SMALL,
    },
    footerContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 2,
        borderColor: COLOR.border,
        padding: 15,
    },
    button: {
        backgroundColor: COLOR.tertiaryLight,
        width: '100%',
    },
    buttonText: {
        fontFamily: SSP.bold,
        letterSpacing: 1.2,
        fontSize: FONT_SIZE.MEDIUM,
    },
    buttonContainer: {
        width: '100%',
        borderRadius: 10,
    },
    indicator: {
        marginTop: 20,
        marginBottom: 10,
    },
});
