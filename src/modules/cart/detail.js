import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { $show } from './rx/action';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import CartItem from './com/cart_item';

class CartDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: false,
        };

        this.onRefresh = this.onRefresh.bind(this);
    }

    componentDidMount() {
        this.onLoad();
    }

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.cart !== prevProps.cart) {
            this.onLoad();
        }
    }

    onLoad() {
        this.props.$show(this.props.cart.id).then((res) => {
            if (res) {
                this.setState({ data: res });
            } else {
                this.props.onRender();
            }
        });
    }

    onRefresh() {
        this.props.$show(this.props.cart.id).then((res) => {
            if (res) {
                this.setState({ data: res });
                this.props.onChange();
            } else {
                this.props.onRender();
            }
        });
    }

    _renderItems() {
        const { data } = this.state;
        const { cart, navigation } = this.props;
        const isWebLink = cart.channel === 100002 ? true : false;

        if (!data.cart_items) {
            return null;
        }

        return (
            <View>
                {data.cart_items.map((item, i) => {
                    return (
                        <CartItem
                            disabled={this.props.stock === 0}
                            key={item.id}
                            navigation={navigation}
                            kk={cart.kk}
                            catalog={item.catalog}
                            item={item}
                            is_group_buying={cart.is_group_buying === 1}
                            quantity={item.quantity}
                            onChange={this.onRefresh}
                            isWebLink={isWebLink}
                            itemsCart={cart.cart_items[i]}
                        />
                    );
                })}
            </View>
        );
    }

    render() {
        return <View style={styles.container}>{this._renderItems()}</View>;
    }
}

CartDetail.propTypes = {
    navigation: PropTypes.object.isRequired,
    cart: PropTypes.object.isRequired,
    onRerender: PropTypes.func,
    onChange: PropTypes.func,
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $show,
        },
        dispatch,
    );
};

export default connect(null, mapDispatchToProps)(CartDetail);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
