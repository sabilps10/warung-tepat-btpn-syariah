import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    carts: [],
    summary: {},
    counter: {},
    checkData: [],
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.DELETE_SUCCESS:
            return {
                ...state,
                errors: {},
            };
        case TYPE.FETCH_CART:
            return {
                ...state,
                carts: action.payload,
                errors: {},
            };
        case TYPE.FETCH_SUMMARY:
            return {
                ...state,
                summary: action.payload,
                errors: {},
            };
        case TYPE.CHECKOUT_SUCCESS:
            return {
                errors: {},
                carts: {},
                summary: {},
            };
        case TYPE.CHECKOUT_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.PROCESS_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.CHECK_CART_SUCCESS:
            return {
                ...state,
                checkData: action.payload,
            };
        case TYPE.CHECK_CART_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.FETCH_COUNTER:
            return {
                ...state,
                counter: action.payload,
            };
        case TYPE.RESET_CART:
            return {
                ...INITIAL_STATE,
            };
        default:
            return state;
    }
}

export function persister({ carts, summary, counter, checkData }) {
    return {
        carts,
        summary,
        counter,
        checkData,
    };
}
