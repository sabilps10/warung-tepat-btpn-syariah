import React, { PureComponent } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { COLOR, FONT_SIZE, SSP, WIDTH } from '../../common/styles';
import CartHeader from './com/cart_header';

export default class CartEmpty extends PureComponent {
    toCatalog() {
        this.props.navigation.navigate('Catalog');
    }

    render() {
        return (
            <View style={styles.container}>
                <CartHeader navigation={this.props.navigation} />
                <View style={styles.bodyContainer}>
                    <Image style={styles.image} source={{ uri: 'https://s3.kora.id/app/assets/empty-cart.png' }} />
                    <Text style={styles.title}>Belum ada barang belanjaan</Text>
                    <Text style={styles.subtitle}>
                        Semua barang belanjaan Anda akan terdaftar disini. {'\n'} Ayo mulai berbelanja dan rasakan
                        kemudahannya
                    </Text>
                    <Button
                        title="LANJUT BERBELANJA"
                        buttonStyle={styles.button}
                        titleStyle={styles.buttonText}
                        containerStyle={styles.buttonContainer}
                        onPress={() => this.toCatalog()}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bodyContainer: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 40,
        paddingHorizontal: 15,
    },
    title: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.EXTRA_LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        letterSpacing: 1,
    },
    subtitle: {
        paddingTop: 10,
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        textAlign: 'center',
    },
    image: {
        width: 300,
        height: 300,
    },
    button: {
        marginTop: 20,
        backgroundColor: COLOR.tertiaryLight,
        width: '100%',
    },
    buttonText: {
        fontFamily: SSP.bold,
        letterSpacing: 1,
        fontSize: FONT_SIZE.MEDIUM,
    },
    buttonContainer: {
        width: WIDTH.full,
    },
});
