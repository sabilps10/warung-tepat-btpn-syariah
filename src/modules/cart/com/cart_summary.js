import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ListItem } from 'react-native-elements';

import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';
import { CurrencyFormatter } from '../../../common/utils';

class CartSummary extends Component {
    render() {
        let { discount, saving } = this.props;

        return (
            <View style={styles.summaryContainer}>
                <View style={styles.summaryTitleContainer}>
                    <Text style={styles.titleSection}>RINGKASAN TRANSAKSI</Text>
                </View>
                <View>
                    <ListItem
                        title="DISCOUNT"
                        rightTitle={`${CurrencyFormatter(discount)}`}
                        containerStyle={styles.listContainer}
                        titleStyle={styles.listTotalTitle}
                        rightTitleStyle={styles.listTitle}
                    />
                    <ListItem
                        title="ONGKOS KIRIM"
                        rightTitle={`${CurrencyFormatter(50000)}`}
                        containerStyle={styles.listContainer}
                        titleStyle={styles.listTotalTitle}
                        rightTitleStyle={styles.listTitleLined}
                    />
                    {saving > 0 ? (
                        <ListItem
                            title="ANDA BERHEMAT"
                            subtitle="Total uang yang Anda hemat berdasarkan diskon dan potongan harga"
                            rightTitle={`${CurrencyFormatter(saving)}`}
                            rightTitleStyle={styles.listTitle}
                            containerStyle={styles.listContainer}
                            titleStyle={styles.listTotalTitle}
                            subtitleStyle={styles.listSubTitle}
                        />
                    ) : null}
                </View>
            </View>
        );
    }
}

export default CartSummary;

const styles = StyleSheet.create({
    summaryContainer: {
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.5,
        borderColor: COLOR.border,
        marginTop: -8,
    },
    summaryTitleContainer: {
        width: '100%',
        backgroundColor: COLOR.backgroundCart,
    },
    titleSection: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        paddingTop: 13,
        paddingBottom: 13,
        paddingLeft: 16,
        borderBottomWidth: 0.5,
        borderColor: COLOR.border,
        letterSpacing: 2,
    },
    listContainer: {
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingVertical: 5,
        borderBottomWidth: 0.5,
        borderColor: COLOR.border,
        backgroundColor: 'transparent',
        paddingTop: 13,
        paddingBottom: 13,
        paddingLeft: 16,
    },
    listTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        letterSpacing: 1,
    },
    listTitleLined: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        letterSpacing: 1,
        textDecorationLine: 'line-through',
        color: COLOR.danger,
    },
    listTotalTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        letterSpacing: 1,
    },
    listSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
});
