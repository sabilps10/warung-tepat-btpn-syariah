import React, { PureComponent } from 'react';
import { Header } from 'react-native-elements';
import { StyleSheet, Text } from 'react-native';
import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';
import { HeaderBackButton } from 'react-navigation-stack';
import PropTypes from 'prop-types';

export default class CartHeader extends PureComponent {
    toHome = () => {
        this.props.navigation.navigate('Main');
    };

    render() {
        return (
            <Header
                containerStyle={styles.container}
                placement="center"
                leftComponent={<HeaderBackButton tintColor={COLOR.secondary} onPress={this.toHome} />}
                centerComponent={<Text style={styles.title}>KERANJANG BELANJA</Text>}
            />
        );
    }
}

CartHeader.propTypes = {
    navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        elevation: 4,
        justifyContent: 'space-around',
        height: 55,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 15,
        paddingLeft: 0,
    },
    title: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        letterSpacing: 1,
        color: COLOR.primary,
    },
});
