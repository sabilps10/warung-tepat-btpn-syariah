import React, { Component } from 'react';
import { Dialog, SlideAnimation } from 'react-native-popup-dialog';
import { ActivityIndicator, StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { $load } from '../../member/rx/action';
import { $loadByCatalog, $process, $show } from '../rx/action';
import { COLOR, FONT_SIZE, PADDING, SSP, MARKOT } from '../../../common/styles';
import { SearchBar } from 'react-native-elements';
import Item from './add_cart_kk';
import { NavigationActions } from 'react-navigation';
import success from '@assets/img/order.png';
import failed from '@assets/img/order_failed_hdpi.png';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 36;

class AddToCart extends Component {
    constructor(props) {
        super(props);
        this.typingTimer = false;

        this.state = {
            selected: {},
            data: false,
            carts: false,
            input: [],
            search: '',
            loading: false,
            notFound: false,
        };

        this.toCart = this.toCart.bind(this);
    }

    _renderFilter() {
        const { search } = this.state;

        return (
            <SearchBar
                lightTheme
                containerStyle={styles.searchContainer}
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.searchInput}
                placeholder="Cari Member ..."
                onChangeText={this.onSearch}
                value={search}
            />
        );
    }

    toCart() {
        this.props.navigation.push('Cart');
    }
    componentWillMount() {
        this._loadCart();
    }

    _loadCart() {
        this.props.$loadByCatalog(this.props.catalog.id).then((res) => {
            if (res) {
                this.setState({ carts: res.data });
            }
        });
    }

    _onLoad(search = '') {
        if (search === '') {
            this.setState({
                data: false,
                loading: false,
                notFound: false,
            });

            this._loadCart();

            return;
        }

        this.props.$load(1, 50, search).then((res) => {
            if (res) {
                this.setState({
                    data: res.data,
                    loading: false,
                    notFound: false,
                });
            } else {
                this.setState({
                    notFound: true,
                    data: false,
                    loading: false,
                });
            }
        });
    }

    _onPressItem = (item) => {
        this.setState({ selected: item });
    };

    onSearch = (search) => {
        this.setState({
            search,
            loading: true,
        });

        clearTimeout(this.typingTimer);
        this.typingTimer = setTimeout(() => {
            this._onLoad(search);
        }, 100);
    };

    _flatItem = ({ index, item }) => (
        <Item
            onSelected={this._onPressItem}
            stock={this.props.catalog.is_available}
            catalogID={this.props.catalog.id}
            selected={this.state.selected.id === item.id}
            kk={item}
        />
    );

    _flatItemCart = ({ index, item }) => (
        <Item
            onSelected={this._onPressItem}
            stock={this.props.catalog.is_available}
            catalogID={this.props.catalog.id}
            selected={item.quantity > 0}
            kk={item}
        />
    );

    _flatKey = (item, index) => index.toString();

    _flatReff = (ref) => (this.listRef = ref);

    render() {
        const { visible, onCancel } = this.props;

        return (
            <Dialog visible={visible} onTouchOutside={onCancel} dialogAnimation={new SlideAnimation()} width={0.8}>
                <View style={styles.container}>
                    <Image
                        source={this.props.showMessage ? success : failed}
                        style={this.props.showMessage ? styles.iconImage : styles.iconImageFailed}
                        resizeMode="contain"
                    />

                    {this.state.loading ? (
                        <ActivityIndicator style={styles.indicator} size="small" color={COLOR.secondary} />
                    ) : null}

                    {this.state.notFound && !this.state.loading ? (
                        <Text style={styles.notFound}>Tidak ada Member dengan nama / no handphone tersebut!</Text>
                    ) : null}

                    {this.props.showMessage ? (
                        <View style={styles.successContainer}>
                            <View style={styles.titleTextContainer}>
                                <Text style={styles.successText}>Barang Sukses Masuk Keranjang</Text>
                            </View>
                            <View style={styles.subtitleTextContainer}>
                                <Text style={styles.successSubitleText}>
                                    Barang yang kamu pilih sudah masuk keranjang, silahkan lihat keranjang atau cari
                                    barang lain.
                                </Text>
                            </View>
                            <Button
                                title="Lihat Keranjang"
                                buttonStyle={styles.cartButton}
                                titleStyle={styles.cartButtonText}
                                onPress={this.toCart}
                            />
                            <Button
                                title="Cari Barang Lain"
                                buttonStyle={styles.anotherButton}
                                titleStyle={styles.anotherbuttonText}
                                onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                            />
                        </View>
                    ) : (
                        <View style={styles.failedContainer}>
                            <View style={styles.failedTextContainer}>
                                <Text style={styles.successText}>Stok barang tidak tersedia</Text>
                            </View>
                            <Button
                                title="Cari Barang Lain"
                                buttonStyle={styles.anotherButton}
                                titleStyle={styles.anotherbuttonText}
                                onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                            />
                        </View>
                    )}
                </View>
            </Dialog>
        );
    }
}

AddToCart.propTypes = {
    visible: PropTypes.bool,
    onCancel: PropTypes.func.isRequired,
    catalog: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: state.Activity.page,
        session: state.Auth.session,
        counter: state.Cart.counter,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $load,
            $loadByCatalog,
            $process,
            $show,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddToCart);

const styles = StyleSheet.create({
    titleContainer: {
        paddingVertical: 10,
    },
    emptyContainer: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 40,
        paddingHorizontal: 15,
    },
    emptyTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        letterSpacing: 0.6,
        marginTop: 20,
        textAlign: 'center',
    },
    emptySubtitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        textAlign: 'center',
    },
    emptyImage: {
        width: 125,
        height: 125,
    },
    notFound: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
        textAlign: 'center',
        marginTop: 20,
    },
    textTitle: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
    },
    dialogContainer: {
        padding: 0,
        paddingHorizontal: 0,
        margin: 0,
    },
    textContent: {
        textAlign: 'center',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.semi_bold,
    },
    button: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.success,
        fontFamily: SSP.semi_bold,
    },
    searchContainer: {
        backgroundColor: 'white',
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        margin: 0,
        borderTopWidth: 0,
        borderColor: COLOR.border,
        borderBottomWidth: 0.7,
    },
    inputContainer: {
        backgroundColor: COLOR.light,
        borderColor: COLOR.border,
        borderWidth: 0.5,
        borderBottomWidth: 0.5,
    },
    searchInput: {
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.black,
        padding: 0,
    },
    itemContainer: {
        padding: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: COLOR.light,
    },
    titleTextContainer: {
        marginTop: 8,
        marginBottom: 16,
        width: 250,
    },
    successText: {
        fontFamily: MARKOT.bold,
        fontSize: 20,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    container: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: COLOR.white,
        minHeight: 400,
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: DEVICE_WIDTH - MARGIN,
        marginTop: -2,
        marginBottom: -2,
    },
    iconImage: {
        alignContent: 'center',
        alignItems: 'center',
        height: 200,
        width: 280,
        marginTop: 8,
    },
    iconImageFailed: {
        alignContent: 'center',
        alignItems: 'center',
        height: 200,
        width: 280,
        marginTop: 24,
    },
    indicator: {
        marginTop: 20,
    },
    subtitleTextContainer: {
        marginBottom: 16,
        width: 250,
    },
    successSubitleText: {
        fontFamily: SSP.regular,
        color: COLOR.textGray,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.MEDIUM,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    successContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    failedContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    failedTextContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 8,
        marginBottom: 16,
        width: 250,
    },
    cartButton: {
        backgroundColor: COLOR.primary,
        width: 250,
        height: 42,
        marginTop: 16,
        borderRadius: 10,
    },
    cartButtonText: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    anotherButton: {
        width: 250,
        height: 42,
        marginTop: 16,
        marginBottom: 16,
        backgroundColor: 'transparent',
        borderRadius: 10,
        borderWidth: 2,
        borderColor: COLOR.tertiary,
    },
    anotherbuttonText: {
        fontFamily: SSP.bold,
        color: COLOR.tertiary,
        fontSize: FONT_SIZE.MEDIUM,
    },
});
