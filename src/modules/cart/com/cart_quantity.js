import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { LmdToast } from '../../auth/com/lmdToast';
import { COLOR, FONT_SIZE } from '../../../common/styles';

export default class CartQuantity extends Component {
    constructor(props) {
        super(props);
    }

    process(qty) {
        this.props.onChange(qty);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.quantity !== nextProps.quantity || this.props.stock !== nextProps.stock) {
            return true;
        }

        return false;
    }

    Increase = () => {
        const { quantity, available } = this.props;
        let qty = quantity + 1;

        if (qty > available) {
            this.process(available);
            this._renderButton();
            LmdToast.showToast(
                `Ups, stock product hanya ${available}`,
                'danger',
                3500,
                ...Array(2),
                { alignSelf: 'center' },
                {
                    backgroundColor: '#ef3636',
                    borderRadius: 6,
                    alignSelf: 'center',
                    top: -64,
                    maxWidth: 400,
                    minHeight: 71,
                },
            );
            return;
        }
        this.process(qty);
        this._renderButton();
    };

    Decrease = () => {
        let qty = this.props.quantity - 1;

        if (this.props.available >= qty) {
            this.process(qty, true);
            this._renderButton();
        } else {
            this.process(qty, false);
            this._renderButton();
        }
    };

    handleTextInput = (number) => {
        const { available } = this.props;
        let qty = +number;
        const defaultNumber = 1;

        if (qty >= 1 && qty <= 999) {
            if (qty > available) {
                this.process(available);
                this._renderButton();
                LmdToast.showToast(
                    `Ups, stock product hanya ${available}`,
                    'danger',
                    3500,
                    ...Array(2),
                    { alignSelf: 'center' },
                    {
                        backgroundColor: '#ef3636',
                        borderRadius: 6,
                        alignSelf: 'center',
                        top: -64,
                        maxWidth: 400,
                        minHeight: 71,
                    },
                );
                return;
            }
            this.process(qty);
            this._renderButton();
        }
        // else {
        //     this.process(defaultNumber);
        //     this._renderButton();
        // }
    };

    _renderButton() {
        const stock = this.props.stock;
        const addCart = this.props.addCart;
        const qty = this.props.quantity;
        if (addCart) {
            const { disabled } = this.props;

            return (
                <View style={styles.container}>
                    <Button
                        onPress={this.Decrease}
                        disabled={qty <= 1 || disabled}
                        buttonStyle={styles.button}
                        title="-"
                        titleStyle={styles.textIcon}
                    />
                    <View style={styles.input}>
                        <TextInput
                            style={styles.textInput}
                            keyboardType="numeric"
                            maxLength={3}
                            maxValue={999}
                            minValue={1}
                            onChangeText={(number) => {
                                this.handleTextInput(number);
                            }}
                            defaultValue={'' + qty}
                            value={'' + qty}
                        />
                    </View>
                    <Button
                        onPress={this.Increase}
                        disabled={qty >= 999 || disabled}
                        buttonStyle={styles.button}
                        title="+"
                        titleStyle={styles.textIcon}
                    />
                </View>
            );
        } else {
            const { disabled } = this.props;
            if (stock === 0) {
                return (
                    <View style={styles.stockContainer}>
                        <Text style={styles.stockTitle}>Stock Habis</Text>
                    </View>
                );
            }

            return (
                <View style={styles.container}>
                    <Button
                        onPress={this.Decrease}
                        disabled={qty <= 1 || disabled}
                        buttonStyle={styles.button}
                        title="-"
                        titleStyle={styles.textIcon}
                    />
                    <View style={styles.input}>
                        <TextInput
                            style={styles.textInput}
                            keyboardType="numeric"
                            maxLength={3}
                            maxValue={999}
                            minValue={1}
                            onChangeText={(number) => {
                                this.handleTextInput(number);
                            }}
                            defaultValue={'' + qty}
                            value={'' + qty}
                        />
                    </View>
                    <Button
                        onPress={this.Increase}
                        disabled={qty >= 999 || disabled}
                        buttonStyle={styles.button}
                        title="+"
                        titleStyle={styles.textIcon}
                    />
                </View>
            );
        }
    }

    render() {
        return <View>{this._renderButton()}</View>;
    }
}

CartQuantity.propTypes = {
    quantity: PropTypes.number,
    delay: PropTypes.number,
    disabled: PropTypes.bool,
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        width: 37,
        height: 37,
        borderRadius: 50,
        backgroundColor: COLOR.secondary,
        zIndex: 2,
        padding: 0,
    },
    textIcon: {
        fontFamily: 'NunitoSans-ExtraBold',
        color: COLOR.white,
        fontSize: FONT_SIZE.MEDIUM,
        fontWeight: FONT_SIZE.FONT_WEIGHT_BOLD,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    input: {
        width: 66,
        height: 37,
        backgroundColor: COLOR.backgroundCart,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: -15,
        marginRight: -15,
    },
    textInput: {
        color: COLOR.black,
        textAlign: 'center',
    },
    stockContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLOR.danger,
        borderRadius: 5,
        width: 109,
        height: 30,
    },
    stockTitle: {
        fontFamily: 'NunitoSans-ExtraBold',
        textAlign: 'center',
        color: COLOR.white,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
});
