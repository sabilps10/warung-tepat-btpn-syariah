import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, BackHandler, Keyboard, Text, ScrollView, TouchableOpacity, Image, AsyncStorage, Linking } from 'react-native';
import { View } from 'react-native-animatable';

import FormHeader from './com/FormHeader';
import { getData } from './com/FunctionStorage';
import { OpeningAccountsService } from './rx/service.openingAccounts';

import iconOption from '@assets/icons/icon_eform_option.png';
import iconNew from '@assets/icons/icon_eform_report.png';
import iconDraft from '@assets/icons/icon_eform_draft.png';
import iconHistory from '@assets/icons/icon_eform_time.png';
import { CUSTCOLOR, MARKOT, NUNITO } from './common/styles';
import { withNavigation } from 'react-navigation';

import { getAllMasterDatas } from './common/masters';
import { useSelector } from 'react-redux';

const EformScreen = ({navigation}) => {
    const [draftCount, setDraftCount] = useState(0);
    const [returnCount, setReturnCount] = useState(0);
    const SESSION = useSelector(store => store.Auth.session);
    const userId = SESSION?.user?.response_login_by_nik?.userId;
    
    MenuButton = (props) => {
        return (
            <TouchableOpacity 
                activeOpacity={0.9}
                style={styles.menuButton}
                onPress={props.onPress}
            >
                <Image source={props.iconSource} style={{width: 24, height: 24, marginRight: 8}} />
                <Text style={styles.menuButtonText}>{props.text}</Text>
                {
                    props.badge > 0 &&
                    <View animation="bounceIn" style={styles.badge} useNativeDriver>
                        <Text style={styles.badgeText}>{props.badge}</Text>
                    </View>
                }
            </TouchableOpacity>
        )
    }
    
    const getParamDraft = async () => {
        const count = navigation.getParam('draftCount')

        if (count == null) return false
        return count
    }

    const getDraftCount = async () => {
        let data = await getParamDraft();
        let datas = [];
        let filteredData = [];

        if (!data) data = await getData('@dataDraft');
        if (data != false) datas = Object.values(data);

        datas.map(item => {
            if (item.Created_By == userId) filteredData.push(item)
        })
        
        setDraftCount(filteredData.length)
    }

    const getHistory = (query, id='', userId) => {
        return OpeningAccountsService.getOpeningAccountsHistory(query, id, userId)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return error;
            })
            .finally((res) => {
                return res;
            });
    }

    const getReturnCount = async () => {
        const queries = {
            query: '',
            status: 'VR_RETUR',
            rowOffSet: 0,
            maxRowCount: 0
        }
        
        const items = await getHistory(queries, '', userId);
        
        if (items) setReturnCount(items.totalItems)
    }

    useEffect(() => {
        // setForm({})
        // removeAllData('@dataDraft')
        focusListener = navigation.addListener("didFocus", () => {
            getDraftCount()
            // getDraftCount()
            getReturnCount()
        });

        raiseProgress()
        console.log('SESSION : ', SESSION)
        
        return () => {
            setDraftCount(0)
        }
    }, [])

    const [progress, setProgress] = useState(0)

    const raiseProgress = (value) => {
        const rand = Math.ceil(Math.random() * 20) 
        if (value < 100) {
            setTimeout(() => {
                value += rand
                setProgress(value)
            }, 10)
        }
    }

    const getMastersData = async () => {
        global.MASTER = await getAllMasterDatas()
    } 

    useEffect(() => {
        if (global.MASTER == null) {
            getMastersData()
        }
    }, [global])

    return (
        <View style={{height: '100%', backgroundColor: '#dadada', justifyContent: 'space-between'}}>
        <FormHeader 
            text="Pendaftaran Calon Mitra Tepat" 
            rightButton={(
                <TouchableOpacity
                    // onPress={() => }
                >
                    <Image source={iconOption} style={{width: 24, height: 24}} />
                </TouchableOpacity>
            )}
        />
        <ScrollView style={{paddingTop: 16}}>
            <MenuButton 
                text="Input Data Baru"
                iconSource={iconNew}
                onPress={() => navigation.navigate('EformNewInput')}
                // onPress={() => navigation.navigate('AddForm1')} // phase 1
                />
            <MenuButton 
                iconSource={iconDraft}
                text="Data Draft"
                onPress={() => {
                    navigation.navigate('EformDraftList')
                }}
                badge={draftCount}
                />
            <MenuButton 
                iconSource={iconHistory}
                text="Histori Input Data"
                onPress={() => navigation.navigate('EformHistory')}
                badge={returnCount}
            />
        </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    menuButton: {
        height: 56,
        backgroundColor: '#fff',
        borderRadius: 8,
        alignItems: 'center',
        flexDirection: 'row',
        padding: 16,
        marginVertical: 8,
        marginHorizontal: 24,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 4,
    },
    menuButtonText: {
        fontFamily: MARKOT.bold,
        color: CUSTCOLOR.textDark,
        fontSize: 18,
        lineHeight: 24,
        flex: 1
    },
    badge: {
        width: 27,
        height: 27,
        borderRadius: 27,
        backgroundColor: '#ef3636',
        justifyContent: 'center',
        alignItems: 'center'
    },
    badgeText: {
        fontFamily: NUNITO.bold,
        color: '#fff',
        fontSize: 14,
        lineHeight: 22
    }
})

export default withNavigation(EformScreen)
