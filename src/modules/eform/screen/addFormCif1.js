import React, { useState, useEffect } from 'react';
import { 
    StyleSheet, 
    Text, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Alert, 
    ToastAndroid 
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native-animatable';

import FormHeader from '../com/FormHeader';
import Pagination from '../com/Pagination';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import HeaderText from '../com/HeaderText';
import FormField from '../com/FormField';
import InputText from '../com/InputText';

import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';
import { Button } from 'react-native-elements';
import { COLOR } from '../../../common/styles';
import { NUNITO } from '../common/styles';
import { SmsService } from '../rx/service.sms';
import Config from '../common/config';

const AddFormCif1 = ({route, navigation}) => {
    const [loading, setLoading] = useState(false)
    
    const form = useEform({});
    const updateForm = useUpdateEform();
    const setForm = useSetEform();

    const [warning, setWarning] = useState({
        Phone_No: '',
        // Email: ''
    });

    useEffect(() => {

    }, [])

    const updateWarning = async (value, index) => {
        setWarning(current => ({
            ...current,
            [index]: value
        }))
        console.log('update', value, index, warning)
    }

    const sendSms = async (data) => {    
        return SmsService.sendSms(data)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return error;
            })
            .finally((res) => {
                return res;
            });
    }

    const onSendSms = async (phone) => {
        const phoneWarning = await validatePhone()
        if (phoneWarning == false) return false

        let body = {
            "phone": phone,
            "message": "Link Ujian Keagenan : https://bit.ly/LatihanMitraTepat" //maksimum 160 character
        }

        const smsResponse = await sendSms(body)

        if (smsResponse.code) {
            Alert.alert(
                'Gagal Mengirim SMS',
                `${smsResponse.message}`,
            )
        } else {
            ToastAndroid.show(
                'SMS berhasil dikirimkan',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        }
    }

    const validatePhone = async () => {
        const patt = /^\d+$/;

        if (form.Phone_No == undefined || form.Phone_No == '') {
            updateWarning('No HP harus diisi', 'Phone_No')
            return false
        } else if (!form.Phone_No.match(patt)) {
            updateWarning('No HP hanya boleh berupa angka', 'Phone_No')
            return false
        } else {
            updateWarning('', 'Phone_No')
            return true
        }
    }

    const validateEmail = async () => {
        const patt = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (form.Email == undefined || form.Email == '') {
            updateWarning('Alamat Email harus diisi', 'Email')
            return false
        } else if (!patt.test(form.Email)) {
            updateWarning('Format Email tidak sesuai', 'Email')
            return false
        } else {
            updateWarning('', 'Email')
            return true
        }
    }

    const handleNextPage = async (page) => {
        await setWarning({})

        // const emailWarning = await validateEmail()
        const phoneWarning = await validatePhone();

        if (phoneWarning) navigation.navigate(page);
        if (!phoneWarning) {
            ToastAndroid.show(
                'Mohon periksa kembali data yang Anda masukkan',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        }
    }

    const exitToMain = () => {
        setForm({})         
        navigation.navigate('Eform')
    }

    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => exitToMain()}
            >
                <Pagination active={1} pageText={['1', '2', '3']} />
            </FormHeader>
            <ScrollView>
            <View style={{padding: 24}}>
                <HeaderText>Informasi Kontak Calon Mitra Tepat</HeaderText>
                <FormField
                    text="Nomor Handphone"
                    info="Nomor dipastikan masih aktif"
                    warning={warning.Phone_No}
                >
                    <InputText
                        keyboardType="phone-pad"
                        placeholder="0800 0000 0000"
                        value={form.Phone_No}
                        onChangeText={(value) => updateForm(value, 'Phone_No')}
                    />
                </FormField>
                <FormField
                    text="Alamat E-mail"
                    // warning={warning.Email}
                >
                    <InputText
                        keyboardType="email-address"
                        placeholder={Config.agentDefaultEmail}
                        value={form.Email}
                        onChangeText={(value) => updateForm(value, 'Email')}
                    />
                </FormField>
                <View style={styles.urlContainer}>
                    <Button 
                        type="clear"
                        titleStyle={styles.url}
                        title="Kirim SMS Link Pelatihan"
                        onPress={() => onSendSms(form.Phone_No)}
                        // disabled={loading}
                        // disabled={true}
                    />
                </View>
                </View>
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 28}}></View>
                    <View style={{flex: 1}}>
                        <ButtonPrimary text="Lanjut" 
                            onPress={() => handleNextPage('AddFormCif2')}
                            // onPress={() => handleNextPage('EformCifPks')}
                        />
                    </View>
                </View>
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    urlContainer: {
        alignItems: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: NUNITO.extraBold,
        fontSize: 14,
    },
})

export default AddFormCif1
