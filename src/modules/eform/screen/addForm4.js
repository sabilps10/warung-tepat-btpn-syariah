import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  Alert,
  ToastAndroid
} from 'react-native';

import { View } from 'react-native-animatable';

import FormHeader from '../com/FormHeader';
import Pagination from '../com/Pagination';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import ButtonSecondary from '../com/ButtonSecondary';
import ButtonPhotoUpload from '../com/ButtonPhotoUpload';
import HeaderText from '../com/HeaderText';
import FormField from '../com/FormField';
import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';

import { AcquisitionService } from '../rx/service.acquisition';
import { removeData } from '../com/FunctionStorage';
import { useSelector } from 'react-redux';

const AddForm4 = ({route, navigation}) => {
    const [loading, setLoading] = useState(false)
    const [warning, setWarning] = useState({})
    const SESSION = useSelector(store => store.Auth.session)

    const form = useEform();
    const updateForm = useUpdateEform();
    const setForm = useSetEform();

    const editAcquisition = (data, id, userId) => {    
        return AcquisitionService.putAcquisition(data, id, userId)
            .then((res) => {
    
                console.log('editAcquisition res')
                console.log(res)
                return res;
            })
            .catch((error) => {
                
                console.log('editAcquisition error', error)
                return [false, error];
            })
            .finally((res) => {
    
                console.log('editAcquisition finally')
                console.log(res)
                return res;
            });
    }

    const onSubmitForm = async (data, id) => {
        setLoading(true);
        const userId = Number(SESSION.user.response_login_by_nik.userId);
        const userPhone = SESSION.user.response_login_by_nik.phone;

        let renderDate = (value) => {
            if (value != 'undefined') {
                let dateArray = (value).split('/')
    
                return `${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`
            }
        }
    
        let dataIdExpDate = () => {
            if (data.id_card_lifetime) {
                return '2100-12-31'
            } else {
                return renderDate(data.Id_Card_Exp_Date)
            }
        } 

        let renderGrossIncome = (value) => {
            const thousandSeparator = '.';
            const thousandSeparatorPatt = new RegExp(`\\${thousandSeparator}`, 'g')

            const result = value.replace(thousandSeparatorPatt, '')

            return Number(result)
        }
        
        let body = {
            "AppAcqCode": "Hello",
            "AppAcqReferralBankClerkNoHp": userPhone,
            "AppAcqFullname": data.Name,
            "AppAcqHp": String(data.Phone_No),
            "AppAcqEmail": data.Email,
            "AppAcqIdCardType": data.Id_Card_Type,
            "AppAcqIdCardNo": String(data.Id_Card_No),
            "AppAcqIdCardExpiredDate": dataIdExpDate(),
            "AppAcqNpwp": data.Is_Tax_Exempted && data.Tax_Card_Number != '' ? data.Tax_Card_Number : null,
            "AppAcqPob": data.Place_Of_Birth,
            "AppAcqDob": renderDate(data.Date_Of_Birth),
            "AppAcqSex": data.Gender,
            "AppAcqAnnualEstimatedDebitTrans": data.Forecast_Transaction,
            "AppAcqAddress": data.Street,
            "AppAcqResidentialAddress": data.is_current_address ? data.Street : data.Street_2,
            "AppAcqRt": Number(data.RT),
            "AppAcqRw": Number(data.RW),
            "AppAcqProvince": data.Province,
            "AppAcqCity": data.City,
            "AppAcqDistrict": data.District,
            "AppAcqSubDistrict": data.Subdistrict,
            "AppAcqZipcode": String(data.Zip_Code),
            "AppAcqReligion": data.Religion,
            "AppAcqMaritalStatus": data.Marital_Status,
            "AppAcqNationality": 'Indonesia',
            "AppAcqMotherMaidenName": data.Mother_Maiden_Name,
            "AppAcqEducation": data.Education,
            "AppAcqLatlongBusinessLocation": data.Location,
            "AppAcqOccupation": data.Occupation,
            "AppAcqOccupationPosition": data.Job,
            "AppAcqBusinessField": Number(data.Industry_Sector_of_Employer),
            "AppAcqIncomeSource": data.Source_of_Fund,
            "AppAcqMonthlyAvgIncome": data.Income,
            "AppAcqAnnualGrossIncome": renderGrossIncome(data.Annual_Income),
            "AppAcqOpeningAccountPurpose": data.Purpose_Of_Account,
            "AppAcqBankRelationship": data.Relation,
            "AppAcqSelfieDocPath": data.AppAcqSelfieDocPath,
            "AppAcqBitaddDocPath": data.AppAcqBitaddDocPath,
            "AppAcqPaofDocPath": data.AppAcqPaofDocPath,
            "AppAcqNpwpDocPath": data.AppAcqNpwpDocPath,
            "AppAcqStatusApproval": data.Status_Approval,
            // "AppAcqSignatureDocPath": data.AppAcqSignatureDocPath,
            "AppAcqSignatureDocPath": 'pathString',
            "AppAcqIdCardDocPath": data.AppAcqIdCardDocPath,
            "AppAcqBusinessCertDocPath": data.AppAcqBusinessCertDocPath,
            "AppAcqOtp": 100,
            "CifCode": data.CifCode
        };

        console.log('body', body);
        
        const insertData = await editAcquisition(body, id, userId);
        console.log(insertData);

        if (typeof insertData == 'object' && insertData[0] == false) {
            setLoading(false)
            
            Alert.alert(
                'Gagal',
                'Terjadi kesalahan. ' + insertData[1],
            )

            return false
        }

        if (insertData.message != null && insertData.message != undefined) {
            Alert.alert(
                'Gagal',
                `Gagal memproses data. ${insertData.message}`,
            )
    
            setLoading(false)
        } else {
            setLoading(false);
            setForm({})
            removeDraft(id, '@dataDraft');
            navigation.navigate('EformSubmitted')
        }
    }

    const removeDraft = async (index, storageKey) => {
        console.log('delete ', index)
        await removeData(index, storageKey);
    }

    const updateWarning = async (value, index) => {
        setWarning(current => ({
            ...current,
            [index]: value
        }))
    }

    const validateIsNull = async (field) => {
        if (form[field] == undefined || form[field] == '' || form[field] == null) {
            updateWarning(`Dokumen harus terlampir`, field)
            return false
        } else {
            updateWarning('', field)
            return true
        }
    }

    const onValidate = async () => {
        const IdCardDocPath = await validateIsNull('AppAcqIdCardDocPath')
        const SelfieDocPath = await validateIsNull('AppAcqSelfieDocPath')
        const BusinessCertDocPath = await validateIsNull('AppAcqBusinessCertDocPath')
        const BitaddDocPath = await validateIsNull('AppAcqBitaddDocPath')
        const PaofDocPath = await validateIsNull('AppAcqPaofDocPath')

        const validate = [
            IdCardDocPath,
            SelfieDocPath,
            BusinessCertDocPath,
            BitaddDocPath,
            PaofDocPath,
        ]

        return validate.every(item => item == true)
    }

    const handleNextPage = async (page) => {     
        await setWarning({})

        const canContinue = await onValidate()
        
        if (canContinue) {
            if (form.repairFormMode) {
                onSubmitForm(form, form.Index)
            } else {
                navigation.navigate(page)
            }
        } else {
            ToastAndroid.show(
                'Mohon periksa kembali data yang Anda masukkan',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        }
    }

    useEffect(() => {
        console.log('form', form)
    }, [])
    
    return (
        <View style={{height: '100%'}}>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => navigation.navigate('Eform')}
                closeToSave={true}
            >
                <Pagination active={4} pageText={['1', '2', '3', '4']} />
            </FormHeader>
            <ScrollView style={{height: '100%', backgroundColor: '#fff'}}>
                <View style={{padding: 24, flex: 1}}>
                    <HeaderText>Dokumen Foto</HeaderText>
                    <FormField
                        // AppAcqIdCardDocPath
                        // AppAcqSignatureDocPath
                        text="Dokumen KTP dan Tanda Tangan Basah"
                        warning={warning.AppAcqIdCardDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqIdCardDocPath}
                            imageNamePrefix="AppAcqIdCardDocPath"
                            title="Dokumen KTP dan Tanda Tangan Basah"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqIdCardDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqSelfieDocPath
                        text="Foto Diri Calon Mitra Tepat"
                        info="Wajah calon Mitra Tepat melihat kedepan"
                        warning={warning.AppAcqSelfieDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqSelfieDocPath}
                            imageNamePrefix="AppAcqSelfieDocPath"
                            title="Foto Diri Calon Mitra Tepat"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqSelfieDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqBusinessCertDocPath
                        text="Surat Keterangan Usaha"
                        warning={warning.AppAcqBusinessCertDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqBusinessCertDocPath}
                            imageNamePrefix="AppAcqBusinessCertDocPath"
                            title="Surat Keterangan Usaha"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqBusinessCertDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqBitaddDocPath
                        text="Bast Pelaksanaan Pelatihan &amp; Ujian Kelayakan Mitra Tepat"
                        warning={warning.AppAcqBitaddDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqBitaddDocPath}
                            imageNamePrefix="AppAcqBitaddDocPath"
                            title="Bast Pelaksanaan Pelatihan &amp; Ujian Kelayakan Mitra Tepat"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqBitaddDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqPaofDocPath
                        text="Form Observasi Calon Mitra Tepat"
                        warning={warning.AppAcqPaofDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqPaofDocPath}
                            imageNamePrefix="AppAcqPaofDocPath"
                            title="Form Observasi Calon Mitra Tepat"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqPaofDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqNpwpDocPath
                        text="NPWP (optional)"
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqNpwpDocPath}
                            imageNamePrefix="AppAcqNpwpDocPath"
                            title="NPWP (optional)"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqNpwpDocPath')}
                        />
                    </FormField>
                </View>
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 28}}>
                        <ButtonSecondary 
                            text="Kembali" 
                            onPress={() => navigation.navigate('AddForm3')} 
                        />
                    </View>
                    <View style={{flex: 1}}>
                        <ButtonPrimary 
                            loading={loading}
                            disabled={loading}
                            text={form.repairFormMode ? 'Submit' : 'Lanjut'} 
                            // onPress={() => handleNextPage('EformTnC')} 
                            onPress={() => handleNextPage('EformPksPhase3')} 
                        />  
                    </View>
                </View>
            </EformFooter>
        </View>
    )
}

export default AddForm4