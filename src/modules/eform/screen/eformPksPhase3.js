import React, { useState } from 'react';
import { StyleSheet, Text, KeyboardAvoidingView, Alert, ToastAndroid } from 'react-native';
import { View } from 'react-native-animatable';
import { COLOR } from '@common/styles';

import FormHeader from '../com/FormHeader';
import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import ButtonPrimary from '../com/ButtonPrimary';

import { Button } from 'react-native-elements';
import { useEform, useSetEform } from '../context/EformContext';
import { useSelector } from 'react-redux';
import { SmsService } from '../rx/service.sms';
import { AcquisitionService } from '../rx/service.acquisition';
import { removeData } from '../com/FunctionStorage';
import Config from '../common/config';

const EformPksPhase3 = ({navigation}) => {
    const [loading, setLoading] = useState(false);
    const SESSION = useSelector(store => store.Auth.session);

    const form = useEform();
    const setForm = useSetEform();

    const sendSmsLink = async (data, userId) => {    
        return SmsService.sendSmsLink(data, userId)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return error;
            })
            .finally((res) => {
                return res;
            });
    }

    const insertAcquisitionTemporary = async (data, userId) => {    
        return AcquisitionService.postAcquisitionTemporary(data, userId)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return error;
            })
            .finally((res) => {
                return res;
            });
    }

    // const getSelectedFromText  = (value, data) => {
    //     let selected = data.find( element => element['text'].toLowerCase() == value.toLowerCase())

    //     if (selected != null) return selected.value;
    // }

    const submitTemporaryAcquisition = async (data, userId) => {
        const userPhone = SESSION.user.response_login_by_nik.phone;

        let renderDate = (value) => {
            if (value != 'undefined') {
                let dateArray = (value).split('/')
    
                return `${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`
            }
        }
    
        let dataIdExpDate = () => {
            if (data.id_card_lifetime) {
                return '2100-12-31'
            } else {
                return renderDate(data.Id_Card_Exp_Date)
            }
        } 

        let renderGrossIncome = (value) => {
            const thousandSeparator = '.';
            const thousandSeparatorPatt = new RegExp(`\\${thousandSeparator}`, 'g')

            const result = value.replace(thousandSeparatorPatt, '')

            return Number(result)
        }
        
        let body = {
            "AppAcqCode": "Hello",
            "AppAcqReferralBankClerkNoHp": userPhone,
            "AppAcqFullname": data.Name,
            "AppAcqHp": String(data.Phone_No),
            "AppAcqEmail": data.Email ? data.Email : Config.agentDefaultEmail,
            "AppAcqIdCardType": data.Id_Card_Type,
            "AppAcqIdCardNo": String(data.Id_Card_No),
            "AppAcqIdCardExpiredDate": dataIdExpDate(),
            "AppAcqNpwp": data.Is_Tax_Exempted && data.Tax_Card_Number != '' ? data.Tax_Card_Number : null,
            "AppAcqPob": data.Place_Of_Birth,
            "AppAcqDob": renderDate(data.Date_Of_Birth),
            "AppAcqSex": data.Gender,
            "AppAcqAnnualEstimatedDebitTrans": data.Forecast_Transaction,
            "AppAcqAddress": data.Street,
            "AppAcqResidentialAddress": data.is_current_address ? data.Street : data.Street_2,
            "AppAcqRt": Number(data.RT),
            "AppAcqRw": Number(data.RW),
            "AppAcqProvince": data.Province,
            "AppAcqCity": data.City,
            "AppAcqDistrict": data.District,
            "AppAcqSubDistrict": data.Subdistrict,
            "AppAcqZipcode": String(data.Zip_Code),
            "AppAcqReligion": data.Religion,
            "AppAcqMaritalStatus": data.Marital_Status,
            "AppAcqNationality": 'Indonesia',
            "AppAcqMotherMaidenName": data.Mother_Maiden_Name,
            "AppAcqEducation": data.Education,
            "AppAcqLatlongBusinessLocation": data.Location,
            "AppAcqOccupation": data.Occupation,
            "AppAcqOccupationPosition": data.Job,
            "AppAcqBusinessField": Number(data.Industry_Sector_of_Employer),
            "AppAcqIncomeSource": data.Source_of_Fund,
            "AppAcqMonthlyAvgIncome": data.Income,
            "AppAcqAnnualGrossIncome": renderGrossIncome(data.Annual_Income),
            "AppAcqOpeningAccountPurpose": data.Purpose_Of_Account,
            "AppAcqBankRelationship": data.Relation,
            "AppAcqSelfieDocPath": data.AppAcqSelfieDocPath,
            "AppAcqBitaddDocPath": data.AppAcqBitaddDocPath,
            "AppAcqPaofDocPath": data.AppAcqPaofDocPath,
            "AppAcqNpwpDocPath": data.AppAcqNpwpDocPath,
            "AppAcqStatusApproval": data.AppAcqStatusApproval,
            "AppAcqSignatureDocPath": 'pathString',
            "AppAcqIdCardDocPath": data.AppAcqIdCardDocPath,
            "AppAcqBusinessCertDocPath": data.AppAcqBusinessCertDocPath,
        };

        console.log('body', body)
        
        const insertData = await insertAcquisitionTemporary(body, userId);

        console.log('insert acq temp, phase 3', insertData)

        return insertData;
    }

    const onSendSms = async (data) => {
        setLoading(true);
        const userId = SESSION.user.response_login_by_nik.userId
        
        let body = {
            "phone": String(data.Phone_No)
        };

        const insertData = await submitTemporaryAcquisition(data, userId)
        console.log('status code insert data', insertData)
        
        if (insertData.code) {
            setLoading(false)
            
            Alert.alert(
                'Gagal Input Data',
                `${insertData.message}`,
            )
            setLoading(false)

            return false
        }

        const sendSms = await sendSmsLink(body, userId)
        console.log('send sms', sendSms)

        if (sendSms.code) {
            Alert.alert(
                'Gagal Mengirim SMS',
                `${sendSms.message}`,
            )
    
            setLoading(false)
        } else {
            setLoading(false);
            setForm({})
            removeDraft(data.Index, '@dataDraft');
            navigation.navigate('EformCifSubmitted')
        }
    }

    const removeDraft = async (index, storageKey) => {
        console.log('delete ', index)
        await removeData(index, storageKey);
    }

    const exitToMain = () => {
        setForm({})         
        navigation.navigate('Eform')
    }

    return (
        <View animation="fadeIn" style={styles.container} useNativeDriver>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => exitToMain()}
                closeToSave={true}
            />
            <KeyboardAvoidingView 
                style={styles.bodyContainer} 
                enabled={false} 
                behavior="height"
            >
                <View style={{flex: 1}}>
                    <View style={styles.screenHeader}>
                        <Text style={styles.screenHeaderText}>Kirim Syarat Ketentuan,</Text>
                        <Text style={styles.screenHeaderText}>Dan Surat Perjanjian</Text>
                    </View>
                    <View style={styles.bodyTextContainer}>
                        <Text style={styles.text}>Untuk proses selanjutnya seperti syarat dan ketentuan, dan Surat Perjanjian akan dikirim melalui SMS ke nomor telepon calon Mitra Tepat.</Text>
                    </View>
                    {/* <View style={styles.urlContainer}>
                        <Button 
                            type="clear"
                            titleStyle={styles.url}
                            title="Kirim Ulang" 
                            onPress={() => onSendSms(form)}
                            disabled={loading}
                        />
                    </View> */}
                </View>
                <ButtonPrimary 
                    text="Kirim SMS" 
                    onPress={() => onSendSms(form)} 
                    // onPress={() => navigation.navigate('EformCifSubmitted')} 
                    disabled={loading}
                    loading={loading}
                />
            </KeyboardAvoidingView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    bodyContainer: {
        flex: 1,
        paddingTop: 32,
        paddingBottom: 16,
        paddingHorizontal: 24,
    },
    screenHeader: {
        marginBottom: 32
    },
    screenHeaderText: {
        fontSize: 24,
        lineHeight: 36,
        color: CUSTCOLOR.textDark,
        fontFamily: MARKOT.book
    },
    bodyTextContainer: {
        marginBottom: 40,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
    },
    urlContainer: {
        alignItems: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: NUNITO.extraBold,
        fontSize: 14,
    },
    verifButton: {
        bottom: 16,
        zIndex: 10, 
        borderWidth: 2,
        borderColor: 'red'
    }
});


export default EformPksPhase3;
