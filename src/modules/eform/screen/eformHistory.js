import React, { Component, useCallback, useEffect, useState } from 'react';
import { StyleSheet, Text, Image, Dimensions, TouchableOpacity, Alert, RefreshControl } from 'react-native';
import { View } from 'react-native-animatable';
import { COLOR } from '@common/styles';

import FormHeader from '../com/FormHeader';
import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import ButtonSecondary from '../com/ButtonSecondary';
import { FlatList, ScrollView, TextInput } from 'react-native-gesture-handler';

import iconLeft from '@assets/icons/icon_eform_arrow_left.png';
import iconSearch from '@assets/icons/icon_eform_search.png';
import iconCloseCircle from '@assets/icons/icon_eform_close_circle.png';
import { OpeningAccountsService } from '../rx/service.openingAccounts';
import { AcquisitionService } from '../rx/service.acquisition';
import { useSetEform, useUpdateEform } from '../context/EformContext';
import { useSelector } from 'react-redux';


const getHistory = (query, id='', userId) => {
    // console.log('insertAcquisition start')
    return OpeningAccountsService.getOpeningAccountsHistory(query, id, userId)
        .then((res) => {
            return res;
        })
        .catch((error) => {
            return error;
        })
        .finally((res) => {
            return res;
        });
}

const getAcquisition = (query, id='') => {
    // console.log('insertAcquisition start')
    return AcquisitionService.getAcquisition(query, id)
        .then((res) => {
            return res;
        })
        .catch((error) => {
            return error;
        })
        .finally((res) => {
            return res;
        });
}

const DraftItem = ({item, onRepair}) => {
    const [statusObj, setStatusObj] = useState({})
    
    const renderStatus = (status) => {
        if (status == 'VR_PROCESS') {
            setStatusObj({
                style: {backgroundColor: '#0092e6'},
                text: 'Dalam Proses'
            })
        } else if (status == 'VR_RETUR') {
            setStatusObj({
                style: {backgroundColor: '#024042'},
                text: 'Perbaikan'
            })
        } else if (status == 'VR_APPROVE') {
            setStatusObj({
                style: {backgroundColor: '#00c781'},
                text: 'Disetujui'
            })
        } else if (status == 'VR_REJECTED') {
            setStatusObj({
                style: {backgroundColor: '#ef3636'},
                text: 'Ditolak'
            })
        }
    }

    const renderDate = (date) => {
        let dateArray = date.substring(0, 10).split('-');

        return `${dateArray[0]}-${dateArray[1]}-${dateArray[2]}`;
    }

    useEffect(() => {
        renderStatus(item.AppAcqStatusVerification)
    }, [])

    return (
        <View style={styles.itemCard} animation="zoomIn" duration={500} useNativeDriver>
            <View style={styles.flexContainer}>
                <View>
                    <Text style={[styles.itemSubtext, {marginBottom: 8}]}>{renderDate(item.CreatedAt)}</Text>
                    <Text style={styles.itemHeader}>{item.AppAcqFullname}</Text>
                    <Text style={styles.itemSubtext}>{item.AppAcqHp}</Text>
                </View>
                <View>
                    <Text 
                        style={[styles.itemStatus, statusObj.style]}>
                        {statusObj.text}
                    </Text>
                    <Text style={[styles.itemSubtext, styles.updatedDate]}>{renderDate(item.UpdatedAt)}</Text>
                </View>
            </View>
            {
                (item.AppAcqNote != '' && item.AppAcqStatusVerification != 'VR_APPROVE') && 
                    <View style={{marginTop: 14}}>
                        <Text style={styles.itemSubtext}>Note</Text>
                        <Text style={styles.itemText}>{item.AppAcqNote}</Text>
                    </View>
            }
            {
                item.AppAcqStatusVerification == 'VR_RETUR' &&
                    <View style={{marginTop: 16}}>
                        <ButtonSecondary text="Perbaikan Data" 
                            onPress={() => onRepair(item.AppAcqId)}
                        />
                    </View>
            }
        </View>
    )
}

const FilterButton = (props) => {
    const [selected, setSelected] = useState(false);

    const containerStyle = {
        borderColor: selected ? '#ff7c00' : COLOR.off,
        backgroundColor: selected ? '#fff7e0' : '#fff'
    }

    const textStyle = {
        color: selected ? '#ff7c00' : COLOR.off
    }

    const handlePress = () => {
        setSelected(true)
        props.onPress(props.filterKey)
    }

    useEffect(() => {
        if (props.filterKey == props.curFilter) {
            setSelected(true)
        }
    }, [])

    useEffect(() => {
        if (props.filterKey !== props.curFilter) {
            setSelected(false)
        }
    }, [props.curFilter])
    
    return (
        <TouchableOpacity 
            activeOpacity={0.9}
            style={[styles.filterButton, containerStyle]}
            onPress={() => handlePress()}
        >
            <Text style={[styles.filterText, textStyle]}>{props.text}</Text>
        </TouchableOpacity>
    )
}

const EformHistory = ({navigation}) => {
    const [data, setData] = useState([]);
    const [endOfRow, setEndOfRow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [rowOffset, setRowOffset] = useState(MAX_ROW_COUNT);
    const [filter, setFilter] = useState("VR_ALL");
    const [query, setQuery] = useState("");
    const MAX_ROW_COUNT = 10;
    const SESSION = useSelector(store => store.Auth.session);

    const setForm = useSetEform();
    const updateForm = useUpdateEform();

    const getItems = async (query, status, rowOffSet, maxRowCount) => {
        const queries = {
            query,
            status,
            rowOffSet,
            maxRowCount
        };

        const userId = SESSION?.user?.response_login_by_nik?.userId;
        console.log('user id :', userId)
        
        const items = await getHistory(queries, '', userId);
    
        console.log('items==', items.data)
        return items.data;
    };

    const parseDate = (dateStr) => {
        dateStr = dateStr.substring(0, 10);
        const dateObj = new Date(dateStr);

        const strPadStart = (str) => {
            return String(str).padStart(2, '0');
        }
        
        const strDate = strPadStart(dateObj.getDate());
        const strMonth = strPadStart(dateObj.getMonth() + 1);
        const strYear = dateObj.getFullYear();

        console.log('parse date', strDate, dateObj.getDate());

        return `${strDate}/${strMonth}/${strYear}`;
    }

    const getItem = async (id) => {
        const queries = {id}
        
        const item = await getAcquisition(queries, id);
    
        // console.log('items==', item)

        if (item == false) {
            return false
        } else if (item != null) {
            console.log('exp date', parseDate(item.AppAcqIdCardExpiredDate))
            let idCardLifetime = parseDate(item.AppAcqIdCardExpiredDate) == '31/12/2100' ? true : false;

            let data = {
                // "Hello": item.AppAcqCode,
                Created_By: item.CreatedBy,
                Referral_No_Hp: item.AppAcqReferralBankClerkNoHp,
                Index: item.AppAcqId,
                Name: item.AppAcqFullname,
                Phone_No: item.AppAcqHp,
                Email: item.AppAcqEmail,
                Id_Card_Type: item.AppAcqIdCardType,
                Id_Card_No: item.AppAcqIdCardNo,
                Id_Card_Exp_Date: idCardLifetime ? '' : parseDate(item.AppAcqIdCardExpiredDate),
                Tax_Card_Number: item.AppAcqNpwp,
                Place_Of_Birth: item.AppAcqPob,
                Date_Of_Birth: parseDate(item.AppAcqDob),
                Gender: item.AppAcqSex,
                Forecast_Transaction: item.AppAcqAnnualEstimatedDebitTrans,
                Street: item.AppAcqAddress,
                Street_2: item.AppAcqResidentialAddress,
                RT: String(item.AppAcqRt),
                RW: String(item.AppAcqRw),
                Province: item.AppAcqProvince,
                City: item.AppAcqCity,
                District: item.AppAcqDistrict,
                Subdistrict: item.AppAcqSubDistrict,
                Zip_Code: item.AppAcqZipcode,
                Religion: item.AppAcqReligion,
                Marital_Status: item.AppAcqMaritalStatus,
                // 'Indonesia': item.AppAcqNationality,
                Mother_Maiden_Name: item.AppAcqMotherMaidenName,
                Education: item.AppAcqEducation,
                Location: item.AppAcqLatlongBusinessLocation,
                Occupation: item.AppAcqOccupation,
                Job: item.AppAcqOccupationPosition,
                Industry_Sector_of_Employer: String(item.AppAcqBusinessField),
                Source_of_Fund: item.AppAcqIncomeSource,
                Income: item.AppAcqMonthlyAvgIncome,
                Annual_Income: String(item.AppAcqAnnualGrossIncome),
                Purpose_Of_Account: item.AppAcqOpeningAccountPurpose,
                Relation: item.AppAcqBankRelationship,
                AppAcqSelfieDocPath: item.AppAcqSelfieDocPath,
                AppAcqBitaddDocPath: item.AppAcqBitaddDocPath,
                AppAcqPaofDocPath: item.AppAcqPaofDocPath,
                AppAcqNpwpDocPath: item.AppAcqNpwpDocPath,
                Status_Approval: item.AppAcqStatusApproval,
                AppAcqSignatureDocPath: item.AppAcqSignatureDocPath,
                AppAcqIdCardDocPath: item.AppAcqIdCardDocPath,
                AppAcqBusinessCertDocPath: item.AppAcqBusinessCertDocPath,
    
                is_current_address: item.AppAcqAddress == item.AppAcqResidentialAddress ? true : false,
                id_card_lifetime: idCardLifetime,
                Is_Tax_Exempted: (item.AppAcqNpwp != undefined && item.AppAcqNpwp == null) ? false : true,
                Created_At: item.CreatedAt,
                CifCode: item.CifCode ? item.CifCode : "", 
            }

            console.log('data', data)
    
            return data
        } else {
            return false
        }
        
    }

    const handleFilter = (value) => {
        setFilter(value)
        getInitialData(query, value)
    }

    const onClearSearch = () => {
        searchInputRef.clear()
        setQuery('')
        // searchInputRef.blur()
    }

    const onRepairItem = async (itemId) => {
        // console.log('on repair item')
        const data = await getItem(itemId);
        // console.log('data on repair', data)

        if (data == false) {
            // console.log('data false')
            Alert.alert(
                'Gagal',
                'Gagal mengambil data'
            )
        } else {
            // console.log('data on repair 2', data)
            await setForm(data);
            await updateForm(true, 'repairFormMode')

            navigation.navigate('AddForm1')
        }
    }

    const getInitialData = async (query='', filter='VR_ALL') => {
        setLoading(true)
        setData([])
        const getData = await getItems(query, filter, 0, MAX_ROW_COUNT);
        setData(getData)

        setRowOffset(MAX_ROW_COUNT)
        setEndOfRow(false)
        setLoading(false)
    }

    const onAddData = async (query='', filter) => {
        setLoading(true)
        const getData = await getItems(query, filter, rowOffset, MAX_ROW_COUNT);

        if (getData == null) {
            setEndOfRow(true)
            setLoading(false)
        }

        setData([...data, ...getData])
        setRowOffset(rowOffset + MAX_ROW_COUNT)
        setLoading(false)
    }

    useEffect(() => {
        setLoading(true)
        getInitialData()
    }, [])

    useEffect(() => {
        getInitialData(query, filter)
    }, [query])

    const onRefresh = useCallback(async () => {
        // setLoading(true)
        getInitialData(query, filter)
    }, [loading])
    
    return (
        <View style={styles.container}>
            <FormHeader 
                text="Histori Input Data" 
                leftButton={
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Eform')} 
                    >
                        <Image source={iconLeft} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
            >
                <View style={[
                    styles.searchContainer, 
                    {borderColor: query == '' ? COLOR.off : '#ff7c00'}
                ]}>
                    <TextInput 
                        style={styles.searchInput} 
                        placeholder="Cari nama disini"
                        value={query}
                        placeholderTextColor={COLOR.off}
                        onChangeText={(value) => setQuery(value)}
                        ref={input => searchInputRef = input}
                    />
                    {
                        query == '' ?
                        <Image source={iconSearch} style={{width: 24, height: 24}} />
                        :
                        <TouchableOpacity onPress={() => onClearSearch()}>
                            <Image source={iconCloseCircle} style={{width: 24, height: 24}} />
                        </TouchableOpacity>
                    }
                </View>
                <View>
                    <ScrollView 
                        horizontal={true} 
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={styles.filterContainer}
                    >
                        <FilterButton text="Semua" onPress={(value) => handleFilter(value)} curFilter={filter} filterKey="VR_ALL" />
                        <FilterButton text="Dalam Proses" onPress={(value) => handleFilter(value)} curFilter={filter} filterKey="VR_PROCESS" />
                        <FilterButton text="Perbaikan" onPress={(value) => handleFilter(value)} curFilter={filter} filterKey="VR_RETUR" />
                        <FilterButton text="Disetujui" onPress={(value) => handleFilter(value)} curFilter={filter} filterKey="VR_APPROVE" />
                        <FilterButton text="Ditolak" onPress={(value) => handleFilter(value)} curFilter={filter} filterKey="VR_REJECTED" />
                    </ScrollView>
                </View>
            </FormHeader>
            <View style={styles.listContainer}>
                <FlatList 
                    style={{flex: 1}}
                    onEndReachedThreshold={0.01}
                    onEndReached={() => {
                        if (endOfRow && loading) return
                        console.log('end')
                        onAddData(query, filter)
                    }}
                    refreshControl={
                        <RefreshControl refreshing={loading} onRefresh={onRefresh} />
                    }
                    contentContainerStyle={styles.flatList}
                    data={data}
                    renderItem={({item}) => (
                        <DraftItem item={item} onRepair={(itemId) => onRepairItem(itemId)} />
                    )}
                    keyExtractor={item => item.AppAcqId}
                    extraData={data}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dadada',
    },
    listContainer: {
        flex: 1,
    },
    flatList: {
        paddingBottom: 32,
        paddingHorizontal: 24,
        paddingTop: 16,
    },
    itemCard: {
        borderRadius: 8,
        backgroundColor: '#fff',
        padding: 16,
        marginBottom: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 4,
    },
    flexContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    itemHeader: {
        fontFamily: NUNITO.bold,
        fontSize: 16,
        lineHeight: 26,
        color: CUSTCOLOR.text,
        flex: 1,
    },
    itemSubtext: {
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        color: CUSTCOLOR.textLight,
    },
    itemText: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
    },
    itemStatus: {
        height: 20,
        paddingHorizontal: 8,
        borderRadius: 4,
        color: '#ffffff',
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        backgroundColor: '#ccc',
        textTransform: 'capitalize',
        alignSelf: 'flex-end',
        marginBottom: 8,
    },
    updatedDate: {
        alignSelf: 'flex-end'
    },
    delText: {
        fontFamily: NUNITO.bold,
        fontSize: 14,
        lineHeight: 22,
        color: COLOR.lightRed,
        marginLeft: 8
    },
    searchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: COLOR.off,
        borderRadius: 8,
        marginHorizontal: 24,
        marginBottom: 16,
        alignItems: 'center',
        backgroundColor: '#f3f4f5',
        paddingHorizontal: 8,
        paddingVertical: 7
    },
    searchInput: {
        height: 26,
        flex: 1,
        padding: 0,
        backgroundColor: 'transparent',
        marginRight: 32,
        fontFamily: NUNITO.regular
    },
    filterContainer: {
        paddingLeft: 24,
        paddingRight: 16,
        paddingBottom: 16,
    },
    filterButton: {
        borderRadius: 8,
        height: 36,
        paddingHorizontal: 16,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8,
        borderWidth: 1,
    },
    filterText: {
        color: COLOR.off,
        fontSize: 14,
        lineHeight: 22,
        fontFamily: NUNITO.regular,
    },
});


export default EformHistory;
