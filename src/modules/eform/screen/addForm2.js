import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, BackHandler, Keyboard, Text, ScrollView, TouchableOpacity, ToastAndroid } from 'react-native';
import { View } from 'react-native-animatable';

// import { DistrictPicker, RegencyPicker, VillagePicker } from '../../../components/region';

import AreaPicker from '../com/AreaPicker';
import FormHeader from '../com/FormHeader';
import Pagination from '../com/Pagination';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import ButtonSecondary from '../com/ButtonSecondary';
import HeaderText from '../com/HeaderText';
import FormField from '../com/FormField';
import InputText from '../com/InputText';
import InputPicker from '../com/InputPicker';
import RadioButton from '../com/RadioButton';
import ButtonSwitch from '../com/ButtonSwitch';
import Checkbox from '../com/Checkbox';
import { NUNITO } from '../common/styles';
import { COLOR } from '@common/styles';

import { useEform, useUpdateEform } from '../context/EformContext';
import ProvincePicker from '../com/ProvincePicker';
import StatePicker from '../com/StatePicker';
import DistrictPicker from '../com/DistrictPicker';
import SubdistrictPicker from '../com/SubdistrictPicker';

const AddForm2 = ({navigation}) => {
    const [provinceId, setProvinceId] = useState(0)
    const [cityId, setCityId] = useState(0)
    const [districtId, setDistrictId] = useState(0)

    const [provinceId_2, setProvinceId_2] = useState(0)
    const [cityId_2, setCityId_2] = useState(0)
    const [districtId_2, setDistrictId_2] = useState(0)

    const [warning, setWarning] = useState({});
    
    const form = useEform();
    const updateForm = useUpdateEform();

    const optionsIdType = [{
        label: 'KTP',
        value: 'KTP'
    // }, {
    //     label: 'SIM',
    //     value: 'SIM'
    // }, {
    //     label: 'AD/ART',
    //     value: 'AD_ART'
    // }, {
    //     label: 'K.I.M',
    //     value: 'KIM'
    // }, {
    //     label: 'K.I.M.S',
    //     value: 'KIMS'
    // }, {
    //     label: 'Kartu Pelajar',
    //     value: 'KARTU_PELAJAR'
    // }, {
    //     label: 'Passport',
    //     value: 'PASSPORT'
    // }, {
    //     label: 'Lainnya',
    //     value: 'OTHER'
    }]

    const optionsReligion = [{
        label: 'Islam',
        value: 'Islam'
    }, {
        label: 'Kristen',
        value: 'Kristen'
    }, {
        label: 'Katolik',
        value: 'Katolik'
    }, {
        label: 'Hindu',
        value: 'Hindu'
    }, {
        label: 'Budha',
        value: 'Budha'
    }, {
        label: 'Khong Hu',
        value: 'Khong_Hu'
    }, {
        label: 'Lainnya',
        value: 'Lainnya'
    }]

    const optionsMaritalStatus = [{
        label: 'Belum Menikah',
        value: 'BelumMenikah'
    }, {
        label: 'Menikah',
        value: 'Menikah'
    }, {
        label: 'Janda/Duda',
        value: 'JandaOrDuda'
    }]

    const optionsEducation = [{
            label: 'SD',
            value: 'SD'
        }, {
            label: 'SLTP',
            value: 'SLTP'
        }, {
            label: 'SLTA',
            value: 'SLTA'
        }, {
            label: 'D3',
            value: 'D3_ACADEMY'
        }, {
            label: 'S1',
            value: 'S1'
        }, {
            label: 'S2',
            value: 'S2'
        }, {
            label: 'S3',
            value: 'S3'
        }, {
            label: 'Lainnya',
            value: 'OTHERS_ED'
        }]

    const updateWarning = async (value, index) => {
        setWarning(current => ({
            ...current,
            [index]: value
        }))
    }

    const validateDate = (string) => {
        const patt = /^(0[1-9]|1\d|2\d|3[01])\/(1[0-2]|0[1-9])\/(\d{4})$/;

        return string.match(patt)
    }
    
    const validateIdCardNo = async () => {
        const patt = /^\d{16}$/;

        if (form.Id_Card_No == undefined || form.Id_Card_No == '' || form.Id_Card_No == null) {
            updateWarning('No identitas harus diisi', 'Id_Card_No')
            return false
        } else if (!form.Id_Card_No.match(patt)) {
            updateWarning('Format tidak sesuai', 'Id_Card_No')
            return false
        } else {
            updateWarning('', 'Id_Card_No')
            return true
        }
    }

    const validateIdCardExpTime = async () => {
        if (! form.id_card_lifetime && (form.Id_Card_Exp_Date == undefined || form.Id_Card_Exp_Date == '')) {
            updateWarning('Masa berlaku harus diisi', 'Id_Card_Exp_Date')
            return false
        } else if (! form.id_card_lifetime && !validateDate(form.Id_Card_Exp_Date)) {
            updateWarning('Format tidak sesuai', 'Id_Card_Exp_Date')
            return false
        } else {
            updateWarning('', 'Id_Card_Exp_Date')
            return true
        }
    }

    const validateDateOfBirth = async () => {
        if (form.Date_Of_Birth == undefined || form.Date_Of_Birth == '') {
            updateWarning('Tanggal lahir harus diisi', 'Date_Of_Birth')
            return false
        } else if (!validateDate(form.Date_Of_Birth)) {
            updateWarning('Format tidak sesuai', 'Date_Of_Birth')
            return false
        } else {
            updateWarning('', 'Date_Of_Birth')
            return true
        }
    }

    const ValidateName = async () => {
        const patt = /^[a-zA-Z]+(([a-zA-Z ])?[a-zA-Z]*)*$/;

        if (form.Name == undefined || form.Name == '') {
            updateWarning('Nama harus diisi', 'Name')
            return false
        } else if (!form.Name.match(patt)) {
            updateWarning('Format tidak sesuai', 'Name')
            return false
        } else {
            updateWarning('', 'Name')
            return true
        }
    }

    const ValidateStreet = async () => {

        if (form.Street == undefined || form.Street == '') {
            updateWarning('Nama harus diisi', 'Street')
            return false
        } else {
            updateWarning('', 'Street')
            return true
        }
    }

    const ValidateProvince = async () => {

        if (form.Province == undefined || form.Province == '') {
            updateWarning('Provinsi harus diisi', 'Province')
            return false
        } else {
            updateWarning('', 'Province')
            return true
        }
    }

    const ValidateCity = async () => {

        if (form.City == undefined || form.City == '') {
            updateWarning('Kabupaten/Kota harus diisi', 'City')
            return false
        } else {
            updateWarning('', 'City')
            return true
        }
    }

    const ValidateDistrict = async () => {

        if (form.District == undefined || form.District == '') {
            updateWarning('Kecamatan harus diisi', 'District')
            return false
        } else {
            updateWarning('', 'District')
            return true
        }
    }

    const ValidateSubdistrict = async () => {

        if (form.Subdistrict == undefined || form.Subdistrict == '') {
            updateWarning('Kelurahan/Desa harus diisi', 'Subdistrict')
            return false
        } else {
            updateWarning('', 'Subdistrict')
            return true
        }
    }

    const ValidateZipCode = async () => {
        const patt = /^([0-9]{5})/;

        if (form.Zip_Code == undefined || form.Zip_Code == '') {
            updateWarning('Kode Pos harus diisi', 'Zip_Code')
            return false
        } else if (!form.Zip_Code.match(patt)) {
            updateWarning('Format tidak sesuai', 'Zip_Code')
            return false
        } else {
            updateWarning('', 'Zip_Code')
            return true
        }
    }

    const ValidateRT = async () => {
        const patt = /^[0-9]+/;

        if (form.RT == undefined || form.RT == '') {
            updateWarning('RT harus diisi', 'RT')
            return false
        } else if (!form.RT.match(patt)) {
            updateWarning('Format tidak sesuai', 'RT')
            return false
        } else {
            updateWarning('', 'RT')
            return true
        }
    }

    const ValidateRW = async () => {
        const patt = /^[0-9]*/;

        if (form.RW == undefined || form.RW == '') {
            updateWarning('RW harus diisi', 'RW')
            return false
        } else if (!form.RW.match(patt)) {
            updateWarning('Format tidak sesuai', 'RW')
            return false
        } else {
            updateWarning('', 'RW')
            return true
        }
    }

    const ValidatePlaceOfBirth = async () => {
        const patt = /^[A-Za-z]+([\ A-Za-z]+)*/;

        if (form.Place_Of_Birth == undefined || form.Place_of_Birth == '') {
            updateWarning('Nama harus diisi', 'Place_Of_Birth')
            return false
        } else if (!form.Place_Of_Birth.match(patt)) {
            updateWarning('Format tidak sesuai', 'Place_Of_Birth')
            return false
        } else {
            updateWarning('', 'Place_Of_Birth')
            return true
        }
    }

    const validateGender = () => {
        if (form.Gender == undefined || form.Gender == null) {
            updateWarning('Jenis kelamin harus diisi', 'Gender')
            return false
        } else {
            updateWarning('', 'Gender')
            return true
        }
    }

    const ValidateEducation = async () => {

        if (form.Education == undefined || form.Education == '') {
            updateWarning('Pendidikan terakhir harus diisi', 'Education')
            return false
        } else {
            updateWarning('', 'Education')
            return true
        }
    }

    const ValidateMotherMaidenName = async () => {
        // const patt = /^[A-Za-z]+([\ A-Za-z]+)*/;
        const patt = /^[a-zA-Z]+(([a-zA-Z ])?[a-zA-Z]*)*$/;

        if (form.Mother_Maiden_Name == undefined || form.Mother_Maiden_Name == '') {
            updateWarning('Nama gadis ibu kandung harus diisi', 'Mother_Maiden_Name')
            return false
        } else if (!form.Mother_Maiden_Name.match(patt)) {
            updateWarning('Format tidak sesuai', 'Mother_Maiden_Name')
            return false
        } else {
            updateWarning('', 'Mother_Maiden_Name')
            return true
        }
    }

    const ValidateReligion = async () => {
        if (form.Religion == undefined || form.Religion == null) {
            updateWarning('Agama harus diisi', 'Religion')
            return false
        } else {
            updateWarning('', 'Religion')
            return true
        }
    }

    const ValidateMaritalStatus = async () => {
        if (form.Marital_Status == undefined || form.Marital_Status == '') {
            updateWarning('Status perkawinan harus diisi', 'Marital_Status')
            return false
        } else {
            updateWarning('', 'Marital_Status')
            return true
        }
    }

    const onValidate = async () => {
        const IdCardNo = await validateIdCardNo()
        const IdCardExpTime = await validateIdCardExpTime()
        const DateOfBirth = await validateDateOfBirth()
        const Name = await ValidateName()
        const Street = await ValidateStreet()
        const Province = await ValidateProvince()
        const City = await ValidateCity()
        const District = await ValidateDistrict()
        const Subdistrict = await ValidateSubdistrict()
        const ZipCode = await ValidateZipCode()
        const RT = await ValidateRT()
        const RW = await ValidateRW()
        const PlaceOfBirth = await ValidatePlaceOfBirth()
        const Gender = await validateGender()
        const Education = await ValidateEducation()
        const MotherMaidenName = await ValidateMotherMaidenName()
        const Religion = await ValidateReligion()
        const MaritalStatus = await ValidateMaritalStatus()

        let validate = [
            IdCardNo,
            IdCardExpTime,
            DateOfBirth,
            Name,
            Street,
            Province,
            City,
            District,
            Subdistrict,
            ZipCode,
            RT,
            RW,
            PlaceOfBirth,
            Gender,
            Education,
            MotherMaidenName,
            Religion,
            MaritalStatus
        ]

        return (validate.every(item => item == true))
    }

    const handleNextPage = async (page) => {
        await setWarning({})
        
        const canContinue = await onValidate()

        console.log('canContinue', canContinue)

        if (canContinue) navigation.navigate(page);
        if (!canContinue) {
            ToastAndroid.show(
                'Mohon periksa kembali data yang Anda masukkan',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        }
    }

    useEffect( () => {
        if (form.Id_Card_Type == null) updateForm('KTP', 'Id_Card_Type')
        if (form.id_card_lifetime == null) updateForm(true, 'id_card_lifetime')
        if (form.is_current_address == null) updateForm(true, 'is_current_address')

        if (form.repairFormMode) onValidate();
    }, [])

    return (
        <View style={{height: '100%'}}>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => navigation.navigate('Eform')}
                closeToSave={true}
            >
                <Pagination active={2} pageText={['1', '2', '3', '4']} />
            </FormHeader>
            <ScrollView 
                style={{height: '100%', backgroundColor: '#fff'}}
                keyboardShouldPersistTaps="handled"
            >
            <View style={{padding: 24}}>               
                <HeaderText>Data Identitas Calon Mitra Tepat</HeaderText>
                <FormField
                text="Jenis Identitas"
                >
                    <InputPicker 
                        options={optionsIdType}
                        selectedValue={form.Id_Card_Type}
                        placeholder="Pilih jenis identitas"
                        onValueChange={(value) => updateForm(value, 'Id_Card_Type')}
                    />
                </FormField>
                <View style={[styles.flexContainer, styles.row]}>
                    <Checkbox value={form.id_card_lifetime} 
                        callback={value => updateForm(value, 'id_card_lifetime')} />
                    <Text style={[styles.text, {flex: 1}]}>
                        Masa berlaku KTP seumur hidup
                    </Text>
                </View>
                <FormField
                    text="Masa Berlaku KTP Tanggal/Bulan/Tahun"
                    info="Tanggal/Bulan/Tahun"
                    warning={warning.Id_Card_Exp_Date}
                >
                    <InputText 
                        value={form.Id_Card_Exp_Date}
                        onChangeText={(value) => updateForm(value, 'Id_Card_Exp_Date')}
                        dateInput={true}
                        placeholder="DD/MM/YYYY"
                        maxLength={10}
                        editable={form.id_card_lifetime ? false : true}
                    />
                </FormField>
                <FormField
                    text="Nomor Identitas"
                    warning={warning.Id_Card_No}
                >
                    <InputText 
                        value={form.Id_Card_No}
                        onChangeText={(value) => updateForm(value, 'Id_Card_No')}
                        keyboardType="number-pad"
                        placeholder="0000 0000 0000 0000"
                    />
                </FormField>
                <FormField
                    text="Nama Lengkap (Sesuai KTP)"
                    warning={warning.Name}
                >
                    <InputText 
                        value={form.Name}
                        onChangeText={(value) => updateForm(value, 'Name')}
                        placeholder="Nama Lengkap"
                    />
                </FormField>
                <FormField
                    text="Tempat Lahir"
                    warning={warning.Place_Of_Birth}
                >
                    <InputText 
                        value={form.Place_Of_Birth}
                        onChangeText={(value) => updateForm(value, 'Place_Of_Birth')}
                        placeholder="Tulis tempat lahir"
                    />
                </FormField>
                <FormField
                    text="Tanggal Lahir"
                    info="Tanggal/Bulan/Tahun"
                    warning={warning.Date_Of_Birth}
                >
                    <InputText 
                        value={form.Date_Of_Birth}
                        onChangeText={(value) => updateForm(value, 'Date_Of_Birth')}
                        dateInput={true}
                        placeholder="DD/MM/YYYY"
                        maxLength={10}
                    />
                </FormField>
                <FormField
                    text="Jenis Kelamin"
                    warning={warning.Gender}
                >
                <RadioButton options={[{
                        key: 'FEMALE',
                        text: 'Perempuan'
                    }, {
                        key: 'MALE',
                        text: 'Laki-laki'
                    }]} 
                    selectedValue={form.Gender}
                    callback={(value) => updateForm(value, 'Gender')}
                />
                </FormField>
                <FormField
                    text="Agama"
                    warning={warning.Religion}
                >
                    <InputPicker 
                        options={optionsReligion}
                        selectedValue={form.Religion}
                        placeholder="Pilih Agama"
                        onValueChange={(value) => updateForm(value, 'Religion')}
                    />
                </FormField>
                <FormField
                    text="Status Perkawinan"
                    warning={warning.Marital_Status}
                >
                    <InputPicker 
                        options={optionsMaritalStatus}
                        placeholder="Pilih Status"
                        selectedValue={form.Marital_Status}
                        onValueChange={(value) => updateForm(value, 'Marital_Status')}
                    />
                </FormField>
                <FormField
                    text="Alamat Tempat Tinggal"
                    warning={warning.Street}
                >
                <InputText 
                    value={form.Street}
                    onChangeText={(value) => updateForm(value, 'Street')}
                    placeholder="Jl.Nama Jalan No.01"
                />
                </FormField>
                <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1, marginRight: 16}}>
                    <FormField
                        text="RT"
                        warning={warning.RT}
                    >
                    <InputText 
                        value={form.RT}
                        onChangeText={(value) => updateForm(value, 'RT')}
                        keyboardType="numeric"
                        placeholder="00"
                        maxLength={3}
                        />
                    </FormField>
                </View>
                <View style={{flex: 1}}>
                    <FormField
                        text="RW"
                        warning={warning.RW}
                    >
                    <InputText 
                        value={form.RW}
                        onChangeText={(value) => updateForm(value, 'RW')}
                        keyboardType="numeric"
                        placeholder="05"
                        maxLength={3}
                    />
                    </FormField>
                </View>
                </View>

                <FormField
                    text="Provinsi"
                    warning={warning.Province}
                >
                    <ProvincePicker 
                        placeholder="Pilih Provinsi"
                        selectedValue={form.Province}
                        areaValue={(value) => {
                            console.log(value)
                            updateForm(value.label, 'Province')
                            setProvinceId(value.value)
                            updateForm(null, 'City')
                            setCityId(null)
                            updateForm(null, 'District')
                            setDistrictId(null)
                            updateForm(null, 'Subdistrict')
                            updateForm(null, 'Zip_Code')
                        }}
                    />
                </FormField>
                <FormField
                    text="Kabupaten/Kota"
                    warning={warning.City}
                >
                    <StatePicker 
                        placeholder="Pilih Kabupaten/Kota"
                        selectedValue={form.City}
                        provinceId={provinceId}
                        disabled={form.Province ? false : true}
                        areaValue={(value) => {
                            console.log(value)
                            updateForm(value.label, 'City')
                            setCityId(value.value)
                            updateForm(null, 'District')
                            setDistrictId(null)
                            updateForm(null, 'Subdistrict')
                            updateForm(null, 'Zip_Code')
                        }}
                    />
                </FormField>
                <FormField
                    text="Kecamatan"
                    warning={warning.District}
                >
                    <DistrictPicker 
                        placeholder="Pilih Kecamatan"
                        selectedValue={form.District}
                        provinceId={provinceId}
                        stateId={cityId}
                        disabled={form.City ? false : true}
                        areaValue={(value) => {
                            console.log(value)
                            updateForm(value.label, 'District')
                            setDistrictId(value.value)
                            updateForm(null, 'Subdistrict')
                            updateForm(null, 'Zip_Code')
                        }}
                    />
                </FormField>
                <FormField
                    text="Kelurahan/Desa"
                    warning={warning.Subdistrict}
                >
                    <SubdistrictPicker 
                        placeholder="Pilih Kelurahan"
                        selectedValue={form.Subdistrict}
                        provinceId={provinceId}
                        stateId={cityId}
                        districtId={districtId}
                        disabled={form.District ? false : true}
                        areaValue={(value) => {
                            console.log(value)
                            updateForm(value.label, 'Subdistrict')
                            updateForm(value.zipCode, 'Zip_Code')
                            console.log('sub district zip code', value)
                        }}
                    />
                </FormField>
                <FormField
                    text="Kode Pos"
                    warning={warning.Zip_Code}
                >
                <InputText 
                    value={form.Zip_Code}
                    placeholder="12345"
                    onChangeText={(value) => updateForm(value, 'Zip_Code')}
                    maxLength={5}
                    keyboardType="numeric"
                />
                </FormField>
                <FormField
                    text="Pendidikan Terakhir"
                    warning={warning.Education}
                >
                    <InputPicker 
                        options={optionsEducation}
                        placeholder="Pilih pendidikan"
                        selectedValue={form.Education}
                        onValueChange={(value) => updateForm(value, 'Education')}
                    />
                </FormField>
                <FormField
                    text="Nama Gadis Ibu Kandung (Tanpa Gelar)"
                    warning={warning.Mother_Maiden_Name}
                >
                <InputText 
                    value={form.Mother_Maiden_Name}
                    placeholder="Nama gadis ibu kandung"
                    onChangeText={(value) => updateForm(value, 'Mother_Maiden_Name')}
                />
                </FormField>
                <View style={{paddingTop: 12, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 28}}>
                    <Text style={[styles.text, {flex: 1, marginRight: 24}]}>Alamat tempat tinggal sesuai identitas calon Mitra Tepat</Text>
                    <ButtonSwitch 
                    value={form.is_current_address}
                    callback={value => updateForm(value, 'is_current_address')} 
                />
                </View>
                {!form.is_current_address ? (
                    <View>

                        <FormField
                            text="Alamat Tempat Tinggal"
                        >
                        <InputText 
                            value={form.Street_2}
                            onChangeText={(value) => updateForm(value, 'Street_2')}
                            placeholder="Jl.Nama Jalan No.01"
                        />
                        </FormField>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1, marginRight: 16}}>
                                <FormField
                                text="RT"
                                >
                                <InputText 
                                    value={form.RT_2}
                                    onChangeText={(value) => updateForm(value, 'RT_2')}
                                    placeholder="00"
                                    maxLength={3}
                                    />
                                </FormField>
                            </View>
                            <View style={{flex: 1}}>
                                <FormField
                                text="RW"
                                >
                                <InputText 
                                    value={form.RW_2}
                                    onChangeText={(value) => updateForm(value, 'RW_2')}
                                    placeholder="05"
                                    maxLength={3}
                                />
                                </FormField>
                            </View>
                        </View>

                        <FormField
                            text="Provinsi"
                        >
                            <ProvincePicker 
                                placeholder="Pilih Provinsi"
                                selectedValue={form.Province_2}
                                areaValue={(value) => {
                                    console.log(value)
                                    updateForm(value.label, 'Province_2')
                                    setProvinceId_2(value.value)
                                    updateForm(null, 'City_2')
                                    setCityId_2(null)
                                    updateForm(null, 'District_2')
                                    setDistrictId_2(null)
                                    updateForm(null, 'Subdistrict_2')
                                    updateForm(null, 'Zip_Code_2')
                                }}
                            />
                        </FormField>
                        <FormField
                            text="Kabupaten/Kota"
                        >
                            <StatePicker 
                                placeholder="Pilih Kabupaten/Kota"
                                selectedValue={form.City_2}
                                provinceId={provinceId_2}
                                disabled={form.Province_2 ? false : true}
                                areaValue={(value) => {
                                    console.log(value)
                                    updateForm(value.label, 'City_2')
                                    setCityId_2(value.value)
                                    updateForm(null, 'District_2')
                                    setDistrictId_2(null)
                                    updateForm(null, 'Subdistrict_2')
                                    updateForm(null, 'Zip_Code_2')
                                }}
                            />
                        </FormField>
                        <FormField
                            text="Kecamatan"
                            warning={warning.District}
                        >
                            <DistrictPicker 
                                placeholder="Pilih Kecamatan"
                                selectedValue={form.District_2}
                                provinceId={provinceId_2}
                                stateId={cityId_2}
                                disabled={form.City_2 ? false : true}
                                areaValue={(value) => {
                                    console.log(value)
                                    updateForm(value.label, 'District_2')
                                    setDistrictId_2(value.value)
                                    updateForm(null, 'Subdistrict_2')
                                    updateForm(null, 'Zip_Code_2')
                                }}
                            />
                        </FormField>
                        <FormField
                            text="Kelurahan/Desa"
                        >
                            <SubdistrictPicker 
                                placeholder="Pilih Kelurahan"
                                selectedValue={form.Subdistrict_2}
                                provinceId={provinceId_2}
                                stateId={cityId_2}
                                districtId={districtId_2}
                                disabled={form.District_2 ? false : true}
                                areaValue={(value) => {
                                    console.log(value)
                                    updateForm(value.label, 'Subdistrict_2')
                                    updateForm(value.zipCode, 'Zip_Code_2')
                                }}
                            />
                        </FormField>
                        <FormField
                            text="Kode Pos"
                        >
                            <InputText 
                                value={form.Zip_Code_2}
                                placeholder="12345"
                                onChangeText={(value) => updateForm(value, 'Zip_Code_2')}
                                maxLength={5}
                                keyboardType="numeric"
                            />
                        </FormField>
                    </View>
                    ):
                    <View></View>
                }
                
            </View>
        
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 28}}>
                        <ButtonSecondary 
                        text="Kembali" 
                        onPress={() => navigation.navigate('AddForm1')} 
                        />
                    </View>
                    <View style={{flex: 1}}>
                        <ButtonPrimary 
                        text="Lanjut" 
                        onPress={() => {
                            handleNextPage('AddForm3');
                        }} 
                        />  
                    </View>
                </View>
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    flexContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    row: {
        marginBottom: 16,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 16,
        lineHeight: 26
    },
    field: {
        paddingHorizontal: 20,
        borderBottomWidth: 0.7,
        borderColor: COLOR.border,
        paddingTop: 5,
    },
})
  
export default AddForm2