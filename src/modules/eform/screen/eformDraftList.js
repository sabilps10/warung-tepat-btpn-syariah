import React, { Component, useCallback, useEffect, useState } from 'react';
import { StyleSheet, Text, Image, Dimensions, Alert, TouchableOpacity, RefreshControl } from 'react-native';
import { View } from 'react-native-animatable';
import { COLOR } from '@common/styles';

import FormHeader from '../com/FormHeader';
import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import ButtonSecondary from '../com/ButtonSecondary';
import { FlatList, TextInput } from 'react-native-gesture-handler';

import iconLeft from '@assets/icons/icon_eform_arrow_left.png';
import iconTrash from '@assets/icons/icon_eform_trash.png';
import iconSearch from '@assets/icons/icon_eform_search.png';
import iconCloseCircle from '@assets/icons/icon_eform_close_circle.png';
import { getData, removeData, setData } from '../com/FunctionStorage';
import { useSetEform } from '../context/EformContext';
import { useSelector } from 'react-redux';

const DraftItem = ({item, onDelete, onContinue}) => {
    const renderDate = (value) => {
        console.log('renderDate', typeof value)
        
        let dateArray = (value.substring(0, 10)).split('-')
        let dateString = `${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`

        return dateString
    }

    return (
        <View style={styles.itemCard} key={item.Index} animation="zoomIn" duration={500} useNativeDriver>
            <View style={styles.flexContainer}>
                <Text style={styles.itemDate}>
                    {renderDate(item.Created_At)}
                </Text>
                <TouchableOpacity
                    style={styles.delButton}
                    onPress={() => onDelete()}
                >
                    <Image source={iconTrash} style={{width: 16, height: 16}} />
                <Text style={styles.delText}>Hapus</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.itemName}>{item.Name ? item.Name : ""}</Text>
            <ButtonSecondary
                text="Lanjutkan"
                onPress={() => onContinue(item)}
            />
        </View>
    )
}

const EformDraftList = ({route, navigation}) => {
    const [dataDraft, setDataDraft] = useState([])
    const [query, setQuery] = useState("")
    const [draftCount, setDraftCount] = useState(0)
    const [loading, setLoading] = useState(false)
    const SESSION = useSelector(store => store.Auth.session)

    const setForm = useSetEform();

    const getDraftData = async () => {
        let data = await getData('@dataDraft')
        console.log('data', data)

        if (data != false) {            
            data = Object.values(data)
        } else {
            data = []
        }

        return data
    }
    
    const onGetDraftData = async (query='') => {
        setLoading(true)
        const data = await getDraftData();
        let patt = new RegExp(query);
        const userId = SESSION?.user?.response_login_by_nik?.userId;

        console.log('user id: ', userId)

        let newData = []
        
        data.map((item) => {
            if (item.Created_By == userId) newData.push(item)
        })

        console.log('on get draft, new data: ', newData)

        if (query != '') {
            let searchedItem = []

            newData.map((item) => {
                console.log('test', patt.test(item.Name))
    
                if (patt.test(item.Name)) {
                    searchedItem.push(item)
                }
            })

            newData = searchedItem
        }

        if (newData != []) {
            setDataDraft(newData)
            setDraftCount(newData.length)
        };
        setLoading(false)
    }

    const removeDraft = async (index, storageKey) => {
        console.log('delete ', index)
        const removed = await removeData(index, storageKey);

        if (removed) {
            await onGetDraftData()
        }
    }

    const onRemoveDraft = (item, storageKey) => {
        console.log('on remove', item)
        Alert.alert(
            'Hapus draft',
            'Apakah Anda yakin untuk menghapus draft?',
            [
                {
                    text: 'Batal',
                    type: 'cancel',
                }, {
                    text: 'Hapus',
                    onPress: () => removeDraft(item.Index, storageKey),
                }
            ]
        )
    }

    const onContinue = async (item) => {
        await setForm(item);
        navigation.navigate('AddForm1')
    }

    const onClearSearch = () => {
        searchInputRef.clear()
        setQuery('')
        // searchInputRef.blur()
    }

    const renderDraftItem = ({item, index}) => {
        console.log('draft item', item)
        return (
            <DraftItem
                index={index}
                item={item}
                onDelete={() => onRemoveDraft(item, '@dataDraft')}
                onContinue={(value) => onContinue(value)}
            />
        )
    }

    const onRefresh = useCallback(async () => {
        setLoading(true)

        onGetDraftData(filter)

        setLoading(false)
    }, [loading])

    useEffect(() => {
        onGetDraftData()
    }, [])

    return (
        <View style={styles.container}>
            <FormHeader
                text="Data Draft"
                leftButton={
                    <TouchableOpacity
                        onPress={() => {
                            navigation.setParams({draftCount: draftCount})
                            navigation.goBack()
                        }}
                    >
                        <Image source={iconLeft} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
            >
                <View style={[
                    styles.searchContainer, 
                    {borderColor: query == '' ? COLOR.off : '#ff7c00'}
                ]}>
                    <TextInput 
                        style={styles.searchInput} 
                        placeholder="Cari nama disini"
                        value={query}
                        placeholderTextColor={COLOR.off}
                        onChangeText={(value) => {
                            setQuery(value)
                            onGetDraftData(value)
                        }}
                        ref={input => searchInputRef = input}
                    />
                    {
                        query == '' ?
                        <Image source={iconSearch} style={{width: 24, height: 24}} />
                        :
                        <TouchableOpacity onPress={() => onClearSearch()}>
                            <Image source={iconCloseCircle} style={{width: 24, height: 24}} />
                        </TouchableOpacity>
                    }
                </View>
            </FormHeader>
            <View style={{flex: 1}}>
                <FlatList 
                    style={{flex: 1}}
                    contentContainerStyle={styles.flatList}
                    data={dataDraft}
                    refreshControl={
                        <RefreshControl refreshing={loading} onRefresh={onRefresh} />
                    }
                    renderItem={renderDraftItem}
                    keyExtractor={(item, index) => item.Index.toString()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dadada',
    },
    flatList: {
        paddingBottom: 32,
        paddingHorizontal: 24,
        paddingTop: 16,
    },
    itemCard: {
        borderRadius: 8,
        backgroundColor: '#fff',
        padding: 16,
        marginBottom: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 4,
    },
    flexContainer: {
        flexDirection: 'row'
    },
    itemDate: {
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        color: CUSTCOLOR.textLight,
        flex: 1,
    },
    itemName: {
        fontFamily: NUNITO.bold,
        fontSize: 16,
        lineHeight: 26,
        color: CUSTCOLOR.text,
        marginBottom: 16
    },
    delButton: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    delText: {
        fontFamily: NUNITO.bold,
        fontSize: 14,
        lineHeight: 22,
        color: COLOR.lightRed,
        marginLeft: 8
    },
    searchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderRadius: 8,
        marginHorizontal: 24,
        marginBottom: 16,
        alignItems: 'center',
        backgroundColor: '#f3f4f5',
        paddingHorizontal: 8,
        paddingVertical: 7
    },
    searchInput: {
        height: 26,
        flex: 1,
        padding: 0,
        backgroundColor: 'transparent',
        marginRight: 32,
        fontFamily: NUNITO.regular
    },


    headerText: {
        fontFamily: MARKOT.bold,
        fontSize: 20,
        lineHeight: 32,
        textAlign: 'center',
        marginBottom: 8,
        color: CUSTCOLOR.textDark,
    },
    bodyTextContainer: {
        marginBottom: 40,
    },

});


export default EformDraftList;
