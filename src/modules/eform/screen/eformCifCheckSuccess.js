import React, { useState, useEffect } from 'react';
import { 
    StyleSheet, 
    Text, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Alert, 
    ToastAndroid 
} from 'react-native';
import { View } from 'react-native-animatable';

import FormHeader from '../com/FormHeader';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';

import iconLeft from '@assets/icons/icon_eform_arrow_left.png';

import { COLOR } from '../../../common/styles';
import { CUSTCOLOR, NUNITO } from '../common/styles';
import { useEform } from '../context/EformContext';

const EformCifCheckSuccess = ({route, navigation}) => {
    const form = useEform();
    
    const handleNextPage = async (page) => { 
        navigation.navigate(page);
    }

    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
            <FormHeader 
                text="Kembali"
                leftButton={
                    <TouchableOpacity
                        onPress={() => {
                            navigation.goBack()
                        }}
                    >
                        <Image source={iconLeft} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
            />
            <ScrollView>
                <View style={{padding: 24}}>
                    <View style={styles.summaryContainer}>
                        <View style={styles.summaryField}>
                            <Text style={styles.fieldTitle}>Nomor CIF</Text>
                            <Text style={styles.fieldContent}>{form.Cif_No}</Text>
                        </View>
                        <View style={styles.summaryField}>
                            <Text style={styles.fieldTitle}>Nama</Text>
                            <Text style={styles.fieldContent}>{form.Name}</Text>
                        </View>
                        <View style={styles.summaryField}>
                            <Text style={styles.fieldTitle}>Tempat Lahir</Text>
                            <Text style={styles.fieldContent}>{form.Place_Of_Birth}</Text>
                        </View>
                        <View style={styles.summaryField}>
                            <Text style={styles.fieldTitle}>Tanggal Lahir</Text>
                            <Text style={styles.fieldContent}>{form.Date_Of_Birth}</Text>
                        </View>
                        <View style={styles.summaryField}>
                            <Text style={styles.fieldTitle}>Nomor KTP</Text>
                            <Text style={styles.fieldContent}>{form.Id_Card_No}</Text>
                        </View>
                        <View style={styles.summaryField}>
                            <Text style={styles.fieldTitle}>Alamat Tempat Tinggal</Text>
                            <Text style={styles.fieldContent}>
                                {form.Street}
                            </Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 28}}>
                        <ButtonPrimary style={{backgroundColor: COLOR.danger}} text="Batal" 
                            onPress={() => handleNextPage('Eform')}
                        />
                    </View>
                    <View style={{flex: 1}}>
                        <ButtonPrimary text="Input Data" 
                            onPress={() => handleNextPage('AddFormCif1')}
                        />
                    </View>
                </View>
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    summaryContainer: {
        borderRadius: 8,
        padding: 16,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    summaryField: {
        marginBottom: 8
    },
    fieldTitle: {
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        color: CUSTCOLOR.textLight
    },
    fieldContent: {
        fontFamily: NUNITO.bold,
        fontSize: 16,
        lineHeight: 26,
        color: CUSTCOLOR.text
    }
})

export default EformCifCheckSuccess
