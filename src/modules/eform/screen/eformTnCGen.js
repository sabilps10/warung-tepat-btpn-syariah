import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import FormHeader from '../com/FormHeader';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import HeaderText from '../com/HeaderText';
import Checkbox from '../com/Checkbox';
import RenderTnc from '../com/RenderTnc';

import { CUSTCOLOR, NUNITO } from '../common/styles';

import {syartumKeagenan} from '../texts/syartumKeagenan'
import { useEform } from '../context/EformContext';

const EformTnCGen = ({route, navigation}) => {
    const paramsForm = navigation.getParam('form')
    // const [form, setForm] = useState({});

    const form = useEform()
  
    useEffect(() => {
        // if (paramsForm) {
        //     setForm({
        //         ...paramsForm
        //     })
        // }
    }, [paramsForm]);

    // const handleInput = (value, inputName) => {
    //     setForm({
    //         ...form,
    //         [inputName]: value
    //     })
    // }

    // const handleCheck = (value, inputName) => {
    //     setCheck({
    //         ...check,
    //         [inputName]: value
    //     })
    // }
  
    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
        <FormHeader 
            text="Syarat dan Ketentuan"
            onClose={() => navigation.navigate('Eform')}
            closeToSave={true}
        />
        <ScrollView style={{padding: 24}}>
            <View style={{paddingBottom: 61}}>
                <Text style={styles.header}>Syarat & Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah</Text>

                <View style={styles.section}>
                    <RenderTnc tncText={syartumKeagenan} />
                </View>
            </View>
        </ScrollView>
        <EformFooter>
            <ButtonPrimary text="Kirim OTP" 
                onPress={() => {
                    console.log('form tnc gen, ', form)
                    if (form.Cif_No != null) {
                        console.log('ada')
                        navigation.navigate('EformOtpCif');
                    } else {
                        console.log('ga ada')
                        navigation.navigate('EformOtp');
                    }
                }} 
            />
        </EformFooter>
      </View>
    )
}

const styles = StyleSheet.create({
    flexContainer: {
        flexDirection: 'row'
    }, 
    header: {
        fontFamily: NUNITO.bold,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
        marginBottom: 8
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 20,
        color: CUSTCOLOR.text,
        marginBottom: 24
    },
    section: {
        marginBottom: 24,
    },
})

export default EformTnCGen
