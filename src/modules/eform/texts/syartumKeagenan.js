export const syartumKeagenan = [{
	text: 'DEFINISi',
	indexStr: 'Pasal 1',
	level: 0,
}, {
	text: 'BTPNS adalah PT BTPN Syariah Tbk sebagaimana dimaksud dalam Perjanjian.',
	indexStr: '1.1',
	level: 1,
}, {
	text: 'Mitra Tepat adalah pihak yang bekerjasama dengan BTPNS untuk memberikan jasanya dalam melayani kegiatan perbankan seperti pembukaan rekening Tabungan BTPN Wow! Syariah, setor tunai dan tarik tunai Nasabah BTPN Wow! Syariah serta layanan perbankan lainnya.',
	indexStr: '1.2',
	level: 1,
}, {
	text: 'BTPN Wow! Syariah adalah Layanan BTPNS yang disediakan kepada Nasabah BTPN Wow! Syariah untuk melakukan Transaksi Perbankan dengan menggunakan sarana elektronik seperti telepon seluler dan jaringan seluler.',
	indexStr: '1.3',
	level: 1,
}, {
	text: 'Hari Kerja adalah hari Senin sampai dengan Jumat di luar hari libur resmi yang ditetapkan oleh Pemerintah dan hari bank tidak buka untuk umum (non bank day) yang ditetapkan oleh Bank Indonesia dan/atau Otoritas Jasa Keuangan.',
	indexStr: '1.4',
	level: 1,
}, {
	text: 'Imbalan Jasa/Komisi adalah suatu bentuk insentif kepada Mitra Tepat dengan jumlah minimum dan maksimum ditentukan oleh BTPNS untuk setiap Transaksi Perbankan sesuai ketentuan yang akan diatur dan ditentukan melalui surat keputusan yang diterbitkan oleh BTPNS.',
	indexStr: '1.5',
	level: 1,
}, {
	text: '1.6	Layanan BTPNS adalah layanan perbankan dengan produk Tabungan BTPN  Wow! Syariah BSA (Basic Saving Account) dan Tabungan BTPN  Wow! Syariah RSA (Regular Saving Account)  yang disediakan dan diberikan oleh BTPNS kepada Nasabah BTPN  Wow! Syariah melalui jaringan Kantor Cabang atau Mitra Tepat',
	indexStr: '1.6',
	level: 1,
}, {
	text: 'Nasabah BTPN  Wow! Syariah adalah perorangan yang telah mendaftar, menerima dan menggunakan BTPN  Wow! Syariah.',
	indexStr: '1.7',
	level: 1,
}, {
	text: 'Perjanjian adalah perjanjian kerja sama penunjukan Mitra Tepat BTPN  Wow! Syariah yang dibuat antara Mitra Tepat dan BTPNS dan merupakan satu kesatuan dan bagian yang tidak terpisahkan dengan Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN  Wow! Syariah.',
	indexStr: '1.8',
	level: 1,
}, {
	text: 'Rekening Mitra Tepat adalah Rekening yang dipergunakan oleh Mitra Tepat untuk melayani transaksi Nasabah BTPN  Wow! Syariah dan pelaksanaan pelimpahan atau pemindahbukuan Imbalan Jasa/Komisi atas dana transaksi Mitra Tepat yang dilaksanakan oleh BTPNS.',
	indexStr: '1.9',
	level: 1,
}, {
	text: 'Tabungan BTPN Wow! Syariah   Basic Saving Account (BSA) adalah tabungan yang dimiliki oleh PT Bank BTPNS Tbk yang diperuntukan bagi nasabah yang belum memiliki rekening di PT Bank BTPNS Tbk.',
	indexStr: '1.10',
	level: 1,
}, {
	text: 'Tabungan BTPN Wow! Syariah Regular Saving Account (RSA) adalah tabungan yang dimiliki oleh PT Bank BTPNS Tbk. yang diperuntukan bagi nasabah yang sudah memiliki rekening di PT Bank BTPNS Tbk.',
	indexStr: '1.11',
	level: 1,
}, {
	text: 'Transaksi Perbankan adalah setiap transaksi penggunaan Layanan BTPNS yang jenisnya sebagaimana dimaksud dalam Perjanjian.',
	indexStr: '1.12',
	level: 1,
}, {
	text: 'Penulisan judul pada setiap klausula dalam Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah versi 01 ini dan atau Perjanjian semata-mata merupakan referensi untuk tujuan memudahkan penyusunan saja dan tidak dapat dipergunakan dalam penafsiran isi dari Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah versi 01 dan Perjanjian.',
	indexStr: null,
	level: 1,
}, {
	text: 'PERINCIAN DAN PENUNJUKAN MITRA TEPAT',
	indexStr: 'Pasal 2',
	level: 0,
}, {
	text: 'Kesepakatan penunjukan (masing-masing) Mitra Tepat akan dituangkan dalam Perjanjian. Para Pihak sepakat bahwa ketentuan penunjukan Mitra Tepat tunduk pada ketentuan-ketentuan sebagai berikut:',
	indexStr: null,
	level: 1,
}, {
	text: 'Layanan BTPNS yang disediakan oleh Mitra Tepat ',
	indexStr: 'a.',
	level: 1,
}, {
	text: 'Akuisisi calon nasabah Tabungan BTPN Wow! Syariah;',
	indexStr: '1.',
	level: 2,
}, {
	text: 'Setor tunai nasabah Tabungan BTPN Wow! Syariah BSA dan Tabungan BTPN Wow! Syariah RSA;',
	indexStr: '2.',
	level: 2,
}, {
	text: 'Tarik tunai nasabah Tabungan BTPN  Wow! Syariah BSA dan Tabungan BTPN  Wow! Syariah RSA;',
	indexStr: '3.',
	level: 2,
}, {
	text: 'Dan kegiatan ataupun transaksi lainnya yang ditentukan oleh BTPNS dari waktu ke waktu ',
	indexStr: '4.',
	level: 2,
}, {
	text: 'Imbalan Jasa dan Komisi',
	indexStr: 'b.',
	level: 1,
}, {
	text: 'Mitra Tepat akan mendapatkan imbalan jasa atau komisi atas transaksi yang dilakukan. Detail besar imbal jasa atau komisi, akan ditentukan dalam ketentuan terpisah.',
	indexStr: null,
	level: 2,
}, {
	text: 'Imbalan jasa/komisi akan ditransfer ke rekening  Mitra Tepat setiap bulan.',
	indexStr: '1.',
	level: 2,
}, {
	text: 'BTPNS berhak untuk melakukan perubahan ketentuan Imbalan Jasa/Komisi sesuai kebijakan internal BTPNS dengan sebelumnya melakukan pemberitahuan kepada Mitra Tepat.',
	indexStr: '2.',
	level: 2,
}, {
	text: 'Pengaturan likuiditas Mitra Tepat ',
	indexStr: 'c.',
	level: 1,
}, {
	text: 'Mitra Tepat berkewajiban  mengelola saldo pada rekening maupun uang tunai untuk dapat memberikan layanan Transaksi Perbankan dengan cara :',
	indexStr: '1.',
	level: 2,
}, {
	text: 'Penambahan saldo uang tunai  Mitra Tepat',
	indexStr: '1)',
	level: 3,
}, {
	text: 'Penambahan uang tunai melalui Kantor cabang BTPNS (tarik tunai).',
	indexStr: 'a)',
	level: 4,
}, {
	text: 'Penambahan uang tunai melalui pihak ketiga yang ditunjuk sebagai representasi BTPNS dengan menggunakan mekanisme seperti transaksi setor tunai, transfer antar rekening Mitra Tepat atau mekanisme yang ditetapkan oleh BTPNS.',
	indexStr: 'b)',
	level: 4,
}, {
	text: 'Penambahan saldo rekening  Mitra Tepat',
	indexStr: '2)',
	level: 3,
}, {
	text: 'Transfer melalui Bank selain BTPNS (transfer antar Bank).',
	indexStr: 'a)',
	level: 4,
}, {
	text: 'Penambahan saldo Rekening Mitra Tepat melalui kantor cabang BTPNS (setor tunai).',
	indexStr: 'b)',
	level: 4,
}, {
	text: 'Penambahan saldo Mitra Tepat melalui pihak yang ditunjuk sebagai representasi BTPNS dengan menggunakan mekanisme seperti transaksi tarik tunai, transfer antar rekening Mitra Tepat atau mekanisme yang ditetapkan oleh BTPNS.',
	indexStr: 'c)',
	level: 4,
}, {
	text: 'Melalui mekanisme lainnya yang ditentukan oleh BTPNS.',
	indexStr: '3)',
	level: 3,
}, {
	text: 'Selisih dan/atau fraud yang terjadi pada waktu dilakukan penambahan saldo pada Rekening Mitra Tepat dan/atau  penambahan uang tunai oleh pihak ketiga  yang  ditunjuk  BTPNS  merupakan tanggung jawab pihak ketiga sepenuhnya.',
	indexStr: '2.',
	level: 2,
}, {
	text: 'Jenis perangkat elektronik : (input jenis hp dan sistem - USSD)',
	indexStr: 'd.',
	level: 1,
}, {
	text: 'Perangkat elektronik yang digunakan dalam transaksi Mitra Tepat adalah handphone.',
	indexStr: '1.',
	level: 2,
}, {
	text: 'Type handphone yang digunakan antara lain feature  phone, dan smartphone (blackberry, android, windows phone, iphone dll).',
	indexStr: '2.',
	level: 2,
}, {
	text: 'Mitra Tepat dapat menggunakan akses USSD dengan Kode akses *365# atau aplikasi Mitra Tepat Wow! Syariah yang dapat di download di playstore.',
	indexStr: '3.',
	level: 2,
}, {
	text: 'Untuk akses USSD Operator	telekomunikasi yang bisa    digunakan: TELKOMSEL, XL dan INDOSAT dan Three (3).',
	indexStr: '4.',
	level: 2,
}, {
	text: 'Standar Minimum Layanan BTPNS',
	indexStr: 'e.',
	level: 1,
}, {
	text: 'Menjelaskan Ringkasan Informasi produk ketika akan melakukan akuisisi nasabah.',
	indexStr: '1.',
	level: 2,
}, {
	text: 'Melakukan proses pengenalan Nasabah standar (customer due diligence) dalam melakukan akuisisi calon nasabah sesuai yang dipersyaratkan oleh BTPNS.',
	indexStr: '2.',
	level: 2,
}, {
	text: 'Memiliki dan memasang material branding standar Mitra Tepat sesuai dengan ketentuan BTPNS.',
	indexStr: '3.',
	level: 2,
}, {
	text: 'Memiliki saldo Rekening Mitra Tepat dan uang tunai untuk melayani transaksi Nasabah BTPN Wow! Syariah.',
	indexStr: '4.',
	level: 2,
}, {
	text: 'HAK DAN KEWAJIBAN PARA PIHAK',
	indexStr: 'Pasal 3',
	level: 0,
}, {
	text: 'Hak dan Kewajiban BTPNS :',
	indexStr: '3.1',
	level: 1,
}, {
	text: 'Hak BTPNS  antara lain :',
	indexStr: '1.',
	level: 2,
}, {
	text: 'Meminta laporan kepada Mitra Tepat;',
	indexStr: 'a.',
	level: 3,
}, {
	text: 'Melakukan pengawasan dan pemeriksaan terhadap Mitra Tepat;',
	indexStr: 'b.',
	level: 3,
}, {
	text: 'Memblokir rekening Mitra Tepat apabila ditemukan indikasi kecurangan (fraud) yang dilakukan Mitra Tepat;',
	indexStr: 'c.',
	level: 3,
}, {
	text: 'Memberi teguran-teguran, surat peringatan apabila Mitra Tepat BTPN  Wow! Syariah tidak memenuhi target yang telah ditentukan oleh BTPNS melalui surat pemberitahuan yang disampaikan kepada Mitra Tepat BTPN  Wow! Syariah secara terpisah;',
	indexStr: 'd.',
	level: 3,
}, {
	text: 'Menyetujui atau menolak permintaan Mitra Tepat untuk pemindahan lokasi Mitra Tepat;',
	indexStr: 'e.',
	level: 3,
}, {
	text: 'Melakukan pengawasan dan pemeriksaan terhadap Mitra Tepat termasuk tidak terbatas pada dokumen, pencatatan dan laporan dari  Mitra Tepat.',
	indexStr: 'f.',
	level: 3,
}, {
	text: 'Melakukan penurunan dan kenaikan level klasiﬁkasi Mitra Tepat.',
	indexStr: 'g.',
	level: 3,
}, {
	text: 'Kewajiban BTPNS  antara lain :',
	indexStr: '2.',
	level: 2,
}, {
	text: 'Memberikan Imbalan Jasa/Komisi kepada Mitra Tepat;',
	indexStr: 'a.',
	level: 3,
}, {
	text: 'Menyediakan Layanan BTPNS  termasuk sistem aplikasi;',
	indexStr: 'b.',
	level: 3,
}, {
	text: 'Melakukan edukasi dan pelatihan;',
	indexStr: 'c.',
	level: 3,
}, {
	text: 'Mengembalikan dana calon nasabah bilamana pembukaan rekening nasabah ditolak dengan pertimbangan dari BTPNS.',
	indexStr: 'd.',
	level: 3,
}, {
	text: 'Hak dan Kewajiban Mitra Tepat',
	indexStr: '3.2',
	level: 1,
}, {
	text: 'Hak Mitra Tepat antara lain ',
	indexStr: '1.',
	level: 2,
}, {
	text: 'Menerima Imbalan Jasa/Komisi dan mendapatkan pembinaan;',
	indexStr: 'a.',
	level: 3,
}, {
	text: 'Memperoleh pelatihan dan edukasi;',
	indexStr: 'b.',
	level: 3,
}, {
	text: 'Memperoleh Layanan BTPNS  termasuk sistem aplikasi;',
	indexStr: 'c.',
	level: 3,
}, {
	text: 'Memperoleh prioritas pertama untuk menjadi mitra di cabang atau kota yang akan dibuka oleh BTPNS  dan terhadap produk-produk baru yang akan diluncurkan oleh BTPNS ;',
	indexStr: 'd.',
	level: 3,
}, {
	text: 'Diikutsertakan dalam diskusi yang akan diadakan oleh BTPNS tentang rencana distribusi dalam upaya mencapai operasional harian yang baik namun tidak mengesampingkan target BTPNS yang telah ditetapkan.',
	indexStr: 'e.',
	level: 3,
}, {
	text: 'Kewajiban Mitra Tepat antara lain :',
	indexStr: '2.',
	level: 2,
}, {
	text: 'Memastikan keamanan dan kerahasiaan pencatatan, data, aset dan dokumen yang diterima olehnya termasuk  menjaga  kerahasiaan  BTPNS  dan data pribadi Nasabah BTPNS ;',
	indexStr: 'a.',
	level: 3,
}, {
	text: 'Melaksanakan prosedur standar yang dipersyaratkan oleh BTPNS  termasuk dalam hal terjadi kondisi tertentu yang menyebabkan Mitra Tepat tidak dapat beroperasi;',
	indexStr: 'b.',
	level: 3,
}, {
	text: 'Mematuhi dan melaksanakan ketentuan penyelenggaraan Layanan BTPNS ;',
	indexStr: 'c.',
	level: 3,
}, {
	text: 'Menyampaikan laporan kepada BTPNS baik secara periodik mingguan, bulanan atau laporan-laporan yang dapat diminta setiap saat oleh BTPNS  termasuk dalam hal Nasabah  BTPNS   Wow! Syariah dicurigai melakukan tindakan yang terkait dengan kejahatan, terorisme atau penyaluran obat-obatan terlarang;',
	indexStr: 'd.',
	level: 3,
}, {
	text: 'Membantu BTPNS  untuk melakukan sosialisasi dan edukasi kepada masyarakat terhadap program- program BTPNS  dengan menggunakan sarana promosi yang ditetapkan dan diberikan oleh BTPNS;',
	indexStr: 'e.',
	level: 3,
}, {
	text: 'Tidak menggunakan logo dan/atau atribut BTPNS tanpa persetujuan tertulis dari BTPNS;',
	indexStr: 'f.',
	level: 3,
}, {
	text: 'Menjamin ketersediaan Layanan BTPNS   dan material promosi serta bertanggung jawab atas penggunaan likuiditas saldo Rekening Mitra Tepat, dan/atau uang tunai Mitra Tepat yang berkaitan dengan Transaksi Perbankan termasuk kehilangan uang ﬁsik;',
	indexStr: 'g.',
	level: 3,
}, {
	text: 'Melakukan koordinasi dengan pihak ketiga yang ditunjuk oleh BTPNS  untuk memenuhi kebutuhan uang saldo Rekening Mitra Tepat maupun uang tunai;',
	indexStr: 'h.',
	level: 3,
}, {
	text: 'Membebaskan BTPNS  dari segala ganti  rugi gugatan,  tuntutan  atau  tindakan  hukum  lain dalam bentuk apapun dalam hal terjadinya risiko material apabila terdapat uang palsu yang diterima Mitra Tepat dalam Transaksi Perbankan;',
	indexStr: 'i.',
	level: 3,
}, {
	text: 'Menjalankan tugas pokoknya dalam hal Mitra Tepat memiliki pekerjaan pokok antara lain guru, buruh atau karyawan perusahaan;',
	indexStr: 'j.',
	level: 3,
}, {
	text: 'Memenuhi dan mentaati  seluruh  persyaratan Mitra Tepat, target, ketentuan Layanan BTPNS , ketentuan Imbalan Jasa/Komisi yang ditetapkan oleh BTPNS  dari waktu ke waktu termasuk  namun  tidak terbatas sebagai akibat dari penurunan maupun kenaikan level klasiﬁkasi Mitra Tepat;',
	indexStr: 'k.',
	level: 3,
}, {
	text: 'Menampung keluhan dan/atau pertanyaan Nasabah BTPN  Wow! Syariah dan meneruskan kepada BTPNS   melalui call center Mitra Tepat atau petugas BTPNS ;',
	indexStr: 'l.',
	level: 3,
}, {
	text: 'Memberitahukan kepada BTPNS apabila mengetahui informasi mengenai suatu keadaan, peristiwa, rencana atau hal-hal lainnya yang dapat diperkirakan mengakibatkan kerugian baik materiil maupun non-materiil terhadap BTPNS, termasuk dan tidak terbatas pada kecurangan yang dilakukan oleh nasabah atau pihak  lain.',
	indexStr: 'm.',
	level: 3,
}, {
	text: 'LARANGAN BAGI MITRA TEPAT',
	indexStr: 'Pasal 4',
	level: 0,

}, { 
	text: 'Larangan pemberian hadiah dan Imbalan Jasa/Komisi. Mitra Tepat dilarang menawarkan, memberikan, atau setuju untuk  memberi hadiah, potongan diskon, Imbalan Jasa/Komisi, rabat, dan bentuk-bentuk hadiah/bingkisan lainnya kepada pegawai BTPNS  dengan maksud untuk mendapatkan, melaksanakan Perjanjian atau sebagai imbal balik atas jasa-jasa karyawan BTPNS  dalam memberikan kemudahan atau pengaturan untuk Mitra Tepat.',
	indexStr: '4.1',
	level: 1,
}, {
	text: 'Mengalihkan, menguasakan dan/atau memindahtangankan dengan cara apapun juga hak dan kewajiban yang timbul berdasarkan  Perjanjian,  baik untuk sebagian atau seluruhnya kepada  pihak  ketiga atau lainnya tanpa terlebih dahulu mendapat persetujuan tertulis dari BTPNS ;',
	indexStr: 'a.',
	level: 2,
}, {
	text: 'Memiliki sistem tersendiri di luar sistem yang disediakan oleh BTPNS  untuk kegiatan aktivitas jasa sistem pembayaran dan perbankan terbatas sesuai Perjanjian;',
	indexStr: 'b.',
	level: 2,
}, {
	text: 'Memperbanyak dan menyalahgunakan tanpa sepengetahuan BTPNS  segala sesuatu baik formulir, perlengkapan dan/atau dokumen pendukung yang merupakan standar BTPNS ;',
	indexStr: 'c.',
	level: 2,
}, {
	text: 'Memungut biaya tambahan atas Transaksi Perbankan yang dilakukan oleh Nasabah BTPN Wow! Syariah selain biaya yang ditetapkan oleh BTPNS ;',
	indexStr: 'd.',
	level: 2,
}, {
	text: 'Pelanggaran terhadap ketentuan pasal ini oleh Mitra Tepat atau pegawainya atau oleh orang yang bekerja untuknya dapat mengakibatkan diakhirinya Perjanjian oleh BTPNS  dan biaya- biaya yang timbul akibat pengakhiran Perjanjian menjadi tanggung jawab Mitra Tepat disertai tindakan penghapusan Mitra Tepat sebagai mitra BTPNS',
	indexStr: '4.3',
	level: 1
}, {
	text: 'EVALUASI',
	indexStr: 'Pasal 5',
	level: 0
}, {
	text: 'BTPNS berhak mengadakan evaluasi terhadap performasi Mitra Tepat setiap 3 (tiga) bulan atau sewaktu-waktu, dengan atau tanpa pemberitahuan dan/atau persetujuan Mitra Tepat. Evaluasi dan penilaian tersebut dilakukan sesuai dengan persyaratan dan ketentuan yang telah ditetapkan berdasarkan ketentuan Perjanjian atau ketentuan lain yang akan ditetapkan BTPNS di kemudian hari.',
	indexStr: '5.1',
	level: 1,
}, {
text: 'Apabila berdasarkan hasil evaluasi dan penilaian BTPNS  dimana Mitra Tepat dianggap berhasil atau memenuhi persyaratan BTPNS  maka Mitra Tepat berhak dan setuju atas kenaikan level klasiﬁkasi Mitra Tepat.',
indexStr: '5.2',
level: 1,
}, {
text: 'Apabila berdasarkan hasil evaluasi dan penilaian Mitra Tepat dianggap gagal atau tidak dapat memenuhi syarat dan ketentuan dalam Perjanjian selama 3 (tiga) kali berturut-turut maka akan berakibat diakhirinya Perjanjian.',
indexStr: '5.3',
level: 1,
}, {
text: 'Evaluasi dan penilaian yang dimaksud dalam ayat (5.1) tersebut di atas termasuk namun tidak terbatas pada tersedianya pelaporan, penentuan target, standar layanan, kompensasi atas tercapainya atau tidak tercapainya target serta aktivitas pemasaran lainnya sebagaimana diatur dalam Perjanjian.',
indexStr: '5.4',
level: 1,
}, {
text: 'Atas hasil evaluasi dan penilaian Mitra Tepat dalam ayat 5.2 dan 5.3 tersebut di atas maka akan diberitahukan secara tertulis oleh BTPNS kepada Mitra Tepat, pemberitahuan tersebut merupakan satu kesatuan dan bagian yang tidak terpisahkan dari Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah versi 01 dan Perjanjian.',
indexStr: '5.5',
level: 1,
}, {
	text: 'KERAHASIAAN',
	indexStr: 'Pasal 6',
	level: 0
}, {
text: 'Para Pihak menjamin bahwa selama Perjanjian berlangsung, Para Pihak tidak akan memanfaatkan data dan/atau informasi dalam bentuk apapun yang telah diterima dari salah satu pihak untuk kepentingan pihak lainnya kepada pihak ketiga manapun juga.',
indexStr: '6.1',
level: 1,
}, {
text: 'Mitra Tepat menjamin dan bertanggung jawab atas semua informasi/data, formulir yang diperoleh baik dari Nasabah BTPNS maupun dari BTPNS dalam bentuk apapun adalah milik BTPNS  yang wajib dijaga kerahasiaannya.',
indexStr: '6.2',
level: 1,
}, {
text: 'Para Pihak sepakat bahwa segala kerahasiaan berkenaan dengan isi dari Perjanjian hanya dapat diungkapkan sesuai hukum yang berlaku.',
indexStr: '6.3',
level: 1,
}, {
	text: 'RELOKASI MITRA TEPAT',
	indexStr: 'Pasal 7',
	level: 0
}, {

text: 'Mitra Tepat harus mengajukan permohonan untuk  relokasi  Mitra Tepat  jika lokasi Mitra Tepat ingin dipindahkan dari lokasi awal.',
indexStr: '7.1',
level: 1,
}, {
text: 'BTPNS berdasarkan pertimbangannya sendiri berhak untuk menyetujui atau menolak permohonan relokasi Mitra Tepat.',
indexStr: '7.2',
level: 1,

}, {
	text: 'MEKANISME SANKSI',
	indexStr: 'Pasal 8',
	level: 0

}, {
text: 'BTPNS akan memberikan surat peringatan tertulis kepada Mitra Tepat jika Mitra Tepat melanggar atau tidak memenuhi persyaratan dan penilaian kinerja yang ditetapkan oleh BTPNS.',
indexStr: '8.1',
level: 1,
}, {
text: 'Jika tidak ada peningkatan dan atau perbaikan terhadap penilaian kinerja Mitra Tepat atas surat peringatan yang diberikan, maka BTPNS akan memberikan surat peringatan  kedua kepada Mitra Tepat. Pengenaan surat peringatan sampai dengan 3 (tiga) kali mengakibatkan berakhirnya Perjanjian.',
indexStr: '8.2',
level: 1,
}, {
text: 'Rekening Mitra Tepat akan diblokir sementara (suspend) selama proses evaluasi dan penyidikan bilamana ditemukan indikasi kecurangan (fraud) yang dilakukan oleh Mitra Tepat.',
indexStr: '8.3',
level: 1,
}, {
text: 'BTPNS berhak untuk menutup Rekening Mitra Tepat jika Mitra Tepat melanggar ketentuan sebagaimana dimaksud dalam Perjanjian termasuk namun tidak terbatas pada Pasal 3, Pasal 4 dan Pasal 6 Perjanjian.',
indexStr: '8.4',
level: 1,
}, {
	text: 'PENGAKHIRAN PERJANJIAN',
	indexStr: 'Pasal 9',
	level: 0

}, {

text: 'Jangka waktu Perjanjian adalah sebagaimana tercantum dalam Perjanjian dan dapat diperpanjang secara otomatis.',
indexStr: '9.1',
level: 1,
}, {
text: 'Jika salah satu pihak ingin mengakhiri perjanjian sesuai jangka waktu, maka pihak tersebut harus memberitahukan kepada pihak lainnya paling lambat 1 bulan sebelum jangka waktu berakhir.',
indexStr: '9.2',
level: 1,
}, {
text: 'Perjanjian dapat berakhir sebelum jangka waktu sebagaimana dimaksud Perjanjian dengan ketentuan pihak  yang  mengakhiri memberitahukan maksudnya secara tertulis kepada pihak lainnya selambat-lambatnya 1 (satu) bulan sebelumnya.',
indexStr: '9.3',
level: 1,
}, {
text: 'Perjanjian dapat diakhiri sebelum jangka waktu berakhir dengan pemberitahuan 1 (satu) bulan sebelumnya apabila Mitra Tepat melakukan tindakan antara lain :',
indexStr: null,
level: 1,
}, {
text: 'Mitra Tepat memungut/mengenakan biaya tambahan kepada Nasabah BTPN Wow! Syariah selain yang telah ditetapkan oleh BTPNS ;',
indexStr: 'a.',
level: 2,
}, {
text: 'Mitra Tepat menawarkan jasa keuangan selain yang :',
indexStr: 'b.',
level: 2,
}, {
text: 'Telah disepakati dalam Perjanjian dengan BTPNS ;',
indexStr: '1)',
level: 3,
}, {
text: 'Telah disepakati dalam perjanjian kerja sama dengan lembaga jasa keuangan lainnya, dan /atau',
indexStr: '2)',
level: 3,
}, {
text: 'Telah mendapatkan persetujuan instansi yang berwenang.',
indexStr: '3)',
level: 3,
}, {
text: 'Fraud, ketidakjujuran dan/atau penyalahgunaan lainnya;',
indexStr: 'c.',
level: 2,
}, {
text: 'Memindahkan, merelokasi atau menutup usaha tanpa pemberitahuan sebelumnya kepada BTPNS ;',
indexStr: 'd.',
level: 2,
}, {
text: 'Melakukan kejahatan atau tindak pidana lainnya;',
indexStr: 'e.',
level: 2,
}, {
text: 'Mengalami kerugian yang diperkirakan akan  mengganggu kelangsungan penyelenggaraan Layanan BTPNS ;',
indexStr: 'f.',
level: 2,
}, {
text: 'Melanggar ketentuan yang mengatur mengenai Layanan BTPNS /Transaksi Perbankan;',
indexStr: 'g.',
level: 2,
}, {
text: 'Apabila hasil evaluasi menyatakan bahwa Mitra Tepat tidak dapat memenuhi persyaratan yang telah ditetapkan berdasarkan ketentuan Perjanjian atau ketentuan lain yang ditetapkan oleh BTPNS  termasuk namun tidak terbatas pada kegagalan memenuhi target akuisisi atau ketentuan-ketentuan lainnya yang akan diatur dan ditentukan kemudian oleh BTPNS ;',
indexStr: 'h.',
level: 2,
}, {
text: 'Diperintahkan oleh Bank Indonesia, Otoritas Jasa Keuangan atau instansi pemerintah lainnya yang berwenang untuk diakhiri;',
indexStr: 'i.',
level: 2,
}, {
text: 'Perjanjian berakhir secara seketika apabila BTPNS  tidak lagi memenuhi kriteria sebagai penyelenggara Layanan BTPNS.',
indexStr: '9.4',
level: 1,
}, {
text: 'Akibat dari berakhirnya Perjanjian, Mitra Tepat setuju bahwa :',
indexStr: '9.5',
level: 1,
}, {
text: 'BTPNS berhak melanjutkan, mengubah atau mengakhiri permohonan atau aplikasi dari Nasabah BTPN Wow! Syariah yang masih dalam proses atas dasar pertimbangannya sendiri;',
indexStr: 'a.',
level: 2,
}, {
text: 'Mitra Tepat berkewajiban segera melepaskan dan selanjutnya memusnahkan segala atribut keagenan (merk dagang) antara lain logo, motto, serta  barang-barang lainnya yang merupakan  hak  eksklusif  dari BTPNS  juga termasuk seluruh informasi  dan  bahan-bahan tertulis yang telah dan dihasilkan dalam kaitannya dengan Perjanjian;',
indexStr: 'b.',
level: 2,
}, {
text: 'BTPNS  berhak menggunakan dana/mendebet yang tersedia di rekening Mitra Tepat untuk menyelesaikan seluruh kewajiban Mitra Tepat yang belum dilaksanakan dan untuk keperluan tersebut BTPNS  berwenang untuk memblokir rekening Mitra Tepat hingga seluruh kewajiban Mitra Tepat sepenuhnya terlaksana;',
indexStr: 'c.',
level: 2,
}, {
text: 'Mitra Tepat diwajibkan untuk menutup rekening Mitra Tepat yang dipergunakan. Selanjutnya jika dalam rekening Mitra Tepat tersebut masih memiliki saldo, Mitra Tepat dapat memindahkan ke dalam rekening pribadi yang dimiliki; ',
indexStr: 'd.',
level: 2,
}, {

text: 'BTPNS tidak akan memberikan imbalan jasa/komisi Mitra Tepat akibat berakhirnya Perjanjian.',
indexStr: 'e.',
level: 2
}, {
text: 'BTPNS berhak untuk mengubah rekening Mitra Tepat menjadi rekening Nasabah BTPN  Wow! Syariah dengan jenis rekening Tabungan Nasabah BTPN  Wow! Syariah Plus (Reguler Saving Account/RSA) termasuk memindahkan sisa saldo yang terdapat di rekening Mitra Tepat sebelumnya ke dalam rekening RSA tersebut.',
indexStr: 'f.',
level: 2
}, {
text: 'Berakhirnya Perjanjian tidak mengurangi dan tidak membebaskan hak dan kewajiban masing masing Pihak yang telah timbul sebelum Perjanjian diakhiri sampai seluruh hak dan kewajiban tersebut selesai dilakukan oleh Pihak lainnya.',
indexStr: '9.6',
level: 1
}, {
text: 'Pengakhiran Perjanjian, Para Pihak dengan ini mengesampingkan berlakunya ketentuan Pasal 1266 dan 1267 Kitab Undang-Undang Hukum Perdata.',
indexStr: '9.7',
level: 1
}, {

	text: 'PERNYATAAN DAN JAMINAN PARA PIHAK',
	indexStr: 'Pasal 10',
	level: 0

}, {	
	text: 'Masing-masing Pihak dengan ini menyatakan dan menjamin Pihak lainnya dalam Perjanjian, sebagai berikut:',
	indexStr: null,
	level: 1

}, {


text: 'Mitra Tepat menyatakan dan menjamin bahwa dalam hal terjadi perselisihan sehubungan dengan penggunaan Layanan BTPNS  maka bukti transaksi elektronik (termasuk sms notiﬁkasi) dapat dijadikan alat bukti dan karenanya Mitra Tepat menyetujui bahwa BTPNS  dapat melakukan monitoring secara elektronik atas data transaksi untuk tujuan keamanan dan mutu pelayanan kepada Nasabah BTPN  Wow! Syariah.',
indexStr: '10.1',
level: 1,
}, {
text: 'Mitra Tepat menyatakan dan menjamin bahwa dana-dana yang ditransaksikan bukan merupakan dana yang dihasilkan dari korupsi, penyuapan, penyelundupan barang, penyelundupan tenaga kerja, penyelundupan imigran, tindak  pidana perbankan, tindak pidana di bidang  pasar  modal,  tindak pidana di bidang asuransi, narkotika dan psikotropika, perdagangan manusia, perdagangan senjata  gelap, penculikan, terorisme, pencurian, penggelapan, penipuan, pemalsuan uang, perjudian,  prostitusi,  tindak  pidana  di bidang perpajakan, tindak pidana di bidang kehutanan, tindak pidana di bidang lingkungan hidup, tindak pidana di bidang kelautan atau tindak pidana lainnya sebagaimana dimaksud dalam Undang-Undang tentang Tindak Pidana  Pencucian Uang sebagaimana diubah dan ditambah dari waktu ke waktu.',
indexStr: '10.2',
level: 1,
}, {
text: 'Mitra Tepat menyatakan dan menjamin BTPNS  sebagai berikut :',
indexStr: '10.3',
level: 1,
}, {
text: 'BTPNS  dibebaskan dari segala tuntutan hukum dan/atau risiko material apabila terdapat  uang  palsu  yang diterima Mitra Tepat dalam Transaksi Perbankan;',
indexStr: 'a.',
level: 2,
}, {
text: 'BTPNS tidak bertanggung jawab atas kerugian yang berkaitan dengan kelalaian Mitra Tepat termasuk kehilangan uang ﬁsik atau pencurian uang oleh pihak manapun.',
indexStr: 'b.',
level: 2,
}, {
	text: 'KEADAAN KAHAR (FORCE MAJEURE)',
	indexStr: 'Pasal 11',
	level: 0
}, {

text: 'Keadaan Kahar adalah segala kejadian yang berada di luar kekuasaan pihak  yang  terkena  kejadian  bersangkutan, seperti namun tidak terbatas pada kebakaran,  huru-hara, banjir, gempa bumi, ledakan, pemogokan, sabotase, pemberontakan, peraturan-peraturan pemerintah perubahan moneter seperti devaluasi atau inﬂasi.',
indexStr: '11.1',
level: 1,
}, {
text: 'Apabila terjadi Keadaan Kahar, Para Pihak akan dibebaskan, dilindungi serta tidak bertanggung jawab atas keterlambatan dan/ atau kegagalan atas pelaksanaan sebagian atau seluruh kewajiban dibawah Perjanjian.',
indexStr: '11.2',
level: 1,
}, {
text: 'Kejadian Keadaan Kahar ini harus  diberitahukan  secara tertulis oleh Pihak yang mengalaminya kepada Pihak  lain dalam waktu 14 (empat belas)  Hari  Kerja  sejak  Force Majeure tersebut terjadi atau diketahui serta menerangkan penyebabnya dan perkiraan masa berlangsungnya keterlambatan atau kegagalan dan mengambil semua langkah yang wajar guna memperbaiki atau memperkecil akibat yang ditimbulkan oleh penyebab tersebut.',
indexStr: '11.3',
level: 1,

}, {
	text: 'PENYELESAIAN PERSELISIHAN',
	indexStr: 'Pasal 12',
	level: 0

}, {


text: 'Keabsahan, penafsiran, dan pelaksanaan dari Perjanjian diatur dan tunduk sepenuhnya pada peraturan hukum yang berlaku di negara Republik Indonesia.',
indexStr: '12.1',
level: 1,
}, {
text: 'Perselisihan yang timbul antara Para Pihak di dalam pelaksanaan Perjanjian akan diselesaikan secara musyawarah untuk mencapai mufakat.',
indexStr: '12.2',
level: 1,
}, {
text: 'Apabila cara musyawarah untuk mencapai mufakat tersebut tidak tercapai, maka Para  Pihak  sepakat  untuk  menyelesaikan segala perselisihan yang timbul melalui Pengadilan Negeri sebagaimana dirujuk dalam Perjanjian, namun tanpa mengurangi hak BTPNS  untuk mengajukan tuntutan/gugatan di Pengadilan Negeri    lainnya.',
indexStr: '12.3',
level: 1,
}, {
	text: 'PENUTUP',
	indexStr: 'Pasal 13',
	level: 0

}, {


text: 'Mitra Tepat dengan ini menyatakan bahwa Mitra Tepat tunduk sepenuhnya kepada semua ketentuan operasional   BTPNS.',
indexStr: '13.1',
level: 1,
}, {
text: 'Semua dan setiap kuasa yang diberikan oleh Mitra Tepat kepada BTPNS dalam melaksanakan Perjanjian merupakan bagian yang terpenting dan tidak dapat dipisahkan dari Perjanjian. Kuasa tersebut tidak dapat ditarik kembali maupun dibatalkan oleh sebab yang tercantum dalam Pasal 1813, 1814 dan 1816 Kitab Undang-Undang Hukum Perdata.',
indexStr: '13.2',
level: 1,
}, {
text: 'Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah versi 01 dan Perjanjian tidak dapat diubah kecuali dengan suatu perjanjian perubahan (addendum) yang ditandatangani Para Pihak.',
indexStr: '13.3',
level: 1,
}, {
text: 'Mengenai pengakhiran Perjanjian, Para Pihak dengan ini melepaskan ketentuan pasal 1266 dan pasal 1267 Kitab Undang-Undang Hukum Perdata.',
indexStr: '13.4',
level: 1,
}, {
text: 'Apabila terdapat inkonsistensi diantara ketentuan yang terdapat dalam Syarat dan Ketentuan Penunjukan Mitra Tepat BTPN  Wow! Syariah versi 01 dengan ketentuan dalam Perjanjian maka yang berlaku adalah ketentuan yang berlaku dalam   Perjanjian.',
indexStr: '13.5',
level: 1,
}, {
text: 'Dalam hal terdapat ketentuan dalam Syarat dan Ketentuan Penunjukan Mitra Tepat BTPN Wow! Syariah versi 01 maupun Perjanjian menjadi tidak berlaku atau tidak dapat diberlakukan karena suatu peraturan perundangan yang berlaku atau karena sebab lainnya maka keadaan tersebut tidak mempengaruhi atau berakibat terhadap ketentuan lainnya.',
indexStr: '13.6',
level: 1,

}]