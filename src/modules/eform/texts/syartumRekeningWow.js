export const syartumRekeningWow = [
    {
        text: 'Definisi',
        indexStr: 'Pasal 1',
        level: 0,
    }, {
        text: 'Akad adalah perjanjian tertulis antara Bank dan Nasabah yang memuat adanya hak dan kewajiban bagi masing-masing pihak berkenaan dengan penempatan dan pengelolaan dana Nasabah pada Bank dengan tunduk kepada prinsip-prinsip syariah.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Akad Wadi’ah Yadh-Dhamanah, selanjutnya disebut dengan Wadi’ah, adalah akad penitipan dan penyimpanan dana Nasabah pada Bank dengan ketentuan Bank dapat memanfaatkan dana Nasabah untuk tujuan produktif dan wajib mengembalikannya secara utuh kepada Nasabah ketika Nasabah menarik kembali dananya. Bank dapat memberikan bonus atas Rekening sesuai dengan kebijakan Bank.',
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Bank adalah PT Bank BTPN Syariah, Tbk yang berkedudukan di Jakarta Selatan, melalui seluruh jaringan kantornya termasuk Mitra Tepat yang ditunjuk oleh Bank (jika ada) di Indonesia.',
        indexStr: '3.',
        level: 1,
    }, {
        text: 'e-Form adalah formulir permohonan pembukaan Rekening yang diajukan Nasabah melalui media elektronik yang berisi data dan informasi Nasabah.',
        indexStr: '4.',
        level: 1,
    }, {
        text: 'Instruksi adalah setiap perintah dari Nasabah kepada Bank untuk melakukan suatu Transaksi Perbankan atas Rekening Nasabah dengan cara yang ditentukan dan diterima oleh Bank.',
        indexStr: '5.',
        level: 1,
    }, {
        text: 'Layanan BTPN Wow! iB selanjutnya disebut BTPN Wow! iB adalah layanan perbankan elektronik yang memungkinkan Nasabah terlebih dahulu melakukan pendaftaran untuk melakukan Transaksi Perbankan melalui media elektronik antara lain telepon selular (handphone) dan/atau komputer.',
        indexStr: '6.',
        level: 1,
    }, {
        text: 'Nasabah adalah perorangan Warga Negara Indonesia (WNI) yang memiliki Rekening di Bank dan/atau menggunakan fasilitas/ layanan perbankan yang disediakan oleh Bank.',
        indexStr: '7.',
        level: 1,
    }, {
        text: 'Penyedia Jasa Telekomunikasi adalah perusahaan telekomunikasi yang telah bekerjasama dengan Bank.',
        indexStr: '8.',
        level: 1,
    }, {
        text: 'PIN(Personal Identification Number) adalah kombinasi angka rahasia yang diperlukan agar Nasabah dapat mengakses BTPN Wow! iB dan kewenangan penggunaannya hanya ada pada Nasabah.',
        indexStr: '9.',
        level: 1,
    }, {
        text: 'Rekening adalah simpanan Nasabah pada Bank dalam bentuk tabungan berdasarkan Akad Wadi’ah Yadh-Dhamanah yang terhubung dengan BTPN Wow! iB dalam rangka Layanan Keuangan Tanpa Kantor Dalam Rangka Keuangan Inklusif (Laku Pandai) sesuai dengan ketentuan yang berlaku.',
        indexStr: '10.',
        level: 1,
    }, {
        text: 'Rekening Bersama adalah Rekening tabungan dengan karakteristik BSA khusus bagi calon Nasabah perorangan yang belum memenuhi syarat untuk memiliki kartu tanda penduduk (KTP) dan/atau surat izin mengemudi (SIM), namun wajib memberikan pengganti dokumen identitas berupa kartu tanda pelajar yang disertai dengan surat persetujuan dari orang tua atau wali yang dilengkapi dengan dokumen identitas orang tua atau wali.',
        indexStr: '11.',
        level: 1,
    }, {
        text: 'Syarat dan Ketentuan adalah Syarat dan Ketentuan Umum Pembukaan Rekening, termasuk setiap perubahan dan pembaharuannya di kemudian hari (jika ada).',
        indexStr: '12.',
        level: 1,
    }, {
        text: 'Transaksi Perbankan adalah transaksi finansial yang berdampak pada perubahan saldo Rekening, misalnya pemindahbukuandan transaksi lainnya yang disetujui oleh Bank, dan transaksi non finansial yang tidak berdampak pada perubahan saldo Rekening, misalnya permintaan informasi saldo dan mutasi Rekening dan transaksi lainnya yang disetujui oleh Bank.',
        indexStr: '13.',
        level: 1,
    }, {
        text: 'Ketentuan Umum',
        indexStr: 'Pasal 2',
        level: 0,
    }, {
        text: 'Nasabah melakukan pembukaan Rekening melalui e-From dan memenuhi segala persyaratan yang ditentukan oleh Bank, serta baru efektif setelah disetujui oleh Bank.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Bank akan memberikan Informasi saldo/mutasi simpanan kepada Nasabah melalui media yang disepakati antara Bank dan Nasabah.',
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Nasabah yang telah disetujui pembukaan Rekeningnya akan menerima PIN dan wajib menjaga kerahasiaan PIN sebagai persetujuan atas setiap Instruksi Transaksi Perbankan melalui menu BTPN Wow! iB.',
        indexStr: '3.',
        level: 1,
    }, {
        text: 'Nasabah hanya dapat melakukan transaksi penyetoran tunai selama proses verifikasi belum selesai dilakukan oleh Bank dan memahami bahwa dalam hal Bank menolak permohonan pembukaan Rekening, dana yang telah disetorkan dapat ditarik melalui penarikan tunai di Mitra Tepat atau kantor Bank.',
        indexStr: '4.',
        level: 1,
    }, {
        text: 'Untuk setiap pelaksanaan Transaksi Perbankan, Nasabah wajib mengisi dan memastikan semua data dan Instruksi secara benar dan lengkap. Bank akan melakukan konfirmasi data yang diinput Nasabah untuk setiap transaksi finansial pada BTPN Wow! iB, dan konfirmasi Nasabah merupakan persetujuan bahwa data yang disampaikan oleh BTPN Wow! iB adalah benar dan persetujuan kepada Bank untuk melaksanakan Instruksi Nasabah. ',
        indexStr: '5.',
        level: 1,
    }, {
        text: 'Bank tidak bertanggung jawab atas segala risiko yang timbul akibat kelalaian, kekeliruan, pemalsuan, penipuan, kesalahpahaman, ketidaklengkapan, ketidakjelasan, ketidaktepatan Instruksi dari Nasabah sepanjang Transaksi Perbankan dilakukan dengan menggunakan PIN dan Nasabah dengan ini membebaskan Bank dari segala tuntutan yang mungkin timbul, baik dari pihak lain maupun Nasabah sendiri',
        indexStr: '6.',
        level: 1,
    }, {
        text: 'Penggunaan PIN mempunyai kekuatan hukum yang setara dengan perintah tertulis yang ditandatangani oleh Nasabah dan dengan ini Nasabah menyatakan bahwa penggunaan PIN juga merupakan pemberian kuasa dari Nasabah kepada Bank untuk melaksanakan Transaksi Perbankan, termasuk namun tidak terbatas untuk melakukan pendebetan pada Rekening Nasabah, baik dalam rangka pelaksanaan Transaksi Perbankan sesuai Instruksi maupun untuk pembayaran biaya-biaya transaksi yang telah dan/atau akan ditetapkan kemudian oleh Bank dengan pemberitahuan terlebih dahulu kepada Nasabah dalam bentuk dan media sarana berdasarkan kebijakan Bank.',
        indexStr: '7.',
        level: 1,
    }, {
        text: 'Pemberian Instruksi kepada Bank dari Rekening Bersama berlaku ketentuan sebagai berikut:', 
        indexStr: '8.',
        level: 1,
    }, {
        text: 'Rekening Bersama dibuka atas nama Nasabah yang belum cakap secara hukum dengan persetujuan dari salah satu orang tua/ walinya.', 
        indexStr: 'a.',
        level: 2,
    }, {
        text: 'Pemberian Instruksi kepada Bank dilakukan oleh Nasabah.', 
        indexStr: 'b.',
        level: 2,
    }, {
        text: 'Penutupan Rekening Bersama harus mendapat persetujuan dari orang tua/ walinya, dengan tetap memperhatikan ketentuan yang berlaku pada Bank.',
        indexStr: 'c.',
        level: 2,
    }, {
        text: 'Nasabah mengetahui dan menyetujui bahwa media elektronik dan SIM Card yang dipergunakan Nasabah pada BTPN Wow! iB sepenuhnya merupakan tanggung jawab Nasabah dan Nasabah wajib untuk menjaga keamanan media elektronik dan SIM Card tersebut agar terhindar dari penyalahgunaan oleh pihak lain. Oleh karena itu, Nasabah dengan ini membebaskan Bank dari segala ganti rugi dan/atau pertanggung jawaban dalam bentuk apapun kepada Nasabah atau pihak lain yang mungkin timbul akibat adanya penyalahgunaan media elektronik dan SIM Card.',
        indexStr: '9.',
        level: 1,
    }, {
        text: 'Jika Nasabah kehilangan media elektronik yang terhubung dengan BTPN Wow! iB, maka Nasabah wajib segera memberitahukan kepada Bank melalui layanan kontak Nasabah (Contact Center:1500300 atau 08001500300) untuk dilakukan pemblokiran Rekening dan selanjutnya Nasabah melakukan tindakan yang diberitahukan oleh petugas layanan kontak Nasabah. Segala biaya yang timbul menjadi beban Nasabah.',
        indexStr: '10.',
        level: 1,
    }, {
        text: 'Bank berhak untuk menghentikan BTPN Wow! iB untuk sementara waktu maupun untuk jangka waktu tertentu yang ditentukan oleh Bank untuk keperluan pembaruan, pemeliharaan atau untuk tujuan lain dengan alasan apapun yang dianggap baik oleh Bank, dengan pemberitahuan terlebih dahulu kepada Nasabah selambat-lambatnya 1 (satu) hari sebelum dilakukan penghentian sementara atau sesuai ketentuan Bank yang berlaku dan untuk itu Bank tidak berkewajiban mempertanggungjawabkannya kepada siapapun serta Nasabah setuju untuk tidak menuntut ganti kerugian kepada Bank.',
        indexStr: '11.',
        level: 1,
    }, {
        text: 'Pembukuan',
        indexStr: 'Pasal 3',
        level: 0,
    }, {
        text: 'Nasabah setuju bahwa dalam hal terdapat perbedaan saldo/ mutasi antara catatan/ pembukuan yang dimiliki oleh Nasabah dengan catatan/ pembukuan Bank, catatan/ pembukuan Bank merupakan bukti yang sah dan mengikat Nasabah, kecuali Nasabah dapat membuktikan lain.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Bank berhak dan dengan ini diberi kuasa oleh Nasabah untuk melakukan koreksi apabila terdapat kekeliruan dalam mengadministrasikan Rekening Nasabah. Jika saldo Rekening Nasabah tidak mencukupi untuk memperbaiki koreksi pembukuan, maka Bank berhak menagih kembali dengan seketika dan seluruh kekurangannya tersebut kepada Nasabah.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Pembuktian',
        indexStr: 'Pasal 4',
        level: 0,
    }, {
        text: 'Nasabah setuju bahwa catatan, salinan atau bentuk penyimpanan informasi atau data lain yang ada pada Bank, demikian juga sarana komunikasi lain yang dihasilkan atau dikirimkan oleh Bank adalah sebagai alat bukti yang sah atas Instruksi.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Nasabah mengakui semua Instruksi dari Nasabah yang diterima Bank akan diperlakukan sebagai alat bukti yang sah meskipun tidak dibuat dalam bentuk dokumen tertulis ataupun dokumen yang ditandatangani, dan Nasabah dengan ini bersedia untuk membebaskan Bank dari segala kerugian, tanggung jawab tuntutan dan biaya (termasuk biaya hukum yang layak) yang timbul sehubungan pelaksanaan Instruksi tersebut.',
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Biaya',
        indexStr: 'Pasal 5',
        level: 0,
    }, {
        text: 'Bank berhak dan dengan ini diberi kuasa oleh Nasabah untuk mendebet Rekening Nasabah guna pembayaran biaya (jika ada) sehubungan dengan layanan Bank, biaya administrasi, bea meterai, biaya sehubungan dengan fasilitas yang dikehendaki Nasabah, biaya transaksi, denda/ penalti dan biaya-biaya lainnya yang berlaku pada Bank.', 
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Pendebetan biaya dilakukan sesuai dengan ketentuan yang ditetapkan oleh Bank dan besarnya biaya (jika ada) telah diinformasikan terlebih dahulu kepada Nasabah.', 
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Nasabah juga akan menanggung biaya jaringan, data, dan biaya lain terkait  penggunaan BTPN Wow! iB dengan tarif yang sudah ditentukan oleh Penyedia Jasa Telekomunikasi.',
        indexStr: '3.',
        level: 1,
    }, {
        text: 'Penutupan, Pemblokiran, Penghentian Sementara dan Pencairan Rekening',
        indexStr: 'Pasal 6',
        level: 0,
    }, {
        text: 'Bank berhak menutup, memblokir, dan menghentikan sementara transaksi Nasabah (termasuk layanan dan/atau fasilitas lainnya yang diperoleh Nasabah berkaitan dengan Rekening), sekaligus membebankan biaya administrasi penutupan Rekening (jika ada) dan biaya-biaya lainnya yang berlaku pada Bank apabila :',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Rekening Nasabah telah menjadi tidak aktif selama 6 (enam) bulan sesuai ketentuan Bank.',
        indexStr: 'a.',
        level: 2,
    }, {
        text: 'Dalam hal Bank melakukan penutupan Rekening Nasabah, maka Bank wajib melakukan pemberitahuan kepada Nasabah melalui kantor-kantor cabang Bank dan/atau media lainnya berdasarkan pertimbangan Bank.',
        indexStr: 'b.',
        level: 2,
    }, {
        text: 'Rekening Nasabah telah disalahgunakan, termasuk namun tidak terbatas untuk menampung dan/atau melakukan kejahatan atau untuk kegiatan yang dapat merugikan masyarakat atau pihak manapun dan/atau Bank berdasarkan alasan dan pertimbangan tertentu oleh Bank.',
        indexStr: 'c.',
        level: 2,
    }, {
        text: 'Nasabah memberikan data/ informasi yang diragukan kebenarannya oleh pihak Bank dan/atau memberikan data/ informasi palsu dan/atau tidak bersedia memberikan data/ informasi yang diminta oleh pihak Bank sesuai dengan peraturan yang berlaku.',
        indexStr: 'd.',
        level: 2,
    }, {
        text: 'Ada permintaan tertulis dari instansi Kepolisian, Kejaksaan, Pengadilan, PPATK, Kantor Pajak atau instansi lainnya yang berwenang sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.',
        indexStr: 'e.',
        level: 2,
    }, {
        text: 'Nasabah belum memenuhi kewajiban/ hutang atau perjanjian lain yang dibuat antara Nasabah dan Bank.',
        indexStr: 'f.',
        level: 2,
    }, {
        text: 'Jika Nasabah meninggal dunia, dinyatakan pailit, tidak mampu membayar, ditaruh dibawah pengampuan atau karena sebab-sebab apapun tidak berhak lagi mengurus, mengelola atau menguasai harta bendanya, maka Rekening hanya dapat ditutup/ dicairkan oleh dan sisa saldonya dibayarkan kepada ahli waris atau pihak yang ditunjuk atau pengganti hak yang sah menurut ketentuan hukum yang berlaku dan sesuai syarat dan ketentuan yang berlaku pada Bank.', 
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Bila terjadi perselisihan antara Nasabah dan pihak lain, maka Bank berhak untuk tidak melakukan pembayaran kepada siapa pun sampai adanya penyelesaian antara Nasabah dan pihak lain atau berdasarkan keputusan pengadilan yang telah berkekuatan hukum tetap.',
        indexStr: '3.',
        level: 1,
    }, {
        text: 'Penjaminan Pemerintah',
        indexStr: 'Pasal 7',
        level: 0,
    }, {
        text: 'Penjaminan simpanan Nasabah mengikuti ketentuan Peraturan Lembaga Penjamin Simpanan (LPS) yang berlaku.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Simpanan Nasabah yang ditempatkan di Bank melebihi batas maksimum ketentuan LPS tidak termasuk dalam program penjaminan LPS dan segala risiko yang timbul menjadi tanggung jawab Nasabah sepenuhnya.',
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Tanggung Jawab Nasabah untuk Melunasi Kewajiban',
        indexStr: 'Pasal 8',
        level: 0,
    }, {
        text: 'Jika Nasabah mempunyai kewajiban kepada Bank yang timbul karena transaksi yang belum diselesaikan oleh Nasabah, maka Nasabah dengan ini berjanji untuk memenuhi/ melunasi segala kewajiban Nasabah kepada Bank dengan seketika dan sekaligus setelah menerima pemberitahuan pertama dari Bank.',
        indexStr: null,
        level: 1,
    }, {
        text: 'Pengajuan Pengaduan atas Fasilitas/ Layanan Perbankan',
        indexStr: 'Pasal 9',
        level: 0,
    }, {
        text: 'Nasabah dapat mengajukan pengaduan atas transaksi/ layanan perbankan baik melalui telepon, ataupun datang langsung ke Bank disertai dengan dokumen pendukung yang dipersyaratkan oleh Bank. Dalam hal penyelesaian pengaduan secara lisan tersebut tidak dapat diselesaikan oleh Bank dalam jangka waktu yang ditentukan oleh Bank, maka Bank berhak meminta kepada Nasabah untuk mengajukan pengaduan secara tertulis disertai dengan dokumen pendukung yang dipersyaratkan oleh Bank.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Penanganan penyelesaian pengaduan tersebut akan diselesaikan sesuai dengan syarat dan ketentuan yang berlaku pada Bank.',
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Hukum yang Berlaku dan Jurisdiksi',
        indexStr: 'Pasal 10',
        level: 0,
    }, {
        text: 'Keabsahan, penafsiran, dan pelaksanaan dari Syarat dan Ketentuan ini diatur dan tunduk pada ketentuan syariah, ketentuan Bank Indonesia/ Otoritas Jasa Keuangan dan hukum yang berlaku di negara Republik Indonesia.', 
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Dalam hal timbul perselisihan atau sengketa atau perbedaan pendapat mengenai penafsiran dan/atau pelaksanaan Syarat dan Ketentuan ini, maka Bank dan Nasabah sepakat menyelesaikan secara musyawarah dengan itikad baik untuk mencapai mufakat.', 
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Jika perselisihan atau sengketa tidak dapat diselesaikan melalui musyawarah, maka Bank dan Nasabah setuju untuk menyelesaikan perselisihan atau sengketa dengan cara:',
        indexStr: '3.',
        level: 1,
    }, {
        text: 'penyelesaian melalui lembaga atau badan alternatif penyelesaian di luar pengadilan sesuai ketentuan perundangan yang berlaku; atau',
        indexStr: 'a.',
        level: 2,
    }, {
        text: 'penyelesaian melalui Pengadilan, maka Bank dan Nasabah sepakat untuk penyelesaian melalui Pengadilan Agama Jakarta Selatan, namun tidak mengurangi hak Bank untuk melakukan tuntutan atau gugatan hukum melalui Pengadilan Agama lain di wilayah Republik Indonesia.',
        indexStr: 'b.',
        level: 2,
    }, {
        text: 'Pernyataan dan Kuasa',
        indexStr: 'Pasal 11',
        level: 0,
    }, {
        text: 'Nasabah dengan ini menyatakan bahwa Bank berhak melakukan pemeriksaan terhadap kebenaran data yang diberikan kepada Bank berkenaan dengan pembukaan Rekening dan berhak meminta data tambahan yang diperlukan oleh Bank.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Nasabah dengan ini menyatakan bahwa setiap data atau keterangan yang diberikan kepada Bank berkenaan dengan pembukaan Rekening adalah benar dan sah.',
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Nasabah wajib segera memberitahukan secara tertulis kepada Bank atas setiap perubahan alamat, nomor telepon dan hal-hal lain yang menyimpang/ berbeda dari data atau keterangan yang pernah diberikan Nasabah kepada Bank berkaitan dengan Rekening Nasabah. Kelalaian Nasabah tidak memberitahukan perubahan tersebut kepada Bank sepenuhnya merupakan tanggung jawab Nasabah.',
        indexStr: '3.',
        level: 1,
    }, {
        text: 'Semua kuasa yang diberikan Nasabah dalam Syarat dan Ketentuan ini diberikan dengan hak substitusi dan selama kewajiban Nasabah kepada Bank belum dipenuhi sepenuhnya, maka kuasa-kuasa tersebut tidak dapat dicabut kembali ataupun tidak akan berakhir karena alasan apapun, termasuk tetapi tidak terbatas pada sebab-sebab yang disebut dalam pasal 1813 Kitab Undang-Undang Hukum Perdata dan kuasa-kuasa tersebut merupakan bagian yang tidak terpisahkan dari Syarat dan Ketentuan ini.',
        indexStr: '4.',
        level: 1,
    }, {
        text: 'Keadaan Kahar (Force Majeure)',
        indexStr: 'Pasal 12',
        level: 0,
    }, {
        text: 'Nasabah setuju untuk membebaskan Bank dari segala klaim/ tuntutan/ gugatan ganti rugi apapun, dalam hal Bank tidak dapat atau terlambat melaksanakan Instruksi baik sebagian maupun seluruhnya karena kejadian-kejadian atau sebab-sebab di luar kekuasaan atau kemampuan Bank termasuk namun tidak terbatas pada bencana alam, perang, huru hara, keadaan peralatan, sistem infrastruktur elektronik atau transmisi yang tidak berfungsi, gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, kegagalan sistem kliring atau hal-hal lain yang ditetapkan oleh Bank Indonesia dan/atau Otoritas Jasa Keuangan.',
        indexStr: null,
        level: 1,
    }, {
        text: 'Lain-lain',
        indexStr: 'Pasal 13',
        level: 0,
    }, {
        text: 'Dengan menyetujui Syarat dan Ketentuan ini, Nasabah dengan ini menyatakan mengikatkan diri pada Syarat dan Ketentuan, dan menyatakan bahwa Bank telah memberikan penjelasan yang cukup mengenai karakteristik, syarat, ketentuan Rekening dan BTPN Wow! iB yang akan dimanafaatkan, dan Nasabah telah mengerti dan memahami segala konsekuensi, termasuk manfaat, risiko, dan biaya-biaya yang melekat pada Rekening dan BTPN Wow! iB.',
        indexStr: '1.',
        level: 1,
    }, {
        text: 'Syarat dan Ketentuan ini merupakan satu kesatuan dan bagian yang tidak terpisahkan dari e-Form berikut perubahannya (jika ada).',
        indexStr: '2.',
        level: 1,
    }, {
        text: 'Syarat dan Ketentuan ini berlaku untuk dan mengikat terhadap Rekening milik Nasabah.',
        indexStr: '3.',
        level: 1,
    }, {
        text: 'Dalam hal terdapat perubahan, penambahan dan/atau pembaharuan Syarat dan Ketentuan ini, termasuk namun tidak terbatas pada biaya-biaya yang mungkin timbul akan diberitahukan melalui Kantor Cabang Bank dan/atau media lainnya berdasarkan pertimbangan Bank, paling lambat 30 (tiga puluh) hari kerja sebelum perubahan tersebut efektif atau jangka waktu pemberitahuan sesuai ketentuan/ peraturan yang berlaku. Apabila tidak terdapat sanggahan/ ketidaksetujuan Nasabah dalam jangka waktu tersebut, maka perubahan, penambahan, dan/atau pembaharuan tersebut berlaku efektif mengikat Nasabah.',
        indexStr: '4.',
        level: 1,
    }, {
        text: 'Nasabah dan Bank sepakat untuk mengesampingkan ketentuan Pasal 1266 Kitab Undang-Undang Hukum Perdata yang berlaku di Indonesia yang memungkinkan Bank mengakhiri Syarat dan Ketentuan secara sepihak.',
        indexStr: '5.',
        level: 1,
    }, {
        text: 'Syarat dan Ketentuan ini telah disesuaikan dengan Ketentuan Peraturan Perundang-undangan termasuk Ketentuan Peraturan Otoritas Jasa Keuangan.',
        indexStr: '6.',
        level: 1,
    }
]