export const pksKeagenan = [
{
    level: 0,
    indexStr: 'Pasal 1',
    text: 'RUANG LINGKUP PERJANJIAN',
}, {
    level: 1,
    indexStr: '1.',
    text: 'Perjanjian ini dibuat dan disepakati oleh Para Pihak dalam rangka akuisisi calon nasabah dan Layanan BTPNS melalui Mitra Tepat. Setiap Mitra Tepat yang ditunjuk oleh BTPNS untuk memberikan layanan akan diklasifikasikan ke dalam beberapa kelas. Pada tahap pertama setiap Mitra Tepat akan diklasifikasikan sebagai Mitra Tepat klasifikasi 1, untuk perpindahan level Mitra Tepat akan ditetapkan sesuai kebijakan BTPNS.', 
}, {
    level: 1,
    indexStr: '2.',
    text: 'Para Pihak sepakat bahwa Perjanjian ini bersifat eksklusif yaitu Mitra Tepat tidak dapat mengadakan perjanjian lainnya yang sama maupun sejenis dengan Perjanjian ini dengan pihak manapun.',
}, {
    level: 1,
    indexStr: '3.',
    text: 'Mitra Tepat sepakat untuk memenuhi dan mentaati seluruh persyaratan Mitra Tepat, target, ketentuan Imbal Jasa/Komisi serta ketentuan Layanan BTPNS yang ditetapkan oleh BTPNS dari waktu ke waktu.',
}, {
    level: 0,
    indexStr: 'Pasal 2',
    text: 'JANGKA WAKTU PERJANJIAN',
}, {
    level: 1,
    indexStr: null,
    text: 'Para Pihak sepakat bahwa ketentuan penunjukan Mitra Tepat tunduk pada ketentuan-ketentuan sebagai berikut :',
}, {
    level: 1,
    indexStr: '1.',
    text: 'Jangka Waktu Perjanjian berlaku selama 5 (lima) tahun sejak Perjanjian ini ditandatangani.', 
}, {
    level: 1,
    indexStr: '2.',
    text: 'Apabila jangka waktu Perjanjian dalam ayat 1 diatas berakhir dan tidak ada pihak yang bermaksud untuk mengakhiri Perjanjian, maka Para Pihak sepakat bahwa jangka waktu Perjanjian dapat diperpanjang secara otomatis.',
}, {
    level: 1,
    indexStr: '3.',
    text: 'Perpanjangan, pengakhiran, dan pengakhiran Perjanjian sebelum jangka waktu diatur sebagaimana dimaksud pada  Pasal 9 butir 9.3 Lampiran 1.',
}, {
    level: 1,
    indexStr: '4.',
    text: 'Mitra Tepat memahami bahwa penunjukan keagenan ini tidak bisa dialihkan sebagaimana dimaksud pada Pasal 4 butir 4.2 Lampiran 1.'
}, {
    level: 0,
    indexStr: 'Pasal 3',
    text: 'KUASA',
}, {
    level: 1,
    indexStr: null,
    text: 'Mitra Tepat dengan ini memberi kuasa penuh kepada BTPNS, khusus untuk melakukan tindakan-tindakan terkait pelaksanaan Perjanjian termasuk namun tidak terbatas pada mendebet dan atau memblokir rekening Mitra Tepat di BTPNS. Pendebetan dan atau pemblokiran rekening Mitra Tepat tersebut tidak membutuhkan izin dari pihak yang berwenang atau pihak lainnya. Mitra Tepat menerima dan menyetujui segala tindakan BTPNS atas rekening Mitra Tepat. Kuasa ini akan terus berlaku dan tidak akan dicabut oleh Mitra Tepat hingga Perjanjian ini berakhir.',
}, {
    level: 0,
    indexStr: 'Pasal 4',
    text: 'KETENTUAN PENUTUP',
}, {
    level: 1,
    indexStr: '1.',
    text: 'Perjanjian ini dan segala dokumen yang berhubungan dan yang timbul akibat Perjanjian ini, tunduk pada hukum negara Republik Indonesia dan merupakan bagian yang tidak terpisahkan dari Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah sebagaimana lampiran 1.',
}, {
    level: 1,
    indexStr: '2.',
    text: 'Mitra Tepat dengan ini menyatakan telah menerima salinan Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah sebagaimana lampiran 1 , membaca, memahami dan menyetujui isinya. Selain ditentukan secara khusus dalam Perjanjian ini maka setiap istilah dan ketentuan dalam Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah sebagaimana lampiran 1 berlaku pula bagi Perjanjian ini.', 
}, {
    level: 1,
    indexStr: '3.',
    text: 'Para Pihak setuju untuk memilih kewenangan lembaga peradilan yang sesuai dengan Undang-Undang atau ketentuan yang berlaku tanpa mengurangi hak dan wewenang BTPNS untuk mengajukan tuntutan/gugatan hukum terhadap Mitra Tepat di muka pengadilan agama dalam wilayah Republik Indonesia.',
}]