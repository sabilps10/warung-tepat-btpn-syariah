const occupation = [{
    label: 'Pegawai Swasta',
    value: 'SWASTA'
}, {
    label: 'Pegawai Negeri',
    value: 'PNS'
}, {
    label: 'Wirausaha',
    value: 'ENTREPRENEUR'
}, {
    label: 'Lainnya',
    value: 'OTHERS_OCC'
}]

const job = [{
    label: 'Pemilik',
    value: 'OWNER'
}, {
    label: 'Direktur',
    value: 'DIRECTOR'
}, {
    label: 'Staf',
    value: 'STAF'
}, {
    label: 'Lainnya',
    value: 'OTHERS_POS'
}]

const industrySector = [{
    label: 'Pertanian',
    value: 'PERTANIAN'
}, {
    label: 'Perikanan',
    value: 'PERIKANAN',
}, {
    label: 'Perkebunan',
    value: 'PERKEBUNAN',
}, {
    label: 'Peternakan',
    value: 'PETERNAKAN',
}, {
    label: 'Kehutanan/Pemotongan Kayu',
    value: 'KEHUTANAN_PEMOTONGAN_KAYU'
}, {
    label: 'Pertambangan',
    value: 'PERTAMBANGAN'
}, {
    label: 'Industri Makanan/Minuman',
    value: 'IND_MAKANAN_MINUMAN'
}, {
    label: 'Industri Tekstil/Pakaian',
    value: 'IND_TEKSTIL_PAKAIAN'
}, {
    label: 'Industri Kayu/Barang Kayu',
    value: 'IND_KAYU_BARANG_KAYU'
}, {
    label: 'Industri Kulit/Bahan Kulit',
    value: 'IND_KULIT_BAHAN_KULIT',
}, {
    label: 'Industri Kimia/Bahan Kimia',
    value: 'IND_KIMIA_BAHAN_KIMIA',
}, {
    label: 'Industri Lainnya',
    value: 'IND_LAINNYA',
}, {
    label: 'Listrik',
    value: 'LISTRIK',
}, {
    label: 'Gas',
    value: 'GAS',
}, {
    label: 'Air',
    value: 'AIR',
}, {
    label: 'Konstruksi',
    value: 'KONSTRUKSI',
}, {
    label: 'Ekspor/Impor',
    value: 'EXPOR_IMPOR',
}, {
    label: 'Distribusi',
    value: 'DISTRIBUSI',
}, {
    label: 'Perdagangan',
    value: 'PERDAGANGAN',
}, {
    label: 'Perhotelan/Pariwisata',
    value: 'PERHOTELAN_PARIWISATA',
}, {
    label: 'Kasino/Eksekutif Club',
    value: 'KASINO_EKSEKUTIF_CLUB',
}, {
    label: 'Jasa Surveyor',
    value: 'JASA_SURVEYOR',
}, {
    label: 'Kantor Notaris',
    value: 'KANTOR_NOTARIS',
}, {
    label: 'Kantor Pengacara',
    value: 'KANTOR_PENGACARA',
}, {
    label: 'Kantor Akuntan',
    value: 'KANTOR_AKUNTAN',
}, {
    label: 'Minimarket/Supermarket',
    value: 'MINIMARKET_SUPERMARKET',
}, {
    label: 'SPBU',
    value: 'SPBU',
}, {
    label: 'Pedagang Isi Pulsa',
    value: 'PEDAGANG_ISI_PULSA',
}, {
    label: 'Jasa Parkir',
    value: 'JASA_PARKIR',
}, {
    label: 'Usaha Barang Antik',
    value: 'USAHA_BARANG_ANTIK',
}, {
    label: 'Pedagang Logam Mulia',
    value: 'PEDAGANG_LOGAM_MULIA',
}, {
    label: 'Restoran/Rumah Makan',
    value: 'RESTORAN_RUMAH_MAKAN',
}, {
    label: 'Kuliner',
    value: 'KULINER',
}, {
    label: 'Transportasi/Angkutan umum',
    value: 'TRANSPORTASI_ANGKUTAN_UMUM',
}, {
    label: 'Biro Perjalanan',
    value: 'BIRO_PERJALANAN',
}, {
    label: 'Developer/Real Estate',
    value: 'DEVELOPER_REAL_ESTATE',
}, {
    label: 'Dealer Motor/Mobil',
    value: 'DEALER_MOTOR_MOBIL',
}, {
    label: 'Konsultan',
    value: 'KONSULTAN',
}, {
    label: 'Hiburan/Kebudayaan',
    value: 'HIBURAN_KEBUDAYAAN',
}, {
    label: 'Rumah Sakit/Layanan Keseha',
    value: 'RUMAH_SAKIT_LAYANAN_KESEHATAN',
}, {
    label: 'Farmasi',
    value: 'FARMASI',
}, {
    label: 'Pendidikan',
    value: 'PENDIDIKAN',
}, {
    label: 'Yayasan',
    value: 'YAYASAN',
}, {
    label: 'Perbankan/Asuransi/Keu',
    value: 'PERBANKAN_ASURANSI_KEU',
}, {
    label: 'Kecantikan',
    value: 'KECANTIKAN',
}, {
    label: 'Otomotif',
    value: 'OTOMOTIF',
}, {
    label: 'Informasi dan Teknologi',
    value: 'INFORMASI_TEKNOLOGI',
}, {
    label: 'Media',
    value: 'MEDIA',
}, {
    label: 'Penerbitan/Pencetakan',
    value: 'PENERBITAN_PERCETAKAN',
}, {
    label: 'Telekomunikasi',
    value: 'TELEKOMUNIKASI',
}, {
    label: 'Pulp and Paper',
    value: 'PULP_PAPER',
}, {
    label: 'Kelautan dan Perikanan',
    value: 'KELAUTAN_PERIKANAN',
}, {
    label: 'Seni dan Budaya',
    value: 'SENI_BUDAYA',
}, {
    label: 'Lainnya',
    value: 'LAINNYA',
}, {
    label: 'Industri',
    value: 'INDUSTRI',
}, {
    label: 'Jasa Dunia Usaha',
    value: 'JASA_DUNIA_USAHA',
}, {
    label: 'Jasa Sosial',
    value: 'JASA_SOSIAL',
}, {
    label: 'Pemerintah',
    value: 'PEMERINTAH',
}, {
    label: 'Indst Pengolahan Tembakau',
    value: 'IND_PENGOLAHAN_TEMBAKAU',
}, {
    label: 'Karaoke',
    value: 'KARAOKE',
}, {
    label: 'PPAT',
    value: 'PPAT',
}, {
    label: 'Konsultan keuangan',
    value: 'KONSULTAN_KEUANGAN',
}, {
    label: 'Dealer/Agen Properti',
    value: 'DEALER_AGEN_PROPERTI',
}, {
    label: 'Dealer Kapal',
    value: 'DEALER_KAPAL',
}, {
    label: 'Money Changer',
    value: 'MONEY_CHANGER',
}, {
    label: 'Money Remittance',
    value: 'MONEY_REMITTANCE',
}, {
    label: 'Perbankan/Asuransi/Keu/BPR',
    value: 'PERBANKAN_ASURANSI_KEU_BPR',
}]

const sourceFund = [{
    label: 'Gaji',
    value: 'GAJI',
}, {
    label: 'Warisan',
    value: 'WARISAN',
}, {
    label: 'Hasil Usaha',
    value: 'USAHA',
}, {
    label: 'Hasil Investasi',
    value: 'INVESTASI',
}, {
    label: 'Lainnya',
    value: 'OTHERS_SRC',
}]

const income = [{
    label: '< 1 Juta',
    value: 'MON_MIL_0_1'
}, {
    label: '1-5 Juta',
    value: 'MON_MIL_1_5'
}, {
    label: '5-10 Juta',
    value: 'MON_MIL_5_10'
}, {
    label: '> 10 Juta',
    value: 'MON_MIL_10'
}]

const forecastTransaction = [{
    label: '1-5 Juta',
    value: 'ANN_MIL_1_5'
}, {
    label: '5-10 Juta',
    value: 'ANN_MIL_5_10'
}, {
    label: '10-25 Juta',
    value: 'ANN_MIL_10_25'
}, {
    label: '25-50 Juta',
    value: 'ANN_MIL_25_50'
}, {
    label: '> 50 Juta',
    value: 'ANN_MIL_50'
}]

const purposeOfAccount = [{
    label: 'Pilih Tujuan',
    value: null
}, {
    label: 'Tabungan/Investasi',
    value: 'TABUNGAN'
}, {
    label: 'Transaksi Usaha',
    value: 'BUSINESS'
}, {
    label: 'Transaksi Pribadi',
    value: 'PERSONAL'
}, {
    label: 'Gaji',
    value: 'SALARY'
}, {
    label: 'Lainnya',
    value: 'OTHERS_PUR'
}]

const options = {
    occupation,
    job,
    industrySector,
    sourceFund,
    income,
    forecastTransaction,
    purposeOfAccount,
}

export default options;