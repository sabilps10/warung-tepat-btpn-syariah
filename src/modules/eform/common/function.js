import { 
    ToastAndroid,
    LayoutAnimation, Platform, UIManager,
} from "react-native";

import Config from "./config";

export const googlePlaceSearch = async (query) => {
    const route = 'https://maps.googleapis.com/maps/api/place/textsearch/json'

    const header = {
        query,
        key: Config.apiKey.google_places
    }

    let opts = {
        method: 'GET',
    };

    let qs = require('qs');
    const param = qs.stringify(header);
    fullRoute = `${route}?${param}`

    console.log('fetch google place', fullRoute, opts)
    
    try {
        let req = await fetch(fullRoute, opts)
            .then(res => {
                console.log('req fetch', res)
                return res;
            })
            .catch((error) => {
                if (!isObject(error)) {
                    ToastAndroid.show(
                        'Terjadi Kesalahan, '+error,
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                }

                throw error;
            });
            
        let resp = await req.json();
        console.log('resp response', resp)

        return resp
    } catch (e) {
        throw e;
    }
}

export const ToggleExpand = () => {    
    if (Platform.OS === 'android') {
        if (UIManager.setLayoutAnimationEnabledExperimental) {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    LayoutAnimation.configureNext(
        LayoutAnimation.create(200, 'easeInEaseOut', 'opacity')
    );
}

export const getDataByField = (data = [], field = 'value', selectedValue) => {
    const selected = data.find( element => element[field] == selectedValue)

    if (selected != null) return selected;
}