import AcquisitionRest from './rest.acquisition';

export const service = class AcquisitionService {
    constructor() {
        this.rest = new AcquisitionRest();
    }

    postAcquisition(data, userId) {
        // console.log('postAcquisition')
        return this.rest.postAcquisition(data, userId)
    }

    postAcquisitionTemporary(data, userId) {
        // console.log('postAcquisition')
        return this.rest.postAcquisitionTemporary(data, userId)
    }


    postAcquisitionProspera(data, userId, cifCode) {
        // console.log('postAcquisition')
        return this.rest.postAcquisitionProspera(data, userId, cifCode)
    }

    getAcquisition(query, id = '', userId) {
        // console.log('GET Acquisition')
        return this.rest.getAcquisition(query, id, userId)
    }

    putAcquisition(data, id, userId) {
        return this.rest.putAcquisition(data, id, userId)
    }
};

export const AcquisitionService = new service();