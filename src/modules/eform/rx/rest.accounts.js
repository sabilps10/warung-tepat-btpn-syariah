import Rest from './rest'

export default class AccountsRest extends Rest {
    getDataFromProspera(cifCode) {
        return this.GET(`v1/accounts/${cifCode}/prospera`)
    }
}