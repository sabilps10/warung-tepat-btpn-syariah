import SmsRest from './rest.smsLink';

export const service = class SmsService {
    constructor() {
        this.rest = new SmsRest();
    }

    sendSms(data, userId = '') {
        return this.rest.sendSms(data, userId)
    }

    sendSmsLink(data, userId = '') {
        return this.rest.sendSmsLink(data, userId)
    }
};

export const SmsService = new service();