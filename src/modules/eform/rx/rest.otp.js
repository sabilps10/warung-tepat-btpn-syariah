import Rest from './rest'

export default class OtpRest extends Rest {
    sendOtp(data) {
        return this.POST('v1/otp', data)
    }
}