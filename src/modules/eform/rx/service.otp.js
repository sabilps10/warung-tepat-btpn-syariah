import OtpRest from './rest.otp';

export const service = class OtpService {
    constructor() {
        this.rest = new OtpRest();
    }

    sendOtp(data, id='') {
        return this.rest.sendOtp(data, id)
    }
};

export const OtpService = new service();