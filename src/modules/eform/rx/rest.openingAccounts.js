import Rest from './rest'

export default class OpeningAccountsRest extends Rest {
    getOpeningAccountsHistory(query, id = '', userId) {
        const headers = [{ 'X-User-Id': userId }]
        return this.GET('v1/opening-accounts/history', query, id, headers)
    }
}