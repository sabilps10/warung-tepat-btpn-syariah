import Rest from './rest'

export default class MastersRest extends Rest {
    getMasters(query) {
        return this.GET('v1/masters', query)
    }

    getAreas(query, provinceId = 0, stateId = 0, districtId = 0, subDistrictId = 0) {
        if (provinceId != 0) {
            return this.GET(`v1/masters/areas/${provinceId}/${stateId}/${districtId}/${subDistrictId}`, query)
        } else {
            return this.GET(`v1/masters/areas`, query)
        }
    }
}