import React, { useState, useContext, createContext } from 'react';

const EformContext = createContext();
const EformInsertContext = createContext();
const EformSetContext = createContext();

export const useEform = () => {
    return useContext(EformContext);
}

export const useUpdateEform = () => {
    return useContext(EformInsertContext);
}

export const useSetEform = () => {
    return useContext(EformSetContext);
}

export const EformProvider = ({children}) => {
    const [form, setForm] = useState({})

    const insertForm = (value, inputName) => {
        setForm(currentForm => ({
            ...currentForm,
            [inputName]: value
        }))
    }

    const updateForm = async (value) => {
        await setForm(value)
    }

    return (
        <EformContext.Provider value={form}>
            <EformInsertContext.Provider value={insertForm}>
                <EformSetContext.Provider value={updateForm}>
                    {children}
                </EformSetContext.Provider>
            </EformInsertContext.Provider>
        </EformContext.Provider>
    )
}