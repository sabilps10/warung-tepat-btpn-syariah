import React from 'react';
import {
  TextInput,
  Text,
  View,
  StyleSheet
} from 'react-native';

import StyledText from '../atoms/StyledText';

import { COLOR } from '@common/styles';

const FormInput = (props) => {
    return (
        <View style={Style.inputContainer}>
            <StyledText style={Style.text}>{props.text}</StyledText>
            <TextInput 
                style={Style.input}
                {...props}
                placeholderTextColor={COLOR.off}
            />
            <StyledText style={Style.infoText}>{props.info}</StyledText>
        </View>
    )
}

const Style = StyleSheet.create({
    inputContainer: {
        marginBottom: 8,
    },
    text: {
        marginVertical: 4,
        fontSize: 12,
        fontFamily: 'NunitoSans-Regular',
        color: COLOR.textGray
    },
    input: {
        borderWidth: 1,
        borderColor: COLOR.off,
        borderRadius: 8,
        paddingVertical: 7,
        paddingHorizontal: 8,
        fontSize: 16,
        height: 40,
        fontFamily: 'NunitoSans-Bold',
        color: COLOR.textGray
    },
    infoText: {
        fontFamily: 'NunitoSans-Regular',
        color: COLOR.lightGray
    }
})

export default FormInput;