import React, { useState, useEffect } from 'react';
import {
    TouchableOpacity,
    View,
    Image,
    Alert,
    ActivityIndicator,
    Text,
    StyleSheet,
    ToastAndroid,
    Modal
} from 'react-native';

import ImagePicker from 'react-native-image-picker';
import RNFS from 'react-native-fs'

import ButtonSecondary from './ButtonSecondary';

import iconCamera from '@assets/icons/icon_eform_camera.png';
import iconClose from '@assets/icons/icon_eform_close.png';
import { COLOR } from '@common/styles';
import { getData } from './FunctionStorage';
import { CUSTCOLOR } from '../common/styles';
import { useEform } from '../context/EformContext';

import { decode } from 'base64-arraybuffer';
import Config from '../common/config';

import { useSelector } from 'react-redux';
import { Button } from 'react-native-elements';

const ButtonPhotoUpload = (props) => {
    const SESSION = useSelector(store => store.Auth.session)
    const [image, setImage] = useState()
    const [modalVisible, setModalVisible] = useState(false)
    const [loading, setLoading] = useState(false)
    const [refresh, setRefresh] = useState(false)
    const accessToken = SESSION?.user?.response_login_by_nik?.accessToken;        

    const form = useEform();
    
    const uploadPhoto = async (content) => {
        const myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + accessToken);
        myHeaders.append("Content-Type", content.type); // diubah sesuai mime type file nya

        // const fileContent = content.data; // diubah sesuai isi file
        const fileName = `${form.Index}-${props.imageNamePrefix != null && props.imageNamePrefix}-${content.fileName}`;

        const base64 = await RNFS.readFile(content.uri, 'base64');
        const arraybuffer = decode(base64)

        const s3EndpointURL = Config.apiURL.storage[global.ENVI];

        let bucketName = "";

        if (global.ENVI == 'dirty') bucketName = 'bucket01';
        if (global.ENVI == 'alpha') bucketName = 'bucket02';
        if (global.ENVI == 'beta') bucketName = 'bucket03';
        if (global.ENVI == 'prod') bucketName = 'cust_assets';

        const filePath = `${bucketName}/acq/agent/${fileName}`; // diubah sesuai nama dan path file
        const requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: arraybuffer,
            redirect: 'follow'
        };

        console.log('path ==', s3EndpointURL+filePath)

        const req = await fetch(`${s3EndpointURL}${filePath}`, requestOptions)
            .then(res => {
                console.log('res then', res)
                if (res.status == 200) {
                    return res
                } else {
                    return false;
                }
            })
            .catch((error) => {
                console.log('res error', error)
                throw error;
            });

        return req
    }

    const getImage = async (imageUrl) => {
        setLoading(true)
        const requestOptions = {
            headers: {
                "Authorization": accessToken,
                "Content-Type": "image/jpeg"
            },
        };
    
        await fetch(imageUrl, requestOptions)
        .then(response => {
            console.log('response fetch image', response)
            if (response.status == 200) { 
                return response.blob()
            } else if (response.status == 404) {
                Alert.alert(
                    "Gagal",
                    "Gagal, file tidak ditemukan."
                )
            } else if (response.status == 403) {
                Alert.alert(
                    "Gagal",
                    "Gagal, silakan login kembali."
                )
            } else if (response.status == 429) {
                setRefresh(true);
                Alert.alert(
                    "Gagal",
                    "Gagal memuat foto. " + response.status + " too many request."
                )
            } else {
                Alert.alert(
                    "Gagal",
                    "Gagal memuat foto. " + response.status
                )
            }
        })
        .then(blob => {
            let reader = new FileReader();
            
            reader.readAsDataURL(blob); // converts the blob to base64 and calls onload
            
            reader.onload = function() {
                setImage(reader.result)
            };
        })
        .catch((error) => {
            console.log('error photo get : ', error)
            console.log('error photo url : ', imageUrl)
            ToastAndroid.show(
                'Tidak Dapat Terhubung Dengan Server',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        });

        setLoading(false)
    }

    const choosePhoto = async () => {
        const options = {
            title: props.title,
            maxWidth: 1920,
            maxHeight: 1920,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, async (response) => {
            setLoading(true)
            if (response.data) {
                const upload = await uploadPhoto(response)

                if (upload !== false) {
                    setImage(response.uri)
                    props.onUploadPhoto(upload.url)
                } else {
                    Alert.alert(
                        "Gagal",
                        "Terjadi kesalahan saat upload, silakan coba kembali"
                    )
                }
                setLoading(false)
            }

            if (response.didCancel) setLoading(false)

            if (response.uri) {
                // this.setState({ avatar: response.uri });
            }
        });
    };

    useEffect(() => {
        if (props.imageUrl != null && props.imageUrl != '' && props.imageUrl !== '-') getImage(props.imageUrl);
    }, [])
    
    return (
        <>
        {
            loading ?
                <View style={styles.imageContainer}>
                    <View style={styles.fullImage}>
                        <ActivityIndicator size="large" color={CUSTCOLOR.secondary} />
                    </View>
                </View>
                :                
                (image == null ?
                    <View style={styles.buttonContainer}>
                        <Image source={iconCamera} style={styles.icon} />
                        {
                            refresh ? 
                            <ButtonSecondary 
                                text="Refresh" 
                                type="clear"
                                textStyle={{fontSize: 14, paddingHorizontal: 19}}
                                containerStyle={{borderWidth: 0}}
                                onPress={() => getImage(props.imageUrl)} 
                            />
                            :
                            <ButtonSecondary 
                                text="Ambil Gambar" 
                                type="clear"
                                textStyle={{fontSize: 14, paddingHorizontal: 19}}
                                onPress={() => choosePhoto()} 
                            />
                        }
                    </View>
                    :
                    <View style={styles.imageContainer}>
                        <Image resizeMode='contain' source={{uri: image}} style={styles.image} />
                        <View style={styles.buttonGroup}>
                            <TouchableOpacity 
                                style={styles.editButton}
                                onPress={() => choosePhoto()}
                            >
                                <Text style={styles.editButtonText}>Ubah</Text>
                            </TouchableOpacity>
                            <View style={styles.partition}></View>
                            <TouchableOpacity 
                                style={styles.editButton}
                                onPress={() => setModalVisible(true)}
                            >
                                <Text style={styles.editButtonText}>Lihat</Text>
                            </TouchableOpacity>
                        </View>
                        <Modal
                            animationType="slide"
                            transparent={false}
                            style={styles.modal}
                            visible={modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.closeButtonContainer}>
                                    <TouchableOpacity
                                        activeOpacity={0.9}
                                        style={{padding: 8}}
                                        onPress={() => {
                                            setModalVisible(!modalVisible);
                                        }}>
                                        <Image source={iconClose} style={{width: 24, height: 24}} />
                                    </TouchableOpacity>
                                </View>
                                <Image resizeMode='contain' source={{uri: image}} style={styles.fullImage} />
                            </View>
                        </Modal>
                    </View>)
            }
        </>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        height: 154,
        paddingBottom: 28,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: COLOR.off,
        alignItems: 'center', 
        marginBottom: 8
    },
    icon: {
        width: 24,
        height: 24,
        margin: 32,
        marginTop: 38,
    },
    imageContainer: {
        height: 154,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: COLOR.off,
        alignItems: 'center', 
        marginBottom: 8,
        padding: 10,
        position: 'relative',
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 10,
    },
    buttonGroup: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'absolute',
        bottom: 0,
        zIndex: 10
    },
    editButton: {
        flex: 1,
        fontSize: 14,
        alignItems: 'center',
        padding: 4,
        backgroundColor: '#fff',
        opacity: 0.8
    },
    editButtonText: {
        fontFamily: 'NunitoSans-ExtraBold',
        fontSize: 14,
        height: 24,
        color: COLOR.tertiary,
    },
    partition: {
        height: 22,
        borderLeftWidth: 1,
        borderColor: COLOR.off
    },
    modalContainer: {
        flex: 1,
        backgroundColor: '#000',
        justifyContent: 'center',
        position: 'relative'
    },
    closeButtonContainer: {
        top: 8,
        right: 16,
        position: 'absolute',
        zIndex: 100
    },
    fullImage: {
        width: '100%',
        height: '100%',
        justifyContent: 'center'
    }
})

export default ButtonPhotoUpload;