import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image
} from 'react-native';

import { COLOR } from '@common/styles';

import iconCheck from '@assets/icons/icon_eform_check.png';

COLOR.green = '#00c781';

const Circle = (props) => {
    return (
        <View style={{
            height: 40,
            width: 40,
            borderRadius: 40,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: props.active == props.name ? COLOR.primary : (props.active > props.name ? COLOR.green : COLOR.textSecondaryInverse)
        }}>
            {
                props.active > props.name ?
                <Image source={iconCheck} style={{width: 24, height: 24}} />
                :
                <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>{props.text}</Text>
            }
        </View>
    )
}

const Strip = (props) => {
    return <View style={{marginHorizontal: 12, height: 4, width: 24, borderRadius: 4, backgroundColor: props.active >= props.name ?COLOR.green : COLOR.textSecondaryInverse, alignSelf: 'center'}}></View>
}

const Pagination = (props) => {
    const renderPageNumber = (pages) => {
        let components;

        for (let i = 1; i <= pages; i++) {
            components += <Circle text={String(i)} name={i} active={props.active} />
            components += <Strip name={i} active={props.active} />
        }

        return components
    }
    
    return (
        <View style={Style.FooterContainer}>
            {
                props.pageText.map((text, index) => {
                    return (
                        <View key={index} style={{flexDirection: 'row'}}>
                            {
                                index > 0 &&
                                <Strip name={text} active={props.active} />
                            }
                            <Circle text={text} name={text} active={props.active} />
                        </View>
                    )
                })
            }
        </View>
    )
}

const Style = StyleSheet.create({
    FooterContainer: {
        paddingVertical: 20,
        paddingHorizontal: 24, 
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'center'
    }
})

export default Pagination;