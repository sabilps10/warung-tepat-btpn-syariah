import React from 'react';
import {
    View,
    StyleSheet
} from 'react-native';
import { COLOR } from '@common/styles';

const EformFooter = (props) => {
    return (
        <View style={[Style.FooterContainer, {...props.style}]}>
            {props.children}
        </View>
    )
}

const Style = StyleSheet.create({
    FooterContainer: {
        paddingVertical: 16,
        paddingHorizontal: 24, 
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 20,
    }
})

export default EformFooter;