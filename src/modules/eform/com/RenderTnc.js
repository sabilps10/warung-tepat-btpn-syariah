import React from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import { COLOR } from '@common/styles';
import { CUSTCOLOR, NUNITO } from '../common/styles';

const RenderTnc = ({tncText}) => {
    return (
        tncText.map((line, key) => {
            if (line.level == 0) {
                return <Text key={key} style={styles.chapterRow}>{line.indexStr} {line.text}</Text>
            } else {
                return (
                    <View style={styles.verseRow} key={key}>
                        <View style={{width: (line.level - 1) * 32}}></View>
                        {line.indexStr != null && <Text style={[styles.verseIndex, styles.tncText]}>{line.indexStr}</Text>}
                        <Text style={[styles.tncText, {flex: 1}]}>{line.text}</Text>
                    </View>
                )
            }
        })
    )
}

const styles = StyleSheet.create({
    chapterRow: {
        flexDirection: 'row',
        marginVertical: 16,
        fontFamily: NUNITO.bold,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
        marginBottom: 8,
    },
    chapterText: {
        textTransform: 'uppercase'
    },
    verseRow: {
        flexDirection: 'row',
    },
    verseIndex: {
        width: 32
    },
    subVerseRow: {
        flexDirection: 'row',
        marginLeft: 32,
    },
    subVerseIndex: {
        width: 24,
    },
    tncText: {
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        color: COLOR.off,
    },
    section: {
        marginBottom: 24
    }
})

export default RenderTnc