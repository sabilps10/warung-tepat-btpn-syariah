import React, { useEffect, useState } from 'react';

import AreaPicker from './AreaPicker';

import { MastersService } from '../rx/service.masters';

const ProvincePicker = (props) => {
    const [options, setOptions] = useState([]);
    
    const getAreas = async (mounted, provinceId = 0, stateId = 0, districtId = 0, subDistrictId = 0) => {
        const query = {
            maxRowCount: 999
        }
        
        return MastersService.getAreas(query, provinceId, stateId, districtId, subDistrictId)
        .then((res) => {
            let mappedOptions = res.data.map((data) => {
                return {
                    ...data,
                    id: data.provinceId,
                    label: data.province, 
                    value: data.provinceId
                }
            })
            console.log('area - province: ', mappedOptions)

            setOptions(mappedOptions)
        })
        .catch((error) => {
            return error;
        })
        .finally((res) => {
            return res;
        });
    }

    useEffect(() => {
        let mounted = true

        if (mounted) getAreas()

        return () => {
            mounted = false
        }
    }, [])

    return (
        <AreaPicker 
            searchInput={true}
            placeholder={props.placeholder}
            areaPicker
            options={options}
            disabled={props.disabled}
            selectedValue={props.selectedValue}
            onValueChange={(value) => {
                props.areaValue(value)
            }}
            selectedValue={props.selectedValue}
        />
    )
}

export default ProvincePicker;