import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import { Item, Input } from 'native-base';
import { Grid, Col } from 'react-native-easy-grid';

import { COLOR } from '@common/styles';
import { CUSTCOLOR, NUNITO } from '../common/styles';

class OtpInputs extends React.Component {
    state = { otp: [] };
    otpTextInput = [];

    componentDidMount() {
        this.otpTextInput[0]._root.focus();
    }

    renderInputs() {
        const inputs = Array(6).fill(0);
        const txt = inputs.map((i, j) => (
            <View key={j} style={styles.col}>
                <Item style={this.props.isError ? styles.itemError : styles.item} regular>
                    <Input
                        style={[
                            styles.input,
                            {fontFamily: this.props.value != '' ? NUNITO.bold : NUNITO.regular}
                        ]}
                        keyboardType="numeric"
                        selectTextOnFocus
                        onChangeText={(v) => this.focusNext(j, v)}
                        onKeyPress={(e) => this.focusPrevious(e.nativeEvent.key, j)}
                        ref={(ref) => (this.otpTextInput[j] = ref)}
                        maxLength={1}
                        placeholder="0"
                        placeholderTextColor={COLOR.off}
                    />
                </Item>
            </View>
        ));
        return txt;
    }

    focusPrevious(key, index) {
        // this.props.resetError();

        if (key === 'Backspace' && index !== 0) {
            this.otpTextInput[index - 1]._root.focus();
            // this.otpTextInput[index - 1]._root.clear();
        }
    }

    focusNext(index, value) {
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1]._root.focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index]._root.blur();
        }
        const otp = this.state.otp;
        otp[index] = value;
        this.setState({ otp });
        this.props.getOtp(otp.join(''));
    }

    render() {
        return <View style={styles.gridContainer}>{this.renderInputs()}</View>;
    }
}

const styles = StyleSheet.create({
    gridContainer: { 
        // flex: 1, 
        marginBottom: 33,
        marginRight: -14,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    col: { 
        marginRight: 14, 
        height: 49,
        flex: 1
    },
    item: { 
        borderRadius: 8,
        borderColor: COLOR.off
    },
    itemError: { borderRadius: 8, borderColor: COLOR.lightRed },
    input: { 
        textAlign: 'center',
        padding: 0,
        fontWeight: 'bold',
        fontSize: 18,
        color: CUSTCOLOR.textDark,
    },
});

export default OtpInputs;
