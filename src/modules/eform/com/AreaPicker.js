import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  TouchableOpacity, 
  Modal,
  FlatList,
  Dimensions,
  ActivityIndicator
} from 'react-native';

import { View } from 'react-native-animatable';

import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';

import iconClose from '@assets/icons/icon_eform_close.png';
import iconChevronDown from '@assets/icons/icon_eform_chevron_down.png';
import { COLOR } from '@common/styles';
import { Button } from 'react-native-elements';
import InputText from './InputText';
import { getDataByField } from '../common/function';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const AreaPicker = (props) => {
    const [modalVisible, setModalVisible] = useState(false)
    const [value, setValue] = useState(null)
    const [OPTIONS, setOPTIONS] = useState([])
    const [loading, setLoading] = useState(true)
    const [searchValue, setSearchValue] = useState('')

    const onValueChange = async (item) => {
        setValue(item.label)
        props.onValueChange(item)
    }

    useEffect(() => {
        getDataByField(OPTIONS, 'label', props.selectedValue)
        setValue(props.selectedValue)
    }, [props.selectedValue])

    useEffect(() => {
        setOPTIONS(props.options)
        props.options && setLoading(false)
    }, [props.options])

    const onSearchArea = async (value) => {        
        const pattern = new RegExp(value, "gi")
        const options = props.options
        
        const filteredOptions = await options.filter(({label}) => label.match(pattern))

        setOPTIONS(filteredOptions)

        setLoading(false)
    }

    const onCloseModal = () => {
        setTimeout(() => {
            setModalVisible(false)
            setSearchValue('')
            onSearchArea('')
        }, 0)
    }

    const optionButton = ({item}) => {
        const btnStyles = {
            buttonContainer: {
                paddingVertical: 8,
                paddingHorizontal: 0,
                justifyContent: 'flex-start',
                backgroundColor: 'transparent'
            },
            buttonText: {
                fontFamily: value === item.label ? NUNITO.extraBold : NUNITO.regular,
                color: value === item.label ? CUSTCOLOR.primary : CUSTCOLOR.text,
                fontSize: 16,
                textAlign: 'left',
                marginHorizontal: 24,
            }
        }
        
        return (
            <Button 
                type="solid"
                title={item.label}
                titleStyle={btnStyles.buttonText}
                buttonStyle={btnStyles.buttonContainer}
                onPress={async () => {
                    onValueChange(item)
                    onCloseModal()
                }}
            />
        )
    }

    return (
        <>
            <TouchableOpacity 
                activeOpacity={0.9} 
                style={[
                    styles.pickerButton,
                    props.disabled && {
                        backgroundColor: '#dadada'
                    }
                ]}
                disabled={props.disabled}
                onPress={() => {
                    setModalVisible(true)
                }}
            >
                <Text style={[
                    styles.pickerText,
                    {
                        fontFamily: props.selectedValue ? NUNITO.bold : NUNITO.regular,
                        color: props.selectedValue ? CUSTCOLOR.text : COLOR.off
                    }
                ]} 
                numberOfLines={1}>
                    {
                        props.selectedValue ? props.selectedValue : props.placeholder
                    }
                </Text>
                <Image source={iconChevronDown} style={{width: 24, height: 24}} />
            </TouchableOpacity>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <View style={styles.modal}>
                    <TouchableOpacity 
                        style={styles.modalOverlay} 
                        onPress={() => setModalVisible(false)}
                        activeOpacity={0}
                    />
                    <View style={styles.modalBody}>
                        <View style={styles.pickerHeader}>
                            <Text style={styles.pickerTitle}>{props.placeholder}</Text>
                            <TouchableOpacity 
                                style={styles.modalCloseButton}
                                onPress={() => setModalVisible(false)}
                            >
                                <Image source={iconClose} style={{width: 24, height: 24}} />
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            marginBottom: 8,
                            marginHorizontal: 24,
                        }}>
                            <InputText 
                                placeholder="Cari..."
                                editable={!loading}
                                value={searchValue}
                                onChangeText={(value) => {
                                    setSearchValue(value)
                                    onSearchArea(value)
                                }}
                            />
                        </View>
                        <View style={{
                            borderTopWidth: 1,
                            borderTopColor: COLOR.off,
                            marginTop: 8,
                        }} />
                        {
                            loading ?
                            <View style={{
                                height: '45%',
                                alignItems: 'center',
                                justifyContent: 'flex-end',
                            }}>
                                <ActivityIndicator size={54} />
                            </View>
                            :
                            
                            <FlatList 
                                data={OPTIONS}
                                style={{flexGrow: 1,}}
                                contentContainerStyle={{
                                    paddingTop: 16,
                                    paddingBottom: 32,
                                    flexGrow: 1,
                                }}
                                renderItem={optionButton}
                                keyExtractor={(item) => item.id}
                                extraData={value}
                            />
                        }
                    </View>
                </View>
            </Modal>
        </>
    )
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        borderColor: COLOR.off,
        borderRadius: 8,
        paddingVertical: 7,
        fontSize: 16,
        height: 40,
    },
    pickerButton: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: COLOR.off,
        borderRadius: 8,
        paddingVertical: 7,
        paddingHorizontal: 8,
        fontSize: 16,
        height: 40,
    },
    pickerText: {
        flex: 1,
        fontSize: 16,
        lineHeight: 26,
        fontFamily: NUNITO.regular,
        color: CUSTCOLOR.text
    },
    arrow: {
        marginHorizontal: 8,
    },
    modal: {
        position: 'relative',
        flexDirection: 'column',
        height: '100%',
        width: '100%',
        paddingTop: 40,
        flex: 1,
        justifyContent: 'flex-end'
    },
    modalOverlay: {
        backgroundColor: '#000',
        height: windowHeight,
        width: windowWidth,
        opacity: 0.5,
        position: 'absolute',
        zIndex: 0,
        top: 0,
        left: 0,
    },
    modalBody: {
        backgroundColor: '#fff',
        position: 'relative',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        height: '100%',
        paddingTop: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 6,
        flexShrink: 1,
    },
    pickerHeader: {
        flexDirection: 'row',
        marginBottom: 8,
        marginHorizontal: 24,
    },
    pickerTitle: {
        fontFamily: MARKOT.bold,
        color: CUSTCOLOR.textDark, 
        fontSize: 18,
        flex: 1,
    }
})

export default AreaPicker;