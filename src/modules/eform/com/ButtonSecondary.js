import React from 'react';
import {
  StyleSheet
} from 'react-native';
import { CUSTCOLOR, NUNITO } from '../common/styles';
import { Button } from 'react-native-elements';

const ButtonSecondary = (props) => {
    return (
        <Button
            {...props}
            type="outline"
            buttonStyle={[
                styles.button, props.containerStyle,
            ]}
            title={props.text}
            titleStyle={styles.text}
        />
    )
}

const styles = StyleSheet.create({
    button: {
        padding: 8,
        borderWidth: 1,
        borderRadius: 8,
        alignItems: 'center',
        borderColor: CUSTCOLOR.secondary,
    },
    text: {
        fontFamily: NUNITO.extraBold,
        fontSize: 16,
        color: CUSTCOLOR.secondary,
    }
})

export default ButtonSecondary;