import React from 'react';
import {
    StyleSheet,
    // Text
} from 'react-native';

import { Text, View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import { CUSTCOLOR, NUNITO } from '../common/styles';

import Icon from 'react-native-vector-icons/FontAwesome';

const FormField = (props) => {    
    return (
        <View style={Style.inputContainer}>
            <Text style={Style.text}>{props.text}</Text>
            {props.children}
            {
                props.warning ?
                <View style={{flexDirection: 'row',}} animation="shake" useNativeDriver>
                    <Icon name='exclamation-circle' color={COLOR.danger} size={12} style={{marginTop: 3}} />
                    <Text style={[
                        Style.infoText, 
                        {
                            color: COLOR.danger,
                            fontFamily: NUNITO.bold,
                            marginLeft: 8
                        }
                    ]}>
                        {props.warning}
                    </Text>
                </View>
                :
                <Text 
                    style={[
                        Style.infoText, 
                        {color: CUSTCOLOR.textLight}
                    ]}
                >
                    {props.info}
                </Text>
            }
        </View>
    )
}

const Style = StyleSheet.create({
    inputContainer: {
        marginBottom: 8,
    },
    text: {
        marginBottom: 4,
        fontSize: 12,
        lineHeight: 20,
        fontFamily: NUNITO.regular,
        color: CUSTCOLOR.text
    },
    infoContainer: {
        height: 20,
    },
    infoText: {
        height: 20,
        fontSize: 12,
        fontFamily: NUNITO.regular
    }
})

export default FormField;