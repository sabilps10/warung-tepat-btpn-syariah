import React from 'react';
import {
  Image,
  StyleSheet
} from 'react-native';
import { COLOR } from '@common/styles';

import { View } from 'react-native-animatable';

import iconCheck from '@assets/icons/icon_eform_check.png';
import { Button } from 'react-native-elements';

const Checkbox = (props) => {
    const { value } = props;
    
    const handleSwitch = () => {
        if (!props.disabled) {
            const newVal = !value
            props.callback(newVal)
        }
    }
  
    return (
        <Button 
            onPress={handleSwitch}
            type={value ? 'solid' : 'outline'}
            buttonStyle={[
                Style.switchContainer,
                {
                    backgroundColor: value == true ? COLOR.primary : 'transparent',
                    borderColor: value == true ? COLOR.primary : COLOR.off,
                    borderWidth: value == true ? 0 : 2,
                }
            ]} 
            icon={value ? 
                <View animation="rubberBand" useNativeDriver><Image source={iconCheck} style={Style.innerCheck} tintColor={props.disabled? 'hsl(208,0%,63%)': '#fff'} /></View> 
                : null}
            {...props}
        />
        // <TouchableOpacity 
        //     disabled={props.disabled}
        //     activeOpacity={1}
        //     style={[
        //         Style.switchContainer,
        //         {
        //             backgroundColor: props.disabled ? COLOR.textSecondaryInverse : (value == true ? COLOR.primary : 'transparent'),
        //             borderColor: props.disabled ? COLOR.textSecondaryInverse : (value == true ? COLOR.primary : COLOR.off),
        //             borderWidth: value == true ? 0 : 2,
        //         }
        //     ]} 
        //     onPress={handleSwitch}
        // >
        //     {
        //         value == true &&
        //         <Image source={iconCheck} style={Style.innerCheck} />
        //     }
        // </TouchableOpacity>
    )
}

const Style = StyleSheet.create({
    switchContainer: {
        width: 24,
        height: 24,
        borderRadius: 4,
        // borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 16
    },
    innerCheck: {
        height: 24,
        width: 24
    }
})

export default Checkbox;