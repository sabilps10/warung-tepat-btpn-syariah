import React, { useEffect, useState } from 'react';
import {
  TextInput,
  Text,
  View,
  StyleSheet
} from 'react-native';

import { COLOR } from '@common/styles';
import { CUSTCOLOR, NUNITO } from '../common/styles';
import { NumberFormatter } from '../../../common/utils';

const InputText = (props) => {    
    const dateAutoDevide = (value) => {
        if (value != null) {
            let removedSlash = value.replace(/\//g, '')
            let stringToArray = removedSlash.split('')
            
            if (removedSlash.length > 2) stringToArray.splice(2, 0, '/')
            if (removedSlash.length > 4) stringToArray.splice(5, 0, '/')
        
            const newValue = stringToArray.join('')
            
            return newValue;
        }
    }

    const numberFormatter = (value) => {
        const thousandSeparator = '.';
        const decimalSeparator = ',';

        const thousandSeparatorPatt = new RegExp(`\\${thousandSeparator}`, 'g')
        const cleanValue = value.replace(thousandSeparatorPatt, '')

        const [firstNumber, secondNumber] = cleanValue.split(decimalSeparator);

        console.log('firstnumber', cleanValue.split(decimalSeparator))
        console.log(firstNumber)
        
        let formattedNumber = String(firstNumber).replace(/\B(?=(\d{3})+(?!\d))/g, `${thousandSeparator}`)
        console.log('formattedNumber', formattedNumber)
        
        if (secondNumber != null) formattedNumber = `${formattedNumber}${decimalSeparator}${secondNumber}` 

        console.log('result', formattedNumber)
        return String(formattedNumber)
    }

    return (
        <View style={[
            styles.inputContainer,
            {
                backgroundColor: props.editable != false && props.editable != 'undefined' ? '#fff' : '#dadada'
            }
        ]}>
            {/* {
                props.leftText &&
                <Text 
                    style={[
                        styles.inputText,
                        styles.textBold,
                    ]}
                >
                    {props.leftText}
                </Text>
            } */}
            <TextInput 
                {...props}
                placeholderTextColor={COLOR.off}
                style={[
                    styles.input,
                    styles.inputText,
                    {
                        fontFamily: (props.value != null && props.value !== '') ? NUNITO.bold : NUNITO.regular,
                    }
                ]}
                onChangeText={(value) => {
                    let returnValue = value;
                    if (props.dateInput) returnValue = dateAutoDevide(value);
                    if (props.numberFormat) returnValue = numberFormatter(value);
                    props.onChangeText(String(returnValue));
                }}
                keyboardType={props.dateInput ? 'numeric' : props.keyboardType}
                selectTextOnFocus={true}
                
            />
        </View>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        borderWidth: 1,
        borderColor: COLOR.off,
        borderRadius: 8,
        paddingHorizontal: 8,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
    },
    inputText: {
        fontSize: 16,
        fontFamily: NUNITO.regular,
        color: CUSTCOLOR.text,
    },
    textBold: {
        fontFamily: NUNITO.bold,
    },
    input: {
        paddingVertical: 7,
        paddingHorizontal: 0,
        height: 40,
        margin: 0,
        flex: 1,
    }
})

export default InputText;