import React from 'react';
import { AsyncStorage } from 'react-native';

export const removeData = async (index, storageKey) => {
    const data = await getData(storageKey);
    delete data[index]

    await setData(data, storageKey)

    return true
}

export const removeAllData = async (storageKey) => {
    await AsyncStorage.removeItem(storageKey)

}

export const addData = async (value, storageKey, index=null) => {
    try {
        const dataValue = await getData(storageKey)
        let newData = {}
        console.log('add data typeof', typeof value, value)
        console.log('add data index', index)
        const dataLen = Object.values(dataValue).length

        index = index == null ? dataLen : String(index)

        console.log('dataValue', dataValue, dataLen, dataLen > 0)
        if (dataLen > 0) {
            newData = {
                ...dataValue,
                [index]: value
            }
            console.log('newData1', newData)

            // newData = [...dataValue]
        //     setData(newData, storageKey)
        } else {
            newData = {[index]: value}
            console.log('index value', index, value)
        }
        console.log('newData2', newData)
        // } else {
        //     const jsonValue = JSON.stringify(value)
        setData(newData, storageKey)
        // }

        return true;
    } catch (e) {
        return e;
    }
}

export const setData = async (value, storageKey) => {
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem(storageKey, jsonValue)

        return true;
    } catch (e) {
        return e;
    }
}

export const getData = async (storageKey) => {
    try {
        const jsonValue = await AsyncStorage.getItem(storageKey)
        console.log(JSON.parse(jsonValue).length)
        if(jsonValue !== null) {
            return JSON.parse(jsonValue)
        } else {
            return false
        }
    } catch(e) {
        return false;
    }
}
