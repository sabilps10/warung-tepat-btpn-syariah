import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  TouchableOpacity, 
  Modal,
  FlatList,
  Dimensions
} from 'react-native';

import { View } from 'react-native-animatable';

import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';

import iconClose from '@assets/icons/icon_eform_close.png';
import iconChevronDown from '@assets/icons/icon_eform_chevron_down.png';
import { COLOR } from '@common/styles';
import { Button } from 'react-native-elements';
import InputText from './InputText';
import { getDataByField } from '../common/function';
import { ActivityIndicator } from 'react-native';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const InputPicker = (props) => {
    const [modalVisible, setModalVisible] = useState(false)
    const [value, setValue] = useState(null)
    const [OPTIONS, setOPTIONS] = useState([])
    const [loading, setLoading] = useState(true)

    const onValueChange = async (itemValue) => {
        // setLoading(true)
        setValue(itemValue)
        await props.onValueChange(itemValue)
        setLoading(false)
    }

    useEffect(() => {
        onValueChange(props.selectedValue)
    }, [props.selectedValue])

    const getSelectedLabel = (selectedValue) => {
        const selected = getDataByField(OPTIONS, 'value', selectedValue)

        if (selected != null) return selected.label
    }

    useEffect(() => {
        setLoading(true)
        return function cleanup() {
            setModalVisible(false)
            setOPTIONS([])
            setLoading(false)
        }
    }, [])
    
    useEffect(() => {
        setOPTIONS(props.options)
        console.log('props.options', props.options)
    }, [props.options])

    const optionButton = ({item}) => {
        const btnStyles = {
            buttonContainer: {
                paddingVertical: 8,
                paddingHorizontal: 0,
                justifyContent: 'flex-start',
                backgroundColor: 'transparent'
            },
            buttonText: {
                fontFamily: value == item.value ? NUNITO.extraBold : NUNITO.regular,
                color: value == item.value ? CUSTCOLOR.primary : CUSTCOLOR.text,
                fontSize: 16,
                textAlign: 'left',
                marginHorizontal: 24,
            }
        }
        
        return (
            <Button 
                type="solid"
                title={item.label}
                titleStyle={btnStyles.buttonText}
                buttonStyle={btnStyles.buttonContainer}
                onPress={async () => {
                    onValueChange(item.value)
                    // setModalVisible(false)
                    setTimeout(() => {setModalVisible(false)}, 0)
                }}
            />
        )
    }

    return (
        <>
            <TouchableOpacity 
                activeOpacity={0.9} 
                style={[
                    styles.pickerButton,
                    props.disabled && {
                        backgroundColor: '#dadada'
                    }
                ]}
                disabled={props.disabled}
                onPress={() => {
                    setModalVisible(true)
                }}
            >
                {loading ? (
                    <View style={{flex: 1, alignItems: 'flex-start'}}>
                        <ActivityIndicator color={CUSTCOLOR.primary} />
                    </View>
                ) : (
                    <Text style={[
                        styles.pickerText,
                        {
                            fontFamily: props.selectedValue ? NUNITO.bold : NUNITO.regular,
                            color: props.selectedValue ? CUSTCOLOR.text : COLOR.off
                        }
                    ]} 
                    numberOfLines={1}>
                        {
                            props.selectedValue ?
                            getSelectedLabel(props.selectedValue) 
                            : props.placeholder
                        }
                    </Text>
                )}
                <Image source={iconChevronDown} style={{width: 24, height: 24}} />
            </TouchableOpacity>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <View style={styles.modal}>
                    <TouchableOpacity 
                        style={styles.modalOverlay} 
                        onPress={() => setModalVisible(false)}
                        activeOpacity={0}
                    />
                    <View style={styles.modalBody}>
                        <View style={styles.pickerHeader}>
                            <Text style={styles.pickerTitle}>{props.placeholder}</Text>
                            <TouchableOpacity 
                                style={styles.modalCloseButton}
                                onPress={() => setModalVisible(false)}
                            >
                                <Image source={iconClose} style={{width: 24, height: 24}} />
                            </TouchableOpacity>
                        </View>
                        {
                            props.searchInput &&
                            <View style={{
                                marginBottom: 8,
                                marginHorizontal: 24,
                            }}>
                                <InputText 
                                    placeholder="Cari..."
                                />
                            </View>
                        }
                        <FlatList 
                            data={OPTIONS}
                            style={{
                                borderTopWidth: 1,
                                borderTopColor: COLOR.off,
                                marginTop: 8,
                                flexGrow: 1,
                            }}
                            contentContainerStyle={{
                                paddingTop: 16,
                                paddingBottom: 32,
                                flexGrow: 1,
                            }}
                            renderItem={optionButton}
                            ListEmptyComponent={
                                <View style={{padding: 24, alignItems: 'center'}}>
                                    <ActivityIndicator color={CUSTCOLOR.primary} size="large" />
                                </View>}
                            keyExtractor={(item) => item.value}
                            extraData={value}
                        />
                    </View>
                </View>
            </Modal>
        </>
    )
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        borderColor: COLOR.off,
        borderRadius: 8,
        paddingVertical: 7,
        fontSize: 16,
        height: 40,
    },
    pickerButton: {
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: COLOR.off,
        borderRadius: 8,
        paddingHorizontal: 8,
        fontSize: 16,
        height: 40,
    },
    pickerText: {
        paddingVertical: 7,
        flex: 1,
        fontSize: 16,
        lineHeight: 26,
        fontFamily: NUNITO.regular,
        color: CUSTCOLOR.text,
    },
    arrow: {
        marginHorizontal: 8,
    },
    modal: {
        position: 'relative',
        flexDirection: 'column',
        height: '100%',
        width: '100%',
        paddingTop: 40,
        flex: 1,
        justifyContent: 'flex-end'
    },
    modalOverlay: {
        backgroundColor: '#000',
        height: windowHeight,
        width: windowWidth,
        opacity: 0.5,
        position: 'absolute',
        zIndex: 0,
        top: 0,
        left: 0,
    },
    modalBody: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        maxHeight: '100%',
        paddingTop: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 6,
        flexShrink: 1,
    },
    pickerHeader: {
        flexDirection: 'row',
        marginBottom: 8,
        marginHorizontal: 24,
    },
    pickerTitle: {
        fontFamily: MARKOT.bold,
        color: CUSTCOLOR.textDark, 
        fontSize: 18,
        flex: 1,
    }
})

export default InputPicker;