import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import { EformProvider } from './eform/context/EformContext';

import EformScreen from './eform/index';
import AddForm1 from './eform/screen/addForm1';
import AddForm2 from './eform/screen/addForm2';
import AddForm3 from './eform/screen/addForm3';
import AddForm4 from './eform/screen/addForm4';
import EformTnC from './eform/screen/eformTnC';
import EformPKS from './eform/screen/eformPKS';
import EformTnCGen from './eform/screen/eformTnCGen';
import EformOtp from './eform/screen/eformOtp';
import EformSubmitted from './eform/screen/eformSubmitted';
import EformDraftList from './eform/screen/eformDraftList';
import EformHistory from './eform/screen/eformHistory';
import EformMap from './eform/screen/eformMap';
import EformNewInput from './eform/screen/eformNewInput';
import EformInputCif from './eform/screen/eformInputCif';
import EformCifCheckSuccess from './eform/screen/eformCifCheckSuccess';
import EformCifCheckError from './eform/screen/eformCifCheckError';
import AddFormCif1 from './eform/screen/addFormCif1';
import AddFormCif2 from './eform/screen/addFormCif2';
import AddFormCif3 from './eform/screen/addFormCif3';
import EformCifMap from './eform/screen/eformCifMap';
import EformCifPks from './eform/screen/eformCifPks';
import EformCifSubmitted from './eform/screen/eformCifSubmitted';
import { StatusBar } from 'react-native';
import EformOtpCif from './eform/screen/eformOtpCif';
import EformPksPhase3 from './eform/screen/eformPksPhase3';

// export const store = setupStore();

const EformStack = createStackNavigator(
    {
        Eform: {
            screen: EformScreen,
        },
        EformNewInput: {
            screen: EformNewInput,
        },
        EformInputCif: {
            screen: EformInputCif,
        },
        EformCifCheckSuccess: {
            screen: EformCifCheckSuccess,
        },
        EformCifCheckError: {
            screen: EformCifCheckError,
        },
        AddFormCif1: {
            screen: AddFormCif1,
        },
        AddFormCif2: {
            screen: AddFormCif2,
        },
        AddFormCif3: {
            screen: AddFormCif3,
        },
        EformCifMap: {
            screen: EformCifMap,
        },
        EformOtpCif: {
            screen: EformOtpCif,
        },
        EformCifPks: {
            screen: EformCifPks,
        },
        EformCifSubmitted: {
            screen: EformCifSubmitted,
        },
        AddForm1: {
            screen: AddForm1,
        },
        AddForm2: {
            screen: AddForm2,
        },
        AddForm3: {
            screen: AddForm3,
        },
        AddForm4: {
            screen: AddForm4,
        },
        EformTnC: {
            screen: EformTnC,
        },
        EformPKS: {
            screen: EformPKS,
        },
        EformTnCGen: {
            screen: EformTnCGen,
        },
        EformOtp: {
            screen: EformOtp,
        },
        EformSubmitted: {
            screen: EformSubmitted,
        },
        EformMap: {
            screen: EformMap,
        },
        EformDraftList: {
            screen: EformDraftList,
        },
        EformHistory: {
            screen: EformHistory,
        },
        EformPksPhase3: {
            screen: EformPksPhase3
        }
    },
    {
        lazy: true,
        headerMode: 'none',
        initialRouteName: 'Eform',
    },
);

const EformRoutesContainer = createAppContainer(EformStack);

const EformRoutes = () => {
    return (
        // <Provider store={store}>
            <EformProvider>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <EformRoutesContainer />
            </EformProvider>
        // </Provider>
    )
};

export default EformRoutes
