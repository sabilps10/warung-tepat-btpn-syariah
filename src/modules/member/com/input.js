import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import { TextInput } from 'react-native'
import { COLOR, PADDING } from '../../../common/styles';

export default class Input extends PureComponent {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <TextInput
                style={{
                    width: '100%',
                    backgroundColor: COLOR.white,
                    borderColor: COLOR.border,
                    borderWidth: 0.5,
                    padding: PADDING.PADDING_CARD
                }}
                placeholderTextColor={COLOR.darkGray}
                placeholder={this.props.placeholder}
                value={this.props.value}
                onChangeText={this.props.onChangeText}
                keyboardType={this.props.keyboardType}
            />
        )
    }
}

Input.propTypes = {
    placeholder: PropTypes.string.isRequired,
}
