import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { $create } from './rx/action';
import { $reset } from '../../services/form/action';
import { connect } from 'react-redux';
import { Keyboard, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import FloatingFooter from '../../components/floating_footer';
import { COLOR } from '../../common/styles';
import { Text } from 'react-native-elements';
import PkaButton from '../../components/button';
import { DistrictPicker, RegencyPicker } from '../../components/region';
import VillagePicker from '../../components/region/village';
import { Toast } from '../../common/utils';
import DialogSuccess from './com/dialog.success';

class MemberCreateScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            phone: '',
            address: '',
            province_id: 0,
            district_id: 0,
            village_id: 0,
            pick_regency: false,
            pick_district: false,
            pick_village: false,
            regency: {},
            district: {},
            village: {},
            form_dialog: false,
            submitted: false,
        };
    }

    onSubmit = () => {
        this.setState({ submitted: true });
        this.resetForm();
        let form = {
            name: this.state.name,
            phone: this.state.phone,
            address: this.state.address,
            province_id: this.state.province_id,
            regency_id: this.state.regency.id,
            district_id: this.state.district.id,
            village_id: this.state.village.id,
        };

        this.props.$create(form).then((res) => {
            if (res) {
                this.setState({
                    name: '',
                    phone: '',
                    address: '',
                    province_id: 0,
                    district_id: 0,
                    village_id: 0,
                    pick_regency: false,
                    pick_district: false,
                    pick_village: false,
                    regency: { name: '' },
                    district: { name: '' },
                    village: { name: '' },
                    form_dialog: true,
                });
            } else {
                this.setState({ submitted: false });
            }
        });
    };

    componentWillUnmount() {
        this.resetForm();
        this.setState({
            name: '',
            phone: '',
            address: '',
            province_id: 0,
            district_id: 0,
            village_id: 0,
            pick_regency: false,
            pick_district: false,
            pick_village: false,
            regency: { name: '' },
            district: { name: '' },
            village: { name: '' },
            form_dialog: false,
        });
    }

    showPickRegency = () => {
        Keyboard.dismiss();
        this.setState({ pick_regency: true });
    };

    hidePickRegency = () => {
        this.setState({ pick_regency: false });
    };

    selectedRegency = (picked) => {
        this.setState({
            pick_regency: false,
            regency: picked,
            district: this.state.regency.id === picked.id ? this.state.district : { name: '' },
            village: this.state.regency.id === picked.id ? this.state.village : { name: '' },
        });
    };

    showPickDistrict = () => {
        if (this.state.regency.id) {
            this.setState({ pick_district: true });
        } else {
            Toast('Pilih Kota/Kabupaten terlebih dahulu');
        }
    };

    hidePickDistrict = () => {
        this.setState({ pick_district: false });
    };

    selectedDistrict = (picked) => {
        this.setState({
            pick_district: false,
            district: picked,
            village: this.state.district.id !== picked.id ? { name: '' } : this.state.village,
        });
    };

    showPickVillage = () => {
        if (this.state.district.id) {
            this.setState({ pick_village: true });
        } else {
            Toast('Pilih Kecamatan terlebih dahulu');
        }
    };

    hidePickVillage = () => {
        this.setState({ pick_village: false });
    };

    selectedVillage = (picked) => {
        this.setState({
            pick_village: false,
            village: picked,
        });
    };

    FormFinish = () => {
        this.setState({
            form_dialog: false,
            submitted: false,
        });
    };

    resetForm() {
        this.props.$reset();
    }

    onChangeName = (name) => {
        this.setState({ name });
    };

    onChangePhone = (phone) => {
        this.setState({ phone });
    };

    onChangeAddress = (address) => {
        this.setState({ address: address.replace(/[\!\<\%\>]/g, '') });
    };

    render() {
        const paddingScroll = { paddingTop: 15 };
        const sectionStyle = { ...styles.field, borderBottomWidth: 0 };
        let { name, phone, address } = this.state;

        return (
            <View style={styles.container}>
                <ScrollView
                    style={paddingScroll}
                    showsVerticalScrollIndicator={true}
                    keyboardShouldPersistTaps="handled"
                >
                    <Text style={styles.title}>INFORMASI DASAR</Text>
                    <View style={styles.section}>
                        <View style={styles.field}>
                            <TextField
                                label="Nama Lengkap"
                                value={name}
                                error={this.props.errors.name}
                                returnKeyType="next"
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                onFocus={() => this.resetForm()}
                                onChangeText={this.onChangeName}
                                autoFocus
                            />
                        </View>
                        <View style={sectionStyle}>
                            <TextField
                                label="No. Handphone"
                                value={phone}
                                error={this.props.errors.phone}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType="next"
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                keyboardType="numeric"
                                onFocus={() => this.resetForm()}
                                onChangeText={this.onChangePhone}
                            />
                        </View>
                    </View>
                    <Text style={styles.title}>DOMISILI MEMBER</Text>
                    <View style={styles.section}>
                        <View style={styles.field}>
                            <TextField
                                label="Alamat Lengkap"
                                value={address}
                                error={this.props.errors.address}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType="done"
                                multiline={true}
                                lineWidth={0}
                                activeLineWidth={0}
                                inputContainerPadding={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                onFocus={() => this.resetForm()}
                                onChangeText={this.onChangeAddress}
                            />
                        </View>
                        <View style={styles.field}>
                            <TouchableOpacity onPress={this.showPickRegency}>
                                <TextField
                                    label="Kota / Kabupaten"
                                    value={this.state.regency.name}
                                    error={this.props.errors.regency_id}
                                    lineWidth={0}
                                    activeLineWidth={0}
                                    inputContainerPadding={0}
                                    tintColor={COLOR.primary}
                                    labelHeight={16}
                                    fontSize={14}
                                    editable={false}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.field}>
                            <TouchableOpacity onPress={this.showPickDistrict}>
                                <TextField
                                    label="Kecamatan"
                                    value={this.state.district.name}
                                    error={this.props.errors.district_id}
                                    lineWidth={0}
                                    activeLineWidth={0}
                                    inputContainerPadding={0}
                                    tintColor={COLOR.primary}
                                    labelHeight={16}
                                    fontSize={14}
                                    editable={this.props.errors.regency_id > 0}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={sectionStyle}>
                            <TouchableOpacity onPress={this.showPickVillage}>
                                <TextField
                                    label="Kelurahan"
                                    value={this.state.village.name}
                                    error={this.props.errors.village_id}
                                    lineWidth={0}
                                    activeLineWidth={0}
                                    inputContainerPadding={0}
                                    tintColor={COLOR.primary}
                                    labelHeight={16}
                                    fontSize={14}
                                    editable={this.props.errors.district_id > 0}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>

                <FloatingFooter>
                    <PkaButton
                        color={COLOR.success}
                        title="Simpan Data Member"
                        onPress={this.onSubmit}
                        loading={this.state.submitted}
                    />
                </FloatingFooter>

                <RegencyPicker
                    visible={this.state.pick_regency}
                    onSubmit={this.selectedRegency}
                    onCancel={this.hidePickRegency}
                />

                {this.state.regency.id ? (
                    <DistrictPicker
                        visible={this.state.pick_district}
                        regencyId={this.state.regency.id}
                        onSubmit={this.selectedDistrict}
                        onCancel={this.hidePickDistrict}
                    />
                ) : null}

                {this.state.district.id ? (
                    <VillagePicker
                        visible={this.state.pick_village}
                        districtId={this.state.district.id}
                        onSubmit={this.selectedVillage}
                        onCancel={this.hidePickVillage}
                    />
                ) : null}

                <DialogSuccess
                    navigation={this.props.navigation}
                    visible={this.state.form_dialog}
                    onPress={this.FormFinish}
                />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: state.Activity.page,
        errors: state.Member.errors,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $create, $reset }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MemberCreateScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.light,
        margin: 0,
        padding: 0,
        paddingBottom: 70,
    },
    section: {
        backgroundColor: COLOR.white,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: COLOR.border,
        paddingBottom: 5,
        marginBottom: 15,
    },
    field: {
        paddingHorizontal: 20,
        borderBottomWidth: 0.7,
        borderColor: COLOR.border,
        paddingTop: 5,
    },
    title: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        fontSize: 11,
    },
});
