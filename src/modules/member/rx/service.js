import MemberRest from '../../../services/rest/rest.member';

export const service = class MemberService {
    constructor() {
        this.rest = new MemberRest();
    }

    get(page, limit, search) {
        return this.rest.get(page, limit, search);
    }

    show(id) {
        return this.rest.show(id);
    }

    create(data) {
        let body = {
            name: data.name,
            address: data.address,
            phone: data.phone,
            regency_id: data.regency_id,
            district_id: data.district_id,
            village_id: data.village_id,
        };

        return this.rest.create(body);
    }

    update(id, data) {
        let body = {
            name: data.name,
            address: data.address,
            phone: data.phone,
            regency_id: data.regency_id,
            district_id: data.district_id,
            village_id: data.village_id,
        };

        return this.rest.update(id, body);
    }

    delete(id) {
        return this.rest.delete(id);
    }
};

export const MemberService = new service();
