import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    member: {},
    screen: {
        data: [],
        page: 1,
        total: 0,
        search: '',
    },
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.DELETE_SUCCESS:
            state.screen.total = state.screen.total - 1;

            return {
                ...state,
                errors: {},
            };
        case TYPE.LOAD_CACHE_MEMBER:
            state.screen.data = state.member;

            if (action.payload.search != '') {
                state.screen = {
                    data: [],
                    page: 1,
                    total: 0,
                    search: action.payload.search,
                };
            }

            return {
                ...state,
                errors: {},
            };
        case TYPE.FETCH_MEMBER:
            if (state.screen.page < action.payload.page) {
                let prev_data = state.screen.data;
                action.payload.data = prev_data.concat(action.payload.data);
            }

            if (action.payload.page == 1 && action.payload.search == '') {
                state.member = action.payload.data;
            }
            return {
                ...state,
                screen: action.payload,
                errors: {},
            };
        case TYPE.SAVE_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case 'form::reset':
            return {
                ...state,
                errors: {},
            };
        case 'logout':
            return INITIAL_STATE;
        default:
            return state;
    }
}

export function persister({ member }) {
    return {
        member,
    };
}
