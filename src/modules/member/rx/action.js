import * as Activity from '../../../services/activity/action';
import { MemberService } from './service';
import { Toast } from '../../../common/utils';

export const TYPE = {
    FETCH_MEMBER: 'account::fetch.member',
    LOAD_CACHE_MEMBER: 'member::cache.load',
    SAVE_FAILED: 'member::create.failed',
    DELETE_SUCCESS: 'member::delete.success',
};

export function $load(page = 1, limit = 15, search = '') {
    return (dispatch) => {
        if (page == 1) {
            Activity.processing(dispatch, 'Member', 'Load');
        } else {
            Activity.componentProcessing(dispatch, 'loadmore');
        }

        return MemberService.get(page, limit, search)
            .then(res => {
                let payload = {
                    page: page,
                    data: res.data,
                    total: res.total,
                    search: search,
                };

                dispatch({
                    type: TYPE.FETCH_MEMBER,
                    payload: payload,
                });

                return payload;
            })
            .catch(err => {
                dispatch({
                    type: TYPE.LOAD_CACHE_MEMBER,
                    payload: { search: search },
                });

                return false;
            })
            .finally(res => {
                Activity.done(dispatch, 'Member', 'Load');
                Activity.componentDone(dispatch, 'loadmore');

                return res;
            });
    };
}

export function $show(id) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Member', 'Show');

        return MemberService.show(id)
            .then(res => {
                return res;
            })
            .catch(error => {
                return false;
            })
            .finally(res => {
                Activity.done(dispatch, 'Member', 'Show');

                return res;
            });
    };
}

export function $update(id, data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Member', 'Update');

        return MemberService.update(id, data)
            .then(res => {
                Toast('Member berhasil di perbaharui');
                return true;
            })
            .catch(error => {
                dispatch({
                    type: TYPE.SAVE_FAILED,
                    payload: error.errors,
                });

                return false;
            })
            .finally(res => {
                Activity.done(dispatch, 'Member', 'Update');

                return res;
            });
    };
}

export function $delete(id) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'member', 'delete');

        return MemberService.delete(id)
            .then(res => {
                dispatch({
                    type: TYPE.DELETE_SUCCESS,
                    payload: id,
                });

                Toast('Member berhasil di hapus');
                return true;
            })
            .catch(error => {
                Toast(error.errors.id);
                return false;
            })
            .finally(res => {
                Activity.componentDone(dispatch, 'member', 'delete');

                return res;
            });
    };
}

export function $create(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Member', 'Create');

        return MemberService.create(data)
            .then(res => {
                return true;
            })
            .catch(error => {
                dispatch({
                    type: TYPE.SAVE_FAILED,
                    payload: error.errors,
                });

                return false;
            })
            .finally(res => {
                Activity.done(dispatch, 'Member', 'Create');

                return res;
            });
    };
}
