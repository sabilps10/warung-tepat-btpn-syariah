import { ToastAndroid, AsyncStorage } from 'react-native';
import Firebase from '@react-native-firebase/app';
import Messaging from '@react-native-firebase/messaging';
import { Notifier } from 'react-native-notifier';
import { StyleSheet, View, Text, SafeAreaView } from 'react-native';
import React from 'react';

import { Config } from '../common/config';
import { store } from '../app';

// import { NotificationService } from '../services/notification/service';
/**
 * @param {any} obj The object to inspect.
 * @returns {boolean} True if the argument appears to be a plain object.
 */
export function isObject(obj) {
    if (typeof obj !== 'object' || obj === null) {
        return false;
    }

    let proto = Object.getPrototypeOf(obj);

    if (proto === null) {
        return true;
    }

    let baseProto = proto;

    while (Object.getPrototypeOf(baseProto) !== null) {
        baseProto = Object.getPrototypeOf(baseProto);
    }

    return proto === baseProto;
}

const currencyCode = 'Rp '; // IDR or USD
const currencyPosition = 'left'; // left or right
const maxFractionDigits = 2;
const thousandSeparator = '.';
const envi = global.ENVI;

function position(curPos, value) {
    return currencyPosition === 'left' ? `${currencyCode}${value}` : `${value}${currencyCode}`;
}

function PositionCode(curPos, value) {
    return currencyPosition === 'left' ? `${value}` : `${value}`;
}

export const CurrencyFormatter = (value) => {
    let string = 'string';
    let result;

    if (value === 0 || value === null || value === undefined || value === '0' || typeof value === string) {
        return 0;
    }

    const currencyCheck = currencyCode.replace(/\s/g, '').toLowerCase();
    if (currencyCheck === 'idr' || currencyCheck === 'rp') {
        value = Math.ceil(value);
    }

    const valueSplit = String(value.toFixed(maxFractionDigits)).split(`${thousandSeparator}`);
    const firstvalue = valueSplit[0];
    const secondvalue = valueSplit[1];
    const valueReal = String(firstvalue).replace(/\B(?=(\d{3})+(?!\d))/g, `${thousandSeparator}`);

    if (Number(secondvalue) > 0) {
        result = position(currencyPosition, `${valueReal}${thousandSeparator}${secondvalue}`);
    } else {
        result = position(currencyPosition, `${valueReal}`);
    }

    return result;
};

export const CurrencySaldoFormatter = (value) => {
    let string = 'string';
    let result;

    if (value === 0 || value === null || value === undefined || value === '0' || typeof value === string) {
        return 0;
    }

    const valueSplit = String(value.toFixed(maxFractionDigits)).split(`${thousandSeparator}`);
    const firstvalue = valueSplit[0];
    const secondvalue = valueSplit[1];
    const valueReal = String(firstvalue).replace(/\B(?=(\d{3})+(?!\d))/g, `${thousandSeparator}`);

    if (Number(secondvalue) > 0) {
        result = PositionCode(currencyPosition, `${valueReal}${thousandSeparator}${secondvalue}`);
    } else {
        result = PositionCode(currencyPosition, `${valueReal}`);
    }

    return result;
};

export const NumberRounder = (value) => {
    return Math.round(value);
};

export function Toast(text) {
    ToastAndroid.show(text, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
}

export const NotificationForeground = {
    async init() {
        if (!Firebase.apps.length) {
            Firebase.initializeApp(Config.firebaseConfig[envi]);
        } else {
            Firebase.app();
        }

        this._onload();
    },
    async _onload() {
        const getToken = await this._getDeviceTokenFromFirebaseMessaging();

        const styles = StyleSheet.create({
            safeArea: {
                backgroundColor: 'white',
            },
            container: {
                padding: 20,
            },
            title: { color: 'black', fontSize: 20, fontWeight: 'bold' },
            description: { color: 'grey', fontSize: 13 },
        });

        const CustomComponent = ({ title, description }) => (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.description}>{description}</Text>
                </View>
            </SafeAreaView>
        );

        store.dispatch({ type: 'notification::token', payload: getToken });
        Messaging().onMessage(async (remoteMessage) => {
            console.log('A new FCM message arrived!', JSON.stringify(remoteMessage));
            Notifier.showNotification({
                title: remoteMessage.notification.title,
                description: remoteMessage.notification.body,
                duration: 10000,
                showAnimationDuration: 800,
                Component: CustomComponent,
            });
        });
    },

    async _saveTokenDevice(token) {
        try {
            await AsyncStorage.setItem('notif.deviceToken', token);
        } catch (err) {}
    },
    async _getDeviceToken() {
        let deviceId;
        try {
            deviceId = await AsyncStorage.getItem('notif.deviceToken');
        } catch (error) {
            deviceId = null;
        }

        return deviceId;
    },
    async _getDeviceTokenFromFirebaseMessaging() {
        const token = await Messaging().getToken();
        return token;
    },
};
