export const LOGIN_FAIL = 'login_fail';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_EFORM = 'login_eform';
export const LOGOUT = 'logout';

export const REST_STARTED = 'rest_started';
export const REST_FINISHED = 'rest_finished';
