export const contactCenter = '+628001500300';

export const blockedAccount = {
    title: 'AKUN ANDA TERBLOKIR',
    text:
        'Rekening anda terblokir dikarenakan kesalahan input pin berulang kali. Silahkan menghubungi pihak Bank BTPN Syariah di nomor +628001500300 atau kunjungi cabang terdekat.',
    buttonText: 'HOME',
    url: 'Home',
};

export const invalidAccount = {
    title: 'REKENING BELUM TERHUBUNG',
    text:
        'Rekening anda belum terhubung untuk menggunakan fitur ini. Silahkan menghubungi pihak Bank BTPN Syariah di nomor +628001500300 atau kunjungi cabang terdekat.',
    buttonText: 'KERANJANG',
    url: 'Cart',
};

export const notConnect = {
    title: 'AKUN BELUM TERHUBUNG',
    text: 'Rekening anda belum terhubung untuk menggunakan fitur ini. Silahkan melakukan connect di halaman utama.',
    buttonText: 'HOME',
    url: 'Home',
};

export const insufficientFund = {
    title: 'SALDO ANDA TIDAK CUKUP',
    text:
        'Saldo uang yang ada di dalam rekening anda tidak mencukupi. Silahkan isi saldo anda untuk menyelesaikan pembayaran.',
    buttonText: 'KERANJANG',
    url: 'Cart',
};

export const serverError = {
    title: 'SERVER ERROR',
    text:
        'Server sedang gangguan. Silahkan menghubungi pihak Bank BTPN Syariah di nomor +628001500300 atau kunjungi cabang terdekat.',
    buttonText: 'HOME',
    url: 'Home',
};

export const orderSuccess = {
    title: 'PEMBAYARAN BERHASIL',
    text:
        'Terima kasih pembayaran Anda telah berhasil. Pesanan Anda sedang dalam proses. Informasi pengiriman dapat dilihat di menu Transaksi.',
    buttonText: 'SELESAI',
    url: 'Home',
};

export const paylaterSuccess = {
    title: 'PESANAN DIPROSES',
    text:
        'Orderan Anda segera diproses oleh pihak toko/partner. Informasi penggunaan Pembiayaan membutuhkan waktu untuk diupdate di halaman Informasi Pembiayaan. Terima kasih atas kepercayaan menggunakan aplikasi Warung Tepat.',
    buttonText: 'SELESAI',
    url: 'Home',
};

export const invalidAccountConnect = {
    title: 'REKENING BELUM TERHUBUNG',
    text:
        'Rekening anda belum terhubung untuk menggunakan fitur ini. Silahkan menghubungi pihak Bank BTPN Syariah di nomor +628001500300 atau kunjungi cabang terdekat.',
    buttonText: 'HOME',
    url: 'Home',
};

export const connectSuccess = {
    title: 'REKENING TERHUBUNG',
    text: 'Selamat, Rekening Anda berhasil terhubung. Anda bisa menggunakan fitur debit langsung.',
    buttonText: 'SELESAI',
    url: 'Home',
};
