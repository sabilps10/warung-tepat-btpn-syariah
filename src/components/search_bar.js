import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from '../modules/auth/com/input';
import { StyleSheet, TextInput, TouchableOpacity, View, Text } from 'react-native';
import { COLOR, FONT_SIZE } from '../common/styles';
import { Icon } from 'react-native-elements';
import { Col, Grid } from 'react-native-easy-grid';

export default class SearchBar extends PureComponent {
    _search() {
        if (this.props.isEmpty) {
            return null;
        } else if (this.props.value !== '') {
            return (
                <TouchableOpacity activeOpacity={0.5} onPress={this.props.onSubmitEditing}>
                    <View style={styles.searchButtonContainer}>
                        <Text style={styles.textButtonSearch}>Cari</Text>
                    </View>
                </TouchableOpacity>
            );
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={styles.wrapContainer}>
                <Grid
                    style={{
                        ...styles.container,
                        ...this.props.containerStyle,
                    }}
                >
                    <Col style={styles.searchCol}>
                        <Icon name="search" size={20} color={COLOR.darkGray} />
                    </Col>
                    <Col style={{ ...styles.inputContainer, ...this.props.inputContainerStyle }}>
                        <TextInput
                            style={{ ...styles.input, ...this.props.inputStyle }}
                            placeholder={this.props.placeholder}
                            placeholderTextColor={COLOR.border}
                            underlineColorAndroid="transparent"
                            onChangeText={this.props.onChange}
                            onSubmitEditing={this.props.onSubmitEditing}
                            returnKeyType="search"
                            autoFocus={this.props.autoFocus}
                            value={this.props.value}
                        />
                    </Col>
                    <Col style={styles.iconCol}>{this._search()}</Col>
                </Grid>
                {this.props.value !== '' ? (
                    <TouchableOpacity onPress={this.props.onClear} style={styles.closeButton}>
                        <View>
                            <Text style={styles.closeButtonText}>Batal</Text>
                        </View>
                    </TouchableOpacity>
                ) : null}
            </View>
        );
    }
}

Input.propTypes = {
    placeholder: PropTypes.string,
    onChangeText: PropTypes.func,
    onEndEditing: PropTypes.func,
    onClear: PropTypes.func,
    value: PropTypes.string,
    containerStyle: PropTypes.object,
    inputContainerStyle: PropTypes.object,
    inputStyle: PropTypes.object,
    autoFocus: PropTypes.bool,
};

const styles = StyleSheet.create({
    wrapContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    container: {
        backgroundColor: COLOR.light,
        borderWidth: 0.5,
        borderColor: COLOR.border,
    },
    inputContainer: {
        width: '70%',
    },
    input: {
        backgroundColor: 'transparent',
        height: 35,
        fontSize: FONT_SIZE.SMALL,
    },
    searchButtonContainer: {
        backgroundColor: COLOR.darkGreen,
        justifyContent: 'center',
        alignSelf: 'center',
        marginRight: 10,
        borderRadius: 4,
        height: 24,
        width: 50,
    },
    textButtonSearch: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.white,
        alignSelf: 'center',
    },
    closeButton: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
    },
    closeButtonText: {
        color: COLOR.lightRed,
        fontSize: FONT_SIZE.SMALL,
        fontWeight: 'bold',
    },
    searchCol: {
        width: '10%',
        paddingTop: 6,
    },
    iconCol: {
        width: '20%',
        paddingTop: 6,
    },
});
