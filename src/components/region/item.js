import React, { PureComponent } from 'react';
import { StyleSheet, TouchableNativeFeedback } from 'react-native';
import { ListItem } from 'react-native-elements';
import PropTypes from 'prop-types';

import { COLOR, FONT_SIZE } from '../../common/styles';

export default class Item extends PureComponent {
    _onPress = () => {
        this.props.onSelected(this.props.item);
    };

    render() {
        const { selected, onSelected, item } = this.props;

        return (
            <TouchableNativeFeedback onPress={this._onPress}>
                <ListItem containerStyle={styles.itemContainer}
                          title={item.name}
                          titleStyle={styles.title}
                          rightIcon={selected ? {
                              name: 'check',
                              color: COLOR.success,
                              size: 15,
                          } : null}
                />
            </TouchableNativeFeedback>
        );
    }
}

Item.propTypes = {
    onSelected: PropTypes.func.isRequired,
    selected: PropTypes.bool,
    item: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
    itemContainer: {
        padding: 13,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: COLOR.light,
    },
    title: {
        fontSize: FONT_SIZE.SMALL,
    },
});
