import React from 'react';
import { StyleSheet, View } from 'react-native';
import Placeholder from 'rn-placeholder';
import Paragraph from 'rn-placeholder/src/paragraph/paragraph';

function loader(style) {
    return (
        <View style={{
            ...style,
            height: 200,
        }}></View>
    );
};

const productLoading = props => {
    const style = { backgroundColor: props.bgColor };
    return (
        <View style={styles.container}>
            <View style={styles.card}>
                <View style={{
                    ...style,
                    height: 200,
                }}/>
                <Paragraph
                    lineNumber={4}
                    lineSpacing={5}
                    textSize={12}
                    color={'#efefef'}
                    style={styles.paragraph}
                />
            </View>
            <View style={styles.card}>
                <View style={{
                    ...style,
                    height: 200,
                }}/>
                <Paragraph
                    lineNumber={4}
                    lineSpacing={5}
                    textSize={12}
                    color={'#efefef'}
                    style={styles.paragraph}
                />
            </View>
            <View style={styles.card}>
                <View style={{
                    ...style,
                    height: 200,
                }}/>
                <Paragraph
                    lineNumber={4}
                    lineSpacing={5}
                    textSize={12}
                    color={'#efefef'}
                    style={styles.paragraph}
                />
            </View>
            <View style={styles.card}>
                <View style={{
                    ...style,
                    height: 200,
                }}/>
                <Paragraph
                    lineNumber={4}
                    lineSpacing={5}
                    textSize={12}
                    color={'#efefef'}
                    style={styles.paragraph}
                />
            </View>
        </View>
    );
};

export default Placeholder.connect(productLoading);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    card: {
        justifyContent: 'flex-start',
        borderWidth: 0.5,
        borderColor: '#dbdbdb',
        alignContent: 'stretch',
        width: '50%',
        height: 300,
    },
    paragraph: {
        marginBottom: 10,
        marginTop: 10,
        padding: 10,
    },
});
