import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, StyleSheet, Text, TouchableNativeFeedback, View } from 'react-native';
import { Card } from 'react-native-elements';
import { Grid, Row } from 'react-native-easy-grid';
import FastImage from 'react-native-fast-image';
import Countly from 'countly-sdk-react-native-bridge';

import { CurrencyFormatter, NumberRounder } from '../../common/utils';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import { checkImage } from './check_image';

class ProductThumb extends PureComponent {
    navigate = () => {
        const event = { eventName: 'Products', eventCount: 1 };

        event.segments = {
            Name: this.props.product.name,
        };

        Countly.sendEvent(event);

        setTimeout(() => {
            this.props.navigation.push('CatalogDetail', {
                catalog_id: this.props.product.id,
                product: this.props.product,
            });
        }, 0);
    };

    _renderDiscount() {
        return this.props.product.discount !== 0 ? (
            <View style={styles.discountLabel}>
                <Text style={styles.discountLabelText}>{NumberRounder(this.props.product.discount)}%</Text>
            </View>
        ) : null;
    }

    render() {
        let { image_default } = this.props.product;

        const image_default_uri = checkImage(image_default);

        return (
            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#f4f4f4')} onPress={this.navigate}>
                <Card containerStyle={this.props.size === 'small' ? styles.containerSmall : styles.container}>
                    <Grid style={styles.grid}>
                        <Row style={this.props.size === 'small' ? styles.rowImageSmall : styles.rowImage}>
                            <FastImage
                                source={{
                                    uri: image_default_uri,
                                    priority: FastImage.priority.high,
                                }}
                                resizeMode={FastImage.resizeMode.cover}
                                style={this.props.size === 'small' ? styles.thumbnailSmall : styles.thumbnail}
                            />
                            {this._renderDiscount()}
                        </Row>

                        <Row style={styles.rowInfo}>
                            <Grid style={styles.grid}>
                                <Row>
                                    <Text style={styles.textProductName} numberOfLines={2}>
                                        {this.props.product.name}
                                    </Text>
                                </Row>
                                <Row>
                                    <Text style={styles.textProductWeight}>{`Isi: ${this.props.product.size}`}</Text>
                                </Row>

                                {this.props.product.item &&
                                (this.props.product.item.size !== '' ||
                                    this.props.product.item.unit !== '' ||
                                    this.props.product.max_monthly !== 0) ? (
                                    <Row>
                                        <Text style={styles.textProductWeight}>
                                            {this.props.product.item
                                                ? this.props.product.item.size !== ''
                                                    ? this.props.product.item.size
                                                    : null
                                                : null}
                                            {this.props.product.item
                                                ? this.props.product.item.unit !== ''
                                                    ? ' / ' + this.props.product.item.unit
                                                    : null
                                                : null}
                                            {this.props.product.max_monthly !== 0 ? (
                                                <Text>&nbsp;(Max: {this.props.product.max_monthly})</Text>
                                            ) : null}
                                        </Text>
                                    </Row>
                                ) : null}
                                {!this.props.product.item && this.props.product.max_monthly !== 0 ? (
                                    <Row>
                                        <Text style={styles.textProductWeight}>
                                            <Text>&nbsp;(Max: {this.props.product.max_monthly})</Text>
                                        </Text>
                                    </Row>
                                ) : null}

                                <Row>
                                    <Text style={styles.textProductPrice}>
                                        {`${CurrencyFormatter(this.props.product.selling_price)}`}
                                        {this.props.product.discount !== 0 ? (
                                            <Text style={styles.textProductPriceNormal}>
                                                {` ${CurrencyFormatter(this.props.product.regular_price)}`}
                                            </Text>
                                        ) : null}
                                    </Text>
                                </Row>
                            </Grid>
                        </Row>
                    </Grid>
                </Card>
            </TouchableNativeFeedback>
        );
    }
}

ProductThumb.propTypes = {
    product: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired,
    size: PropTypes.string,
    hideButton: PropTypes.bool,
};

export default ProductThumb;

const Width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    container: {
        borderColor: COLOR.border,
        width: Width / 2.1,
        elevation: 0,
        borderWidth: 0.5,
        padding: 0,
        margin: 0,
        flex: 0.5,
    },
    containerSmall: {
        borderColor: COLOR.border,
        width: Width / 2.6,
        elevation: 0,
        borderWidth: 0,
        borderLeftWidth: 0.5,
        padding: 0,
        margin: 0,
    },
    grid: {
        alignContent: 'center',
        alignItems: 'center',
    },
    rowImage: {
        height: 220,
        alignItems: 'center',
        alignContent: 'center',
    },
    thumbnail: {
        width: '100%',
        height: 220,
    },
    rowImageSmall: {
        height: 150,
        alignItems: 'center',
        alignContent: 'center',
    },
    thumbnailSmall: {
        width: '100%',
        height: 150,
    },
    discountLabel: {
        elevation: 0,
        backgroundColor: COLOR.red,
        borderRadius: 0,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        position: 'absolute',
        height: 20,
        marginTop: 10,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    discountLabelText: {
        fontFamily: SSP.regular,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.white,
        paddingLeft: PADDING.PADDING_VERTICAL_CONTAINER,
        paddingRight: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    rowInfo: {
        alignContent: 'center',
        alignItems: 'center',
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        paddingBottom: PADDING.PADDING_VERTICAL_CONTAINER,
        paddingHorizontal: PADDING.PADDING_CARD,
    },
    textProductName: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        paddingBottom: 0,
        textAlign: 'center',
    },
    textProductWeight: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        textAlign: 'center',
    },
    textProductPriceNormal: {
        textDecorationLine: 'line-through',
        fontSize: FONT_SIZE.EXTRA_SMALL,
        fontFamily: SSP.light,
        color: COLOR.darkGray,
    },
    textProductPrice: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    rowButton: {
        paddingTop: 5,
    },
});
