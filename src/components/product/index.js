import ProductThumb from './card_product';
import ProductLoading from './card_loading';

export { ProductThumb, ProductLoading };
