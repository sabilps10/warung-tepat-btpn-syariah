import React from 'react';
import { StyleSheet } from 'react-native';
import { Item, Input } from 'native-base';
import { Grid, Col } from 'react-native-easy-grid';

import { COLOR } from '@common/styles';

class OtpInputs extends React.Component {
    state = { otp: [] };
    otpTextInput = [];

    componentDidMount() {
        this.props.resetError();
        this.otpTextInput[0]._root.focus();
    }

    renderInputs() {
        const inputs = Array(6).fill(0);
        const txt = inputs.map((i, j) => (
            <Col key={j} style={styles.col}>
                <Item style={this.props.isError ? styles.itemError : styles.item} regular>
                    <Input
                        style={styles.input}
                        keyboardType="numeric"
                        onChangeText={(v) => this.focusNext(j, v)}
                        onKeyPress={(e) => this.focusPrevious(e.nativeEvent.key, j)}
                        ref={(ref) => (this.otpTextInput[j] = ref)}
                        maxLength={1}
                        secureTextEntry={true}
                    />
                </Item>
            </Col>
        ));
        return txt;
    }

    focusPrevious(key, index) {
        this.props.resetError();

        if (key === 'Backspace' && index !== 0) {
            this.otpTextInput[index - 1]._root.focus();
        }
    }

    focusNext(index, value) {
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1]._root.focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index]._root.blur();
        }
        const otp = this.state.otp;
        otp[index] = value;
        this.setState({ otp });
        this.props.getOtp(otp.join(''));
    }

    render() {
        return <Grid style={styles.gridPad}>{this.renderInputs()}</Grid>;
    }
}

const styles = StyleSheet.create({
    gridPad: { flex: 1, marginBottom: 8 },
    col: { margin: 6 },
    item: { borderRadius: 8 },
    itemError: { borderRadius: 8, borderColor: COLOR.lightRed },
    input: { textAlign: 'center' },
});

export default OtpInputs;
