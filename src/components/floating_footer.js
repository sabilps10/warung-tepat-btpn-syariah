import React, { PureComponent } from 'react';
import { StyleSheet, View } from 'react-native';
import { COLOR, PADDING } from '../common/styles';

export default class FloatingFooter extends PureComponent {

    render() {
        return (
            <View style={styles.container}>
                {this.props.children}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        height: 'auto',
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
});
